//
//package com.hibernate.test.Dao;
//
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.junit.jupiter.api.Assertions.fail;
//
//import java.util.Date;
//import java.util.List;
//
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//
//import com.hibernate.myapp.dao.Dao;
//import com.hibernate.myapp.model.Person;
//
//class TestDao {
//
//	static Dao dao;
//
//	@BeforeAll
//	public static void beforeAll() {
//		dao = new Dao();
//		dao.init();
//	}
//
//	@AfterAll
//	public static void afterAll() {
//		dao.close();
//	}
//
//
//	@BeforeEach
//	public void setUp() {// pour plus tard
//
//	}
//
//
//	@AfterEach
//	public void tearDown() {// pour plus tard}@Testpublic void testVide()
//	}
//
//
//
//	@Test
//	void testAddPerson() {
//
//		Person p = new Person();
//		p.setFirstName("clement");
//		p.setBirthDay(new Date());
//
//		dao.addPerson(p);
//
//		assertTrue(p.getFirstName().equalsIgnoreCase("clement"));
//
//
//
//	}
//	@Order(2)
//	@Test
//	void testFindPerson() {
//		Person p = new Person();
//
//		p=dao.findPerson(1);
//		assertTrue(p.getId()==1);
//
//
//	}
//
//	@Order(3)
//	@Test
//	void testremove() {
//
//		Person p = new Person();
//		dao.removePerson(6);
////		p=dao.findPerson(5);
//
//		assertTrue(p==null);
//
//
//	}
//
//	@Order(4)
//	@Test
//	void testupdate() {
//		Person p = new Person();
//		p=dao.findPerson(3);
//		p.setFirstName("jacky");
//		dao.updatePerson(p);
//
//
//	}
//
//	@Order(5)
//	@Test
//	void testfindall() {
//
//		List <Person>all =dao.findAllPersons();
//		for (Person person : all) {
//			System.err.println(person.getId()+" "+person.getFirstName());
//		}
//		assertTrue(all.size()>2);
//	}
//	
//	@Order(6)
//	@Test
//	void testfindbyName() {
//		
//
//		List <Person>all =dao.findPersonsByFirstName("clement");
//		for (Person person : all) {
//			System.err.println(person.getId()+" "+person.getFirstName());
//		}
//		assertTrue(all.size()>1);
//	}
//	
//
//}
