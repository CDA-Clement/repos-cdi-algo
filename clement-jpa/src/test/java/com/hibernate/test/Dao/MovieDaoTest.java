package com.hibernate.test.Dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.hibernate.myapp.dao.MovieDao;
import com.hibernate.myapp.dao.PersonneDao;
import com.hibernate.myapp.model.Movie;
import com.hibernate.myapp.model.Person;


@TestMethodOrder(value = OrderAnnotation.class)
public class MovieDaoTest {
	
	static MovieDao dao;
	static PersonneDao Daop;

	@BeforeAll
	public static void beforeAll() {
		dao = new MovieDao();
		Daop = new PersonneDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	//	Daop.close();
	}

	static Movie movie;
	
	@Test
	@Order(1)
	public void creerFilm() {
		Person p = new Person();
		p.setFirstName("clement"+System.currentTimeMillis());
		p.setLastName("kiki");
		p.setBirthDay(Date.from(LocalDateTime.of(1999, 12, 12, 0, 0).toInstant(ZoneOffset.UTC)));
		
		Person p1 = new Person();
		p1.setFirstName("clement3"+System.currentTimeMillis());
		p1.setLastName("kiki");
		p1.setBirthDay(Date.from(LocalDateTime.of(1999, 12, 12, 0, 0).toInstant(ZoneOffset.UTC)));
		
		Movie m = Movie.builder().name("M"+System.currentTimeMillis()).build();
		assertNotNull(m);
		assertNotEquals(0, m.getId());
		movie = m;
		Set<Person> listActeurs = new HashSet<Person>();
		listActeurs.add(p);
		listActeurs.add(p1);

		
		m.setActeurs(listActeurs);
		m = dao.add(m);
		for (Person person : m.getActeurs()) {
			
			System.err.println(person);
		}
//		System.err.println(m.getActeurs());
	}
	

	
	@Test
	@Order(2)
	public void chercherFilm() {
		Movie m = dao.find(movie.getId());
		assertNotNull(m);
		assertEquals(movie.getId(), m.getId());
	}
}

