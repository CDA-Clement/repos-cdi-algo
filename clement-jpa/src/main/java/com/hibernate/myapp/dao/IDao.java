package com.hibernate.myapp.dao;

import java.util.Collection;

interface IDao<T> {
	public void remove(Object pk);
	public T update(T entity);
	public T add(T entity);
	public Collection<T> findAllByNamedQuery(String query);
	public T find(Object id);
}
