package com.hibernate.myapp.dao;

import java.util.Collection;

import com.hibernate.myapp.model.Person;



public interface IPersonDao extends IDao<Person> {
	public Person findByName(String n);
	public Collection<Person> findAll();
}
