//package com.hibernate.myapp.dao;
//
//import java.util.List;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.EntityTransaction;
//import javax.persistence.Persistence;
//import javax.persistence.PersistenceException;
//import javax.persistence.TypedQuery;
//
//import com.hibernate.myapp.model.Person;
//
//import lombok.Getter;
//@Getter
//public class Dao {
//	@Getter
//	private EntityManagerFactory factory = null;
//
//	private EntityManager newEntityManager() {
//		EntityManager em = factory.createEntityManager();
//		em.getTransaction().begin();
//		return (em);
//	}
//
//	private void closeEntityManager(EntityManager em) {
//		if (em != null) {
//			if (em.isOpen()) {
//				EntityTransaction t = em.getTransaction();
//				if (t.isActive()) {
//					try {
//						t.rollback();
//					} catch (PersistenceException e) {
//					}
//				}
//				em.close();
//			}
//		}
//	}
//
//	public void init() {
//		factory = Persistence.createEntityManagerFactory("myBase");
//	}
//
//	public void close() {
//		if (factory != null) {
//			factory.close();
//		}
//	}
//
//	public Person addPerson(Person p) {
//		EntityManager em = null;
//		try {
//			em = newEntityManager();
//			// utilisation de lâ€™EntityManager
//			em.persist(p);
//			em.getTransaction().commit();
//			System.err.println("addPersonâ�£witdhâ�£id=" + p.getId());
//			return (p);
//		} finally {
//			closeEntityManager(em);
//		} 
//	}
//
//
//	public Person findPerson(long id) {
//		EntityManager em = null;
//		try {
//			em = factory.createEntityManager();
//			em.getTransaction().begin();
//			// utilisation de lï¿½EntityManager
//			Person p = em.find(Person.class, id);
//			return p;
//		} 
//		finally {
//			closeEntityManager(em);
//
//		}
//	}
//
//	public void updatePerson(Person p) {
//		EntityManager em = null;
//		try {
//			em = factory.createEntityManager();
//			em.getTransaction().begin();
//			em.merge(p);
//			em.getTransaction().commit();
//
//		} 
//		finally {
//			closeEntityManager(em);
//
//		}
//
//	}
//
//
//	public void removePerson(long id) {
//
//		EntityManager em = null;
//		try {
//
//			em = factory.createEntityManager();
//			em.getTransaction().begin();
//			// utilisation de lï¿½EntityManager
//			Person p = em.find(Person.class, id);
//			em.remove(p);
//			em.getTransaction().commit();
//
//		} 
//		finally {
//			closeEntityManager(em);
//
//		}
//
//	}
//
//	public List<Person> findAllPersons() {
//		EntityManager em = null;
//		try {
//			em = newEntityManager();
//			String query = "SELECT p FROM Person p";
//			TypedQuery<Person> q = em.createQuery(query, Person.class);
//
//			return q.getResultList();
//		} finally {
//			closeEntityManager(em);
//		} 
//	}
//
//	public List<Person> findPersonsByFirstName(String pattern) {
//		EntityManager em = null;
//		try {
//			em = newEntityManager();
//			String query = "SELECT p FROM Person p WHERE p.firstName= ?1";
//			TypedQuery<Person> q = em.createQuery(query, Person.class);
//			q.setParameter(1, pattern);
//
//			return q.getResultList();
//		} finally {
//			closeEntityManager(em);
//		} 
//	}
//}