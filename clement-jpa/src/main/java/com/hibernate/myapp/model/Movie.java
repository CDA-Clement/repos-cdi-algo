package com.hibernate.myapp.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Builder
@Table(name = "t_movie")
public class Movie {
// primary key
	@Id()
	@GeneratedValue()
	private Long id;
	
	@Column(length = 150, nullable = false)
	private String name;
	
	@ManyToMany(
			fetch = FetchType.LAZY, 
			cascade = { CascadeType.MERGE, CascadeType.PERSIST },
			mappedBy = "movies")
	Set<Person> acteurs;
}

