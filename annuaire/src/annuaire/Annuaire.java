package annuaire;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;

public class Annuaire {
	private HashMap<String, Coordonnees> listContact = new HashMap<>();

	//METHODES
	public void ajout(String nom,Coordonnees contact) {
		listContact.put(nom, contact);
	}

	public void affCoord(String nom) {
		System.out.println(listContact.get(nom));

	}
	public void modifCoord(String nom, Coordonnees contact) {
		if(listContact.containsKey(nom)){
			listContact.replace(nom, contact);
		}
	}

	public void suppression(String nom) {
		if(listContact.containsKey(nom)) {
			listContact.remove(nom);
		}
	}

	public void listerNoms() {
		Iterator<String> iterat = listContact.keySet().iterator();
		while(iterat.hasNext()){
			String key = iterat.next();
			System.out.println("KKKKKKKKKKKK"+key);
		}
	}

	public void afficherTotalite() {
		Iterator<String> iterat = listContact.keySet().iterator();
		while(iterat.hasNext()){
			String key = iterat.next();
			System.out.println("############"+key+listContact.get(key));
		}
	}
	public void listerAdresse() {
		Set<String> ensembleDesCoordonnees = listContact.keySet();
		for (String coord : ensembleDesCoordonnees) {
			System.out.println("**************"+coord + " -- " + listContact.get(coord).getAddress());


		}
	}
}