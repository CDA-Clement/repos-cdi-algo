package annuaire;
import java.util.Scanner;
public class Coordonnees {
	
	//ATTRIBUTS
	private String tel;
	private String address;
	
	// CONSTRUCTOR
	Coordonnees() {
	}

	//GETTER SETTER
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddress() {
		return address;
	}



	@Override
	public String toString() {
		return "Coordonnees [tel=" + tel + ", address=" + address + "]";
	}


}
