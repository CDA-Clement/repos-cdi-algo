package barnYard;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

public class Poulet extends Volaille implements Slaugtherhouse {
	private static double prixPoulet=9.5;
	private static double poidsabattage = 5;

	public Poulet(double a) {
		super(a);
		double prix =prixPoulet;
	}


	public static void creerPoulet(double a) {
		if(getToutesLesVollailes().size()<7) {
			boolean trouve = false;
			int cptPo=0;
			if(getToutesLesVollailes().size()>0) {
				for (Volaille volaille : getToutesLesVollailes()) {

					trouve = false;
					if(volaille instanceof Poulet) {
						cptPo++;
					}
				}
				if(cptPo==5) {
					System.out.println("Impossible you have reached the limit of chicken which is set to 5");
					trouve=true;
				}
				if(trouve==false) {
					new Poulet(a);
					System.out.println("                                   Chicken added successfully");
				}

			}
			else{
				new Poulet(a);
				System.out.println("                                   Chicken added successfully");
			}

		}else {
			System.out.println("You have reached the limit of the barn");
		}



	}

	// WEIGHT


	public static void listpoidAbattage() {
		if(getToutesLesVollailes().size()>0) {
			boolean trouve=false;
			for (Volaille volaille : getToutesLesVollailes()) {

				if(volaille instanceof Poulet &&volaille.getPoids()>=poidsabattage) {
					if(volaille.getPoids()>=poidsabattage) {
						trouve=true;
						System.out.println(volaille.toString());
					}

				}
			}
			if(trouve==false) {
				System.out.println("your stock doesn't contain any chicken ready yet");
			}
		}
		else {
			System.out.println("your stock is empty");
		}


	}



	public static void ChangerPoidsAbattage(double d) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		setPoidsabattage(d);
		getHistorySlaugtherWeightPoulet().put(formatDateTime, Poulet.poidsabattage);

	}
	
	// SALES AND PRICE


	public static void ChangerPrixDuJour(double d) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		setPrixPoulet(d);
		getHistorymarketpricePoulet().put(formatDateTime, Poulet.getPrixPoulet());
	}


	@Override
	public void vente() {
		double a=getPoids();
		double b=prixPoulet;
		System.out.println("le prix de vente est de "+a*b+"euros");
	}

	///GETTER & SETTER

	public static double getPrixPoulet() {
		return prixPoulet;
	}
	public static void setPrixPoulet(double prixPoulet) {
		Poulet.prixPoulet = prixPoulet;
	}


	public static double getPoidsabattage() {
		return poidsabattage;
	}

	public static void setPoidsabattage(double poidsabattage) {
		Poulet.poidsabattage = poidsabattage;
	}

	/// TO STRING
	@Override
	public String toString() {
		return "Poulet ["+super.toString()+" prix au kilo= "+prixPoulet+"]";
	}
}

