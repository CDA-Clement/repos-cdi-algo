package geometrie;
import java.lang.Math;
public class Triangle {
	private Point a;
	private Point b;
	private Point c;

	public Triangle() {
		a = new Point();
		b = new Point();
		c = new Point();
	}

	public Point getA() {
		return a;
	}

	public Point getB() {
		return b;
	}

	public Point getC() {
		return c;
	}

	public static void initialisationTriangle( Triangle t) {
		System.out.println("initisalisation du Triangle : ");
		Point.initialisationPoint(t.a);
		Point.initialisationPoint(t.b);
		Point.initialisationPoint(t.c);


	}
	boolean bool = false;
	boolean bool1 =false;
	boolean bool2= false;
	public void afficher() {
		System.out.println("affichage du triangle "+this.a.getNom()+this.b.getNom()+this.c.getNom());
		a.afficher();
		b.afficher();
		c.afficher();

	}
	
	public void afficherTailleSegTriangle() {
		System.out.println(a.distance(b));
		System.out.println(a.distance(c));
		System.out.println(b.distance(c));
	}

	public boolean isocele() {
		bool = false;

		if(a.distance(b) == a.distance(c) || a.distance(b) == c.distance(a) || b.distance(a)== a.distance(c)|| b.distance(a)== c.distance(a)|| a.distance(b)== b.distance(c)|| a.distance(b)== c.distance(b)|| b.distance(a)== c.distance(b)|| b.distance(a)== b.distance(c)|| b.distance(c)== c.distance(a)|| b.distance(c)== a.distance(c)|| c.distance(b)== a.distance(c)|| c.distance(b)== c.distance(a)){
			bool = true;}
		System.out.println("LA TAILLE DU SEGMENT "+a.getNom() +b.getNom() +" EST DE "+a.distance(b));
		System.out.println("LA TAILLE DU SEGMENT "+a.getNom() +c.getNom() +" EST DE "+a.distance(c));
		System.out.println("LA TAILLE DU SEGMENT "+b.getNom() +c.getNom() +" EST DE "+b.distance(c));
		if(bool == true) {
			System.out.println("le triangle est isocele car il y a deux cotes egaux");
		}else {
			System.out.println("le triangle n'est pas isocele");
		}
		return bool;

	}

	public boolean rectangle() {
		boolean bool1 = false;
		if(Math.pow(a.distance(b), 2) == Math.pow(a.distance(c), 2)+ Math.pow(b.distance(c), 2)||Math.pow(a.distance(c), 2) == Math.pow(a.distance(b), 2)+ Math.pow(b.distance(c), 2)||Math.pow(b.distance(c), 2) == Math.pow(a.distance(b), 2)+ Math.pow(a.distance(c), 2)) {
			bool1 = true;
		}

		System.out.println("LA TAILLE DU SEGMENT "+a.getNom() +b.getNom() +" EST DE "+Math.pow(a.distance(b), 2));
		System.out.println("LA TAILLE DU SEGMENT "+a.getNom() +c.getNom() +" EST DE "+Math.pow(a.distance(c), 2));
		System.out.println("LA TAILLE DU SEGMENT "+b.getNom() +c.getNom() +" EST DE "+Math.pow(b.distance(c), 2));
		if(bool1==true) {
			System.out.println("le triangle est donc rectangle car la taille de l'hypotenus au carre egale la somme des deux autres cotes.");
		}else {
			System.out.println("le triangle n'est pas rectangle");
		}

		return bool1;
	}

	public boolean equilateral () {
		bool2 = false;
		if(bool == false && bool1 == false) {
			if(a.distance(b) == a.distance(c) && b.distance(c) == a.distance(b)){
				bool2 = true;
			}
		}
		if (bool2 == true) {
			System.out.println("LA TAILLE DU SEGMENT "+a.getNom() +b.getNom() +" EST DE "+a.distance(b));
			System.out.println("LA TAILLE DU SEGMENT "+a.getNom() +c.getNom() +" EST DE "+a.distance(c));
			System.out.println("LA TAILLE DU SEGMENT "+b.getNom() +c.getNom() +" EST DE "+b.distance(c));
			System.out.println("le triangle est donc equilateral car ses 3 cotes sont identiques.");
		}else {
			System.out.println("le triangle n'est pas equilateral");
		}
		return bool2;	
	}


	public double surface() {
		double  surface =0;
		double p = 0;
		p = (a.distance(b)+ a.distance(c)+ b.distance(c))/2;

		surface = Math.sqrt(p*(p-a.distance(c))*(p-a.distance(b))*(p-b.distance(c)));

		return surface;
	}



}

