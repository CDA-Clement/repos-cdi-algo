package geometrie;

import java.util.Scanner;

public class Senario {
	/**
	 * le senario pour tester le code de la classe point
	 */
	public static void testPoint() {
		Scanner sc = new Scanner(System.in);
		Point p = new Point(); 

		Point.initialisationPoint(p);

		p.afficher();

		System.out.println("Veuillez entrer un coefficient de translation pour X et Y");
		p.translation(sc.nextDouble());
		p.afficher();

		System.out.println("*********************************************************");
		System.out.println("nouveau point");

		Point p1 = new Point();
		Point.initialisationPoint(p1);
		p1.afficher();

		System.out.println("Veuillez entrer un coefficient de translation pour X et Y");
		p1.translation(sc.nextDouble());
		p1.afficher();

		double retourDeUsine = p.distance(p1);
		System.out.println("La distance entre les deux points est de "+retourDeUsine);

		Point psym= p.symetrique();
		System.out.println("Le point symetrique a pour Abscisse "+psym.getAbscisse()+ " et pour Ordonnee "+ psym.getOrdonnee());
		boolean bool = p.equals(p1);
		if(bool == true) {
			System.out.println("les points 2 points sont superposes.");
		}else {
			System.out.println("Les 2 points sont distant de "+(p.distance(psym)));
		}
	}




	/**
	 * le senario pour tester le code de la classe segment
	 */
	public static void testSegment() {
		Scanner sc = new Scanner(System.in);
		Segment s = new Segment(); 

		Segment.initialisationSegment(s);
		s.afficher();
		double retourDeTaille = s.taille();
		System.out.println("La taille du Segment est de "+ retourDeTaille);

		System.out.println("Veuillez entrer un coefficient de translation pour le Segment");
		s.translate(sc.nextDouble());
		s.afficher();

		Segment sym = s.symetriqueSeg();
		System.out.println("Après symétrie");
		sym.afficher();

		Segment s1 = new Segment();
		Segment.initialisationSegment(s1);
		s1.afficher();

		s.equalsSeg(s1);

	}
	/**
	 * le senario pour tester le code de la classe triangle.
	 */
	public static void testTriangle() {
		Scanner sc = new Scanner(System.in);
		Triangle t = new Triangle();

		Triangle.initialisationTriangle(t);
		t.afficher();

		t.isocele();
		t.rectangle();
		t.equilateral();

		double surface = t.surface();
		System.out.println("La surface du triangle est de "+surface);
		sc.close();
	}

}
