package geometrie;
import java.lang.Math;

public class Segment {
	private Point a;
	private Point b;

	public Segment() {
		a = new Point();
		b = new Point();
	}

	public Point getA() {
		return a;
	}
	public Point getB() {
		return b;
	}

	public static void initialisationSegment(Segment s) {
		System.out.println("initisalisation du segment : ");
		Point.initialisationPoint(s.a);
		Point.initialisationPoint(s.b);
	}
	public void afficher() {
		System.out.println("le segment commence aux coordonnees abscisse "+a.getAbscisse()+" pour ordonnee "+a.getOrdonnee()+" le segment termine aux coordonnees abscisse "+b.getAbscisse()+ " pour ordonnee" +b.getOrdonnee() );
	}
	public double taille () {
		double taillet = 0;
		taillet = Math.sqrt(Math.pow((a.getAbscisse()-b.getAbscisse()),2) + Math.pow((a.getOrdonnee()-b.getOrdonnee()),2));
		return taillet;
	}

	public void translate(double d) {
		a.translation(d);
		b.translation(d); 
	}

	public Segment symetriqueSeg() {
		Segment sym = new Segment();
		sym.a = this.a.symetrique();
		sym.b = this.b.symetrique();
		return sym;
	}

	public boolean equalsSeg(Segment param) {
		boolean bool = false;
		if(this.a.equals(param.a) && this.b.equals(param.b) || this.b.equals(param.a) && this.a.equals(param.b)){
			bool = true;}
		if(bool ==true) {
			System.out.println("les deux segments sont Egaux");
		}else {
			System.out.println("les deux segments ne sont pas Egaux");
		}

		return bool;
	}


}




