package geometrie;
import java.lang.Math;
import java.util.Scanner;
/**
 * 
 * @author 59013-57-03
 *
 */
public class Point {
	Scanner sc = new Scanner(System.in);
	private double abscisse;
	private double ordonnee;
	private char nom;
	public static void initialisationPoint(Point p) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Veuillez entrer un nom de point");
		p.nom =sc.next().charAt(0);

		System.out.println("Veuillez entrer un abscisse");
		p.abscisse = sc.nextDouble();

		System.out.println("Veuillez entrer un ordonnee");
		p.ordonnee = sc.nextDouble();

	}


	public double getAbscisse() {
		return abscisse;
	}

	public double getOrdonnee() {
		return ordonnee;
	}

	public char getNom() {
		return nom;
	}

	public void afficher() {
		System.out.println("le point de nom "+this.nom+" pour abscisse "+this.abscisse+" pour ordonnee "+this.ordonnee);
	}


	public void translation(double d) {
		abscisse = d+abscisse;
		ordonnee = d+ordonnee;
	}
	/**
	 * calcule la distance entre le point en cours et le point pass� en param�tre
	 * @param param le point avec lequel on calcule la distance
	 * @return la distance
	 */
	public double distance(Point param) {
		double distance = 0;
		distance = Math.sqrt(Math.pow((param.getAbscisse()-this.getAbscisse()),2) + Math.pow((param.getOrdonnee()-this.getOrdonnee()),2));
		return( Math.round(distance * 100.0) / 100.0);

	}

	public Point symetrique() {
		Point psym = new Point(); 
		psym.abscisse = this.getAbscisse()*-1;
		psym.ordonnee = this.getOrdonnee()*-1;

		return psym;
	}

	public boolean equals(Point param) {
		boolean bool = false;
		if(param.getAbscisse()== this.getAbscisse() && param.getOrdonnee() == this.getOrdonnee()) {
			bool = true;		
		}
		if(bool==true) {
		}

		return bool;	
	}
}





