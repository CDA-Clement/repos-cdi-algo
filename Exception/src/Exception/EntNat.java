package Exception;

public class EntNat  {
	private int num;

	public EntNat(int numb) throws ErrConst {
		if(numb<0 || numb>Integer.MAX_VALUE) {
			ErrConst e = new ErrConst(numb);
			throw e;
		}
		this.num=numb;
	}

	public int getN() {
		return this.num;
	}

	public static EntNat somme(EntNat a, EntNat b)throws ErrSomme, ErrConst {
		EntNat som= new EntNat(a.getN()+b.getN());
		if(a.getN()+b.getN()<0) {
			ErrSomme s = new ErrSomme();
			throw s;
		}
		else {
			System.out.println("la somme est de "+som.num);
			return som;	
		}


	}



	public static EntNat diff(EntNat a, EntNat b)throws ErrDiff, ErrConst {
		if(a.num>b.num) {
			EntNat diff = new EntNat(a.getN()-b.getN());
			if(diff.num<0) {
				ErrDiff d = new ErrDiff();
				throw d;
			}
			else {
				System.out.println("la diff est de "+diff.num);
				return diff;	
			}


		}else{
			EntNat diff = new EntNat(b.getN()-a.getN());
			if(diff.num<0) {
				ErrDiff d = new ErrDiff();
				throw d;
			}
			else {
				System.out.println("la diff est de "+diff.num);
				return diff;	
			}
		}
	}





	public static EntNat produit(EntNat a, EntNat b)throws ErrProd, ErrConst {
		double produit = (double)a.num * (double) b.num;
		if(produit > Integer.MAX_VALUE) {
			ErrProd p = new ErrProd();
			throw p;
		}

		EntNat prod =new EntNat((int)produit);
		return prod;
	}


	public static void main(String[] args) throws ErrConst, ErrSomme, ErrDiff, ErrProd  {
		try {
			EntNat ent= new EntNat(2147483647);
			EntNat ent1 = new EntNat(2147483647);
			EntNat.somme(ent, ent1);
			EntNat.diff(ent, ent1);
			EntNat.produit(ent, ent1);


		} 
		catch (ErrConst e) {
			System.err.println("l'entier qui pose probleme est "+e.getNum()+" "+e.getMessage());	

		}
		catch (ErrSomme s) {
			System.err.println("you've reach the maximum value of integer");	
		}
		catch (ErrDiff d) {
			System.err.println("you've reach the maximum value of integer");	
		}
		catch (ErrProd p) {
			System.err.println("you've reach the maximum value of integer");	
		}

		System.out.println("termine");
	}



	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
}
