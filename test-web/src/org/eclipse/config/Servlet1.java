package org.eclipse.config;




import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet1 extends HttpServlet {
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException {
		System.out.println("hello");
		response.setContentType("text/html");
		// indiquer l’encodage UTF-8 pour ́eviter lesproblemes avec les accents
		response.setCharacterEncoding("UTF-8");
		PrintWriter out=response.getWriter();
		out.print("<h1>hello World</h1>");
		
		
	}
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) {
		
	}

}
