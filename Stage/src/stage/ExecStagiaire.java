package stage;

import java.util.Scanner;

public class ExecStagiaire {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Veuillez entrer un nombre de stagiaires");
		int nombre = sc.nextInt();
		
		Stagiaire[] vitel = Tools.initTabStag(nombre);
		
		Tools.tableauDeStagiaire(vitel);
		
		Tools.afficherTableauStagiaire(vitel);
		
		System.out.println("******************* apres tri : ");
		
		Tools.triAscendant(vitel);
		
		Tools.afficherTableauStagiaire(vitel);
		
		sc.close();
		
	}
}
