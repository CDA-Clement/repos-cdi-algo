package com.example.demo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Personne;

@Repository
public interface PersonneDao extends CrudRepository<Personne, Integer> {
	
	public List<Personne> findAll();

	
	
}
