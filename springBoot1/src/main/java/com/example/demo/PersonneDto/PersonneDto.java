package com.example.demo.PersonneDto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PersonneDto {
	int id;
	String nom;
	String prenom;
	Integer age;
	

}
