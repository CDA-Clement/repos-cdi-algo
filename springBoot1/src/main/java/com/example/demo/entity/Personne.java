package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Personne {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONNE_SEQ")
	private int id;
	
	String nom;
	
	String prenom;
	
	Integer age;

}
