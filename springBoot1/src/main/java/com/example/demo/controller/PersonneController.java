package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.PersonneDto.PersonneDto;
import com.example.demo.service.IPersonneService;

@Controller

public class PersonneController {
	@Autowired
	private IPersonneService personneService;

	@GetMapping("/list")
	public ModelAndView liste(ModelAndView mv) {
		List<PersonneDto> liste=personneService.chercherToutLeMonde();

		mv.addObject("liste", liste);
		mv.setViewName("liste");
		return mv;
	}
	
	@GetMapping(value = "/ajouter")
	public ModelAndView ajouter(ModelAndView mv) {
		mv.setViewName("ajouter");
		
		return mv;
		
	}
	
	@PostMapping(value = "/ajouter",params = { "nom", "prenom", "age" })
	public ModelAndView ajouterpost(
			@RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "age") Integer age,
			ModelAndView mv) {
		
		personneService.ajouterPersonne(nom, prenom, age);
		List<PersonneDto> liste=personneService.chercherToutLeMonde();
		mv.setViewName("liste");
		mv.addObject("liste", liste);
		
		return mv;
		
	}
}


