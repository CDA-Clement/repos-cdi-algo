package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.PersonneDto.PersonneDto;

public interface IPersonneService {
	
	public List<PersonneDto> chercherToutLeMonde();

	public String deleteById(int id);

	public Optional<PersonneDto> findById(int id);


	public Boolean ajouterPersonne(String nom, String prenom, Integer age);

}
