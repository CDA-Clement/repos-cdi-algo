package com.example.demo.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.PersonneDto.PersonneDto;
import com.example.demo.dao.PersonneDao;
import com.example.demo.entity.Personne;
@Service
public class PersonneServiceImpl implements IPersonneService{
	@Autowired
	private PersonneDao personneRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<PersonneDto> chercherToutLeMonde() {

		List<PersonneDto>maliste =this.personneRepository
				.findAll()
				.stream()
				.map(u->PersonneDto.builder()
						.id(u.getId())
						.nom(u.getNom())
						.prenom(u.getPrenom())
						.age(u.getAge())
						.build())
				.collect(Collectors.toList());
		return maliste;
	}

	@Override
	public String deleteById(int id) {
		if (this.personneRepository.existsById(id)) {
			try {
				this.personneRepository.deleteById(id);	
				return "OK";
			} catch (Exception e) {
				return "KO_SQL_EXCEPTION";				
			}
		}
		return "KO";
	}

	@Override
	public Optional<PersonneDto> findById(int id) {
		Optional<Personne> personne = this.personneRepository.findById(id);
		Optional<PersonneDto> res = Optional.empty();
		if(personne.isPresent()) {
			Personne u = personne.get();
			PersonneDto personneDto = this.modelMapper.map(u, PersonneDto.class);
			res = Optional.of(personneDto);
		}
		return res;
	}

	@Override
	public Boolean ajouterPersonne(String nom, String prenom, Integer age) {
		Boolean creer = true;
		Iterable<Personne> utilisateurs = this.personneRepository.findAll();
		for (Personne utilisateur : utilisateurs) {
			if (utilisateur.getNom().equals(nom)&&utilisateur.getPrenom().equals(prenom)) {
				creer = false;
				break;
			}
		}
		if (creer) {
			Personne utilisateur1 = Personne.builder().nom(nom).prenom(prenom).age(age).build();
			try {
				personneRepository.save(utilisateur1);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return creer;
	}

}
