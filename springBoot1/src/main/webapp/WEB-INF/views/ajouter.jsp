<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
	 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js" ></script>
<script src="resources/jquery/clickable.js" ></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<title>Inscription</title>
</head>
<body>

	<div class="container my-4">
		<h1>Inscription</h1>

		<form action="ajouter" method="post">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputLogin">Nom</label> 
					<input type="text"
						class="form-control" name="nom" placeholder="nom">
				</div>			
				<div class="form-group col-md-6">
					<label for="inputLogin">Prenom</label> 
					<input type="text"
						class="form-control" name="prenom" placeholder="prenom">
				</div>	
				
				<div class="form-group col-md-6">
					<label for="inputLogin">Age</label> 
					<input type="number"
						class="form-control" name="age" placeholder="age">
				</div>			
			</div>
			<button type="submit" class="btn btn-primary">Valider</button>
		</form>
	</div>


</body>
</html>