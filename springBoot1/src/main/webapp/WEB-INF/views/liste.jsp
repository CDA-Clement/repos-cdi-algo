<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
	 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js" ></script>
<script src="resources/jquery/clickable.js" ></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>

<table class="table">
  <thead>
    <tr>
      <th scope="col">nb</th>
      <th scope="col">nom</th>
      <th scope="col">prenom</th>
      <th scope="col">age</th>
    </tr>
  </thead>
<c:forEach items="${liste}" var="p" varStatus="s">
  <tbody>
    <tr>
      <th scope="row">${s.count}</th>
      <td>${p.nom }</td>
      <td>${p.prenom}</td>
      <td>${p.age}</td>
    </tr>
</c:forEach>
  </tbody>
</table>


</body>
</html>