package placement;
import java.util.Collections;
import java.util.Random;

public class Scenario {

	public static void main(String[] args) {
		Stagiaire s = new Stagiaire();
		System.out.println("\n");
		Collections.shuffle(s.lisTete, new Random());
		Collections.shuffle(s.lisTagiaire, new Random());

		int k =0;
		int j = 0;
		Ilot ilot1 = new Ilot();

		ilot1.listIlot.add(s.lisTete.get(k));
		k++;
		ilot1.listIlot.add(s.lisTagiaire.get(j));
		j++;
		ilot1.listIlot.add(s.lisTagiaire.get(j));
		j++;
		Collections.shuffle(ilot1.listIlot, new Random());

		Ilot ilot2 = new Ilot();

		ilot2.listIlot.add(s.lisTete.get(k));
		k++;
		ilot2.listIlot.add(s.lisTagiaire.get(j));
		j++;
		ilot2.listIlot.add(s.lisTagiaire.get(j));
		j++;

		Ilot ilot3 = new Ilot();

		ilot3.listIlot.add(s.lisTete.get(k));
		k++;
		ilot3.listIlot.add(s.lisTagiaire.get(j));
		j++;
		ilot3.listIlot.add(s.lisTagiaire.get(j));
		j++;


		Ilot ilot4 = new Ilot();
		ilot4.listIlot.add(s.lisTete.get(k));
		k++;
		ilot4.listIlot.add(s.lisTagiaire.get(j));
		j++;
		ilot4.listIlot.add(s.lisTagiaire.get(j));
		j++;


		Ilot ilot5 = new Ilot();

		ilot5.listIlot.add(s.lisTete.get(k));
		k++;

		ilot5.listIlot.add(s.lisTagiaire.get(j));
		j++;
		ilot5.listIlot.add(s.lisTagiaire.get(j));
		j++;

		Collections.shuffle(ilot1.listIlot, new Random());
		Collections.shuffle(ilot2.listIlot, new Random());
		Collections.shuffle(ilot3.listIlot, new Random());
		Collections.shuffle(ilot4.listIlot, new Random());
		Collections.shuffle(ilot5.listIlot, new Random());

		System.out.println("ilot1 "+ilot1.numero+" est compos� de "+ilot1.listIlot);
		System.out.println("\n");
		System.out.println("ilot1 "+ilot2.numero+" est compos� de "+ilot2.listIlot);
		System.out.println("\n");
		System.out.println("ilot1 "+ilot3.numero+" est compos� de "+ilot3.listIlot);
		System.out.println("\n");
		System.out.println("ilot1 "+ilot4.numero+" est compos� de "+ilot4.listIlot);
		System.out.println("\n");
		System.out.println("ilot1 "+ilot5.numero+" est compos� de "+ilot5.listIlot);
	}
}
