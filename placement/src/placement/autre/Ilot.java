package placement.autre;
import java.util.ArrayList;
import java.util.List;
public class Ilot {
    // attributes
    private final Integer numero;
    private List<Stagiaire> StagIlot = new ArrayList<>();
    public final static Integer MAX_PERSONNES = 3;
    public static List<Ilot> tousLesIlots = new ArrayList<>();
    // getters && setters
    public Integer getNumero() {
        return numero;
    }
    public List<Stagiaire> getStagIlot() {
        return StagIlot;
    }
    public static Integer getMaxPersonnes() {
        return MAX_PERSONNES;
    }
    public void setStagIlot(List<Stagiaire> stagIlot) {
        StagIlot = stagIlot;
    }
    public static List<Ilot> getTousLesIlots() {
        return tousLesIlots;
    }
    public static void setTousLesIlots(List<Ilot> tousLesIlots) {
        Ilot.tousLesIlots = tousLesIlots;
    }
    @Override
    public String toString() {
        return "Ilot [numero= " + numero + ", StagIlot= " + StagIlot + "]";
    }
    
    // constructors
    public Ilot(Integer numero) {
        this.numero = numero;
        tousLesIlots.add(this);
    }
}