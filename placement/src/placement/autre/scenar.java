package placement.autre;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class scenar {
	public static void main(String[] args) {
		Stagiaire s1 = new Stagiaire("Maxime", "Huart", true);
		Stagiaire s2 = new Stagiaire("Anthony", "Godderidge", true);
		Stagiaire s3 = new Stagiaire("Laurent", "Fx", true);
		Stagiaire s4 = new Stagiaire("redouane", "Mathematicien", true);
		Stagiaire s5 = new Stagiaire("Jean-Philippe", "Catteau", true);
		Stagiaire s6 = new Stagiaire("Clement", "Leuridon", false);
		Stagiaire s7 = new Stagiaire("Richard", "Maciejewski", false);
		Stagiaire s8 = new Stagiaire("Linda", "!", false);
		Stagiaire s9 = new Stagiaire("Amaia", "Igos", false);
		Stagiaire s10 = new Stagiaire("Mustapha", "Belkaci", false);
		Stagiaire s11 = new Stagiaire("Ketsia", "Bertille", false);
		Stagiaire s12 = new Stagiaire("Badrane", "Houmadi", false);
		Stagiaire s13 = new Stagiaire("Mohammed", "Akharroub", false);
		Stagiaire s14 = new Stagiaire("Soulaiman", "!", false);
		Stagiaire s15 = new Stagiaire("Yassine", "Roudane", false);
		Ilot ilot1 = new Ilot(1);
		Ilot ilot2 = new Ilot(2);
		Ilot ilot3 = new Ilot(3);
		Ilot ilot4 = new Ilot(4);
		Ilot ilot5 = new Ilot(5);
		List<Stagiaire> stagiaires = new ArrayList<>();
		List<Stagiaire> stagiairesHead = new ArrayList<>();
		// Separation des stagiaire en 2 listes
		for (int i = 0; i < Stagiaire.tousLesStagiaires.size(); i++) {
			if (Stagiaire.tousLesStagiaires.get(i).isHead()) {
				stagiairesHead.add(Stagiaire.tousLesStagiaires.get(i));
			} else {
				stagiaires.add(Stagiaire.tousLesStagiaires.get(i));
			}
		}
		// premier shuffle des listes s�par�es
		Collections.shuffle(stagiaires);
		Collections.shuffle(stagiairesHead);
		// repartition des heads dans les ilots

		ilot1.getStagIlot().add(stagiairesHead.get(0));
		ilot2.getStagIlot().add(stagiairesHead.get(1));
		ilot3.getStagIlot().add(stagiairesHead.get(2));
		ilot4.getStagIlot().add(stagiairesHead.get(3));
		ilot5.getStagIlot().add(stagiairesHead.get(4));

		int j = 0;
		for (int i = 0; i < stagiaires.size(); i++) {
			if (i == 2 || i == 4 || i == 6 || i == 8) {
				j++;
			}
			Ilot.getTousLesIlots().get(j).getStagIlot().add(stagiaires.get(i));
		}
		Collections.shuffle(ilot1.getStagIlot());
		Collections.shuffle(ilot2.getStagIlot());
		Collections.shuffle(ilot3.getStagIlot());
		Collections.shuffle(ilot4.getStagIlot());
		Collections.shuffle(ilot5.getStagIlot());

		for (Ilot ilot : Ilot.getTousLesIlots()) {
			System.out.println(ilot);
		}
	}
}