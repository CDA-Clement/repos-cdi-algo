package swing;

import javax.swing.JFrame;

public class MaFenetre extends JFrame {
	public MaFenetre(int posX, int posY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, 1200, 1200);
		this.setTitle(titre);
		this.setVisible(true);
	}

	public static void main(String[] args) throws InterruptedException {
		MaFenetre m = new MaFenetre(120, 110, "test");
		Ecouteur e = new Ecouteur();
		m.addWindowListener(e);
		Thread t1 = new Thread();
		m.toFront();
		t1.sleep(4000);
		m.toBack();
		t1.sleep(4000);
		m.toFront();

	}
}
