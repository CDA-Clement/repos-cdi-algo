package swing;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Ecouteur extends WindowAdapter {
	@Override
	public void windowClosing(WindowEvent e) {
		System.out.println(e);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.out.println("window is closed");
		super.windowClosed(e);
	}

	@Override
	public void windowOpened(WindowEvent e) {
		System.out.println("window is opened");
		super.windowOpened(e);
	}

	@Override
	public void windowActivated(WindowEvent e) {
		System.out.println("window is activated");
		super.windowActivated(e);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		System.out.println("window is desactivated");
		super.windowDeactivated(e);
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		System.out.println("window is deiconified");
		super.windowDeiconified(e);
	}

	@Override
	public void windowIconified(WindowEvent e) {

		super.windowIconified(e);
	}

}
