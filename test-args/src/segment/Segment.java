package segment;

public class Segment {
int s;
int s1;

public Segment(int i, int j) {
	s = i;
	s1 = j;
	
}
public String toString() {
	int min =0;
	int max =0;
	if(this.getS()>this.getS1()) {
		min = this.getS1();
		max = this.getS();
	}else {
		min = this.getS();
		max = this.getS1();
	}
return "Segment ["+min+", "+max+"]";
}

public boolean appartient(int point) {
	boolean bool = false;
	int min =0;
	int max =0;
	if(this.getS()>this.getS1()) {
		min = this.getS1();
		max = this.getS();
	}else {
		min = this.getS();
		max = this.getS1();
	}
	if( point <min || point >max) {
		bool = false;
	}else {
		bool = true;
	}
		
	
	
	return bool;
}

public int longueur() {
	int min =0;
	int max = 0;
	if(this.getS()>this.getS1()) {
		min = this.getS1();
		max = this.getS();
	}else {
		min = this.getS();
		max = this.getS1();
	}
	
	return max-min;
}

public int getS() {
	return s;
}

public int getS1() {
	return s1;
}
}
