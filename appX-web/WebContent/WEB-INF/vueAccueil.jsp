<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="com.afpa.cda.dto.PersonneDto"%>
<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.dto.ReponseStatut"%>
<%@page import="com.afpa.cda.dto.ReponseDto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% String origine=(String)request.getAttribute("origine");

System.out.println("origine "+origine);

%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>


<div class="container my-4">
<h1>Bienvenue à l'Accueil</h1>
<a class="btn btn-primary" href="list.do" role="button">Personne</a>
<a class="btn btn-primary" href="listMetier.do" role="button">Metier</a>
</div>


<script src="jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>
</html>