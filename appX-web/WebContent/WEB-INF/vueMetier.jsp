<%@page import="com.afpa.cda.service.MetierServiceImpl"%>
<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="com.afpa.cda.dto.PersonneDto"%>
<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.dto.ReponseStatut"%>
<%@page import="com.afpa.cda.dto.ReponseDto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<% String origine=(String)request.getAttribute("origine");
Boolean creer=(Boolean)request.getAttribute("boolean");
MetierDto metier=(MetierDto)request.getAttribute("metier");
List<MetierDto> liste=(List<MetierDto>)request.getAttribute("liste");
int page1=0;
String pageStr=(String)request.getAttribute("pageStr");
ReponseDto reponse=(ReponseDto)request.getAttribute("reponse");


System.out.println("origine "+origine);
System.out.println("creer "+creer);
System.out.println("metier "+metier);
if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {

}else {
	page1 = Integer.parseInt(pageStr);
}
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>

<%if(origine.equalsIgnoreCase("com.afpa.cda.servletMetier.AddServletMetier")){ %>

<div class="container my-4">
<h1>Ajouter un nouveau Metier</h1>
<form method="post" action="metier.do">
<div class="form-row">
<div class="form-group col-md-6">
<label for="inputNom">Nom</label>
<input type="text" class="form-control" name="nom" placeholder="nom">
</div>
</div>
<button type="submit" class="btn btn-primary">Ajouter</button>
</form>
<br>
<form action="listMetier.do">
<button type="submit" class="btn btn-primary">Retour a la liste</button>
</form>
</div>
<%if(creer!=null) {
if(creer==false) { %>
<script>
alert("Ce metier existe deja!");
</script>
<%}else{%>
<div class="d-flex justify-content-center align-items-center" style="height:100px;">
    <div class="spinner-border text-success" role="status"></div>
</div>


<script>
function myFunction() {
	setTimeout(function(){ alert("success"); document.location='listMetier.do'; }, 2000);
}
myFunction();
</script>

<%}%>
<%}%>
<%}else if(origine.equalsIgnoreCase("com.afpa.cda.servletMetier.ShowServletMetier")){
	String nomMetier=metier.getNom();
	String idMetier=metier.getId().toString();
System.out.println("metierhhh "+metier.getNom());%>
<div class="container my-4">
<div class="row justify-content-md-center">
<div class="col card col-lg-8">

<div class="form-group row">
<label for="idPers" class="col-sm-2 col-form-label">identifiant</label>
<div class="col-sm-10">
<input type="text" readonly class="form-control-plaintext" id="idPers" value="<%=idMetier%>">
</div>
</div>
<div class="form-group row">
<label for="nomPers" class="col-sm-2 col-form-label">nom</label>
<div class="col-sm-10">
<input type="text" readonly class="form-control-plaintext" id="nomPers" value="<%=nomMetier%>">
</div>
</div>
</div>

<a class="btn btn-secondary" href="listMetier.do" role="button">retour vers la liste</a>
</div>
</div>

<%}%>

<%if(origine.equalsIgnoreCase("com.afpa.cda.servletMetier.ListServletMetier")){ %>
<%if(reponse != null) {
	String affiche=reponse.getMsg();
			String typeMsg=reponse.getStatus()==ReponseStatut.OK?"primary":"danger";
	System.out.println("typeMsg "+typeMsg);	%>
<div class="alert alert-<%=typeMsg%>"alert-dismissible fade show role="alert">
<strong><%=affiche%></strong>
</div>
<%}%>
<div class="container my-4">
<div class="row justify-content-md-center">
<table class="table table-hover">
<thead class="thead-dark">
<tr>
<th>id</th>
<th>Nom</th>
<th>Supprimer</th>
<th></th>
</tr>
</thead>

<tbody>
<%
for(MetierDto p : liste){
	System.out.println(p.getId());	
	String idStr=Integer.toString(p.getId());%>
			<tr onclick>

	<td><a style="display:block;width:100%;height:100%;" href="showMetier.do?id=<%=idStr%>"><%=idStr%></a></td>
	<td><a style="display:block;width:100%;height:100%;" href="showMetier.do?id=<%=idStr%>"><%=p.getNom()%></a></td>
	<td><i class ="fas fa-trash-alt" type="button" data-toggle="modal" data-target="#exampleModal"></i><td>
	
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
	<div class="modal-header">
	<h5 class="modal-title" id="exampleModalLabel">Achtung!!!!</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button>
	 </div>
	 <div class="modal-body">
	    Veux-tu tuer Terminer ce metier?
	 </div>
	 <div class="modal-footer">
	 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	 <a href="deleteMetier.do?id=<%=idStr%>" type="button" class="btn btn-primary">valider la mort</a>
	  </div>
	   </div>
	   </div>
	   </div>
	   </tr>
	   </tbody>
<%};%>

	   </table>
		
	   <%if(page1 >0) { %>
	   <a href="listMetier.do?page=<%=page1-1%>"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		<%};%>
		<%if(MetierServiceImpl.fin == 5) { %>
		
	   <a href="listMetier.do?page=<%=page1+1%>"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
	   <%}; %>
		<a href="metier.do" type="button" class="btn btn-info btn-lg btn-block">Ajouter un Metier</a>
		<a href="index.html" type="button" class="btn btn-info btn-lg btn-block">Revenir a l'accueil</a>
			<%}; %>

<script src="jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>

</body>
</html>