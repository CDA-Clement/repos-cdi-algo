<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="com.afpa.cda.dto.PersonneDto"%>
	<%
	String idPers = request.getParameter("id");
	PersonneDto personne =(PersonneDto)request.getAttribute("personne");
	String naissanceP = personne.getDateNaissance().toString();
	naissanceP = naissanceP.substring(0,10);

	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Details personne</title>
</head>
<body>

	<div class="container my-4">
		 <div class="row justify-content-md-center">
			<div class="col card col-lg-8\">
			
				<form>
						<div class="form-group row">
						    <label for="idPers" class="col-sm-2 col-form-label">identifiant</label>
						    <div class="col-sm-10">
						      <input type="text" readonly class="form-control-plaintext" id="idPers" value="<%=idPers%>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="nomPers" class="col-sm-2 col-form-label">nom</label>
						    <div class="col-sm-10">
						      <input type="text" readonly class="form-control-plaintext" id="nomPers" value="<%=personne.getNom()%>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="prenom" class="col-sm-2 col-form-label">prenom</label>
						    <div class="col-sm-10">
						      <input type="text" readonly class="form-control-plaintext" id="prenomPers" value="<%=personne.getPrenom()%>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="naissance" class="col-sm-2 col-form-label">date de naissance</label>
						    <div class="col-sm-10">
						      <input type="text" readonly class="form-control-plaintext" id="naissancePers" value="<%=naissanceP%>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="metier" class="col-sm-2 col-form-label">metier</label>
						    <div class="col-sm-10">
						      <input type="text" readonly class="form-control-plaintext" id="metierPers" value="<%=personne.getMetier()==null?"":personne.getMetier().getNom()%>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="adresse" class="col-sm-2 col-form-label">adresse</label>
						    <div class="col-sm-10">
						      <input type="text" readonly class="form-control-plaintext" id="nomPers" value="<%=personne.getAdresse()%>">
						    </div>
						  </div>
						</form>
						
				<a class="btn btn-secondary" href="list.do" role="button">retour vers la liste</a>
			</div>
		</div>
	</div>

<script src="jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>
</html>