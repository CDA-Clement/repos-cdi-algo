<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <%@page import="com.afpa.cda.dto.ReponseDto"%>
    <%@page import="com.afpa.cda.dto.ReponseStatut"%>
    <%@page import="com.afpa.cda.dto.ReponseDto"%>
    <%@page import="com.afpa.cda.service.PersonneServiceImpl"%>
    <%@page import="com.afpa.cda.service.IPersonneService"%>
    <%@page import="com.afpa.cda.dto.PersonneDto"%>
    
    
    <% String origine=(String)request.getAttribute("origine");
PersonneDto personne=(PersonneDto)request.getAttribute("personne");
List<PersonneDto> liste=(List<PersonneDto>)request.getAttribute("liste");
int page1=0;
String pageStr=(String)request.getAttribute("pageStr");
ReponseDto reponse=(ReponseDto)request.getAttribute("reponse");

if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {

}else {
	page1 = Integer.parseInt(pageStr);
}
%>
    
    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Liste des personnes</title>
</head>
<body>

		<% if(reponse != null) {
			String typeMsg= reponse.getStatus()==ReponseStatut.OK?"primary":"danger";
			%>
			
			<div class="alert alert-"<%=typeMsg%>" alert-dismissible fade show" role="alert">
					<strong>"+<%=reponse.getMsg()%>+"</strong>
					</div>
			<%}%>
			
		<div class="container my-4"> 
		<div class="row justify-content-md-center">
		<div class="col col-lg-8">

		<table class="table table-hover">
		<thead class="thead-dark">
		<tr>
		<th>id</th>
		<th>nom</th>
		<th>prenom</th>
		<th>terminator</th>
		<th></th>
		</tr>
		</thead>
		<tbody>
		
		<% for(PersonneDto p : liste){
	System.out.println(p.getId());	
	String idStr=Integer.toString(p.getId());%>
			
			<tr onclick>
			<td><a style="display:block;width:100%;height:100%;" href="show.do?id=<%=idStr%>"><%=idStr%></a></td>
			<td><a style="display:block;width:100%;height:100%;" href="show.do?id=<%=idStr%>"><%=p.getNom()%></a></td>
			<td><a style="display:block;width:100%;height:100%;" href="show.do?id=<%=idStr%>"><%=p.getPrenom()%></a></td>
			<td><i class ="fas fa-trash-alt" type="button" data-toggle="modal" data-target="#exampleModal"></i><td>
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Achtung!!!!</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        Veux-tu tuer ce pauvre Bernie?
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        <a href="delete.do?id=<%=idStr%>"type="button" class="btn btn-primary">Valider la mort</a>
					     </div>
					    </div>
					  </div>
					</div>
			</tr>
			</div>
			</div>
			</div>
			</tr>
			<%}; %>
			
		</table>

		<% if(page1>0) { %>

			<a href="list.do?page= <%=page1-1%>"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		<% } %>
		<% if(PersonneServiceImpl.fin == 5) { %>
			<a href="list.do?page=<%=page1+1%>"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>

		<%} %>
		<br>
		<br>
		<a href="add.do" type="button" class="btn btn-success btn-lg btn-block">Ajouter une Personne</button>
		<br>
		<a href="index.html" type="button" class="btn btn-info btn-lg btn-block">Revenir a l'accueil</button>
	
<script src="jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>
</html>