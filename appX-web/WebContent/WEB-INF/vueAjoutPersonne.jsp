<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="java.util.List"%>
<%
	MetierDto metier = (MetierDto) request.getAttribute("metier");
%>
<%
	List<MetierDto> liste = (List<MetierDto>) request.getAttribute("liste");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Ajouter Personne</title>
</head>
<body>

	<div class="container my-4">
		<h1>Ajouter un nouveau Bernie</h1>

		<form method="post">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputNom">Nom</label> <input type="text"
						class="form-control" name="nom" placeholder="nom">
				</div>
				<div class="form-group col-md-6">
					<label for="inputPrenom">Prenom</label> <input type="text"
						class="form-control" name="prenom" placeholder="prenom">
				</div>
				<div class="form-group col-md-6">
					<label for="inputDOB">Date de Naissance</label> <input type="date"
						class="form-control" name="naissance" placeholder="00/00/00">
				</div>
				<div class="form-group">
					<label for="inputMetier">Metier</label> <select
						class="form-control" name="metier">
						<%
							for (MetierDto p : liste) {
						%>
						<option><%=p.getNom()%></option>
						<%
							}
						%>

					</select>
				</div>
				<div class="form-group col-md-8">
					<label for="inputAdresse">Adresse</label> <input type="text"
						class="form-control" name="adresse"
						placeholder="Num rue ville code Postal">
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Ajouter</button>
		</form>
	</div>

	<script src="jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>