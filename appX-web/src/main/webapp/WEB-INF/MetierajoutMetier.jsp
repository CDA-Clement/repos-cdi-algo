<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.service.MetierServiceImpl"%>
<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="com.afpa.cda.dto.PersonneDto"%>
<%@page import="com.afpa.cda.dto.ReponseStatut"%>
<%@page import="com.afpa.cda.dto.ReponseDto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c" %>



<!--<c:set var = "creer" value='${requestScope["iscreated"]}' scope="session"/>  pour lui donner un autre nom dans cette jsp ou autre scope  -->
<!--<c:out value = "${creer}"/><br>-->
<!--CREER:<c:out value='${requestScope["boolean"]}'/><br>-->


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@include file="header.jsp" %>

<div class="container my-4">
<h1>Ajouter un nouveau Metier</h1>
<form method="post" action="metier.do">
<div class="form-row">
<div class="form-group col-md-6">
<label for="inputNom">Nom</label>
<input type="text" class="form-control" name="nom" placeholder="nom">
</div>
</div>
<button type="submit" class="btn btn-primary">Ajouter</button>
</form>
<br>
<form action="listMetier.do">
<button type="submit" class="btn btn-primary">Retour a la liste</button>
</form>
</div>

<c:if test="${iscreated==false}">
<script>
alert("Ce metier existe deja!");
</script>
</c:if>
<c:if test="${ iscreated }">
<div class="d-flex justify-content-center align-items-center" style="height:100px;">
    <div class="spinner-border text-success" role="status"></div>
</div>
<script>
function myFunction() {
	setTimeout(function(){ alert("success"); document.location='listMetier.do'; }, 2000);
}
myFunction();
</script>

</c:if>



<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="js/scriptMetier.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
<%@include file="footer.jsp" %>
</body>
</html>