
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c" %>
	


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Liste des personnes</title>
</head>
<body>
<%@include file="header.jsp" %>


<c:if test="${not empty reponse}">
<div class="alert alert-${reponse.status.name()=='OK'?'primary':'danger'}"alert-dismissible fade show role="alert">
<strong>${reponse.msg}</strong>
</div>
</c:if>
		<div class="container my-4">
		<div class="row justify-content-md-center">
		<div class="col col-lg-8">
		<table class="table table-hover">
		<thead class="thead-dark">
		<tr>
		<th>id</th>
		<th>nom</th>
		<c:if test="${sessionScope.idSession != null}">
		<th>terminator</th>
		</c:if>
		<th></th>
		</tr>
		</thead>
		<tbody>
			<c:forEach items="${liste}" var="p">
			<c:set var = "idStr" value = "${Integer.toString(p.getId())}" />
			<tr id="${idStr}">
				<td><a style="display:block;width:100%;height:100%;" href="showMetier.do?id=${idStr}">${idStr}</a></td>
				<td><a style="display:block;width:100%;height:100%;" href="showMetier.do?id=${idStr}">${p.getNom()}</a></td>
				<c:if test="${sessionScope.idSession != null}">
				<td><i class ="fas fa-trash-alt" type="button" data-toggle="modal" data-target="#exampleModal${idStr}"></i></td>
				<div class="modal fade" id="exampleModal${idStr}" tabindex="-1" role="dialog"  aria-hidden="true">
					<div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Achtung!!!!</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        Veux-tu tuer ce pauvre Metier?
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        <input type="hidden" value="${idStr}" id="identifiant">
					        <a href="#" type="button"  class="validation-suppr btn btn-primary">Valider la mort</a>
					     </div>
					    </div>
					  </div>
					</div>
					</c:if>
			</tr>
	</c:forEach>
		</tbody>
		</table>
			</div>
			</div>
			</div>
	<div class="row center-block" style="text-align: center; display: block; margin : auto;">
	<c:set var = "page1" value = "${pageStr}" />
	<c:out value="page ${page1+1}"></c:out>
	<c:if test="${page1>0}">
			<a href="list.do?page=${page1-1}"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
			</c:if>
	<c:if test="${fin==5}">
			<a href="list.do?page=${page1+1}"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
			</c:if>
		</div>
		<br/>
		<div class="row center-block" style="text-align: center; display: block; margin : auto;">
		<c:if test="${sessionScope.idSession != null}">
				<a class="btn btn-success" href="metier.do" role="button">Ajouter Metier</a>
		</c:if>
				<a class="btn btn-warning" href="index.html" role="button">Revenir a l'accueil</a>
		</div>
		
		
		
<%@include file="footer.jsp" %>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="js/scriptMetier.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>
</html>