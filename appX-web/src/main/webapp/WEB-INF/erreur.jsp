<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Pas de chance</title>
</head>
<body>
<%@include file="header.jsp" %>

	<h1>OUPS</h1>
	<%=exception.getMessage()%>
<%@include file="footer.jsp" %>
</body>
</html>