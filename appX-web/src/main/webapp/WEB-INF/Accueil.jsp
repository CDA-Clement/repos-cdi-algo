<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="com.afpa.cda.dto.PersonneDto"%>

<%@page import="com.afpa.cda.dto.ReponseStatut"%>
<%@page import="com.afpa.cda.dto.ReponseDto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
<% String origine=(String)request.getAttribute("origine");

System.out.println("origine "+origine);

%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>
<%@include file="header.jsp" %>


<div class="container">
<div class="row center-block" style="text-align: center; display: block; margin : auto;">
<h1>Bienvenue à l'Accueil ${sessionScope.idSession.login}</h1>

<br/>
<br/>
</div>
  <div class="row">
    <div class="col">
      <img src="personne.jpg" style="width : 400px; height: 500px">
      <a class="btn btn-success" href="list.do" role="button">Personne</a>
    </div>
    <div class="col">
     <img src="metier2.jpg" style="width : 400px; height: 500px">
     <a class="btn btn-warning" href="listMetier.do" role="button">Metier</a>
    </div>
  </div>
  </div>

<script src="jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
<%@include file="footer.jsp" %>
</body>
</html>