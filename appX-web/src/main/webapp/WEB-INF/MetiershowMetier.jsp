<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.service.MetierServiceImpl"%>
<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="com.afpa.cda.dto.PersonneDto"%>
<%@page import="com.afpa.cda.dto.ReponseStatut"%>
<%@page import="com.afpa.cda.dto.ReponseDto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@include file="header.jsp" %>


	
<div class="container my-4">
<div class="row justify-content-md-center">
<div class="col card col-lg-8">
<div class="form-group row">
<label for="idPers" class="col-sm-2 col-form-label">identifiant</label>
<div class="col-sm-10">
<input type="text" readonly class="form-control-plaintext" id="idPers" value="${metier.id}">
</div>
</div>
<div class="form-group row">
<label for="nomPers" class="col-sm-2 col-form-label">nom</label>
<div class="col-sm-10">
<input type="text" readonly class="form-control-plaintext" id="nomPers" value="${metier.nom}">
</div>
</div>
</div>
<a class="btn btn-secondary" href="listMetier.do" role="button">retour vers la liste</a>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="js/scriptMetier.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
<%@include file="footer.jsp" %>
</body>
</html>


