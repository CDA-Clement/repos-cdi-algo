<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Ajouter Personne</title>
</head>
<body>
<%@include file="header.jsp" %>
	<div class="spinner-border text-danger" role="status">
				  <span class="sr-only">Loading...</span>
				</div>
		<script>
				function myFunction() {
					setTimeout(function(){ document.location='list.do'; }, 2000);
				}
				myFunction();
		</script>
<%@include file="footer.jsp" %>
</body>
</html>