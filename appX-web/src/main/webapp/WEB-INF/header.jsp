    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header>
	<div class="pos-f-t">
		<div class="collapse" id="navbarToggleExternalContent">
			<div class="bg-dark p-4">
				<h5 class="text-white h4">Menu Principal</h5>
				<span class="text-muted">(Exemple flagrant d'include pas inspir�)</span>
			
				<div class="row">
					<a href="index.html">Accueil</a>
    			</div>
    			<div class="row">
					<a href="listMetier.do">Metier</a>
    			</div>
    			<div class="row">
					<a href="list.do">Personne</a>
				</div>
				
    			<c:if test="${sessionScope.idSession == null}">
    			<div class="row">
					<a href="connexion.do">Connexion</a>
				</div>
    			<div class="row">
					<a href="inscription.do">Inscription</a>
				</div>
				</c:if>
				<c:if test="${sessionScope.idSession != null}">
    			<div class="row">
					<a href="deconnexion.do">Deconnexion</a>
				</div>
				</c:if>
			</div>
		</div>
		
		<nav class="navbar navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarToggleExternalContent"
				aria-controls="navbarToggleExternalContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>
</header>