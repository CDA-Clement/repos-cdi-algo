$(document).ready(function() {
	$(".validation-suppr").on('click',function(event) {
		event.stopPropagation();
		var idToDelete = $(this).parent().find("input[id='identifiant']")[0].value;
		var toHide="#exampleModal"+idToDelete;
		console.log(idToDelete);
		console.log(toHide);
		$.post("deleteAjaxMetier.do", {
			id : idToDelete,
		}).done(function(data, status) {
			console.log("ça marche");
			// fermer la popupboo
			$('#exampleModal'+idToDelete).modal('hide');
			// supprimer la ligne
			$("tr[id='"+idToDelete+"']").hide();
		}).fail(function(data, status) {
			console.log("ça marche pas");
			// fermer la popupboo
			$('#exampleModal'+idToDelete+'.modal.fade').modal('hide');
		});
		return false;
	});
});