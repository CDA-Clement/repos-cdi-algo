package com.afpa.cda.servletMetier;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.service.IMetierService;


@WebServlet(urlPatterns = { "/metier.do" })
public class AddServletMetier extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierService metierService;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		metierService = ctx.getBean(IMetierService.class);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		request.setAttribute("origine",getServletName());
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/MetierajoutMetier.jsp").forward(request, response);
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		String nom = request.getParameter("nom");
		Boolean creer=metierService.ajouterMetier(nom);
		request.setAttribute("iscreated", creer);
		request.setAttribute("origine",getServletName());
		this.getServletContext().getRequestDispatcher("/WEB-INF/MetierajoutMetier.jsp").forward(request, response);
		}
	}
}