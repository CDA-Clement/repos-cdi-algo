package com.afpa.cda.servletMetier;
import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IMetierService;
import com.afpa.cda.tools.Utils;
@WebServlet(urlPatterns = { "/showMetier.do" })
public class ShowServletMetier extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierService metierService;
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		metierService = ctx.getBean(IMetierService.class);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idParam = request.getParameter("id");
		if (idParam == null || idParam.length() == 0 ) {
			Utils.redirect(
					request,
					response,
					getServletContext(),
					"/list.do",
					ReponseStatut.KO,
					"le parametre id est obligatoire");
			return;
		} else {
			if(!idParam.matches("^\\p{Digit}+$")) {
				Utils.redirect(
						request,
						response,
						getServletContext(),
						"/list.do",
						ReponseStatut.KO,
						"le parametre id doit etre un numero");
				return;
			}
			int id = Integer.parseInt(idParam);
			Optional<MetierDto> res = this.metierService.findById(id);
			if(res.isPresent()) {
				MetierDto metier = res.get();
				
				request.setAttribute("metier", metier);
				request.setAttribute("origine",getServletName());
				this.getServletContext().getRequestDispatcher("/WEB-INF/MetiershowMetier.jsp").forward(request, response);
				
				
			} else {
				Utils.redirect(
						request,
						response,
						getServletContext(),
						"/listMetier.do",
						ReponseStatut.KO,
						"aucune metier n'a cet id "+id);
				return;
			}
		}
	}
}
