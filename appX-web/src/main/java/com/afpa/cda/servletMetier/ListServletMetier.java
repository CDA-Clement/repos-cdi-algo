package com.afpa.cda.servletMetier;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.service.IMetierService;
import com.afpa.cda.service.MetierServiceImpl;
@WebServlet(urlPatterns = {"/listMetier.do"})
public class ListServletMetier extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierService metierService;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		metierService = ctx.getBean(IMetierService.class);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
		String pageStr= request.getParameter("page");
		System.out.println("page "+pageStr);
		int page = 0;
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr="0";
		}else {
			page = Integer.parseInt(pageStr);
		}
		ReponseDto reponse = (ReponseDto)request.getAttribute("reponse");
		request.setAttribute("origine", getServletName());
		request.setAttribute("page", page);
		request.setAttribute("pageStr", pageStr);
		request.setAttribute("reponse", reponse);
		request.setAttribute("liste", this.metierService.chercherToutesLesMetiers(page));
		request.setAttribute("fin",MetierServiceImpl.fin );
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/MetiervueMetier.jsp").forward(request, response);
	}
	
}
