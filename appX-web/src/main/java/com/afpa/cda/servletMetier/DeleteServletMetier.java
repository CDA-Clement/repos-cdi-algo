package com.afpa.cda.servletMetier;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IMetierService;
import com.afpa.cda.tools.Utils;

@WebServlet(urlPatterns = { "/deleteMetier.do" })
public class DeleteServletMetier extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IMetierService metierService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		metierService = ctx.getBean(IMetierService.class);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		String idParam = request.getParameter("id");
		String msg = "";
		ReponseStatut statut = ReponseStatut.OK;
		if (idParam == null || idParam.length() == 0) {
			statut = ReponseStatut.KO;
			msg = "le parametre id est obligatoire";
		} else {
			if (!idParam.matches("^\\p{Digit}+$")) {
				Utils.redirect(request, response, getServletContext(), "/listMetier.do", ReponseStatut.KO,
						"le parametre id doit etre un numero");
				return;
			}
			int id = Integer.parseInt(idParam);
			if (this.metierService.deleteById(id)) {
				msg = "suppression reussie avec succes";
			} else {
				statut = ReponseStatut.KO;
				msg = "aucune metier n'a cet id " + id;
			}
		}
		request.setAttribute("reponse", ReponseDto.builder().status(statut).msg(msg).build());
		this.getServletContext().getRequestDispatcher("/listMetier.do").forward(request, response);
		}

	}
}