package com.afpa.cda.servletInscription;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.UtilisateurDto;
import com.afpa.cda.service.IUtilisateurService;

@WebServlet(urlPatterns = { "/inscription.do" })
public class InscriptionServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
     private IUtilisateurService utilisateurService;
	
  
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		utilisateurService = ctx.getBean(IUtilisateurService.class);
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") != null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {			
			this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);	
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") != null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		this.utilisateurService.add(
				UtilisateurDto.builder()
				.login(username)
				.password(password)
				.build());
		
		this.getServletContext().getRequestDispatcher("/connexion.do").forward(request, response);	
		}
	}

}
