package com.afpa.cda.sevletConnexion;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.UtilisateurDto;
import com.afpa.cda.service.IUtilisateurService;

@WebServlet("/connexion.do")
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IUtilisateurService utilisateurService;
	
	
	

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		utilisateurService = ctx.getBean(IUtilisateurService.class);
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") != null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") != null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		try {
			List<UtilisateurDto> laListe =utilisateurService.chercherToutLeMonde();
			if (!laListe.isEmpty()) {
				
				for (UtilisateurDto u : laListe) {
					if (u.getLogin().equals(username) && u.getPassword().equals(password)) {
						session.setAttribute("idSession", u);
						System.out.println("GG");
						break;
					}else {
						System.out.println("c'est pas GG");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		
	}
	}
}
