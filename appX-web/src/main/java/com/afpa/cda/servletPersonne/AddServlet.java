package com.afpa.cda.servletPersonne;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.service.IMetierService;
import com.afpa.cda.service.IPersonneService;

@WebServlet(urlPatterns = { "/add.do" })
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IPersonneService personneService;
	private IMetierService metierService;
	
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
		metierService = ctx.getBean(IMetierService.class);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		int page = 0;
		String pageStr= request.getParameter("page");
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {

		}else {
			page = Integer.parseInt(pageStr);
		}
		ReponseDto reponse = (ReponseDto)request.getAttribute("reponse");
		request.setAttribute("origine", getServletName());
		request.setAttribute("page", page);
		request.setAttribute("pageStr", pageStr);
		request.setAttribute("reponse", reponse);
		request.setAttribute("liste", this.metierService.chercherToutesLesMetiers(page));
		this.getServletContext().getRequestDispatcher("/WEB-INF/vueAjoutPersonne.jsp").forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		}else {	
		int page = 0;
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String adresse = request.getParameter("adresse");
		String strnaissance = request.getParameter("naissance");
		String metier2 = request.getParameter("metier");
	
		System.err.println(metier2);
		Date naissance = new Date();
		try {
			naissance = new SimpleDateFormat("yyyy-MM-dd").parse(strnaissance);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	
		List<MetierDto> allmetier = this.metierService.chercherToutesLesMetiers(page);
		
		Integer idMet = null;
		for (MetierDto metierDto : allmetier) {
			if (metierDto.getNom().equals(metier2)) {
				idMet =metierDto.getId();
			}
		}
		
		this.personneService.ajouter(
				PersonneDto.builder()
				.nom(nom)
				.prenom(prenom)
				.adresse(adresse)
				.dateNaissance(naissance)
				.metier(MetierDto.builder().id(idMet).build())
				.build());
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/vueAjoutPersonne2.jsp").forward(request, response);
		}
	}
}