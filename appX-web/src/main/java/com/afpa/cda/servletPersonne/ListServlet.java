package com.afpa.cda.servletPersonne;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.service.IPersonneService;
import com.afpa.cda.service.PersonneServiceImpl;
@WebServlet(urlPatterns = {"/list.do"})
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IPersonneService personneService;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pageStr= request.getParameter("page");
		System.out.println("page "+pageStr);
		int page = 0;
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr = "0";
		}else {
			page = Integer.parseInt(pageStr);
		}
		ReponseDto reponse = (ReponseDto)request.getAttribute("reponse");
		request.setAttribute("origine", getServletName());
		request.setAttribute("page", page);
		request.setAttribute("pageStr", pageStr);
		request.setAttribute("reponse", reponse);
		request.setAttribute("liste", this.personneService.chercherToutesLesPersonnes(page));
		request.setAttribute("fin", PersonneServiceImpl.fin );
		this.getServletContext().getRequestDispatcher("/WEB-INF/vuePersonne.jsp").forward(request, response);
	}

}