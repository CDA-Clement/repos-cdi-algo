package com.afpa.cda.servletPersonne;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IPersonneService;

@WebServlet(urlPatterns = { "/deleteAjax.do" })
public class DeleteAjaxServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("idSession") == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		} else {
			String idParam = request.getParameter("id");

			String msg = "";
			ReponseStatut statut = ReponseStatut.OK;

			if (idParam == null || idParam.length() == 0) {
				statut = ReponseStatut.KO;
				msg = "le parametre id est obligatoire";
			} else {
				if (!idParam.matches("^\\p{Digit}+$")) {
					statut = ReponseStatut.KO;
					msg = "le parametre id doit etre un numero";
				} else {
					int id = Integer.parseInt(idParam);

					if (this.personneService.deleteById(id)) {
						msg = "suppression reussie avec succes";
					} else {
						statut = ReponseStatut.KO;
						msg = "aucune personne n'a cet id " + id;
					}
				}
			}

			PrintWriter writer = response.getWriter();
			writer.append(statut.name());
		}
	}
}
