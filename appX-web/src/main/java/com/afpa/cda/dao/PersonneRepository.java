package com.afpa.cda.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Personne;

@Repository
public interface PersonneRepository extends PagingAndSortingRepository<Personne, Integer> {
	
	@Query("select p from Personne p")
	public Page<Personne> findAll(Pageable pageable);
}

