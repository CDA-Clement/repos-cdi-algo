package com.afpa.cda.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Utilisateur;

@Repository
public interface UtilisateurRepository extends PagingAndSortingRepository<Utilisateur, Integer> {

	@Query("select u from Utilisateur u")
	public Page<Utilisateur> findAll(Pageable pageable);
}
