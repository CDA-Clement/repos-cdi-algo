package com.afpa.cda.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Metier;

@Repository
public interface MetierRepository extends PagingAndSortingRepository<Metier, Integer> {

	@Query("select p from Metier p")
	public Page<Metier> findAll(Pageable pageable);

	
}