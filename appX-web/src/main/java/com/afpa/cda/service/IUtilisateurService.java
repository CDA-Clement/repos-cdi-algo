package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.UtilisateurDto;

public interface IUtilisateurService {

	public List<UtilisateurDto> chercherToutLeMonde(int page);

	public boolean deleteById(int id);

	public Optional<UtilisateurDto> findById(int id);

	Integer add(UtilisateurDto user);

}