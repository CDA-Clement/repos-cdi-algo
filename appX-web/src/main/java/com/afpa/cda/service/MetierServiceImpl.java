package com.afpa.cda.service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import com.afpa.cda.dao.MetierRepository;
import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.entity.Metier;

@Service
public class MetierServiceImpl implements IMetierService {
	static int INC = 5;
	public static int fin;
	@Autowired
	private MetierRepository metierRepository;
	@Override
	public List<MetierDto> chercherToutesLesMetiers(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<MetierDto> maliste = this.metierRepository.findAll(firstPageWithTwoElements).stream()
				.map(e -> MetierDto.builder().id(e.getId()).nom(e.getNom()).build()).collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	@Override
	public boolean deleteById(int id) {
		if (this.metierRepository.existsById(id)) {
			this.metierRepository.deleteById(id);
			return true;
		}
		return false;
	}
	@Override
	public Optional<MetierDto> findById(int id) {
		Optional<Metier> pers = this.metierRepository.findById(id);
		Optional<MetierDto> res = Optional.empty();
		if (pers.isPresent()) {
			Metier p = pers.get();
			res = Optional.of(MetierDto.builder().id(p.getId()).nom(p.getNom()).build());
		}
		return res;
	}
	@Override
	public Boolean ajouterMetier(String nom) {
		Boolean creer = true;
		Iterable<Metier> metiers = this.metierRepository.findAll();
		for (Metier metier : metiers) {
			if (metier.getNom().equalsIgnoreCase(nom)) {
				creer = false;
				break;
			}
		}
		if (creer == true) {
			Metier metier1 = Metier.builder().nom(nom).build();
			metierRepository.save(metier1);
		}
		return creer;
	}
	@Autowired
	private ModelMapper modelMapper;
}