package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.UtilisateurRepository;
import com.afpa.cda.dto.UtilisateurDto;
import com.afpa.cda.entity.Utilisateur;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService{
	static int INC = 5;
	public static int fin;
	@Autowired
	private UtilisateurRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<UtilisateurDto> chercherToutLeMonde(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		return this.userRepository
				.findAll()
				.stream()
				.map(u->this.modelMapper.map(u, UtilisateurDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public boolean deleteById(int id) {
			
		if (this.userRepository.existsById(id)) {
			this.userRepository.deleteById(id);
		}
		return false;
	}

	@Override
	public Optional<UtilisateurDto> findById(int id) {
		Optional<Utilisateur> user = this.userRepository.findById(id);
		Optional<UtilisateurDto> res = Optional.empty();
		if(user.isPresent()) {
			Utilisateur u = user.get();
			UtilisateurDto userDto = this.modelMapper.map(u, UtilisateurDto.class);
			res = Optional.of(userDto);
		}
		return res;
	}

	@Override
	public Integer add(UtilisateurDto user) {
		Utilisateur u = this.modelMapper.map(user,Utilisateur.class);
		u = this.userRepository.save(u);
		return u.getId();
	}
}
