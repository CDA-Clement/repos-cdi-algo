package com.expenseManager.ExpenseManagerAPI.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Expense {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXPENSE_SEQ")
	Integer id;
	
	String description;
	
	Integer amount;
	
	String month;
	
	int year;
	

}
