package org.eclipse.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.eclipse.main.Calcul;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Test;
class CalculTest {
	static Calcul calcul;
	@BeforeAll
	static void setUpBeforeClass() throws Exception { 
		calcul = new Calcul();
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception { }
	
	@BeforeEach
	void setUp() throws Exception { }
	
	@AfterEach
	void tearDown() throws Exception {
		
	}
	
	
	@Test
	void testSomme() {
		assertTrue("c'est bon", calcul.somme(2,3)==5);
		
		if(calcul.somme(2,3)!=5)
			fail("faux pour deux entiers positifs");
			if(calcul.somme(-2,-3)!=-5)
			fail("faux pour deux entiers negatifs");
			if(calcul.somme(-2,3)!=1)
			fail("faux pour deux entiers de signe different");
			if(calcul.somme(0,3)!=3)
			fail("faux pour x nul");
			if(calcul.somme(2,0)!=2)
			fail("faux pour y nul");
			if(calcul.somme(0,0)!=0)
			fail("faux pour x et y nuls");
	}
	
	
	@Test
	void testDivision() {
		if(calcul.division(5, 5)!=1) 
		fail("faux ca doit etre egal a 1");
		
		
		try {
			calcul.division(5, 0);
		}catch(Exception e) {
			System.out.println("arythmetic exception");
		}
		
	}
}
