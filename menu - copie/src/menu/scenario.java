package menu;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class scenario {

	public static void main(String[] args) throws Exception {
		String fin = "13";
		boolean kontinue = false;
			Scanner sc = new Scanner(System.in);
		//Scanner sc = new Scanner(new File("C:\\git-repos\\repos-cdi-algo\\menu\\scenarii\\scenario1.txt"));
	//	Scanner sc = new Scanner(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/menu/scenarii/scenario1.txt"));

		String n="0";
		Scolarite school = new Scolarite ();

		while(kontinue==false) {
			System.out.println("********************************************************************************************");
			System.out.println("1 - ajouter une Promotion ");
			System.out.println("2 - supprimer une Promotion ");
			System.out.println("3 - afficher les Promotions ");
			System.out.println("4 - ajouter un Etudiant à une promotion ");
			System.out.println("5 - supprimer un Etudiant ");
			System.out.println("6 - rechercher un Etudiant par son nom ");
			System.out.println("7 - rechercher un Etudiant avec son id "); 
			System.out.println("8 - rechercher la promotion d'un Etudiant ");
			System.out.println("9 - lister les Etudiants d'une Promotion ");
			System.out.println("10 - ajouter des notes à un Etudiant ");
			System.out.println("11 - afficher la moyenne de l'étudiant ");
			System.out.println("12 - afficher la moyenne de la promotion ");
			System.out.println("13 - Terminer ");
			System.out.println("********************************************************************************************");
			System.out.println("Veuillez entrer un numero entre 1 et 5 compris");
			String n1 = sc.nextLine();
			System.err.println(n1);


			if(n1.equalsIgnoreCase("1")) {
				System.out.println("veuillez entrer le nom de la promotion a ajouter");
				String nom = sc.nextLine();
				System.err.println(nom);
				school.ajouterPromotionParnom(nom);

			}


			if (n1.equalsIgnoreCase("2")) {
				System.out.println("veuillez entrer le nom de la promotion a supprimer");
				String nom = sc.nextLine();
				System.err.println(nom);
				school.supprimerPromotionParnom(nom);
			}


			if(n1.equalsIgnoreCase("3")) {
				school.afficherScolarite();
			}


			if (n1.equalsIgnoreCase("4")) {

				System.out.println("veuillez entrer le nom du stagiaire a ajouter");
				String nomEtudiant = sc.nextLine();
				System.err.println(nomEtudiant);
				System.out.println("veuillez entrer le prenom du stagiaire a ajouter");
				String prenomEtudiant = sc.nextLine();
				System.err.println(prenomEtudiant);
				System.out.println("veuillez entrer le nom de la promotion");
				String nomDeLaPromotion = sc.nextLine();
				System.err.println(nomDeLaPromotion);
				school.ajouterUnEtudiant(nomEtudiant, prenomEtudiant, nomDeLaPromotion);


			}

			if(n1.equalsIgnoreCase("5")) {
				System.out.println("veuillez entrer le nom du stagiaire a ajouter");
				String nomEtudiant = sc.nextLine();
				System.err.println(nomEtudiant);
				System.out.println("veuillez entrer le prenom du stagiaire a ajouter");
				String prenomEtudiant = sc.nextLine();
				System.err.println(prenomEtudiant);
				System.out.println("veuillez entrer le nom de la promotion");
				String nomDeLaPromotion = sc.nextLine();
				System.err.println(nomDeLaPromotion);
				school.supprimerUnEtudiant(nomEtudiant, prenomEtudiant, nomDeLaPromotion);
			}
			if(n1.equalsIgnoreCase("6")) {
				System.out.println("veuillez entrer le nom du stagiaire a rechercher");
				String nomEtudiant = sc.nextLine();
				System.err.println(nomEtudiant);
				System.out.println("veuillez entrer le prenom du stagiaire a rechercher");
				String prenomEtudiant = sc.nextLine();
				System.err.println(prenomEtudiant);
				school.rechercherUnEtudiantParNom(nomEtudiant, prenomEtudiant);

			}

			if(n1.equalsIgnoreCase("7")) {
				System.out.println("veuillez entrer l'id du stagiaire a rechercher");
				int id = sc.nextInt();
				school.rechercherUnEtudiantParId(id);
			}

			if(n1.equalsIgnoreCase("8")) {
				System.out.println("veuillez entrer le nom du stagiaire a rechercher");
				String nomEtudiant = sc.nextLine();
				System.err.println(nomEtudiant);
				System.out.println("veuillez entrer le prenom du stagiaire a rechercher");
				String prenomEtudiant = sc.nextLine();
				System.err.println(prenomEtudiant);
				school.retrouverLaPromotionEtudiant(nomEtudiant, prenomEtudiant);
			}
			if (n1.equalsIgnoreCase("9")){
				System.out.println("veuillez entrer le nom du stagiaire a ajouter");
				String nomPromotion = sc.nextLine();
				System.err.println(nomPromotion);
				school.listerunePromotionEtudiantAlphabetique(nomPromotion);
			}

			if (n1.equalsIgnoreCase("10")){
				System.out.println("veuillez entrer le nom du stagiaire pour lequel vous souahiatez ajouter des notes");
				String nomEtudiant = sc.nextLine();
				System.err.println(nomEtudiant);
				System.out.println("veuillez entrer le prenom du stagiaire pour lequel vous souahiatez ajouter des notes");
				String prenomEtudiant = sc.nextLine();
				System.err.println(prenomEtudiant);
				System.out.println("veuillez entrer le nombre de notes que vous souhaitez ajouter");
				int nombreNote = sc.nextInt();
				System.err.println(nombreNote);
				for (int i = 0; i < nombreNote; i++) {
					System.out.println("veuillez entrer la note du stagiaire");
					int note = sc.nextInt();
					System.err.println(note);

					school.ajouterNotesEtudiant(nomEtudiant, prenomEtudiant, note);
				}
			}

			if (n1.equalsIgnoreCase("11")){
				System.out.println("veuillez entrer le nom du stagiaire pour lequel vous souahiatez afficher les notes et moyenne");
				String nomEtudiant = sc.nextLine();
				System.err.println(nomEtudiant);
				System.out.println("veuillez entrer le prenom du stagiaire pour lequel vous souahiatez afficher les notes et moyenne");
				String prenomEtudiant = sc.nextLine();
				System.err.println(nomEtudiant);
				school.afficherNote(nomEtudiant, prenomEtudiant);
				double moyEtudiant =school.afficherMoyenne(nomEtudiant,prenomEtudiant);
				System.out.println("Sa moyenne est de "+moyEtudiant);
			}

			if (n1.equalsIgnoreCase("12")) {
				System.out.println("veuillez entrer le nom de la promotion pour laquelle vous souhaitez afficher la moyenne");
				String nomPromo = sc.nextLine();
				System.err.println(nomPromo);
				school.afficherMoyennePromotion(nomPromo);
				System.out.println();
			}


			if (n1.equalsIgnoreCase("13")){
				Methodes.terminer();
				kontinue = true;
			}

		}

	}

}
