package menu;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
public class Scolarite {

	private LinkedList<Promotion> promotionList;
	public Scolarite() {
		this.promotionList = new LinkedList<Promotion>();
	}

	public void ajouterPromotionParnom(String nomPromotion) {
		boolean unePromotionAvecMemeNomTrouvee =false;
		for (Promotion p : promotionList) {
			if(p.getNom().equalsIgnoreCase(nomPromotion)) {
				System.out.println("ERREUR une promotion ayant le meme nom existe deja !!!!");
				unePromotionAvecMemeNomTrouvee = true;	
			}

		}
		if(!unePromotionAvecMemeNomTrouvee) {
			Promotion p = new Promotion(nomPromotion);
			this.promotionList.add(p);

		}
	}

	public void supprimerPromotionParnom(String nomPromotion) {
		boolean unePromotionAvecMemeNomTrouvee =false;
		for (Promotion p : promotionList) {
			unePromotionAvecMemeNomTrouvee =false;
			if(p.getNom().contains(nomPromotion)) {
				this.promotionList.remove(p);
				break;	
			}else {
				unePromotionAvecMemeNomTrouvee = true;
			}

		}
		if(unePromotionAvecMemeNomTrouvee) {
			System.out.println("ERREUR aucune promotion ayant ce nom existe !!!!");
		}

	}

	public void ajouterUnEtudiant(String nomEtudiant, String prenomEtudiant, String nomPromotion) {
		boolean unEtudiantAvecMemeNom =false;
		boolean EtudiantMemeNom = false;
		boolean MemePromotion = false;
		int i =0;
		for (Promotion p : promotionList) { 

			unEtudiantAvecMemeNom =false;
			EtudiantMemeNom = false;

			if (p.getNom().equalsIgnoreCase(nomPromotion)) {
				MemePromotion=true;

				if(p.getEtudiantList().size()>0 ) { 
					for (int j = 0; j < p.getEtudiantList().size(); j++) {
						//System.out.println(p.getEtudiantList().get(j).getnomEtudiant());
						//System.out.println(p.getEtudiantList().get(j).getPrenomEtudiant());

						//System.out.println(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant));
						if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {


							System.out.println("ERREUR un etudiant ayant comme nom  "+nomEtudiant+" et prenom "+prenomEtudiant+ " existe deja dans la liste "+ p.getNom());
							EtudiantMemeNom =true;
							break;
						}

					}
					if(EtudiantMemeNom ==false) {
						Etudiant e = new Etudiant(nomEtudiant,prenomEtudiant);
						p.getEtudiantList().add(e);	
						EtudiantMemeNom=true;
						break;
					}

				}
				if(EtudiantMemeNom==false&&i==0) {
					Etudiant e = new Etudiant(nomEtudiant,prenomEtudiant);
					p.getEtudiantList().add(e);
					EtudiantMemeNom =true;
					break;}
				else if (EtudiantMemeNom==false&&i>0 ){
					for (Promotion p1 : promotionList) { 

						unEtudiantAvecMemeNom =false;
						EtudiantMemeNom = false;



						if(p1.getEtudiantList().size()>0 ) { 
							for (int j = 0; j < p1.getEtudiantList().size(); j++) {
								//System.out.println(p1.getEtudiantList().get(j).getnomEtudiant());
								//System.out.println(p1.getEtudiantList().get(j).getPrenomEtudiant());

								//System.out.println(p1.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p1.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant));
								if(p1.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p1.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {


									System.out.println("ERREUR un etudiant ayant comme nom  "+nomEtudiant+" et prenom "+prenomEtudiant+ " existe deja dans la liste "+ p1.getNom());
									EtudiantMemeNom =true;
									break;
								} 

								if (EtudiantMemeNom==true) {
									break;
								}
							}if(EtudiantMemeNom==false) {
								Etudiant e = new Etudiant(nomEtudiant,prenomEtudiant);
								p.getEtudiantList().add(e);
								EtudiantMemeNom =true;
							}
						}

						if(MemePromotion==true) {
							break;
						}
					}
				}
			}
			i++;

		}
		if(MemePromotion == false) {
			System.out.print("ERREUR aucune Promotion s'intitulant "+nomPromotion+ " Veillez a creer d'abord une promotion comportant "+nomPromotion+" comme nom de promotion. Ou alors veuillez inscrire "+nomEtudiant+" "+prenomEtudiant);
			System.out.println("dans une autre promotion existante parmi\"+ promotionList");
			System.out.println("Scolarite : ");
			for (Promotion promotion : promotionList) {
				System.out.println(promotion);
			}
		}
	}


	public void supprimerUnEtudiant(String nomEtudiant, String prenomEtudiant, String nomPromotion) {
		boolean nompromotion = false;
		boolean etudiantSupprime = false;
		for (Promotion p : promotionList) {



			if (p.getNom().equalsIgnoreCase(nomPromotion)) {
				nompromotion =true;


				if(p.getEtudiantList().size()>0 ) {
					for (int j = 0; j < p.getEtudiantList().size(); j++) {
//						System.out.println(p.getEtudiantList().get(j).getnomEtudiant());
//						System.out.println(p.getEtudiantList().get(j).getPrenomEtudiant());
//
//						System.out.println(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant));
						if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {
							p.getEtudiantList().remove(j);
							etudiantSupprime=true;

							System.out.println("L'Etudiant  "+nomEtudiant+" "+prenomEtudiant+ " a bien ete supprime de la liste "+ p.getNom());
							break;
						}

					}
				}
				if (etudiantSupprime ==true) {
					break;
				}else {
					System.out.println("la liste "+p.getNom()+ " ne contient pas "+nomEtudiant+" "+prenomEtudiant);
				}
			}
		}
		if (nompromotion == false) {
			System.out.println("La session de l'etudiant a supprimer n'existe pas");
		}
	}



	public LinkedList<Promotion> getPromotionList() {
		return promotionList;
	}

	public void setPromotionList(LinkedList<Promotion> promotionList) {
		this.promotionList = promotionList;
	}

	public void afficherScolarite() {
		System.out.println("Scolarite : ");
		for (Promotion promotion : promotionList) {
			promotion.afficherPromotion();
		}
	}

	@Override
	public String toString() {
		return "Scolarite [promotionList=" + promotionList + "]";
	}

	public void rechercherUnEtudiantParNom(String nomEtudiant, String prenomEtudiant) {
		boolean nompromotion = false;
		boolean etudiantTrouve = false;
		for (Promotion p : promotionList) { 


			if(p.getEtudiantList().size()>0 ) {
				for (int j = 0; j < p.getEtudiantList().size(); j++) {
					//System.out.println(p.getEtudiantList().get(j).getnomEtudiant());
					//System.out.println(p.getEtudiantList().get(j).getPrenomEtudiant());

					//System.out.println(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant));
					if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {
						System.out.println(p.getEtudiantList().get(j).getnomEtudiant() +" "+ p.getEtudiantList().get(j).getPrenomEtudiant()+" " + p.getEtudiantList().get(j).getId());
						etudiantTrouve=true;
						break;
					}

				}
			}
			if (etudiantTrouve ==true) {
				break;
			}

			if (etudiantTrouve ==true) {
				break;
			}
		} 
		if (etudiantTrouve==false) {
			System.out.println(nomEtudiant+" "+prenomEtudiant+" n'existe pas");
		}



	}

	public void rechercherUnEtudiantParId(int id) {
		boolean nompromotion = false;
		boolean etudiantTrouve = false;
		for (Promotion p : promotionList) { 


			if(p.getEtudiantList().size()>0 ) {
				for (int j = 0; j < p.getEtudiantList().size(); j++) {
//					System.out.println(p.getEtudiantList().get(j).getnomEtudiant());
//					System.out.println(p.getEtudiantList().get(j).getPrenomEtudiant());
					if(p.getEtudiantList().get(j).getMonid()==(id)) {
						System.out.println(p.getEtudiantList().get(j).getnomEtudiant() +" "+ p.getEtudiantList().get(j).getPrenomEtudiant()+" " + p.getEtudiantList().get(j).getId());
						etudiantTrouve=true;
						break;
					}

				}
			}
			if (etudiantTrouve ==true) {
				break;
			}

			if (etudiantTrouve ==true) {
				break;
			}
		} 
		if (etudiantTrouve==false) {
			System.out.println("L'etudiant portant le numero d'identification "+id+" n'existe pas");
		}

	}

	public void retrouverLaPromotionEtudiant(String nomEtudiant, String prenomEtudiant) {
		boolean nompromotion = false;
		boolean etudiantTrouve = false;
		for (Promotion p : promotionList) { 


			if(p.getEtudiantList().size()>0 ) {
				for (int j = 0; j < p.getEtudiantList().size(); j++) {
//					System.out.println(p.getEtudiantList().get(j).getnomEtudiant());
//					System.out.println(p.getEtudiantList().get(j).getPrenomEtudiant());

//					System.out.println(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant));
					if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {
						System.out.println("le nom de la promotion est "+p.getNom()+" le numero d'identification de la promotion est "+ p.getIdpromotion());
						etudiantTrouve=true;
						break;
					}

				}
			}
			if (etudiantTrouve ==true) {
				break;
			}

			if (etudiantTrouve ==true) {
				break;
			}
		} 
		if (etudiantTrouve==false) {
			System.out.println(nomEtudiant+" "+prenomEtudiant+" n'existe pas");
		}
	}

	public void listerunePromotionEtudiantAlphabetique(String nomPromotion) {
		boolean nompromotion = false;
		boolean etudiantTrouve = false;
		for (Promotion p : promotionList) {
			ArrayList<Etudiant> listEtudiantOrdre= new ArrayList<Etudiant>();
			

			if(p.getEtudiantList().size()>0 ) {
//				System.out.println(p.getNom().equalsIgnoreCase(nomPromotion));
				if(p.getNom().equalsIgnoreCase(nomPromotion)) {
					for (Etudiant etudiant : p.getEtudiantList()) {
						listEtudiantOrdre.add(etudiant);
					}	

					triAscendant(listEtudiantOrdre);
					System.out.println("Les Etudiants de la promotion "+nomPromotion+ " sont");
					for (int j = 0; j < listEtudiantOrdre.size(); j++) {
						System.out.println(listEtudiantOrdre.get(j));

					}
				}

				etudiantTrouve=true;
				break;
			}
		}
		if (etudiantTrouve==false) {
			System.out.println(nompromotion+" n'existe pas");
		}
	}

	public void triAscendant( ArrayList<Etudiant> listEtudiantOrdre) {

		for (int i = 0; i < listEtudiantOrdre.size(); i++) {
			for (int j = i+1; j < listEtudiantOrdre.size(); j++) {
				if (listEtudiantOrdre.get(i).comparer(listEtudiantOrdre.get(j)) > 0) {
					Etudiant temp = listEtudiantOrdre.get(i);
					Etudiant temp1 = listEtudiantOrdre.get(j);
					listEtudiantOrdre.set(j, temp);
					listEtudiantOrdre.set(i,temp1);
				}

			}

		}

	}

	public void ajouterNotesEtudiant(String nomEtudiant, String prenomEtudiant, int note) {
		boolean nompromotion = false;
		boolean etudiantTrouve = false;
		for (Promotion p : promotionList) { //premiere promotion cd


			if(p.getEtudiantList().size()>0 ) {
				for (int j = 0; j < p.getEtudiantList().size(); j++) {
//					System.out.println(p.getEtudiantList().get(j).getnomEtudiant());
//					System.out.println(p.getEtudiantList().get(j).getPrenomEtudiant());
//
//					System.out.println(p.getEtudiantList().get(j).getnomEtudiant());
					if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant) && p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {
						p.getEtudiantList().get(j).getNote().add(note);
						etudiantTrouve= true;
						break;
					}

				}
			}
			if (etudiantTrouve ==true) {
				break;
			}

			if (etudiantTrouve ==true) {
				break;
			}
		} 
		if (etudiantTrouve==false) {
			System.out.println(nomEtudiant+" "+prenomEtudiant+" n'existe pas");
		}
	}

	public double afficherMoyenne(String nomEtudiant, String prenomEtudiant) {
		double moy = 0;
		boolean nompromotion = false;
		boolean etudiantTrouve = false;
		for (Promotion p : promotionList) { 

			if(p.getEtudiantList().size()>0 ) {
				for (int j = 0; j < p.getEtudiantList().size(); j++) {
					
					if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant)&& p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {
						etudiantTrouve=true;
					}
					if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant)&& p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant) && p.getEtudiantList().get(j).getNote().size()>0) {
						
						for (int i = 0; i < p.getEtudiantList().get(j).getNote().size(); i++) {

							moy += (p.getEtudiantList().get(j).getNote().get(i))/p.getEtudiantList().get(j).getNote().size();
						}
						
						etudiantTrouve=true;
						break;
					}

				}
			}
			if (etudiantTrouve ==true) {
				break;
			}

			if (etudiantTrouve ==true) {
				break;
			}
		} 
		if (etudiantTrouve==false) {
			System.out.println(nomEtudiant+" "+prenomEtudiant+" n'existe pas");
		}
		return moy;
	}
	
	public void afficherNote(String nomEtudiant, String prenomEtudiant) {
		double moy = 0;
		boolean nompromotion = false;
		boolean etudiantTrouve = false;
		for (Promotion p : promotionList) { 

			if(p.getEtudiantList().size()>0 ) {
				for (int j = 0; j < p.getEtudiantList().size(); j++) {
					
					if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant)&& p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant)) {
						etudiantTrouve=true;
					}
					if(p.getEtudiantList().get(j).getnomEtudiant().equalsIgnoreCase(nomEtudiant)&& p.getEtudiantList().get(j).getPrenomEtudiant().equalsIgnoreCase(prenomEtudiant) && p.getEtudiantList().get(j).getNote().size()>0) {
						System.out.println("les notes de "+p.getEtudiantList().get(j).getnomEtudiant()+ " "+p.getEtudiantList().get(j).getPrenomEtudiant()+ " sont");
						System.out.println(p.getEtudiantList().get(j).getNote());
					}
				}
			}
		}
	}

	public void afficherMoyennePromotion(String nomPromotion) {
		double moyPromo=0;
		boolean unePromotionAvecMemeNomTrouvee =false;
		for (Promotion p : promotionList) {
			unePromotionAvecMemeNomTrouvee =false;
			if(p.getNom().contains(nomPromotion)) {
				for (int j = 0; j < p.getEtudiantList().size(); j++) {
				moyPromo+= (afficherMoyenne(p.getEtudiantList().get(j).getnomEtudiant() , p.getEtudiantList().get(j).getPrenomEtudiant()))/p.getEtudiantList().size();	
				}
				System.out.println("la moyenne de la promotion "+nomPromotion+" est de "+moyPromo);		
			break;
			}
			
			else {
				unePromotionAvecMemeNomTrouvee = true;
			}

		}
		if(unePromotionAvecMemeNomTrouvee) {
			System.out.println("ERREUR aucune promotion ayant ce nom existe !!!!");
		}

	}

}







