package menu;

import java.util.ArrayList;

public class Etudiant {
	private final String nomEtudiant;
	private final String prenomEtudiant;
	private static int id=0;
	private final int monid;
	private ArrayList <Integer> note;



	public Etudiant(String n, String p) {
		id = id +1;
		monid = id;
		this.nomEtudiant = n;
		this.prenomEtudiant = p;
		this.note= new ArrayList<Integer>();
	}

	//	methodes 

	public ArrayList<Integer> getNote() {
		return note;
	}

	public void setNote(ArrayList<Integer> note) {
		this.note = note;
	}

	@Override
	public String toString() {
		return "Etudiant [nomEtudiant = " + this.nomEtudiant + ", PrenomEtudiant = " + this.prenomEtudiant + " IDENTIFIANT = "+this.monid + "]";
	}

	public int comparer(Etudiant v) {
		if(this.nomEtudiant.compareTo(v.nomEtudiant)>0) {
			//System.out.println(this.nomEtudiant.compareTo(v.nomEtudiant));
			return 1;
		} else if(this.nomEtudiant.compareTo(v.nomEtudiant)<0) {
		//	System.out.println(this.nomEtudiant.compareTo(v.nomEtudiant));
			return -1;
		} else {
			//System.out.println(this.nomEtudiant.compareTo(v.nomEtudiant));
			int res = this.prenomEtudiant.compareTo(v.prenomEtudiant);
			if(res != 0) {
				return res;
			} else{
				if (this.id>(v.id)) {
					return 1;}

				else { return -1;
				}
			}
		}
	}

	//************ GETTERS & SETTERS
	public int getMonid() {
		return monid;
	}
	
	public  String getnomEtudiant() {
		return nomEtudiant;
	}

	public String getPrenomEtudiant() {
		return prenomEtudiant;
	}

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		Etudiant.id = id;
	}

}