package menu;

import java.util.LinkedList;

public class Promotion {
	private static int id;
	private final String nom;
	private final int idpromotion;
	private LinkedList<Etudiant> etudiantList;

	public Promotion(String nomPromo) {
		id = id +1;
		this.nom=nomPromo;
		this.idpromotion=id;
		this.etudiantList = new LinkedList<Etudiant>();
	}

	// GETTERS & SETTERS
	public LinkedList<Etudiant> getEtudiantList() {
		return etudiantList;
	}

	public void setEtudiantList(LinkedList<Etudiant> etudiantList) {
		this.etudiantList = etudiantList;
	}

	public String getNom() {
		return nom;
	}

	public int getIdpromotion() {
		return idpromotion;
	}

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		Promotion.id = id;
	}

	@Override
	public String toString() {
		return "Promotion [nom=" + nom + ", idpromotion="+ idpromotion + "]";
	}

	public void afficherPromotion() {
		System.out.println(this);
		for (Etudiant etudiant : etudiantList) {
			System.out.println("   "+etudiant);
		}
		
	}

}
