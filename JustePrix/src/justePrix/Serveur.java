package justePrix;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Serveur  {
	static Socket client = null;
	Integer a;
	static Thread t1;
	static Integer random;
	static int player=0;
	static PrintWriter out;
	static ServerSocket listener=null;
	static ExecutorService pool=Executors.newFixedThreadPool(4);
	static ArrayList <ClientHandler> clients= new ArrayList<>();
	static ArrayList <Thread> threads= new ArrayList<>();
	static ArrayList <Thread> threadMain= new ArrayList<>();
	static ArrayList <Integer> number= new ArrayList<>();


	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}





	public static void main(String[] args) throws IOException, InterruptedException {
		random = getRandomNumberInRange(0, 100);
		number.add(random);
		listener=new ServerSocket(8080);
		threadMain.add(Thread.currentThread());
		System.err.println(random);
		boolean con=false;
		while(true&& con==false) {
			for (ClientHandler client : clients) {
				if(client.gagne==true) {
					con=true;
					break;
				}
			}
			if(con==true) {
				break;
			}
			
			System.err.println(player);
			System.err.println("waiting to connect");
			if(player>4) {
				out = new PrintWriter(client.getOutputStream(), true);
				out.println("DESOLE VOUS ARRIVEZ TROP TARD");
				break;
			}
			client = listener.accept();
			player++;
			

			ClientHandler clientThread = new ClientHandler(client, clients);
			clients.add(clientThread);
			//	pool.execute(clientThread);
			Thread t1= new Thread(clientThread);
			threads.add(t1);
			t1.start();
			

			for (Thread t : threads) {
				if(t.isInterrupted()) {
					con=true;
					break;
				}
			}

		}

		client.close();
		listener.close();



	}

	public static void setRandom(Integer random) {
		Serveur.random = random;
	}

	public static Integer getRandom() {
		return Serveur.random;
	}
	
	public static void closeConnect(ArrayList<ClientHandler> client) throws IOException {
		for (ClientHandler clientHandler : client) {
			clientHandler.out.close();
			clientHandler.in.close();
			//listener.close();
		}
	}



}


