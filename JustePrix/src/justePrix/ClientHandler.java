package justePrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientHandler implements Runnable {
	Socket client;
	String name="";
	boolean gagne=false;
	boolean con=false;
	int tour=0;
	boolean perdu=false;
	public PrintWriter out;
	public BufferedReader in;
	private static Pattern p;
	private static Matcher m;
	private ArrayList<ClientHandler> clients;
	public ClientHandler(Socket client, ArrayList<ClientHandler> clients) throws IOException {
		this.client=client;
		this.clients=clients;
		out = new PrintWriter(client.getOutputStream(), true);
		in =new BufferedReader(new InputStreamReader(client.getInputStream()));

	}


	@Override
	public void run() {
		Boolean begin=true;
		String request="";
		name="";
		out.println("CONNEXION RÉUSSIE\n VEUILLEZ ENTRER VOTRE NOM DE JOUEUR\n"); 
		try {
			name = in.readLine();
			out.println("BIENVENU " +name+ " CHOISISSEZ UN NOMBRE ENTRE 0 ET 100");
			System.err.println("name of player "+name);
			for (ClientHandler client : Serveur.clients) {
				if(client.gagne==true) {
					
					out.println("\nDESOLE "+name.toUpperCase()+" VOTRE ADVERSAIRE "+client.name+" A TROUVÉ LE JUSTE PRIX ");						
					
				}
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		while (true && con==false) {
			if(begin ==true) {	
				begin =false;
			}else {
				out.println("ESSAI ENCORE");
			}

			String request1=null;
			
			try {
				request=in.readLine();
				System.err.println(request);
				for (ClientHandler client :clients) {
					if(client.gagne==true) {
						out.println("\nDESOLE "+name.toUpperCase()+" VOTRE ADVERSAIRE "+client.name.toUpperCase()+" A TROUVÉ LE JUSTE PRIX ");						
						con=true;
						
					}	
			}
				if(con==true) {
					break;
				}
				p = Pattern.compile("[0-9]+");
				m = p.matcher(request);
				while (m.find()) {
				request1=m.group();
				}
				
				System.err.println("request "+request1+">");
				
				

			} catch (Exception e) {
				System.out.println("server ferme");
			
			}
			if(request1==null) {
				request1="105";
			}
			if(Serveur.random==Integer.parseInt(request1)) {
				gagne=true;	
				break;
			}
			else {
				if(Serveur.random<Integer.parseInt(request1) &&Integer.parseInt(request1)!=105) {
					out.println("C'EST MOINS\n"); 
				}


				if(Serveur.random>Integer.parseInt(request1)) {
					out.println("C'EST PLUS\n");
				}
				
				if (Integer.parseInt(request1)==105) {
					out.println("VEUILLEZ ENTRER UN NOMBRE");
				}

			}
			if(tour==3) {
				perdu = true;
				break;
			}
			tour++;


			System.out.println("end of loop ");
		}
		

		if(perdu ==true) {
			out.println(name.toUpperCase()+" VOUS AVEZ PERDU\n");
			
			
		}
		else {
			if(gagne==true) {
				out.println("\nBRAVO "+name.toUpperCase()+" VOUS AVEZ TROUVÉ LE JUSTE PRIX ");	
				try {
					Serveur.closeConnect(clients);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			

		}
		try {
			out.close();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}







