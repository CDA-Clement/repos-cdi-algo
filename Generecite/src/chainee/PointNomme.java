package chainee;

public class PointNomme extends CoupleNomme<Integer> {
	public PointNomme( Integer un,  Integer deux, String nom) {
		super(un, deux,nom);
	}

	public void affichepoint() {
		System.out.println ("premiere valeur : " + getX()
				+ " - deuxieme valeur : " + getY() + " - le nom est : "+getNom());
	}

}
