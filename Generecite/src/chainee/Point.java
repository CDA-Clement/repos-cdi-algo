package chainee;

public class Point<E> implements Comparable<Point> {

	private int x, y;

	Point(int x, int y) {
		this.x = x;
		this.y = y;

	}

	public double distance() {
		double distance = Math.sqrt((this.y) * (this.y) + (this.x) * (this.x));
		return distance;
	}

	public void affiche() {
		System.out.println("coordonnees : " + x + " " + y);
	}

	@Override
	public int compareTo(Point arg) {
		if (this.distance() > arg.distance()) {
			return 1;

		} else if (this.distance() == arg.distance()) {
			return 0;
		} else {

			return -1;
		}

	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
}
