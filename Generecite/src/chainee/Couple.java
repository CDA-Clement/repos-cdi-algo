package chainee;

class Couple<T> {
	private T x;
	private T y;

	// les deux �l�ments du couple
	public Couple(T premier, T second) {
		x = premier;
		y = second;
	}

	public void affiche() {
		System.out.println("premiere valeur : " + x + " - deuxieme valeur : " + y);
	}

	public T getX() {
		return x;
	}

	public void setX(T x) {
		this.x = x;
	}

	public T getY() {
		return y;
	}

	public void setY(T y) {
		this.y = y;
	}
}