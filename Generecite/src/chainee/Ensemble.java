package chainee;

import java.util.ArrayList;
import java.util.List;

public class Ensemble<E> {
	private List<E> listEnsemble;

	public Ensemble() {
		listEnsemble = new ArrayList<E>();
	}

	public boolean ajouter(E o) {

		if (listEnsemble.contains(o)) {

			return false;
		} else {
			listEnsemble.add(o);
			return true;
		}

	}

	public boolean enlever(E o) {
		if (listEnsemble.contains(o)) {
			listEnsemble.remove(o);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public String toString() {
		return "Ensemble [le cardinal est " + listEnsemble.size() + " listEnsemble=" + listEnsemble + "]";
	}

	public List<E> getListEnsemble() {
		return listEnsemble;
	}

	public void setListEnsemble(List<E> listEnsemble) {
		this.listEnsemble = listEnsemble;
	}

}
