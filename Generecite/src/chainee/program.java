package chainee;

import java.util.ArrayList;
import java.util.List;

public class program {

	public static void main(String[] args) {
		Triplet<String> tp1 = new Triplet<String>("toto", "ddd�", "dffg");
		Triplet<Integer> tp2 = new Triplet<Integer>(1, 2, 3);

		tp1.affiche();
		tp2.affiche();
		tp1.getPremier();

		TripletH<String, Integer, Boolean> tp3 = new TripletH<>("clement", 29, true);

		TripletH<int[], String, ArrayList<Triplet<String>>> tp4 = new TripletH<>(new int[5], "clement",
				new ArrayList<>());
		tp4.affichep();
		tp4.getTroisieme().add(tp1);
		tp4.affichep();

		Integer[] Tableau = {};
		String[] Tableau1 = { "cl", "dr", "gg", "hj", "hl", "za" };
		Methode.printArray(Tableau1);

		Couple<String> tp5 = new Couple<>("premier", "second");
		tp5.affiche();

		CoupleNomme<String> tp6 = new CoupleNomme<>("15", "22", "nomm");
		tp6.afficheNomme();

		PointNomme tp7 = new PointNomme(12, 10, "nom2");
		tp7.affichepoint();

		CoupleNomme<Boolean> tp9 = new CoupleNomme<>(true, false, "nomm3");
		tp9.afficheNomme();

		Integer n = Methode.getLargestElementTableau(Tableau);
		System.out.println(n);

		String m = Methode.getLargestElementTableau(Tableau1);
		System.out.println(m);

		Point[] tp = new Point[4];
		tp[0] = new Point(5, 1);
		tp[1] = new Point(1, 4);
		tp[2] = new Point(6, 4);
		tp[3] = new Point(5, 2);
		List<Point> z3 = new ArrayList<Point>();
		z3.add(new Point(5, 1));
		z3.add(new Point(1, 4));
		z3.add(new Point(6, 4));
		z3.add(new Point(5, 2));
		Point max13 = Methode.getLargestElementCollection(z3);
		Point max12 = Methode.getLargestElementTableau(tp);
		System.out.println("tableau " + max12);
		System.out.println("list " + max13);

	}

}
