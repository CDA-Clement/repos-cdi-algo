package chainee;

public class CoupleNomme<T> extends Couple<T> {
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	private String nom;

	public CoupleNomme(T premier, T second, String nomm) {
		super(premier, second);
		this.nom= nomm;
	}
	
	public void afficheNomme (){
        System.out.print("le nom est "+this.nom+" "); 
        super.affiche();
    }
	

}
