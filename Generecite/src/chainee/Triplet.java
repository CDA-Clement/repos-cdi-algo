package chainee;

import java.util.Objects;

public class Triplet<T> {
	private  T premier;
	private  T second;
	private  T Troisieme;
	
	
	public void setPremier(T premier) {
		this.premier = premier;
	}

	@Override
	public String toString() {
		return "Triplet [premier=" + premier + ", second=" + second + ", Troisieme=" + Troisieme + "]";
	}

	public void setSecond(T second) {
		this.second = second;
	}

	public void setTroisieme(T troisieme) {
		Troisieme = troisieme;
	}

	public Triplet (T Val, T Val1, T Val2) {
		this.premier= Val;
		this.second= Val1;
		this.Troisieme= Val2;
	}
	
	public  T getPremier() {
		return premier;
	}
	
	
	public  T getSecond() {
		return second;
	}
	
	
	public  T getTroisieme() {
		return Troisieme;
	}

	public void affiche() {
		 System.out.println("" + this.premier + " "+ this.second + " " + this.Troisieme);
	}
	
}
