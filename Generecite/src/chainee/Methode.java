package chainee;

import java.util.Collection;
import java.util.Random;

public class Methode {
	static Random rand = new Random();

	public static <E> E printArray(E[] inputArray) {
		int n = rand.nextInt(inputArray.length);

		return inputArray[n];

	}

	public static <A> A unDesDeux(A un, A deux) {
		double trou = 1 + (Math.random() * 3);
		if (trou <= 2) {
			return deux;
		}
		return un;
	}

	public static <E extends Comparable<E>> E getLargestElementTableau(E[] list) {
		if (list.length > 0) {
			E max = list[0];
			for (int i = 1; i <= list.length - 1; i++) {
				if (list[i].compareTo(max) > 0) {
					max = list[i];
				}
			}
			return max;
		} else {
			return null;
		}
	}

	public static <E extends Comparable<E>> E getLargestElementCollection(Collection<E> list) {
		E max = null;
		if (list.size() > 0) {
			int i = 0;
			for (E e : list) {
				if (i == 0) {
					max = e;
					i++;
				} else if (e.compareTo(max) > 0) {
					max = e;
				}
			}
			return max;

		} else {
			return null;
		}
	}

}
