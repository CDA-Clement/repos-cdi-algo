package project;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Menu {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		String action="";
		ArrayList<String> list = new ArrayList<String>();
		
		boolean run=false;
		System.out.println("####################################################################################################################");
		System.out.println("####################################################################################################################");
		ASCIIArtGenerator.ART_SIZE_MEDIUM("   TERMINAL");
		System.out.println("####################################################################################################################");
		System.out.println("####################################################################################################################");
		System.out.println();
		System.out.println();
		
		while(run==false ) {
			if(list.size()==11) {
			list.remove(0);
		}
			System.out.print("۞ ");
			action=sc.next();
			action= action.toUpperCase();
			
			switch(action) {
			case "HELP":
				LocalDateTime now = LocalDateTime.now();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
				String formatDateTime = now.format(formatter);
				String history = action +" Effectue "+ formatDateTime ;
				list.add(history);
				System.out.println("\n					Bonjour sur la console cdi");
				System.out.println("\nEXIT		: Shut down the program");
				System.out.println("\nHELP 		: Display all the commands available");
				System.out.println("\nPWD 		: Display the line utility for printing the current working directory ");
				System.out.println("\nRIVER		: Display the first intersection of two digits  \n");
				System.out.println("\nISPRIME   : Display whether or not it s an integer");
				System.out.println("\nHISTORY   : Display a command");
				System.out.println("\nHISTCLEAR : Clean a command");
				continue;


			case "PWD": 
				LocalDateTime now1 = LocalDateTime.now();
				DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
				String formatDateTime1 = now1.format(formatter1);
				String history1 = action + " Effectue (NOT IMPLEMENTED YET)"+ formatDateTime1 ;
				list.add(history1);
				System.out.println(" \nNOT IMPLEMENTED YET  \n");

				continue
				;
			case "RIVER":
				
				
				System.out.println("\nPlease enter the first number");
				String number1 = sc.next();
				System.out.println("Please enter the second number");
				String number2 = sc.next();
				
				System.out.print("\nthe meeting point of these two digits is: ");
				double B =River.river(Double.parseDouble(number1), Double.parseDouble(number2));
				String history3 =  action + " the first number " + number1 + " the second number " + number2 +" Result : " + B;
				list.add(history3);
				continue;
				
			case "ISPRIME":
				
				
				System.out.println("\nPlease enter the first number");
				String number3= sc.next();
				
				
				
				String A = Isprime.Isprime(Double.parseDouble(number3));
				String history4 =  action + " Effectue the first number " + number3 + " result : " + A ;
				list.add(history4);
				continue;
			case "HISTORY":
				System.out.println(list.toString());
				for (String string : list) {
					System.out.println(string);
				}
				continue;
			case "HISTCLEAR":
				list.clear();
				continue;
			case "QUIT":
				System.out.println();
				System.out.println("####################################################################################################################");
				System.out.println("####################################################################################################################");
				System.out.println();
				System.out.println();
				ASCIIArtGenerator.ART_SIZE_SMALL("   SHUTTING DOWN");
				return;
			case "EXIT":
			System.out.println();
			System.out.println("####################################################################################################################");
			System.out.println("####################################################################################################################");
			System.out.println();
			System.out.println();
			ASCIIArtGenerator.ART_SIZE_SMALL("   SHUTTING DOWN");
			return;
			default:
				System.out.println("command not found \n");
				continue;
			}
		}


	}
}
