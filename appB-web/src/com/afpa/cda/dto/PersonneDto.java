package com.afpa.cda.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class PersonneDto {
	private Integer id;
	private String nom;
	private String prenom;
	private Date date;
	private String metier;
	private String adresse;
	
	public PersonneDto() {
	}

	public PersonneDto(int id, String nom, String prenom, Date date, String metier, String adresse) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom=prenom;
		this.adresse=adresse;
		this.date=date;
		this.metier=metier;
	}


	
}