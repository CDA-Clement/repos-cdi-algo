package com.afpa.cda.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.PersonneRepository;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.entity.Personne;

@Service
public class gg implements IPersonneService {

	@Autowired
	private PersonneRepository personneRepository;

	@Override
	public List<PersonneDto> chercherToutesLesPersonnes() {
		return this.personneRepository
				.findAll()
				.stream()
				.map(e->new PersonneDto(e.getId(), e.getNom(),e.getPrenom(),e.getDateDeNaissance(),e.getAdresse(),e.getMetier()))
				.collect(Collectors.toList());
	}

	@Override
	public void supprimer(Integer id) {
		List<Personne> personnes = this.personneRepository.findByIdOrderById(id);

		for (Personne personne : personnes) {
			if(personne.getId()==id) {
				this.personneRepository.delete(personne);

			}

		}
	}
}


