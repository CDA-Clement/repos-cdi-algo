package com.afpa.cda.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
public class Personne {
	
	@Id
	private int id;
	
	private String nom;

	private String prenom;
	
	private Date dateDeNaissance;
	
	private String metier;
	
	private String adresse;
	
}