package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.main.BinString;
import com.afpa.main.ChaineVideException;
import com.afpa.main.ParametreNegatifException;
import com.afpa.main.Representation;


@TestMethodOrder(OrderAnnotation.class)
class RepresentationTest {

	BinString binstring;

	@BeforeAll
	static void setUpBeforeClass() throws Exception { 

	}

	@AfterAll
	static void tearDownAfterClass() throws Exception { }


	@BeforeEach
	void setUp() throws Exception { 
		binstring = new Representation();

	}

	@AfterEach
	void tearDown() throws Exception {

	}

	@Order(1) 
	@Test
	void test_convert_normal() {
		try {
			assertEquals("0", binstring.convert("0"));
			assertEquals("1000000", binstring.convert("@"));
			assertEquals("1100001", binstring.convert("a"));
			assertEquals("100000", binstring.convert(" "));
		} catch (Exception e2) {
			fail("normalement pas d'exception !!!! ");
		}
	}
	
	@Order(2) 
	@Test
	void test_convert_chaine_vide() {
		Throwable e = null;
		try {
			binstring.convert("");
		} catch (Exception e2) {
			e = e2;
		}
		assertTrue(e instanceof ChaineVideException);
	}

	@Test
	@Order(3) 
	void testSum() {
		
		try {
			if(binstring.sum("a")!=97)
				fail("la somme n'est pas bonne");
			if(binstring.sum("ab")!=195)
				fail("la somme n'est pas bonne");

		} catch (Exception e2) {
			fail("normalement pas d'exception !!!! ");
		}
	}
	
	@Test
	@Order(4) 
	void testSumVide() {
		Throwable e = null;
		try {
			binstring.sum("");
		} catch (Exception e2) {
			e = e2;
		}
		assertTrue(e instanceof ChaineVideException);
	}

	@Test
	@Order(5) 
	void testBinarise() {
		Throwable e = null;
		try {
			binstring.binarise(-1);
		} catch (Exception e2) {
			e = e2;
		}
		assertTrue(e instanceof ParametreNegatifException);
		try {
			assertEquals("101", binstring.binarise(5));
		} catch (ParametreNegatifException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	



}