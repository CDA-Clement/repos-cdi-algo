package School;

import java.util.Comparator;

public class PersonneStatutComparator implements Comparator<Personne> {

	
	
	@Override
	public int compare(Personne o1, Personne o2) {
		if(o1.getStatus().compareTo(o2.getStatus())==0) {
			return o1.getNom().compareTo(o2.getNom());
		}else {
			return o1.getStatus().compareTo(o2.getStatus());
		}
		
	}

}
