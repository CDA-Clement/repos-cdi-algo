package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import geometrie.Point;
import geometrie.PointExistant;


@TestMethodOrder(OrderAnnotation.class)
class TestPoint {
	private Point p;
	private Point p1;
	@AfterEach
	void release() throws Exception { 
		p=null;

	}

	@BeforeEach
	void setUp() throws Exception { 

	}
	@Order(1) 
	@Test
	void testCreationZero() {
		p= new Point("A", 1, 3.0, 0.0);
		assertTrue(p.getAbscisse()==3.0 && p.getOrdonnee()==0.0);
	}
	@Order(2) 
	@Test
	void testCreationdoublon() {
		Throwable e1=null;
		try {
			Point.creerPoint("A", 1, 3.0, 1.0);
			Point.creerPoint("A", 1, 3.0, 1.0);
			fail("il devrait y avoir une exception");
		} catch (Exception e) {
			e1=e;

		}
		assertTrue(e1 instanceof PointExistant);

		System.err.println(Point.getListPoints().size());
	}
	@Order(3) 
	@Test
	void testdistance() {
		p= new Point("A", 1, 3.0, 0.0);

		assertTrue(p.distanceOrigin()==3.0);

	}
	@Order(4) 
	@Test
	void testList()  {
		System.err.println("++++"+Point.getListPoints().size());

		try {
			Point.creerPoint("A", 3, 3.0, 0.0);
			Point.creerPoint("B", 2, 2.0, 1.0);
		} catch (Exception e) {


		}
		
		List <Point>listpoint = new ArrayList <Point>();
		listpoint=Point.getListPoints();
		Point.listerPoints(listpoint);
		System.err.println("++++"+Point.getListPoints().size());
		System.out.println(Point.getListPoints().get(0).getNum());

		assertTrue(Point.getListPoints().get(0).getNum().equals(2));
	}

	@Order(5) 
	@Test
	void testDistance()  {
		System.err.println("++++"+Point.getListPoints().size());
		p=new Point("A", 6, 3.0, 0.0);
		p1=new Point("A", 6, 0.0, 0.0);

		System.err.println("+++FF+"+Point.getListPoints().size());

		System.out.println("dist"+p.distanceEntrePoint(p1));
		assertTrue(p.distanceEntrePoint(p1)==3);
	}




}
