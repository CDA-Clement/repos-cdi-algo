package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import geometrie.Point;
import geometrie.PointExistant;
import geometrie.PointUtilise;
import geometrie.Triangle;


@TestMethodOrder(OrderAnnotation.class)
class TestTriangle {
	Triangle t;
	Point a;
	Point b;
	Point c;
	
	@Order(1)
	@Test
	void testCreationZero() throws PointUtilise {
		try {
			assertTrue(Triangle.getListTriangle().size()==0);
			Point.creerPoint("B", 1, 1.0, 3.0);
			Point.creerPoint("A", 1, 1.0, 3.0);
			Point.creerPoint("C", 1, 1.0, 3.0);
			Triangle.creerTriangle("A", 1);
			Triangle.creerTriangle("B", 1);
			Triangle.creerTriangle("C", 1);
		} catch (PointExistant e) {
			
			e.printStackTrace();
		}
		
	}
	@Order(2)
	@Test
	void testCreerTriangleException() {

		
		Throwable e1=null;
		try {
			Triangle.creerTriangle("A", 1);
			Triangle.creerTriangle("A", 1);
			fail("il devrait y avoir une exception");
		} catch (Exception e) {
			e1=e;

		}
		assertTrue(e1 instanceof PointUtilise);

		
	}
	@Order(3)
	@Test
	void testListerTriangle() {
		try {
			Point.creerPoint("D", 1, 1.0, 3.0);
			Point.creerPoint("E", 1, 1.0, 3.0);
			Point.creerPoint("F", 1, 1.0, 3.0);
		} catch (PointExistant e) {
			
			e.printStackTrace();
		}
		try {
			Triangle.creerTriangle("D", 1);
			Triangle.creerTriangle("E", 1);
			Triangle.creerTriangle("F", 1);
		} catch (PointUtilise e) {
			
			e.printStackTrace();
		}
		
		List<Triangle>listtriangle= new ArrayList<Triangle>();
		listtriangle=Triangle.getListTriangle();
		Triangle.listerTriangle(listtriangle);
		assertTrue(Triangle.getListTriangle().get(0).getA().getNom().equals("A"));
		
	}



	@Test
	void testOrder() {
		assertTrue(Triangle.getListTriangle().get(0).getA().getNom().equals("A"));
	}

	@Test
	void testSurface() {
		t=Triangle.getListTriangle().get(0);
		System.err.println(t.getSurface());
		t.getSurface();
		assertTrue(t.surface()==0.0);
	}

	@Test
	void testGetListTriangle() {
		System.err.println(Triangle.getListTriangle().size());
		assertTrue(Triangle.getListTriangle().size()==1);
	}

	@Test
	void testGetListPointsPourTriangle() {
		System.err.println(Triangle.getListPointsPourTriangle().size());
		assertTrue(Triangle.getListPointsPourTriangle().size()==1);
	}

}
