package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import geometrie.Cercle;
import geometrie.Point;
import geometrie.PointExistant;
import geometrie.PointUtilise;
import geometrie.Triangle;

class TestCercle {
	Cercle c;
	Point a;
	Double rayon;
	
	@BeforeEach
	void setup() {
		
		a= new Point("a", 1, 3.0, 0.0);
		
		rayon =5.0;
		c= new Cercle(a,rayon); 
		
	}
	
	@AfterEach
	void release() {
		a=null;
		rayon=null;
		c=null;
	}

	@Test
	void testCercle() throws PointExistant {
		Point.getListPoints().clear();
		Point.creerPoint(c.getA().getNom(), c.getA().getNum(), c.getA().getAbscisse(), c.getA().getOrdonnee());
		assertTrue(c.getA().getNom().equals("a"));
		
		
	}

	@Test
	void testCreerCercle() {
		try {
			Cercle.creerCercle("a", 1, rayon);
		} catch (PointUtilise e) {
			
			e.printStackTrace();
		}
		assertTrue(Cercle.getListCercle().size()==0);
	}

	

	@Test
	void testListerCercle() throws PointExistant {
		try {
			Point.getListPoints().clear();
			Point.creerPoint(c.getA().getNom(), c.getA().getNum(), c.getA().getAbscisse(), c.getA().getOrdonnee());
			Cercle.creerCercle("a", 1, rayon);
			Cercle.listerCercle(Cercle.getListCercle());
		} catch (PointUtilise e) {
			
			e.printStackTrace();
		}
		assertTrue(Cercle.getListCercle().get(0).getA().getNom().equals("a"));
	
	}

	@Test
	void testOrder() throws PointExistant {
		try {
			Point.getListPoints().clear();
			Point.creerPoint(c.getA().getNom(), c.getA().getNum(), c.getA().getAbscisse(), c.getA().getOrdonnee());
			Cercle.creerCercle("a", 1, rayon);
			Cercle.order(Cercle.getListCercle());
		} catch (PointUtilise e) {
			
			e.printStackTrace();
		}
		assertTrue(Cercle.getListCercle().get(0).getA().getNom().equals("a"));
	}

	@Test
	void testSurface() {
		assertTrue(c.Surface()==78.5);
	}

	@Test
	void testGetListCercle() {
		assertFalse(Cercle.getListCercle().contains(c));
	}

}
