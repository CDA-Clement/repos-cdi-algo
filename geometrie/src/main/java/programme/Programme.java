package programme;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import geometrie.Cercle;
import geometrie.Point;
import geometrie.PointExistant;
import geometrie.Triangle;

public class Programme {


	public static void main(String[] args) throws PointExistant {
		System.out.println("Bienvenue");
		System.out.println();
		boolean continuer = true;
		int choix = 0;

		while (continuer) {
			Scanner sc = new Scanner(System.in);

			System.out.println();
			System.out.println("0- arreter le programme");
			System.out.println("1- point");
			System.out.println("2- triangle");
			System.out.println("3- cercle");
			System.out.print("> ");

			choix = sc.nextInt();
			sc.nextLine();

			switch (choix) {
			case 0:
				System.out.println("au revoir !");
				continuer = false;
				break;



			case 1:
				System.out.println("1- creer un point");
				System.out.println("2- lister les points");
				choix = sc.nextInt();
				sc.nextLine();
				switch (choix) {
				case 1:
					Integer numero=0;
					System.out.println("veuillez indiquer le nom du point");
					String nom = sc.next();
					System.out.println("veuillez indiquer le num du point");
					String num = sc.next();
					try {
						numero=numero.parseInt(num);
					} catch ( Exception e) {
						System.out.println("Veuillez saisir un numero ");
						System.out.println("veuillez indiquer le num du point");
						num = sc.next();
						numero=numero.parseInt(num);
					}

					System.out.println("veuillez indiquer l'abscisse du point");
					Double abscisse = sc.nextDouble();
					System.out.println("veuillez indiquer l'ordonne du point");
					Double ordonne = sc.nextDouble();

					try {
						Point.creerPoint(nom, numero, abscisse, abscisse);

					} catch (Exception e) {
						e.getCause();


					}

					break;

				case 2:

					List <Point>listpoint = new ArrayList <Point>();
					listpoint=Point.getListPoints();
					Point.listerPoints(listpoint);
					break;
				}

				break;
			case 2:
				System.out.println("1- creer un triangle");
				System.out.println("2- lister les triangles");
				choix = sc.nextInt();
				sc.nextLine();
				switch (choix) {
				case 1:
					Integer numero=0;
					System.out.println("veuillez indiquer le nom du point");
					String nom = sc.next();
					System.out.println("veuillez indiquer le num du point");
					String num = sc.next();
					try {
						numero=numero.parseInt(num);
					} catch ( Exception e) {
						System.out.println("Veuillez saisir un numero ");
						System.out.println("veuillez indiquer le num du point");
						num = sc.next();
						numero=numero.parseInt(num);
					}


					try {
						Triangle.creerTriangle(nom, numero);

					} catch (Exception e) {
						e.getCause();


					}

					break;

				case 2:
					List <Triangle>listtriangle = new ArrayList <Triangle>();
					listtriangle=Triangle.getListTriangle();
					Triangle.listerTriangle(listtriangle);
					
					break;
				}

				break;

			case 3:
				System.out.println("1- creer un cercle");
				System.out.println("2- lister les cercles");
				choix = sc.nextInt();
				sc.nextLine();
				switch (choix) {
				case 1:
					Integer numero=0;
					Double rayon=0.0;
					System.out.println("veuillez indiquer le nom du point");
					String nom = sc.next();
					System.out.println("veuillez indiquer le num du point");
					String num = sc.next();
					System.out.println("veuillez indiquer le rayon du point");
					String r = sc.next();
					try {
						numero=numero.parseInt(num);
						rayon=rayon.parseDouble(r);
					} catch ( Exception e) {
						System.out.println("Veuillez saisir un numero ");
						System.out.println("veuillez indiquer le num du point");
						num = sc.next();
						numero=numero.parseInt(num);
					}


					try {
						Cercle.creerCercle(nom, numero,rayon);

					} catch (Exception e) {
						e.getCause();


					}

					break;

				case 2:
					List <Cercle>listCercle = new ArrayList <Cercle>();
					listCercle=Cercle.getListCercle();
					Cercle.listerCercle(listCercle);
					break;
				}

				break;
			}
		}
	}
}

