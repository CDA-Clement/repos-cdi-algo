package geometrie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString

public class Triangle {
	private static Logger log = LoggerFactory.getLogger(Triangle.class);
	private Point a;
	private Point b;
	private Point c;
	private Double surface;

	public Triangle(Point aT, Point bT, Point cT) {
		a = aT;
		b = bT;
		c = cT;
		surface = this.surface();
	}

	static private List<Triangle> listTriangle = new ArrayList<Triangle>();
	static private List<Point> listPointsPourTriangle = new ArrayList<Point>();

	/**
	 * cette methode permet de creer un triangle a partir des parametres suivants
	 * 
	 * @param nomP
	 * @param numP
	 * @throws PointUtilise si un point est utilisé plusieurs fois pour construire
	 *                      le triangle
	 */

	public static void creerTriangle(String nomP, Integer numP) throws PointUtilise {

		if (Point.getListPoints().size() < 3) {
			System.out.println("il faut au minimum creer 3 points");
		} else {
			for (Point point : Point.getListPoints()) {

				if (point.getNom().equals(nomP) && point.getNum() == numP) {
					if (listPointsPourTriangle.size() == 0) {
						listPointsPourTriangle.add(point);
					} else {
						if (listPointsPourTriangle.contains(point)) {
							log.error("Point utilise");
							throw new PointUtilise();
						} else {
							listPointsPourTriangle.add(point);
							log.info("point ajouté pour la creation du triangle");

						}

					}
				}

			}
		}
		if (listPointsPourTriangle.size() == 3) {

			Point.order(listPointsPourTriangle);
			Triangle t = new Triangle(listPointsPourTriangle.get(0), listPointsPourTriangle.get(1),
					listPointsPourTriangle.get(2));
			listTriangle.add(t);
			log.info("triangle cree");
			listPointsPourTriangle = new ArrayList<Point>();
		}
	}

	/**
	 * permet de lister les triangles par surface puis distance de l'origine puis
	 * nom du point le plus proche puis le num
	 * 
	 * @param listtriangle
	 */
	public static void listerTriangle(List<Triangle> listtriangle) {
		Triangle.order(listtriangle);
		for (int i = 0; i < listTriangle.size(); i++) {
			System.out.println(listTriangle.get(i).toString());

		}

	}

	/**
	 * permet de lister les triangles par surface puis distance de l'origine puis
	 * nom du point le plus proche puis le num
	 * 
	 * @param listtriangle
	 */
	public static void order(List<Triangle> listtriangle) {

		Collections.sort(listtriangle, new Comparator<Triangle>() {

			public int compare(Triangle t1, Triangle t2) {

				Double surf1 = t1.surface();
				Double surf2 = t2.surface();
				int sComp2 = surf1.compareTo(surf2);

				if (sComp2 != 0) {
					return sComp2;
				}

				Double dist1 = t1.a.distanceOrigin();
				Double dist2 = t2.a.distanceOrigin();
				int sComp = dist1.compareTo(dist2);

				if (sComp != 0) {
					return sComp;
				}

				String nom1 = t1.a.getNom();
				String nom2 = t2.a.getNom();
				int sComp1 = nom1.compareTo(nom2);

				if (sComp1 != 0) {
					return sComp1;
				}

				Integer num1 = t1.a.getNum();
				Integer num2 = t2.a.getNum();
				return num1.compareTo(num2);
			}

		});
	}

	/**
	 * permet de calculer la surface d'un triangle
	 * 
	 * @return un double correspondant a la surface d'un triangle
	 */
	public double surface() {
		double surface = 0;
		double p = 0;
		p = (a.distanceEntrePoint(b) + a.distanceEntrePoint(c) + b.distanceEntrePoint(c)) / 2;

		surface = Math.sqrt(
				p * (p - a.distanceEntrePoint(c)) * (p - a.distanceEntrePoint(b)) * (p - b.distanceEntrePoint(c)));

		return surface;
	}

	public static List<Triangle> getListTriangle() {
		return listTriangle;
	}

	public static List<Point> getListPointsPourTriangle() {
		return listPointsPourTriangle;
	}

}
