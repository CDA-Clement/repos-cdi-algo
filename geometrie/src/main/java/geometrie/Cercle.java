package geometrie;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter

public class Cercle {
	private static Logger log= LoggerFactory.getLogger(Cercle.class);
	Point a;
	Double rayon;
	Double surface;

	public Cercle(Point aC, Double rayonC) {
		a=aC;
		rayon=rayonC;
		surface=this.Surface();

	}
	static private List<Cercle> listCercle = new ArrayList<Cercle>();

/**
 * permet de creer un cercle a partir des parametres suivants
 * @param nom
 * @param numero
 * @param rayon
 * @throws PointUtilise si on essaie de creer un cercle avec un point deja utilisé
 */
	public static void creerCercle(String nom, Integer numero, Double rayon) throws PointUtilise {
		
		if(Point.getListPoints().size()==0) {
			System.out.println("veuillez creer un point d'abord");
		}
		
		boolean trouve=false;
		for (Point point : Point.getListPoints()) {
			trouve=false;

			if(point.getNom().equals(nom)&&point.getNum()==numero){
				if(listCercle.size()==0) {
					Cercle c= new Cercle(point, rayon);
					listCercle.add(c);
					log.info("Cercle cree");
				}else {
					if(listCercle.contains(point)) {
						log.error("Point utilise");
						throw new PointUtilise();
					}
					else {
						Cercle c= new Cercle(point, rayon);
						listCercle.add(c);
						log.info("Cercle cree");

					}

				}
			}else {
				trouve=true;
			}

		}
		if(trouve==true) {
			System.out.println("veuillez saisir un point de la liste");
		}

	}


	
/**
 * lister les cercles créés
 * @param listcercle
 */
	public static void listerCercle(List<Cercle> listcercle) {

		Cercle.order(listcercle);
		for (int i = 0; i < listcercle.size(); i++) {
			System.out.println(listcercle.get(i).toString());

		}

	}
/**
 * permet de lister les cercles par surface, distance du point centrale, nom du point et num du point
 * @param listCercle
 */
	public static void order(List<Cercle> listCercle) {

		Collections.sort(listCercle, new Comparator<Cercle>() {

			public int compare(Cercle c1, Cercle c2) {

				Double surf1 = c1.Surface();
				Double surf2 = c2.Surface();
				int sComp2 = surf1.compareTo(surf2);

				if (sComp2 != 0) {
					return sComp2;
				} 
				Double dist1 = c1.a.distanceOrigin();
				Double dist2 = c2.a.distanceOrigin();
				int sComp = dist1.compareTo(dist2);

				if (sComp != 0) {
					return sComp;
				} 

				String nom1 = c1.a.getNom();
				String nom2 = c2.a.getNom();
				int sComp1 = nom1.compareTo(nom2);

				if (sComp1 != 0) {
					return sComp1;
				}

				Integer num1 = c1.a.getNum();
				Integer num2 = c2.a.getNum();
				return num1.compareTo(num2);
			}



		});
	}
/**
 * permet de calculer la surface d'un cercle
 * @return un double 
 */
	public double Surface(){
		return 3.14*Math.pow(this.rayon, 2);
	}

	public static List<Cercle> getListCercle() {
		return listCercle;
	}



}
