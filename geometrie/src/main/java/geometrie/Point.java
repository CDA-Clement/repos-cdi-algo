package geometrie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString

/**
 * 
 * @author clement
 *
 */


public class Point {
	private String nom;
	private Integer num;
	private Double abscisse;
	private Double ordonnee;
	private Double distance;
	static private List<Point> listPoints = new ArrayList<Point>();
	private static Logger log= LoggerFactory.getLogger(Point.class);

	public static List<Point> getListPoints() {
		return listPoints;
	}

	public Point(String nomP, Integer numP, Double abscisseP, Double ordonneP) {
		nom=nomP;
		num=numP;
		abscisse=abscisseP;
		ordonnee=ordonneP;
		distance=distanceOrigin();
	}
	
	/**
	 *  cette methode permet de creer un point a partir des parametres suivants
	 * @param nomP
	 * @param numP
	 * @param abscisseP
	 * @param ordonneP
	 * @throws PointExistant si le nom du point existe deja
	 */

	public static void creerPoint(String nomP, Integer numP, Double abscisseP, Double ordonneP) throws PointExistant  {
		boolean trouve=false;
		if(listPoints.size()==0) {
			Point p = new Point(nomP, numP, abscisseP, ordonneP);
			listPoints.add(p);
			log.info("Point cree");
		}else {
			for (Point point : listPoints) {
				trouve=false;

				if(point.nom.equals(nomP)&&point.num==numP){
					log.error("Point Existant");
					throw new PointExistant();
					
				}

				else {
					trouve=true;

				}
			}
			if(trouve==true){
				Point p = new Point(nomP, numP, abscisseP, ordonneP);
				listPoints.add(p);
				log.info("Point cree");
			}
		}
	}

	/**
	 * permet de lister tous les points créés
	 * @param une liste de type point
	 */
	
	public static void listerPoints(List<Point> listpoints) {
		Point.order(listpoints);
		for (int i = 0; i < listpoints.size(); i++) {
			System.out.println(listpoints.get(i).toString());

		}

	}

/**
 * permet de calculer la distance d'un point de l'origine 0.0
 * @return la distance du point  de type double
 */
	public double distanceOrigin() {
		double distance = 0;
		distance = Math.sqrt(Math.pow((this.getAbscisse()),2) + Math.pow((this.getOrdonnee()),2));
		return( Math.round(distance * 100.0) / 100.0);

	}

	public static void order(List<Point> listpoints) {

		Collections.sort(listpoints, new Comparator<Point>() {

			public int compare(Point p1, Point p2) {

				Double dist1 = p1.distanceOrigin();
				Double dist2 = p2.distanceOrigin();
				int sComp = dist1.compareTo(dist2);

				if (sComp != 0) {
					return sComp;
				} 

				String nom1 = p1.nom;
				String nom2 = p2.nom;
				int sComp1 = nom1.compareTo(nom2);

				if (sComp1 != 0) {
					return sComp1;
				}

				Integer num1 = p1.num;
				Integer num2 = p2.num;
				return num1.compareTo(num2);
			}});
	}

	
/**
 * permet de calculer la distance entre 2 points
 * @param un point de comparaison
 * @return un double qui correspond a la distance entre le point en cours et le point passé en parametre
 */
	public double distanceEntrePoint(Point param) {
		double distance = 0;
		distance = Math.sqrt(Math.pow((param.getAbscisse()-this.getAbscisse()),2) + Math.pow((param.getOrdonnee()-this.getOrdonnee()),2));
		return( Math.round(distance * 100.0) / 100.0);

	}


}
