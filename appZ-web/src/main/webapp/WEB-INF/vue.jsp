<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.List"%>
<%@page import="entity.Personne"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<body>
<%

List<Personne> personnes=(List<Personne>)request.getAttribute("personnes"); 
%>
<table>
<thead>
  <tr>
    <th>Nom</th>
    <th>Prenom</th>
    <th>Age</th>
  </tr>
  </thead>
  <tbody>
  <% int m =0;%>
  <% for(Personne p : personnes){
	  m++;
	  if(m%2==0){%>
		  <tr style="color:blue;"> 
		 
	<%}else{%>
	   <tr style="color:red;">
	 <%}%>

  <td>
  <%=p.getNom()%>
  </td>
   <td>
  <%=p.getPrenom()%>
  </td>
   <td>
  <%=p.getAge()%>
  </td>
  </tr>
  </tbody>
 <% }%> 
</table>
</body>
</html>