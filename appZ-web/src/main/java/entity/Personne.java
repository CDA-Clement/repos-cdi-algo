package entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Personne {
	
	String nom;
	String prenom;
	Integer age;
}
