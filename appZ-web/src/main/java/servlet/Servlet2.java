package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet2
 */

@WebServlet("/index.html")
public class Servlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/vue1.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nb1 = request.getParameter("param1");
		String nb2 = request.getParameter("param2");
		String signe = request.getParameter("signe");
		Integer total = null;
		if(signe.equalsIgnoreCase("+")) {
			total=Integer.parseInt(nb1)+Integer.parseInt(nb2);			
		}
		if(signe.equalsIgnoreCase("-")) {
			total=Integer.parseInt(nb1)-Integer.parseInt(nb2);			
		}
		if(signe.equalsIgnoreCase("/")) {
			total=Integer.parseInt(nb1)/Integer.parseInt(nb2);			
		}
		if(signe.equalsIgnoreCase("*")) {
			total=Integer.parseInt(nb1)*Integer.parseInt(nb2);			
		}
		
		request.setAttribute("param1", nb1);
		request.setAttribute("param2", nb2);
		request.setAttribute("total", total);
		this.getServletContext().getRequestDispatcher("/WEB-INF/vue1.jsp").forward(request, response);
	}

}
