package heritage;

public class Enseignant extends Personne {
	 public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	int salaire;

	public Enseignant(String nom, String prenom, int salaire) {
		super(nom, prenom);
		this.salaire=salaire;
	}

}
