
public enum Suite {
	ut, re, mi, fa, sol, la, si;

	public static void main(String[] args) {

		for (Suite suite : Suite.values()) {
			System.out.println(suite);
		}

		System.out.println(Suite.values().length);
		Suite.impair();
		Suite.last();


	}

	public static void impair() {
		int i=0;
		for (Suite suite : Suite.values()) {
			if(i%2>0) {
				System.out.println("value of odds index for suite= "+suite);	
			}
			i++;
		}
	}

	public static void last() {
		int i=0;
		for (Suite suite : Suite.values()) {
			if(i==Suite.values().length-1) {
				System.out.println("the last value of suite= "+suite);	
			}
			i++;
		}
	}

}
