
public enum Mois {
	
	janvier(31, "jan", "january"),
	fevrier(28, "fev", "february"), 
	mars(31, "mar", "march"), 
	avril(30,"avr", "april"), 
	mai(31,"mai", "may"), 
	juin(30, "jui", "june"), 
	juillet(31, "juil", "july"), 
	aout(31, "aou", "august"), 
	septembre(30, "sep", "september"), 
	octobre(31, "oct", "october"), 
	novembre(30, "nov", "november"), 
	decembre(31, "dec", "december");
	private int journee;
	private String abre;
	private String traduc;
	
	Mois(int days, String abv, String trad){
		this.journee=days;
		this.abre=abv;
		this.traduc=trad;
	}
	

	
	public static void main(String[] args) {
		for (Mois mois : Mois.values()) {
			System.out.println(mois.abre+"="+mois.toString()+"="+mois.traduc+"-"+mois.journee);
			
		}
	}
}


