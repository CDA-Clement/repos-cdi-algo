
public final class Suit {
	public final static Suit Spades = new Suit("SPADES");
	private final String symbol;
	public final static Suit Diamonds = new Suit("DIAMONDS");
	public final static Suit Heart = new Suit("HEART");
	public final static Suit Clubs = new Suit("CLUBS");
	
	private Suit(String symbol) {
		this.symbol=symbol;
	}

	public String getSymbol() {
		return symbol;
	}
	

}
