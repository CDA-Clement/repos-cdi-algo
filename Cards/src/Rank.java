
public class Rank {
	private final String value;;
	public final static Rank As = new Rank("AS");

	public final static Rank Two = new Rank("2");
	public final static Rank Three = new Rank("3");
	public final static Rank Four = new Rank("4");
	public final static Rank Five = new Rank("5");
	public final static Rank Six = new Rank("6");
	public final static Rank Seven = new Rank("7");
	public final static Rank Eigth = new Rank("8");
	public final static Rank Nine = new Rank("9");
	public final static Rank Ten = new Rank("10");
	public final static Rank Jack = new Rank("JACK");
	public final static Rank Queen = new Rank("QUEEN");
	public final static Rank King = new Rank("KING");

	
	
	private Rank(String value) {
		this.value=value;
	}
	
	public String getValue() {
		return value;
	}

}
