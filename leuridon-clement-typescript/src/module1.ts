export enum TypeDocuments {
    Livre,Magasine,Autre

}

export class Documents  {
    static list: Array<Documents>
    id: number;
    titre: string;
    type:TypeDocuments;
    disponible: boolean

    constructor(id: number, titre: string, type: TypeDocuments) {
        this.type=type;
        this.id = id;
        this.titre = titre;
        this.disponible = true;
        Documents.list.push(this);

    }
    
}


