import { TypeDocuments, Documents } from './module1.js';

export enum TypePersonne {
    Normal, Regulier

}
export class Personne {
    static listp: Array<Personne>

    id: number;
    nom: string;
    prenom: string;
    livre: Array<Documents>;
    type: TypePersonne;

    constructor(id: number, nom: string, prenom: string, type: TypePersonne) {

        this.id = id;
        this.nom = nom;
        this.prenom = prenom
        this.livre = new Array;
        this.type = type;
        Personne.listp.push(this);
    }


}

export function emprunter(p: Personne, d: Documents) {
    if (d.disponible === false) {
        console.log("produit non disponible")
    } else {
        if (p.type === TypePersonne.Normal) {
            if (p.livre.length === 0) {
                p.livre.push(d);
                d.disponible = false;
                console.log("vous venez d'emprunter un livre")
            } else {
                console.log("vous avez deja emprunté un livre")
            }
        }
        else {
            if (p.livre.length <= 5) {
                p.livre.push;
                d.disponible = false;
                console.log("vous venez d'emprunter un livre")
            } else {
                console.log("vous avez deja atteint votre limite d'emprunt")
            }
        }
    }
}

export function retour(p: Personne, ...id: number[]) {
    if (p.type === TypePersonne.Normal) {
        p.livre[0].disponible = true;
        p.livre = [];

    } else {
        let j = 0;
        p.livre.forEach(element => {
            for (let i = 0; id.length - 1; i++) {
                if (element.id === id[i]) {
                    p.livre[j].disponible = true;
                    p.livre.splice(j, 1);

                }
            }
            j++;

        });
    }
}


