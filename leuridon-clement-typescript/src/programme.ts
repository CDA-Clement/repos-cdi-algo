import { TypeDocuments, Documents } from './module1.js';
import{TypePersonne, Personne, emprunter,retour} from './module2.js'
function programme(){

    let livre1 = new Documents(1, 'titre1', TypeDocuments.Livre);
    let livre2 = new Documents(2, 'titre2', TypeDocuments.Livre);
    let livre3 = new Documents(3, 'titre3', TypeDocuments.Magasine);
    let personne1 = new Personne(1, 'nom1', 'prenom1', TypePersonne.Normal);
    emprunter(personne1, livre1);
    emprunter(personne1, livre2);
    retour(personne1,1)
    emprunter(personne1,livre3)
    
}

programme();
