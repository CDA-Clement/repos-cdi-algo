"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var TypePersonne = /** @class */ (function () {
    function TypePersonne(type) {
        this.type = type;
    }
    return TypePersonne;
}());
var Personne = /** @class */ (function (_super) {
    __extends(Personne, _super);
    function Personne(id, nom, prenom, type) {
        var _this = _super.call(this, type) || this;
        _this.id = id;
        _this.nom = nom;
        _this.prenom = prenom;
        _this.livre = 0;
        return _this;
    }
    return Personne;
}(TypePersonne));
function emprunter(p, d) {
    if (p.type === "normal" && p.livre === 0 && d.disponible === true && d.type) {
        console.log("vous venez d'emprunter un livre");
    }
}
