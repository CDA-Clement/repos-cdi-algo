"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TypeDocuments;
(function (TypeDocuments) {
    TypeDocuments[TypeDocuments["Livre"] = 0] = "Livre";
    TypeDocuments[TypeDocuments["Magasine"] = 1] = "Magasine";
    TypeDocuments[TypeDocuments["Autre"] = 2] = "Autre";
})(TypeDocuments = exports.TypeDocuments || (exports.TypeDocuments = {}));
var Documents = /** @class */ (function () {
    function Documents(id, titre, type) {
        this.type = type;
        this.id = id;
        this.titre = titre;
        this.disponible = true;
    }
    return Documents;
}());
exports.Documents = Documents;
