package stagaire;

import java.util.HashMap;
import java.util.Map;

public class TestHashMap {
    public static void main(String[] args) {
        Stagiaire s1 = new Stagiaire(0, 5, "st1");
        Stagiaire s2 = new Stagiaire(1, 15, "st2");
        Stagiaire s3 = new Stagiaire(0, 5, "st3");
        Stagiaire s4 = new Stagiaire(1, 25, "st4");
        
        HashMap<Stagiaire, Integer> notes = new HashMap<>();
        notes.put(s1, 10);
        notes.put(s2, 20);
        notes.put(s3, 30);
        notes.put(s4, 40);
        
        afficherNotes(notes);
    }
    private static void afficherNotes(HashMap<Stagiaire, Integer> notes) {
        for(Map.Entry<Stagiaire, Integer> e : notes.entrySet()) {
            System.out.println(e.getKey()+"- note : "+e.getValue());
        }
    }
    
}