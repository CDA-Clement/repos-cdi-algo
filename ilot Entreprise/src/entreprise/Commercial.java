package entreprise;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Commercial extends Employe {
    // attributes
    private double sommeDesventes;
    private static double interessement = 10;
    // methods
 
    private double calculePrime() {
        return this.sommeDesventes*(interessement/100);
    }
    public double calculeSalaireCommercial() {
        return Employe.getSalaire() * this.getIndiceSalaire() + this.calculePrime();
    }
    public static void remiseZeroVentes() {
        for (Employe employed : Employe.getTousLesEmployes()) {
            if (employed instanceof Commercial) {
                ((Commercial) employed).setSommeDesventes(0);
            }
        }
    }
    
    
    // constructors
    
    public Commercial(String nom, double indiceSalarial, double sommeDesventes) {
        super(nom, indiceSalarial);
        this.sommeDesventes = sommeDesventes;
    }
    

	//----- modifier somme des vznte d un Commercial
    
    
    
    public static void creerCommercial(String nomEmploye, double indiceSalEmploye , double vente) throws FileNotFoundException {
        Commercial e = new Commercial(nomEmploye, indiceSalEmploye, vente);
        boolean ajouter = false;
        if(Employe.getTousLesEmployes().size()>0) {
            for (Employe employe : Employe.getTousLesEmployes()) {
                if(employe.getNom().equalsIgnoreCase(nomEmploye)==true) {
                    System.out.println("Attention un employ� portant le meme nom existe deja");
                    System.out.println("Voulez vous vraiment ajouter cet employ�, (oui, non)");
                    Scanner sc = new Scanner(System.in);
                    
                    String confirm = sc.next();
                    if (confirm.equalsIgnoreCase("oui")){
                        Employe.getTousLesEmployes().remove(employe);
                        Employe.getTousLesEmployes().add(e);
                        System.out.println("l'employ� a bien �t� promu Commercial");
                        ajouter = true;
                    break;
                    }
                    else{
                        ajouter = true;
                        System.out.println("l'employ� n'a pas �t� ajout�");
                break;
                }
                }else {
                    ajouter = false;
                        
                }
                
                
            }
            if(ajouter==false) {
                Employe.getTousLesEmployes().add(e);
                System.out.println("bien ajout�");   
            }
        }
        if(Employe.getTousLesEmployes().size()==0) {
            Employe.getTousLesEmployes().add(e);
            System.out.println("bien ajout�");
        }
    }
    
    @Override
    public String toString() {
        return "[Commercial] " + super.toString() + " sommeDesventes = " + sommeDesventes;
    }
    
    
    

	public static void modifSommeVente(int matricommercial, double newSommeVente) {

		for(Employe el : getTousLesEmployes()) {
			if(el.getMonmat() == matricommercial && el instanceof Commercial) {
				Commercial com = (Commercial) el;
				com.setSommeDesventes(newSommeVente);
			}
		}

	}


	public static void modifInteret(double interet) {
		Commercial.setInteressement(interet);
		System.out.println("Le nouveau taux d'interessement est de "+ interet);
	}
    
    // getters & setters
    
    public double getSommeDesventes() {
        return sommeDesventes;
    }
    public static double getInteressement() {
        return interessement;
    }
    public static void setInteressement(double interessement) {
        Commercial.interessement = interessement;
    }
    public void setSommeDesventes(double sommeDesventes) {
        this.sommeDesventes = sommeDesventes;
    }
}