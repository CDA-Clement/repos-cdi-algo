package entreprise;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

public class ResponsableCom extends Commercial {
    
    // attrinbut
        private HashSet<Employe> listeInferieursHierarchiquesResCom;
        // constructeur
        public ResponsableCom (String nom, int indiceSalarial, double sommeDesventes) {
            super(nom, indiceSalarial, sommeDesventes);
            this.listeInferieursHierarchiquesResCom = new HashSet<Employe>();
        }
        
        
        public static void creerResponsableCommercial(String nomEmploye, int indiceSalEmploye , double vente) throws FileNotFoundException {
        	ResponsableCom e = new ResponsableCom(nomEmploye, indiceSalEmploye, vente);
            boolean ajouter = false;
            if(Employe.getTousLesEmployes().size()>0) {
                for (Employe employe : Employe.getTousLesEmployes()) {
                    if(employe.getNom().equalsIgnoreCase(nomEmploye)==true) {
                        System.out.println("Attention un employ� portant le meme nom existe deja");
                        System.out.println("Voulez vous vraiment ajouter cet employ�, (oui, non)");
                        Scanner sc = new Scanner(System.in);
                        
                        String confirm = sc.next();
                        if (confirm.equalsIgnoreCase("oui")){
                            Employe.getTousLesEmployes().remove(employe);
                            Employe.getTousLesEmployes().add(e);
                            System.out.println("l'employ� a bien �t� promu responsable Commercial");
                            ajouter = true;
                        break;
                        }
                        else{
                            ajouter = true;
                            System.out.println("l'employ� n'a pas �t� ajout� en tant que Responsable Commercial");
                    break;
                    }
                    }else {
                        ajouter = false;
                            
                    }
                    
                    
                }
                if(ajouter==false) {
                    Employe.getTousLesEmployes().add(e);
                    System.out.println("bien ajout�");   
                }
            }
            if(Employe.getTousLesEmployes().size()==0) {
                Employe.getTousLesEmployes().add(e);
                System.out.println("bien ajout�");
            }
        }
        public String toString() {
            
            return "[Responsable ] " + super.toString();
        }
        
        // get set
        public HashSet<Employe> getListeInferieursHierarchiquesResCom() {
            return listeInferieursHierarchiquesResCom;
        }
    
}
