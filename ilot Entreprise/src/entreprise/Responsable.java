package entreprise;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

public class Responsable extends Employe {

	// attrinbut
	private HashSet<Employe> listeInferieursHierarchiques;


	// constructeur

	public Responsable(String Nom, double IndiceSalarial){
		super(Nom,IndiceSalarial);
		this.listeInferieursHierarchiques = new HashSet<Employe>();
	}

	//--------- methodess
	
	//----------------------------------------                                                                         methode responsable


	
	public static void creerResponsable(String nomEmploye, double indiceSalEmploye ) throws FileNotFoundException {
        Responsable e = new Responsable(nomEmploye, indiceSalEmploye);
        boolean ajouter = false;
        if(Employe.getTousLesEmployes().size()>0) {
            for (Employe employe : Employe.getTousLesEmployes()) {
                if(employe.getNom().equalsIgnoreCase(nomEmploye)==true) {
                    System.out.println("Attention un employ� portant le meme nom existe deja");
                    System.out.println("Voulez vous vraiment promouvoir cet employ� en tant que Responsable, (oui, non)");
                    Scanner sc = new Scanner(System.in);
                    
                    String confirm = sc.next();
                    if (confirm.equalsIgnoreCase("oui")){
                        Employe.getTousLesEmployes().remove(employe);
                        Employe.getTousLesEmployes().add(e);
                        System.out.println("l'employ� a bien �t� promu Responsable");
                        ajouter = true;
                    break;
                    }
                    else{
                        ajouter = true;
                        System.out.println("l'employ� n'a pas �t� ajout�");
                break;
                }
                }else {
                    ajouter = false;
                        
                }
                
                
            }
            if(ajouter==false) {
                Employe.getTousLesEmployes().add(e);
                System.out.println("bien ajout�");   
            }
        }
        if(Employe.getTousLesEmployes().size()==0) {
            Employe.getTousLesEmployes().add(e);
            System.out.println("bien ajout�");
        }
    }
    
    public String toString() {
        
        return "[Responsable] " + super.toString();
    }
	
	
	


		//  ajouter un employer a la liste des soufifre	

		
		public static void ajoutSousEmpl(int matriculeResponsable, int matriculeE) {
			//cherche le responsable
			for(Employe e : getTousLesEmployes()) {
				//si  il est un responsable
				if(e.getMonmat() == matriculeResponsable && e instanceof Responsable && matriculeResponsable != matriculeE) {
					Responsable res = (Responsable) e;
					//cherche l' employer dans la liste des employer
					for(Employe r : getTousLesEmployes()) {
						// si on trouve l'employer et qu il n est pas dans la liste du responsable on l'ajoute
						if(r.getMonmat() == matriculeE && !res.getListeInferieurs().contains(r)) {
							res.getListeInferieurs().add(r);
							System.out.println("bien ajouter");
						}
					}
				}else {
					System.out.println("tu ne peut pas ajouter ton supperieur");
				}
			}
		}







		//                                              supprimer un employ� de la liste des souffrifre
		public static void retireUnEmp(int matriculRes, int matriculEmp) {
			for (Employe e : getTousLesEmployes()) {
				if(e.getMonmat() == matriculRes && e instanceof Responsable) {
					Responsable res = (Responsable) e;
					for(Employe a : res.getListeInferieurs()) {
						if (a.getMonmat() == matriculEmp) {
							res.getListeInferieurs().remove(a);
							System.out.println("l'employer a �t� suprimer");
						}
					}
				}else {
					System.out.println("cette employer n'existe pas");
				}
			}
		}
		//                                                    afficher la liste des sous employer d un responsable
		public static void afficheListSoufiffre(int matriculRes) {
			for(Employe z : getTousLesEmployes()) {
				if(z.getMonmat() == matriculRes && z instanceof Responsable) {
					Responsable res = (Responsable) z;
					for (Employe h : res.getListeInferieurs()) {
						System.out.println(" Empluy� n� : " +z.getMonmat()+" Nom : "+h.getNom()+ " Salaire : "+ h.getSalaire());
					}
				}
			}
		}
		//--------------     supprimer un reponsable et refiler ses larbin a so supperieur
		public static void supRes(int matrespon) {
			for(Employe resDuRes : getTousLesEmployes()) {
				if (resDuRes instanceof Responsable) {
					for(Employe arp : ((Responsable) resDuRes).getListeInferieurs()) {
						if(matrespon == arp.getMonmat()) {
							Responsable a = (Responsable)arp;
							if(!a.getListeInferieurs().isEmpty()) {
								for (Employe fr : a.getListeInferieurs()) {
									((Responsable) resDuRes).getListeInferieurs().add(fr);
								}
								getTousLesEmployes().remove(arp);
							}
						}
					}

				}
			}
		}


	//                   set et get

	public HashSet<Employe> getListeInferieurs() {
		return listeInferieursHierarchiques;
	}

	

}