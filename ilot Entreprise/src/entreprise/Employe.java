package entreprise;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
public class Employe {
	private static ArrayList<Employe> tousLesEmployes= new ArrayList<Employe>();
	private static double salaire = 1000;
	public static void setSalaire(double salaire) {
		Employe.salaire = salaire;
	}
	private static int matricule;
	private String nom;
	private int monmat;
	private double indiceSalaire;
	public Employe (String nom, double indice) {    
		matricule++;
		this.nom=nom;
		this.indiceSalaire=indice;
		monmat= matricule;
	}
	public static double calSalaire(int mat) {  
		boolean pastrouve=false;
		if(tousLesEmployes.size()==0) {
			System.out.println("Ton entreprise est vide, il faut recruter si tu veux afficher le salaire d'un employ� pauvre con");
		}
		System.out.println();
		for (Employe employe : tousLesEmployes) {
			pastrouve=false;
			if(mat==employe.getMonmat()) {
				if(employe instanceof Commercial==true) {
					System.out.println(employe.toString()+" le salaire du commercial primes incluses "+ ((Commercial) employe).calculeSalaireCommercial()+" �");
				}else {
					double salaire=0;
					salaire = getSalaire()*employe.indiceSalaire;
					System.out.println("    "+employe.toString()+" "+salaire);
					return salaire;
				}
			}else { pastrouve = true;
			}
		}
		if(pastrouve==true) {
			System.out.println("Aucun salarie ne correspond au matricule rentr�, Veuillez verifier dans la liste le bon matricule");
			System.out.println();
			for (Employe employe : tousLesEmployes) {
				System.out.println(employe.toString()); 
			}
		}
		return 0;
	}
	public static void creerEmploye(String nomEmploye, double indiceSalEmploye ) throws FileNotFoundException {
		Employe e = new Employe(nomEmploye, indiceSalEmploye);
		boolean ajouter = false;
		if(tousLesEmployes.size()>0) {
			for (Employe employe : tousLesEmployes) {
				if(employe.nom.equalsIgnoreCase(nomEmploye)==true) {
					System.out.println("Attention un employ� portant le meme nom existe deja");
					System.out.println("Voulez vous vraiment ajouter cet employ�, (oui, non)");
					Scanner sc = new Scanner(System.in);
					String confirm = sc.next();
					if (confirm.equalsIgnoreCase("oui")){
						tousLesEmployes.add(e);
						System.out.println("bien ajout�");
						ajouter = true;
						break;
					}
					else{
						ajouter = true;
						System.out.println("l'employ� n'a pas �t� ajout�");
						break;
					}
				}else {
					ajouter = false;

				}


			}
			if(ajouter==false) {
				tousLesEmployes.add(e);
				System.out.println("bien ajout�");   
			}
		}
		if(tousLesEmployes.size()==0) {
			tousLesEmployes.add(e);
			System.out.println("bien ajout�");
		}

	}


	public static void supprimeEmploye(double mat) {
		for (Employe employe : tousLesEmployes) {
			if(employe.monmat==mat) {
				tousLesEmployes.remove(employe);
				System.out.println("L'employe a bien ete supprime");
				break;
			}

		}
	}



	public static void afficherSalaire(int mat) {
		boolean pastrouve=false;
		if(tousLesEmployes.size()==0) {
			System.out.println("Ton entreprise est vide, il faut recruter si tu veux afficher le salaire d'un employ� pauvre con");
		}
		System.out.println();
		for (Employe employe : tousLesEmployes) {
			pastrouve=false;
			if(mat==employe.getMonmat()) {
				if(employe instanceof Commercial==true) {
					System.out.println(employe.toString()+" le salaire du commercial primes incluses "+ ((Commercial) employe).calculeSalaireCommercial()+" �");
				}else {
					double salaire = employe.getIndiceSalaire()*getSalaire();
					System.out.println("    "+employe.toString()+salaire);
					break;}
			}else { pastrouve = true;
			}
		}
		if(pastrouve==true) {
			System.out.println("Aucun salarie ne correspond au matricule rentr�, Veuillez verifier dans la liste le bon matricule");
			System.out.println();
			for (Employe employe : tousLesEmployes) {
				System.out.println(employe.toString()); 
			}
		}
	}
	public static void masseSalariale() {
		double total = 0;
		if(tousLesEmployes.size()==0) {
			System.out.println("Ton entreprise est vide, il faut recruter si tu veux afficher le salaire d'un employe");
		}
		for (Employe employe : tousLesEmployes) {
			total+=employe.getIndiceSalaire()*getSalaire();
		}
		System.out.println("    "+"la masse Salariale est de "+total);  
	}
	public static void modifIndiceSalarial(int m, double d) {
		boolean pastrouve = false;
		for (Employe employe : tousLesEmployes) {
			pastrouve=false;
			if(m==employe.monmat) {
				employe.setIndiceSalaire(d);
				break;
			}else { pastrouve = true;
			}
		}
		if(pastrouve==true) {
			System.out.println("Aucun salarie ne correspond au matricule rentre, Veuillez verifier dans la liste le bon matricule");
			System.out.println();
			for (Employe employe : tousLesEmployes) {
				System.out.println(tousLesEmployes.toString()); 
			}
		}
	}
	public static void modifSalairebase(double nouveau) {
		setSalaire(nouveau);
	}
	public static void afficher() {
		for (Employe employe : tousLesEmployes) {
			if(employe instanceof Responsable) {
				System.out.println( employe);
			}
			else if(employe instanceof Commercial) {
				System.out.println( employe);   
			}
			else if(employe instanceof ResponsableCom) {
				System.out.println( employe);
			}
			else {
				System.out.println(employe); 
			}

		}
	}

	@Override
	public String toString() {
		return "Employe [nom=" + nom + ", monmat=" + monmat + ", indiceSalaire=" + indiceSalaire + "]";
	}
	public static ArrayList<Employe> getTousLesEmployes() {
		return tousLesEmployes;
	}
	public static void setTousLesEmployes(ArrayList<Employe> tousLesEmployes) {
		Employe.tousLesEmployes = tousLesEmployes;
	}
	public static int getMatricule() {
		return matricule;
	}
	public static void setMatricule(int matricule) {
		Employe.matricule = matricule;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getMonmat() {
		return monmat;
	}
	public void setMonmat(int monmat) {
		this.monmat = monmat;
	}
	public double getIndiceSalaire() {
		return indiceSalaire;
	}
	public void setIndiceSalaire(double indiceSalaire) {
		this.indiceSalaire = indiceSalaire;
	}
	public static double getSalaire() {
		return salaire;
	}
}

