package space;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.PopupMenu;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Timer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.imageio.*;
import java.awt.image.*;
import java.io.*;


public class Board2  extends JPanel implements Runnable,KeyListener 
{
	boolean start=false;
	Random random= new Random();
	int r = getRandomNumberInRange(1, 3);
	private BufferedImage ship;
	private BufferedImage shipRight;
	private BufferedImage shipLeft;
	private BufferedImage background;
	private Image gameOver;
	private BufferedImage asteroid;
	private BufferedImage asteroidfire;
	private BufferedImage asteroidfire1;
	private BufferedImage asteroid1;
	private BufferedImage crash;
	private static ArrayList<String> list = new ArrayList<String>();
	boolean ingame = true;
	private Dimension d;
	int BOARD_WIDTH=600;
	int BOARD_HEIGHT=800;
	int score = 0;
	int crashnb1 =0;
	int crashnb2 =0;
	int crashnb3 =0;
	int firedisplay=0;
	int life=5;
	BufferedImage img;
	private Thread animator;
	Player p;
	Asteroid [] a = new Asteroid[3];
	AsteroidFire [] b = new AsteroidFire[3];
	Starter starter;
	int vitesse=15;
	boolean debut=false;
	int difficulte =0;
	Background back;
	int motion = 0;
	int motion1 = (BOARD_HEIGHT)*-1;

	private Board2 leThis;

	public Board2(Starter s) throws IOException{
		leThis = this;
		starter = s;
		shipRight = ImageIO.read(Board2.class.getResource("/image/shipright.png"));
		shipLeft= ImageIO.read(Board2.class.getResource("/image/shipleft.png"));
		background = ImageIO.read(Board2.class.getResource("/image/space.png"));
		ship = ImageIO.read(Board2.class.getResource("/image/spaceship.png"));
		asteroid = ImageIO.read(Board2.class.getResource("/image/asteroid.png"));
		asteroid = ImageIO.read(Board2.class.getResource("/image/asteroid1.png"));
		asteroidfire = ImageIO.read(Board2.class.getResource("/image/fire.png"));
		asteroidfire1 = ImageIO.read(Board2.class.getResource("/image/fire1.png"));
		crash = ImageIO.read(Board2.class.getResource("/image/crash1.png"));
		gameOver = ImageIO.read(Board2.class.getResource("/image/gm.png"));

		//				shipRight = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/shipright.png"));
		//				shipLeft= ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/shipleft.png"));
		//				background = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/space.png"));
		//				ship = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/spaceship.png"));
		//				asteroid = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/asteroid.png"));
		//				asteroidfire = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/fire.png"));
		//				crash = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/broken.png"));

		addKeyListener(this);
		setFocusable(true);
		p = new Player(BOARD_WIDTH/2, BOARD_HEIGHT-120, 5);
		d = new Dimension(BOARD_WIDTH, BOARD_HEIGHT);

		for (int i = 0; i < a.length; i++) {
			int ax =random.nextInt(BOARD_WIDTH);
			int ay=BOARD_HEIGHT-1;
			a[i]=new Asteroid(ax, ay, 10);


		}
		for (int i = 0; i < b.length; i++) {
			int ax =random.nextInt(BOARD_WIDTH);
			int ay=BOARD_HEIGHT-1;
			b[i]=new AsteroidFire(ax, ay, 10);


		}


		if (animator == null || !ingame) {
			animator = new Thread(this);
			animator.start();
		}


		setDoubleBuffered(true);
		System.err.println("init");
	}






	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}



	public void paint(Graphics g){
		
		super.paint(g);

		if(motion<d.height) {
			motion+=4;
		}else if(motion>=d.height){
			motion=(d.height-25)*-1;
		}
		if(motion1<d.height) {
			motion1+=4;
		}else if(motion1>=d.height){
			motion1=(d.height-25)*-1;
		}

		g.drawImage(background, 0, motion, d.width, d.height, null);
		g.drawImage(background, 0, motion1, d.width, d.height, null);

		if(difficulte>3 ) {
			System.err.println("difficult");
			vitesse-=2;
			difficulte = 0;

		}
		if(crashnb1>=3) {
			life--;
			crashnb1=0;
		}

		if(crashnb2>=4) {
			life--;
			crashnb2=0;
		}

		if(crashnb3>=3) {
			life--;
			crashnb3=0;
		}


		moveAsteroid();
		moveAsteroidfire();
		newWave();

		if(start==false) {
			g.drawImage(ship, p.x, p.y, 80, 80, null);
			start=true;
		}

		//		System.err.println("boucle");
		//		System.err.println(BOARD_HEIGHT);
		System.err.println("r "+r);


		//g.setColor(Color.white);
		//g.fillRect(0, 0, d.width, d.height);
		//g.fillOval(x,y,r,r);


		//player
		//g.setColor(Color.black);
		//g.fillRect(p.x, p.y, 30, 30);
		if(p.moveLeft==false && p.moveRight==false) {
			g.drawImage(ship, p.x, p.y, 80, 80, null);
		}

		if(p.moveRight==true) {
			g.drawImage(shipRight, p.x, p.y, 80, 80, null);
			if(p.x>=BOARD_WIDTH-80) {
				p.moveRight=false;
				//System.err.println("ici");	
			}else {
				p.x+=p.speed;
			}
		}



		if(p.moveLeft==true) {
			g.drawImage(shipLeft, p.x, p.y, 80, 80, null);
			if(p.x==0) {
				p.moveLeft=false;

			}else {
				//	System.err.println("++++++++++++++++++++++++++++++++++++++ici");
				p.x-=p.speed;
			}

		}


		if(p.moveUp==true) {
			if(p.y==0) {
				p.moveUp=false;
				//				System.err.println("ici");	
			}else {
				p.y-=p.speed;
			}
		}


		if(p.moveDown==true) {
			if(p.y==BOARD_HEIGHT-100) {
				p.moveDown=false;
				//System.err.println("ici");	
			}else {
				p.y+=p.speed;
			}
		}
System.err.println("ra"+ r);


		if(r==1) {
			for (int i = 0; i < a.length; i++) {
				//	g.setColor(Color.red);
				g.drawImage(asteroid,a[i].x, a[i].y, 80, 80, null);
				//	g.fillRect(a[i].x, a[i].y, 80, 80);
				//				System.err.println("test CRASH");
				//				System.out.println(p.x+" "+p.y);
				//				System.out.println("a[0] "+a[0].x+" "+ a[0].y);
				//				System.out.println("a[1] "+a[1].x+" "+ a[1].y);
				//				System.out.println("a[2] "+a[2].x+" "+ a[2].y);

				if(p.x>=(a[0].x)-10 &&p.x<=(a[0].x)+30 && p.y>=(a[0].y)-1 && p.y<=(a[0].y)+7) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 140, 140, null);
					try {
						animator.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					crashnb1+=1;


					System.err.println("crash r1 a de 0");

				}else if (p.x>=(a[1].x)-10 &&p.x<=(a[1].x)+30 && p.y>=(a[1].y)-1 && p.y<=(a[1].y)+7) {
					System.err.println("crash r1 a de 1");
					g.drawImage(crash, p.x, p.y, 140, 140, null);
					try {
						animator.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					crashnb1+=1;



				}else if (p.x>=(a[2].x)-10 &&p.x<=(a[2].x)+30 && p.y>=(a[2].y)-1 && p.y<=(a[2].y)+7) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 140, 140, null);
					try {
						animator.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					crashnb1+=1;
					System.err.println("crash r1 a de 2");



				}
			}
		}

		if(r==2) {
			for (int i = 0; i < b.length; i++) {
				if(firedisplay ==1) {
					g.drawImage(asteroidfire,b[i].x, b[i].y, 90, 90, null);
					firedisplay =0;
				}
				
				else {
					g.drawImage(asteroidfire1,b[i].x, b[i].y, 90, 90, null);
					firedisplay =1;
				}
				//	g.setColor(Color.red);
				
				
				//	g.fillRect(a[i].x, a[i].y, 80, 80);
				//				System.err.println("test CRASH");
				//				System.out.println(p.x+" "+p.y);
				//				System.out.println("b[0] "+b[0].x+" "+ b[0].y);
				//				System.out.println("b[1] "+b[1].x+" "+ b[1].y);
				//				System.out.println("b[2] "+b[2].x+" "+ b[2].y);

				if(p.x>=(b[0].x)-30 &&p.x<=(b[0].x)+30 && p.y>=(b[0].y)-1 && p.y<=(b[0].y)+7) {
					System.err.println("crash en r2 b de 0");
					g.drawImage(crash, p.x, p.y, 140, 140, null);
					g.drawImage(asteroidfire1,b[i].x, b[i].y, 90, 90, null);
					try {
						animator.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					crashnb2+=1;



				}else if (p.x>=(b[1].x)-30 &&p.x<=(b[1].x)+30 && p.y>=(b[1].y)-1 && p.y<=(b[1].y)+7) {
					System.err.println("crash en r2 b de 1");
					g.drawImage(crash, p.x, p.y, 140, 140, null);
					g.drawImage(asteroidfire1,b[i].x, b[i].y, 90, 90, null);
					try {
						animator.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					crashnb2+=1;


				}else if (p.x>=(b[2].x)-30 &&p.x<=(b[2].x)+30 && p.y>=(b[2].y)-1 && p.y<=(b[2].y)+7) {
					System.err.println("crash en r2 b de 3");
					g.drawImage(crash, p.x, p.y, 140, 140, null);
					try {
						animator.sleep(30);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					crashnb2+=1;


				}
			}
		}

		if(r==3) {
			//	g.setColor(Color.red);
			
				g.drawImage(asteroid,a[0].x, a[0].y, 80, 80, null);
				g.drawImage(asteroid,a[1].x, a[1].y, 80, 80, null);
				if(firedisplay ==1) {
					g.drawImage(asteroidfire,b[0].x, b[0].y, 90, 90, null);
					firedisplay =0;
				}
				
				else {
					g.drawImage(asteroidfire1,b[0].x, b[0].y, 90, 90, null);
					firedisplay =1;
				}
				
			
			

			//	g.fillRect(a[i].x, a[i].y, 80, 80);

			//			System.err.println("test CRASH");
			//			System.out.println(p.x+" "+p.y);
			//			System.out.println("a[0] "+a[0].x+" "+ a[0].y);
			//			System.out.println("a[1] "+a[1].x+" "+ a[1].y);
			//			System.out.println("b[0] "+b[0].x+" "+ b[0].y);

			if(p.x>=(a[0].x)-30 &&p.x<=(a[0].x)+30 && p.y>=(a[0].y)-1  &&p.y<=(a[0].y)+7) {
				System.err.println("crash en r3 a de 0");
				g.drawImage(crash, p.x, p.y, 140, 140, null);
				try {
					animator.sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				crashnb3+=1;

			}else if (p.x>=(a[1].x)-30 &&p.x<=(a[1].x)+30&& p.y>=(a[1].y)+1 && p.y<=(a[1].y)+7) {
				System.err.println("crash en r3 a de 1");
				g.drawImage(crash, p.x, p.y,140, 140, null);
				crashnb3+=1;
				try {
					animator.sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}else if (p.x>=(b[0].x)-30 &&p.x<=(b[0].x)+30&& p.y>=(b[0].y)+1  &&  p.y<=(b[0].y)+7) {

				System.err.println("crash en r3 b de 0");
				g.drawImage(crash, p.x, p.y,140, 140, null);
				crashnb3+=2;
				try {
					animator.sleep(30);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}


		if (ingame) {
			// g.drawImage(img,0,0,300,300 ,null);

		}
		Toolkit.getDefaultToolkit().sync();
		g.dispose();
		System.err.println("rb"+ r);
		
	}
	






	public void moveAsteroid() {
		for (int i = 0; i < a.length; i++) {
			if(a[i].moveDown==true) {
				a[i].y+=a[i].speed/2;
				//System.err.println("a de i = "+a[i].y);
			}
		}

	}

	public void moveAsteroidfire() {
		for (int i = 0; i < a.length; i++) {
			if(b[i].moveDown==true) {
				b[i].y+=a[i].speed/3;
			}

		}
	}

	public void newWave() {


		if(r==2 && b[0].y>BOARD_HEIGHT&& b[1].y>BOARD_HEIGHT&& b[2].y>BOARD_HEIGHT) {

			b[0].y=getRandomNumberInRange(-400, 10);
			b[1].y=getRandomNumberInRange(-400, 10);
			b[2].y=getRandomNumberInRange(-400, 10);
			b[0].x= random.nextInt(BOARD_WIDTH-80);
			b[1].x=random.nextInt(BOARD_WIDTH-80);
			b[2].x= random.nextInt(BOARD_WIDTH-80);
			a[0].y=getRandomNumberInRange(-400, 10);
			a[1].y=getRandomNumberInRange(-400, 10);
			a[2].y=getRandomNumberInRange(-400, 10);
			a[0].x= random.nextInt(BOARD_WIDTH-80);
			a[1].x= random.nextInt(BOARD_WIDTH-80);
			a[2].x= random.nextInt(BOARD_WIDTH-80);
			r= getRandomNumberInRange(1, 3);
			score+=3;
			difficulte++;
			System.out.println("reset r2");


		}







		if(r==1 && a[0].y>BOARD_HEIGHT && a[1].y>BOARD_HEIGHT&& a[2].y>BOARD_HEIGHT) {
			System.out.println("reset r1");
			b[0].y=getRandomNumberInRange(-400, 10);
			b[1].y=getRandomNumberInRange(-400, 10);
			b[2].y=getRandomNumberInRange(-400, 10);
			b[0].x= random.nextInt(BOARD_WIDTH-80);
			b[1].x=random.nextInt(BOARD_WIDTH-80);
			b[2].x= random.nextInt(BOARD_WIDTH-80);
			a[0].y=getRandomNumberInRange(-400, 10);
			a[1].y=getRandomNumberInRange(-400, 10);
			a[2].y=getRandomNumberInRange(-400, 10);
			a[0].x= random.nextInt(BOARD_WIDTH-80);
			a[1].x= random.nextInt(BOARD_WIDTH-80);
			a[2].x= random.nextInt(BOARD_WIDTH-80);
			r= getRandomNumberInRange(1, 3);
			score+=3;
			difficulte++;



		}
		//a voir cette ligne
		if(r==3 && a[0].y>BOARD_HEIGHT&& a[1].y>BOARD_HEIGHT&& b[0].y>BOARD_HEIGHT) {
			b[0].y=getRandomNumberInRange(-400, 10);
			b[1].y=getRandomNumberInRange(-400, 10);
			b[2].y=getRandomNumberInRange(-400, 10);
			b[0].x= random.nextInt(BOARD_WIDTH-80);
			b[1].x=random.nextInt(BOARD_WIDTH-80);
			b[2].x= random.nextInt(BOARD_WIDTH-80);
			a[0].y=getRandomNumberInRange(-400, 10);
			a[1].y=getRandomNumberInRange(-400, 10);
			a[2].y=getRandomNumberInRange(-400, 10);
			a[0].x= random.nextInt(BOARD_WIDTH-80);
			a[1].x= random.nextInt(BOARD_WIDTH-80);
			a[2].x= random.nextInt(BOARD_WIDTH-80);
			r= getRandomNumberInRange(1, 3);
			score+=3;
			difficulte++;
			System.out.println("reset r3");

		}



	}


	//	
	//	
	//												FIN DU CODE JEU
	//	
	//	
	public void run() {
//      try {
//          Starter.JETON1.wait();
//      } catch (InterruptedException e1) {
//          // TODO Auto-generated catch block
//          e1.printStackTrace();
//      }
        
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa");
        
        long beforeTime, timeDiff, sleep;
        beforeTime = System.currentTimeMillis();
        int animationDelay = vitesse;
        long time = 
                System.currentTimeMillis();
        while (true) {//infinite loop
            // spriteManager.update();
             animationDelay = vitesse;
            
            if(this.starter.label != null) {
                this.starter.label.setText("score : "+score+"                                    life : "+life+"       "+Starter.name );
                this.starter.label.repaint();
            }
            if(life<=0) {
            	try {
            	
            	
					
				JFrame f = new JFrame();
				f.setBackground(Color.blue);
				f.setSize(650, 500);
				f.setLocation(BOARD_WIDTH-300,300);
				f.setContentPane(new drawing(gameOver));
				f.setVisible(true);
				
					animator.sleep(5000);
					f.setVisible(false);
				} catch (InterruptedException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
            	
            	System.err.println("je suis passé");
            	String fin = "Pseudo : "+ Starter.name+ " Score : " + score +" \n";
            	list.add(fin);
                b[0].y=getRandomNumberInRange(-400, 10);
                b[1].y=getRandomNumberInRange(-400, 10);
                b[2].y=getRandomNumberInRange(-400, 10);
                b[0].x= random.nextInt(BOARD_WIDTH-80);
                b[1].x=random.nextInt(BOARD_WIDTH-80);
                b[2].x= random.nextInt(BOARD_WIDTH-80);
                a[0].y=getRandomNumberInRange(-400, 10);
                a[1].y=getRandomNumberInRange(-400, 10);
                a[2].y=getRandomNumberInRange(-400, 10);
                a[0].x= random.nextInt(BOARD_WIDTH-80);
                a[1].x= random.nextInt(BOARD_WIDTH-80);
                a[2].x= random.nextInt(BOARD_WIDTH-80);
                r= getRandomNumberInRange(1, 3);
                JButton b= new JButton("REPLAY");
                JButton blist = new JButton("SCORE");
                
               // JPanel p = new JPanel();
                
               // p.setName("GAMEOVER");
                JFrame f=new JFrame();
               // p.setBackground(Color.RED);
                f.setSize(900, 300);
                f.setLocationRelativeTo(null);
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.setName("Replay");
               // p.add(b);
                f.add(b, BorderLayout.NORTH);
                f.add(blist, BorderLayout.SOUTH);
                JLabel jlb = new JLabel();
                f.setVisible(true);
                f.add(jlb,BorderLayout.CENTER);
                jlb.setText(fin);
                b.addMouseListener((new MouseListener() {
                
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        System.err.println("ddddddddddddd");
                        life=5;
                        score=0;
                        vitesse=15;
                        new Thread(leThis).start();
                        
                        f.setVisible(false);
                       
                        
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    @Override
                    public void mousePressed(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    @Override
                    public void mouseReleased(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    
                }));
                
                
                
                blist.addMouseListener((new MouseListener() {
                    
                    @Override
                    public void mouseClicked(MouseEvent e) {
                     JFrame jscore = new JFrame();
                     JLabel lblbl = new JLabel();
                     jscore.add(lblbl);
                     lblbl.setText(list.toString()+"\n");
                     jscore.setSize(300, 300);
                     jscore.setVisible(true);   
                        
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    @Override
                    public void mousePressed(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    @Override
                    public void mouseReleased(MouseEvent e) {
                        // TODO Auto-generated method stub
                        
                    }
                    
                }));
                
                break;
                //              System.exit(0); 
            }
            this.repaint();
            try {
                time += animationDelay;
                Thread.sleep(Math.max(0,time - 
                        System.currentTimeMillis()));
            }catch (InterruptedException e) {
                System.out.println(e);
            }//end catch
        }//end while loop
    }//end of run
    //  
    //          KEY LISTENER
    //  
    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println( e.getKeyCode());
        // message = "Key Pressed: " + e.getKeyCode();
        int key = e.getKeyCode();
        if(key==39){
            p.moveRight=true; 
        }
        if(key==37){
            p.moveLeft=true;
        }
        if(key==38){
            p.moveUp=true;
        }
        if(key==40){
            p.moveDown=true;
        }
        
        if(key==10) {
            debut=true;
            
        }
    }
    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        p.moveRight=false;
        p.moveLeft=false;
        p.moveUp=false;
        p.moveDown=false;
    }
    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
    }






	
}//end of class

//	public void run() {
//		
//		
//		//		try {
//		//			Starter.JETON1.wait();
//		//		} catch (InterruptedException e1) {
//		//			// TODO Auto-generated catch block
//		//			e1.printStackTrace();
//		//		}
//
//		System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa");
//
//		long beforeTime, timeDiff, sleep;
//
//		beforeTime = System.currentTimeMillis();
//		int animationDelay = vitesse;
//		long time = 
//				System.currentTimeMillis();
//		while (true) {//infinite loop
//			// spriteManager.update();
//			animationDelay = vitesse;
//
//			if(this.starter.label != null) {
//
//				this.starter.label.setText("score : "+score+" 									life : "+life+"       "+Starter.name	);
//				this.starter.label.repaint();
//
//			}
//			this.repaint();
//			if(life<=0) {
//				JButton b1= new JButton("REPLAY");
//				b1.setVisible(true);
//				this.add(b1);
//				
//					
//				System.out.println("ssssssssssssssssssssssssssssssssssssssssssss");
//				
//				
//				b[0].y=getRandomNumberInRange(-400, 10);
//				b[1].y=getRandomNumberInRange(-400, 10);
//				b[2].y=getRandomNumberInRange(-400, 10);
//				b[0].x= random.nextInt(BOARD_WIDTH-80);
//				b[1].x=random.nextInt(BOARD_WIDTH-80);
//				b[2].x= random.nextInt(BOARD_WIDTH-80);
//				a[0].y=getRandomNumberInRange(-400, 10);
//				a[1].y=getRandomNumberInRange(-400, 10);
//				a[2].y=getRandomNumberInRange(-400, 10);
//				a[0].x= random.nextInt(BOARD_WIDTH-80);
//				a[1].x= random.nextInt(BOARD_WIDTH-80);
//				a[2].x= random.nextInt(BOARD_WIDTH-80);
//				r= getRandomNumberInRange(1, 3);
//			
//
//				//JPanel p = new JPanel();
//				
//				//p.setName("GAMEOVER");
//				//p.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/2-this.getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height/2)-this.getHeight()/2);
//				
//				//JFrame f=new JFrame();
//				//p.setBackground(Color.RED);
//				
//				//f.setSize(900, 300);
//				//f.setLocationRelativeTo(null);
//				//f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//				//f.setName("Replay");
//				
//				
//				//p.setLocation(500, 800);
//				//p.setVisible(true);
//				
//				
//				this.repaint();
//				
//				
//				//f.add(p, BorderLayout.CENTER);
//				
//				System.out.println("sssssssssssssghgfhhgfjhgjhgjhgfjhgjhgjhjhjgjhhhjjhgsssssssssssssssssssssssssssssss");
//				//f.setVisible(true);
//				b1.addMouseListener((new MouseListener() {
//					
//					@Override
//					public void mouseClicked(MouseEvent e) {
//						System.err.println("ddddddddddddd");
//						
//						life=5;
//						score=0;
//						vitesse=15;
//						
//						b1.setVisible(false);
//						new Thread(leThis).start();
//						
//
//					//	f.setVisible(false);
//
//
//					}
//
//					@Override
//					public void mouseEntered(MouseEvent e) {
//						// TODO Auto-generated method stub
//
//					}
//
//					@Override
//					public void mouseExited(MouseEvent e) {
//						// TODO Auto-generated method stub
//
//					}
//
//					@Override
//					public void mousePressed(MouseEvent e) {
//						// TODO Auto-generated method stub
//
//					}
//
//					@Override
//					public void mouseReleased(MouseEvent e) {
//						// TODO Auto-generated method stub
//
//					}
//
//				}));
//
//
//
//				break;
//
//
//
//
//				//				System.exit(0);	
//			}
//
//			this.repaint();
//			try {
//				time += animationDelay;
//				Thread.sleep(Math.max(0,time - 
//						System.currentTimeMillis()));
//			}catch (InterruptedException e) {
//				System.out.println(e);
//			}//end catch
//		}//end while loop
//
//	}//end of run
//
//	//	
//	//			KEY LISTENER
//	//	
//
//	@Override
//	public void keyPressed(KeyEvent e) {
//		System.out.println( e.getKeyCode());
//		// message = "Key Pressed: " + e.getKeyCode();
//		int key = e.getKeyCode();
//		if(key==39){
//
//			p.moveRight=true; 
//		}
//		if(key==37){
//			p.moveLeft=true;
//		}
//		if(key==38){
//			p.moveUp=true;
//		}
//		if(key==40){
//			p.moveDown=true;
//		}
//
//		if(key==10) {
//			debut=true;
//
//		}
//
//	}
//
//	@Override
//	public void keyReleased(KeyEvent e) {
//		int key = e.getKeyCode();
//		p.moveRight=false;
//		p.moveLeft=false;
//		p.moveUp=false;
//		p.moveDown=false;
//
//	}
//
//	@Override
//	public void keyTyped(KeyEvent e) {
//		// TODO Auto-generated method stub
//
//	}
//
//}//end of class