package space;

import java.awt.Graphics;
import java.awt.Paint;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class Setup extends JFrame{
	private BufferedImage background2;
	
	
	
	public Setup() throws IOException {
		setTitle("SpaceSetup");
		background2 = ImageIO.read(Board2.class.getResource("/image/space.jpg"));
		setSize(400, 400);
		setLocation((Toolkit.getDefaultToolkit().getScreenSize().width/2)-this.getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height/2)-this.getHeight()/2);
		setVisible(true);
		setResizable(false);
	}
	
	
	public void paint(Graphics g){
		g.drawImage(background2, 0, 0, 400, 400, null);
	}
}
