package space;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Timer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.imageio.*;
import java.awt.image.*;
import java.io.*;


public class Board  extends JPanel implements Runnable, KeyListener
{
	boolean start=false;
	Random random= new Random();
	int r = getRandomNumberInRange(1, 3);
	private BufferedImage ship;
	private BufferedImage shipRight;
	private BufferedImage shipLeft;
	private BufferedImage background;
	private BufferedImage asteroid;
	private BufferedImage asteroidfire;
	private BufferedImage crash;
	boolean ingame = true;
	private Dimension d;
	int BOARD_WIDTH=600;
	int BOARD_HEIGHT=800;
	Integer score = 0;
	public Integer getScore() {
		return score;
	}


	JLabel jl= new JLabel("score"+ score);
	JTextField jt= new JTextField();
	BufferedImage img;
	String message = "";
	private Thread animator;
	Player p;
	Asteroid [] a = new Asteroid[3];
	AsteroidFire [] b = new AsteroidFire[3];

	public Board(Starter starter) throws IOException{
//		shipRight = ImageIO.read(new File("C:\\Users\\59013-57-03\\Desktop\\GIT kraken\\repos-cdi-algo\\space\\src\\image\\shipright.png"));
//		shipLeft= ImageIO.read(new File("C:\\Users\\59013-57-03\\Desktop\\GIT kraken\\repos-cdi-algo\\space\\src\\image\\shipleft.png"));
//		background = ImageIO.read(new File("C:\\Users\\59013-57-03\\Desktop\\GIT kraken\\repos-cdi-algo\\space\\src\\image\\space.jpg"));
//		ship = ImageIO.read(new File("C:\\Users\\59013-57-03\\Desktop\\GIT kraken\\repos-cdi-algo\\space\\src\\image\\spaceship.png"));
//		asteroid = ImageIO.read(new File("C:\\Users\\59013-57-03\\Desktop\\GIT kraken\\repos-cdi-algo\\space\\\\src\\image\\asteroid.png"));
//		asteroidfire = ImageIO.read(new File("C:\\Users\\59013-57-03\\Desktop\\GIT kraken\\repos-cdi-algo\\space\\src\\image\\fire.png"));
//		crash = ImageIO.read(new File("C:\\Users\\59013-57-03\\Desktop\\GIT kraken\\repos-cdi-algo\\space\\src\\image\\broken.png"));


				shipRight = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/shipright.png"));
				shipLeft= ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/shipleft.png"));
				background = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/space.png"));
				ship = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/spaceship.png"));
				asteroid = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/asteroid.png"));
				asteroidfire = ImageIO.read(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/space/src/image/fire.png"));
		//	addKeyListener(new TAdapter());
		//addMouseListener(this);
		addKeyListener(this);
		setFocusable(true);
		this.add(jl,BorderLayout.NORTH);
		p = new Player(BOARD_WIDTH/2, BOARD_HEIGHT-100, 5);
		d = new Dimension(BOARD_WIDTH, BOARD_HEIGHT);
		//		jl.add(jt);
		//		jt.setVisible(true);
		jl.setVisible(true);




		//FILLING STOCK OF ASTEROID
		for (int i = 0; i < a.length; i++) {
			int ax =random.nextInt(BOARD_WIDTH);
			int ay=BOARD_HEIGHT-1;
			a[i]=new Asteroid(ax, ay, 10);


		}
		//FILLING STOCK OF ASTEROID FIRE
		for (int i = 0; i < b.length; i++) {
			int ax =random.nextInt(BOARD_WIDTH);
			int ay=BOARD_HEIGHT-1;
			b[i]=new AsteroidFire(ax, ay, 10);


		}

		if (animator == null || !ingame) {
			animator = new Thread(this);
			animator.start();

		}


		setDoubleBuffered(true);
		System.err.println("init");
	}


	// RANDOM NUMBERS GENERATOR
	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}


	// DRAWING PART
	public void paint(Graphics g){

//		Starter.i=score;
//		System.out.println(Starter.i);


		System.err.println("au tout debut "+a[0].y+" "+b[0].y);
		System.out.println("Mon score est "+score);
		super.paint(g);
		moveAsteroid();
				moveAsteroidfire();
		asteroidWave();

		if(start==false) {
			g.drawImage(ship, p.x, p.y, 50, 50, null);
			start=true;
		}

		System.err.println("boucle");
		System.err.println(BOARD_HEIGHT);
		System.err.println(r);

		g.drawImage(background, 0, 0, d.width, d.height, null);

		//g.setColor(Color.white);
		//g.fillRect(0, 0, d.width, d.height);
		//g.fillOval(x,y,r,r);


		//player
		//g.setColor(Color.black);
		//g.fillRect(p.x, p.y, 20, 20);
		if(p.moveLeft==false && p.moveRight==false) {
			g.drawImage(ship, p.x, p.y, 80, 80, null);
		}

		if(p.moveRight==true) {
			g.drawImage(shipRight, p.x, p.y, 80, 80, null);
			if(p.x==BOARD_WIDTH-80) {
				p.moveRight=false;
				System.err.println("ici");	
			}else {
				p.x+=p.speed;
			}
		}



		if(p.moveLeft==true) {
			g.drawImage(shipLeft, p.x, p.y, 80, 80, null);
			if(p.x==0) {
				p.moveLeft=false;
				System.err.println("ici");	
			}else {
				p.x-=p.speed;
			}

		}


		if(p.moveUp==true) {
			if(p.y==0) {
				p.moveUp=false;
				System.err.println("ici");	
			}else {
				p.y-=p.speed;
			}
		}


		if(p.moveDown==true) {
			if(p.y==BOARD_HEIGHT-90) {
				p.moveDown=false;
				System.err.println("ici");	
			}else {
				p.y+=p.speed;
			}
		}



		if(r==1) {
			for (int i = 0; i < a.length; i++) {
				//	g.setColor(Color.red);
				g.drawImage(asteroid,a[i].x, a[i].y, 50, 50, null);
				//	g.fillRect(a[i].x, a[i].y, 30, 30);

				System.err.println("test CRASH");
				System.out.println(p.x+" "+p.y);
				System.out.println("a[0] "+a[0].x+" "+ a[0].y);
				System.out.println("a[1] "+a[1].x+" "+ a[1].y);
				System.out.println("a[2] "+a[2].x+" "+ a[2].y);

				if(p.x==a[0].x && p.y==a[0].y) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 50, 50, null);
				}else if (p.x==a[1].x && p.y==a[1].y) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 50, 50, null);

				}else if (p.x==a[2].x && p.y==a[2].y) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 50, 50, null);
				}
			}
		}





		if(r==2) {
			for (int i = 0; i < b.length; i++) {
				//	g.setColor(Color.red);
				g.drawImage(asteroidfire,b[i].x, b[i].y, 90, 70, null);
				//	g.fillRect(a[i].x, a[i].y, 30, 30);
				System.err.println("test CRASH");
				System.out.println(p.x+" "+p.y);
				System.out.println("b[0] "+b[0].x+" "+ b[0].y);
				System.out.println("b[1] "+b[1].x+" "+ b[1].y);
				System.out.println("b[2] "+b[2].x+" "+ b[2].y);

				if(p.x==b[0].x && p.y==b[0].y) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 50, 50, null);
				}else if (p.x==b[1].x && p.y==b[1].y) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 50, 50, null);

				}else if (p.x==b[2].x && p.y==b[2].y) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 50, 50, null);
				}

			}
		}

		if(r==3) {
			//	g.setColor(Color.red);
			g.drawImage(asteroid,a[0].x, a[0].y, 50, 50, null);
			g.drawImage(asteroid,a[1].x, a[1].y, 30, 50, null);
			g.drawImage(asteroidfire,b[0].x, b[0].y, 90, 70, null);
			//	g.fillRect(a[i].x, a[i].y, 30, 30);

			System.err.println("test CRASH");
			System.out.println(p.x+" "+p.y);
			System.out.println("a[0] "+a[0].x+" "+ a[0].y);
			System.out.println("a[1] "+a[1].x+" "+ a[1].y);
			System.out.println("b[0] "+b[0].x+" "+ b[0].y);

			if(p.x>=(a[0].x)-5 && p.y==a[0].y) {
				if(	p.x<=(a[0].x)+5 && p.y==a[0].y) {
					System.err.println("crash");
					g.drawImage(crash, p.x, p.y, 50, 50, null);
				}else if (p.x>=(a[1].x)-5 && p.y==a[1].y) {
					if(	p.x<=(a[1].x)+5 && p.y==a[1].y) {
						System.err.println("crash");
						g.drawImage(crash, p.x, p.y, 50, 50, null);

					}else if (p.x>=(b[0].x)-5 && p.y==b[0].y) {
						if(p.x<=(b[0].x)+5 && p.y==b[0].y) {

							System.err.println("crash");
							g.drawImage(crash, p.x, p.y, 50, 50, null);
						}

					}
				}
			}
		}



		//				Font small = new Font("Helvetica", Font.BOLD, 14);
		//				FontMetrics metr = this.getFontMetrics(small);
		//				g.setColor(Color.RED);
		//				g.setFont(small);
		//				g.drawString(score.toString(), 150, 10);
		//				g.drawRect(0, 0, d.width, 10);
		//				g.setColor(Color.RED);


		for (int i = 0; i < a.length; i++) {
			if(b[i].moveDown==true) {
				b[i].y+=a[i].speed/3; //SPEED CHANGE
			}

		}






		if (ingame) {
			// g.drawImage(img,0,0,200,200 ,null);
		}



		Toolkit.getDefaultToolkit().sync();
		g.dispose();
	}





	//ASTEROID FALLS
	public void moveAsteroid() {
		for (int i = 0; i < a.length; i++) {
			if(a[i].moveDown==true) {
				a[i].y+=a[i].speed/2; //SPEED CHANGE
				System.err.println("a de i = "+a[i].y);
			}
		}

	}
	//ASTEROID FIRE FALLS
	public void moveAsteroidfire() {
		for (int i = 0; i < a.length; i++) {
			if(b[i].moveDown==true) {
				b[i].y+=b[i].speed/3; //SPEED CHANGE
				System.err.println("a de i = "+a[i].y);


			}
		}

	}




	//WAVES OF ASTEROID  
	public void asteroidWave() {

		if(r==2 && b[0].y>BOARD_HEIGHT&& b[1].y>BOARD_HEIGHT&& b[2].y>BOARD_HEIGHT) {
			System.err.println("new wave r2");
			b[0].y=getRandomNumberInRange(-400, 10);
			b[1].y=getRandomNumberInRange(-400, 10);
			b[2].y=getRandomNumberInRange(-400, 10);
			b[0].x= random.nextInt(BOARD_WIDTH-50);
			b[1].x=random.nextInt(BOARD_WIDTH-50);
			b[2].x= random.nextInt(BOARD_WIDTH-50);
			a[0].y=getRandomNumberInRange(-400, 10);
			a[1].y=getRandomNumberInRange(-400, 10);
			a[2].y=getRandomNumberInRange(-400, 10);
			a[0].x= random.nextInt(BOARD_WIDTH-50);
			a[1].x= random.nextInt(BOARD_WIDTH-50);
			a[2].x= random.nextInt(BOARD_WIDTH-50);
			r= getRandomNumberInRange(1, 3); //        RANDOM FOR NEW WAVE
			score+=3;
		}


		//WAVES OF ASTEROID 
		if(r==1 && a[0].y>BOARD_HEIGHT && a[1].y>BOARD_HEIGHT&& a[2].y>BOARD_HEIGHT) {
			System.err.println("new wave r1");
			b[0].y=getRandomNumberInRange(-400, 10);
			b[1].y=getRandomNumberInRange(-400, 10);
			b[2].y=getRandomNumberInRange(-400, 10);
			b[0].x= random.nextInt(BOARD_WIDTH-50);
			b[1].x=random.nextInt(BOARD_WIDTH-50);
			b[2].x= random.nextInt(BOARD_WIDTH-50);
			a[0].y=getRandomNumberInRange(-400, 10);
			a[1].y=getRandomNumberInRange(-400, 10);
			a[2].y=getRandomNumberInRange(-400, 10);
			a[0].x= random.nextInt(BOARD_WIDTH-50);
			a[1].x= random.nextInt(BOARD_WIDTH-50);
			a[2].x= random.nextInt(BOARD_WIDTH-50);
			r= getRandomNumberInRange(1, 3); //        RANDOM FOR NEW WAVE
			score+=3;
		}

		//WAVES OF 2 ASTEROIDS AND 1 FIRE 

		if(r==3 && a[0].y>BOARD_HEIGHT&& a[1].y>BOARD_HEIGHT&& a[2].y>BOARD_HEIGHT&& b[0].y>BOARD_HEIGHT&& b[1].y>BOARD_HEIGHT&& b[2].y>BOARD_HEIGHT) {
			System.err.println("new wave r3");
			b[0].y=getRandomNumberInRange(-400, 10);
			b[1].y=getRandomNumberInRange(-400, 10);
			b[2].y=getRandomNumberInRange(-400, 10);
			b[0].x= random.nextInt(BOARD_WIDTH-50);
			b[1].x=random.nextInt(BOARD_WIDTH-50);
			b[2].x= random.nextInt(BOARD_WIDTH-50);
			a[0].y=getRandomNumberInRange(-400, 10);
			a[1].y=getRandomNumberInRange(-400, 10);
			a[2].y=getRandomNumberInRange(-400, 10);
			a[0].x= random.nextInt(BOARD_WIDTH-50);
			a[1].x= random.nextInt(BOARD_WIDTH-50);
			a[2].x= random.nextInt(BOARD_WIDTH-50);
			r= getRandomNumberInRange(1, 3); //        RANDOM FOR NEW WAVE
			score+=3;
		}

	}




	public void run() {
		System.err.println("je passe par run");

		long beforeTime, timeDiff, sleep;

		beforeTime = System.currentTimeMillis();
		int animationDelay = 5;
		long time = 
				System.currentTimeMillis();
		while (true) {//infinite loop
			// spriteManager.update();
			//System.err.println("repaint");


			//			
			//			jt.setName("score");
			//			jt.setSize(BOARD_WIDTH, 50);
			//			setFocusable(true);
			//			jt.add(jt);
			//			jt.setVisible(true);

			repaint();



			try {
				time += animationDelay;
				Thread.sleep(Math.max(0,time - 
						System.currentTimeMillis()));
			}catch (InterruptedException e) {
				System.out.println(e);
			}//end catch
		}//end while loop




	}//end of run


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}


	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println( e.getKeyCode());
		// message = "Key Pressed: " + e.getKeyCode();
		int key = e.getKeyCode();
		if(key==39){

			p.moveRight=true; 
		}
		if(key==37){
			p.moveLeft=true;
		}
		if(key==38){
			p.moveUp=true;
		}
		if(key==40){
			p.moveDown=true;
		}

	}


	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		p.moveRight=false;
		p.moveLeft=false;
		p.moveUp=false;
		p.moveDown=false;

	}
}

// OTHER WAY TO HANDLE KEYLISTENER AND MOUSELISTENER

//private class TAdapter extends KeyAdapter {
//
//	public void keyReleased(KeyEvent e) {
//		int key = e.getKeyCode();
//		p.moveRight=false;
//		p.moveLeft=false;
//		p.moveUp=false;
//		p.moveDown=false;
//
//	}
//
//	public void keyPressed(KeyEvent e) {
//		System.out.println( e.getKeyCode());
//		// message = "Key Pressed: " + e.getKeyCode();
//		int key = e.getKeyCode();
//		if(key==39){
//
//			p.moveRight=true; 
//		}
//		if(key==37){
//			p.moveLeft=true;
//		}
//		if(key==38){
//			p.moveUp=true;
//		}
//		if(key==40){
//			p.moveDown=true;
//		}
//
//
//
//
//
//	}
//
//}




//public void mousePressed(MouseEvent e) {
//	int x = e.getX();
//	int y = e.getY();

//}
//
//public void mouseReleased(MouseEvent e) {
//
//}
//
//public void mouseEntered(MouseEvent e) {
//
//}
//
//public void mouseExited(MouseEvent e) {
//
//}
//
//public void mouseClicked(MouseEvent e) {
//
//}

//end of class