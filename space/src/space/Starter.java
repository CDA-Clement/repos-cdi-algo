package space;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Starter extends JFrame  {

	JLabel label;
	static String name;
	static boolean setup =false;


	public Starter() throws IOException {
		add(new Board2(this));
		setTitle("SpaceGame");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(600, 800);
		setLocationRelativeTo(null);
		setResizable(false);
		label = new JLabel("score : 0"+"             "+name);
		label.setSize(Board.WIDTH, 0);
		this.add(label, BorderLayout.NORTH);
		setVisible(true);


	}

//	public static final String JETON1 = "";

	public static void main(String[] args) throws IOException {
		Setup f1 = new Setup();
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLUE);
		JOptionPane jo = new JOptionPane();
		name= jo.showInputDialog("your name");
		name = name.toUpperCase();
		
		System.out.println(panel.getLocation());
//		
		JButton jButton = new JButton("play");
		panel.add(jButton);
		
		final JDialog frame = new JDialog(f1, "setup", true);
		jButton.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(java.awt.event.MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(java.awt.event.MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(java.awt.event.MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(java.awt.event.MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(java.awt.event.MouseEvent e) {
				System.err.println("BONJOUR");
				try {
					Starter f = new Starter();
					frame.setVisible(false);
					f1.setVisible(false);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				  
			}
		});
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width/2)-frame.getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height/2)-frame.getHeight()/2);
		frame.setVisible(true);
		

	}


}
