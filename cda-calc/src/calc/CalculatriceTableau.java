package calc;

public class CalculatriceTableau {

	public static int sommeElements(int[] tab) {
		int res = 0;
		for(int i = 0; i<tab.length; i++) {
			res = res + tab [i];
		}
		return res;
	}

	public static int plusPetitElement(int[] tab) {
		int mini = Integer.MAX_VALUE;
		for (int i = 0; i<tab.length; i++) {
			System.out.println("***************************************");
			System.out.println("i = "+i);
			System.out.println("mini avant compare : "+mini);
			System.out.println("tab[i] : "+tab[i]);
			if (mini>tab [i]) {
				mini = tab[i];
			}
			System.out.println("mini apres compare : "+mini);
			System.out.println("***************************************");
		}
		return mini;
	}

	public static long sommeElementsDeuxTableaux(int[] tab, int[] tab2) {
		int res = 0;
		for(int i = 0; i<tab.length && i<tab2.length; i++) {
			res = res+ tab[i]+ tab2[i];
		}
		return res;
	}

	public static int[] triAscendant(int[] tab) {

		/*int tampon = 0;
		boolean c;

		do {
			// hypoth�se : le tableau est tri�
			c = false;
			for (int i = 0; i < tab.length - 1; i++) {
				// Teste si 2 �l�ments successifs sont dans le bon ordre ou non
				if (tab[i] > tab[i + 1]) {
					// s'ils ne le sont pas, on �change leurs positions
					tampon = tab[i];
					tab[i] = tab[i + 1];
					tab[i + 1] = tampon;
					c = true;
				}

			}
		} while (c);
		return tab;

	}*/





		for (int i = 0; i < tab.length; i++) {

			for (int j = i+1; j < tab.length; j++) {
				System.out.println("j egale"+j);
				if (tab[i] < tab[j]) {
					int temp = tab[i];
					tab[i] = tab[j];
					tab[j] = temp;

				}

			}

		}		
		return tab;
	}


	/* 5,3,1,4,2
	 * J=1 I=0 5,3,1,4,2
	 * J=2 I=0 5,3,1,4,2
	 * J=3 I=0 5,3,1,4,2
	 * J=4 I=0 5,3,1,4,2
	 * 
	 * J=0 I=1 3,5,1,4,2
	 * J=1 I=1 3,5,1,4,2
	 * J=2 I=1 3,5,1,4,2
	 * J=3 I=1 3,5,1,4,2
	 * J=4 I=1 3,5,1,4,2
	 * 
	 * J=0 I=2 1,5,3,4,2
	 * J=1 I=2 1,3,5,4,2
	 * J=2 I=2 1,3,5,4,2
	 * J=3 I=2 1,3,5,4,2
	 * J=4 I=2 1,3,5,4,2
	 * 
	 * J=0 I=3 1,3,5,4,2
	 * J=1 I=3 1,3,5,4,2
	 * J=2 I=3 1,3,4,5,2
	 * J=3 I=3 1,3,4,5,2
	 * J=4 I=3 1,3,4,5,2
	 * 
	 * J=0 I=4 1,3,4,5,2
	 * J=1 I=4 1,2,4,5,3
	 * J=2 I=4 1,2,3,5,4
	 * J=3 I=4 1,2,3,4,5
	 * J=4 I=4 1,2,3,4,5
	 *
	 */

	public static boolean conjonction(boolean[] tabBool) {
		boolean res = true;
		for(int i=0; res&&i< tabBool.length;i++) {
			res = tabBool[i] & res;

		}
		return res ;
	}

	public static long nombreDElementsPair(int[] tab) {
		int i = 0;
		int p = 0;
		for(i=0; i<tab.length;i++) {
			if(tab[i]%2 ==0) {
				p++;
			}
		}
		return p;
	}

	public static int[] tabDElementsPair(int[] tab) {
		int nbElmPairs = (int) nombreDElementsPair(tab);
		int[] mesElemtPairs = new int[nbElmPairs];
		int j = 0;
		for(int i=0; i<tab.length;i++) {
			if(tab[i]%2 ==0) {
				mesElemtPairs[j] = tab[i];
				j++;
			}
		}
		return mesElemtPairs;
	}

	public static boolean chercheSiUnElementExiste(int param, int[] tab) {
		boolean element = false;
		for(int i=0;!element && i<tab.length;i++) {
			if(param == tab[i]) {
				element = true;
			}
		}	
		return element;
	}

	public static int[] mettreZeroDansLesCasesAIndicesImpair(int[] tab) {

		for(int i = 0; i<tab.length;i++) {
			if(i%2>0) {
				tab[i] = 0;
			}

		}

		return tab;
	}

	public static int[] decalerLesElementsTableauDUneCase(int[] tab) {
		int valFin = tab[tab.length-1];

		for(int i = tab.length-2; i>=0;i--) {
			System.out.println(i);
			tab[i+1] = tab[i];
			System.out.println("**************"+i);
		}
		tab[0] = valFin;
		return tab; 
	}
	/*public static int[] decalerLesElementsTableauDUneCase(int[] tab) {
		int valFin = tab[0];
		for(int i = 0; i< tab.length-1;i++) {
			tab[i] = tab[i+1];
		}
		tab[5] = valFin;
		return tab;
	}*/

	public static int[] triAscendantDeuxTableaux(int[] tab, int[] tab2) {
		int[] tab3 = new int[tab.length+ tab2.length];
		for (int i = 0; i < tab.length; i++) {
			tab3[i] =tab[i];
		}

		for (int i = tab.length; i < tab3.length; i++) {
			tab3[i] =tab2[i-tab2.length];


		}
		tab3 = triAscendant(tab3);
		return tab3;

	}
}