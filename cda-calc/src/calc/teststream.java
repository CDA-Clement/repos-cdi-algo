package calc;

import java.util.Arrays;

public class teststream {

	public static void main(String[] args) {
		int[] entiersTab = { 4, 5, -6, 4, 8 };
		int sum = CalculatriceTableauStream.sommeElements(entiersTab);
		System.out.println("somme d'un tableau " + sum);
		System.out.println();

		int min = CalculatriceTableauStream.plusPetitElement(entiersTab);
		System.out.println("min " + min);
		System.out.println();

		int max = CalculatriceTableauStream.plusGrandElement(entiersTab);
		System.out.println("max " + max);
		System.out.println();

		int som2tab = CalculatriceTableauStream.sommeElementsDeuxTableaux(entiersTab, entiersTab);
		System.out.println("somme de 2 tableaux " + som2tab);
		System.out.println();

		int[] sort = CalculatriceTableauStream.triAscendant(entiersTab);
		System.out.print("tri ascendant ");
		for (int i = 0; i < sort.length; i++) {
			System.out.print(sort[i] + ", ");
		}
		System.out.println();
		System.out.println();

		long nbPair = CalculatriceTableauStream.nombreDElementsPair(entiersTab);
		System.out.println("nombre elements pair " + nbPair);
		System.out.println();

		Object[] tabpair = CalculatriceTableauStream.tabDElementsPair(entiersTab);
		System.out.print("un tableau de nombre pair ");
		for (int i = 0; i < tabpair.length; i++) {
			System.out.print(tabpair[i] + ", ");
		}
		System.out.println();
		System.out.println();
		boolean existe = CalculatriceTableauStream.chercheSiUnElementExiste(4, entiersTab);
		System.out.println("element existe " + existe);

		System.out.println();
		Object[] tri2tab = CalculatriceTableauStream.triAscendantDeuxTableaux(entiersTab, entiersTab);
		System.out.print("tri ascendant de deux tableaux ");
		for (int i = 0; i < tri2tab.length; i++) {
			System.out.print(tri2tab[i] + ", ");
		}
		System.out.println();
		System.out.println();
		
		int[] tab0 = CalculatriceTableauStream.mettreZeroDansLesCasesAIndicesImpair(entiersTab);
		System.out.println(Arrays.toString(tab0));

	}

}
