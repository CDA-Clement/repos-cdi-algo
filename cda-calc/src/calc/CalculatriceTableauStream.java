package calc;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CalculatriceTableauStream {

	public static int sommeElements(int[] tab) {
		return Arrays.stream(tab).sum();
	}

	public static int plusPetitElement(int[] tab) {
		return Arrays.stream(tab).min().getAsInt();
	}

	public static int plusGrandElement(int[] tab) {
		return Arrays.stream(tab).max().getAsInt();
	}

	public static int sommeElementsDeuxTableaux(int[] tab, int[] tab2) {
		return Arrays.stream(tab).sum() + Arrays.stream(tab2).sum();
	}

	public static int[] triAscendant(int[] tab) {
		return Arrays.stream(tab).sorted().toArray();
	}

	public static boolean conjonction(boolean[] tabBool) {
		boolean res = true;
		for (int i = 0; res && i < tabBool.length; i++) {
			res = tabBool[i] & res;
		}
		return res;
	}

	public static long nombreDElementsPair(int[] tab) {
		return Arrays.stream(tab).filter(m -> m % 2 == 0).count();
	}

	public static Object[] tabDElementsPair(int[] entiersTab1) {
		Integer[] entiersTab = new Integer[entiersTab1.length];
		for (int i = 0; i < entiersTab.length; i++) {
			entiersTab[i] = entiersTab1[i];
		}
		List<Integer> Listpair = Arrays.stream(entiersTab).filter(m -> m % 2 == 0).collect(Collectors.toList());
		return Listpair.toArray();
	}

	public static boolean chercheSiUnElementExiste(int param, int[] tab) {
		return Arrays.stream(tab).anyMatch((m -> m == param));
	}

	public static int[] mettreZeroDansLesCasesAIndicesImpair(int[] tab) {
		Stream.iterate(0, i -> i + 1).limit(tab.length).filter(x -> x % 2 == 1).forEach(x -> tab[x] = 0);
		return tab;
	}

	public static int[] decalerLesElementsTableauDUneCase(int[] tab) {
		int valFin = tab[tab.length - 1];

		for (int i = tab.length - 2; i >= 0; i--) {
			System.out.println(i);
			tab[i + 1] = tab[i];
			System.out.println("**************" + i);
		}
		tab[0] = valFin;
		return tab;
	}

	public static Object[] triAscendantDeuxTableaux(int[] tab, int[] tab2) {
		
		return Stream.concat(Stream.of(tab), Stream.of(tab2)).toArray();
	}

}
