package com.spring.test;


import org.junit.jupiter.api.Test;

import myapp.imp.BeanFileLogger;
import myapp.services.ILogger;

class TestBeanFileLogger {
	
	void use(ILogger logger) { logger.log("Voila␣le␣r ́esultat␣=␣hello");
	}

	@Test
	public void testBeanFileLogger() {
	    // create the service
	    BeanFileLogger logger = new BeanFileLogger();
	    // set parameter
	    logger.setFileName("/tmp/myapp.log");
	    // start
	    logger.start();
	    // use
	    use(logger);
	    // stop
	    logger.stop();
	}

}
