package com.spring.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import myapp.imp.StderrLogger;
import myapp.services.ILogger;

public class TestLoggerServices {
	@BeforeEach
	public void beforeEachTest() {
		System.err.println("====================");
	}

	@AfterEach
	public void afterEachTest() {
	}

	// use a logger
	void use(ILogger logger) { logger.log("Voila␣le␣r ́esultat␣=␣hello");
	}
	
	
	// Test StderrLogger
	@Test
	public void testStderrLogger() {
		// create the service
		StderrLogger logger = new StderrLogger();
		// start the service
		logger.start();
		// use the service
		use(logger);
		// stop the service
		logger.stop();
	}
}
