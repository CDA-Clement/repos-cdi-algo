package com.spring.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import myapp.imp.FileLogger;
import myapp.services.ILogger;

class TestFileLogger {
	
	void use(ILogger logger) { logger.log("Voila␣le␣r ́esultat␣=␣hello");
	}

	@Test
	public void testFileLogger() {
	    FileLogger logger = new FileLogger("/tmp/myapp.log");
	    logger.start();
	    use(logger);
	    logger.stop();
	}


}
