package com.spring.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import myapp.imp.BeanFileLogger;
import myapp.imp.SimpleCalculator;
import myapp.imp.StderrLogger;
import myapp.services.ICalculator;

class TestSimpleCalculor {

	void use(ICalculator calc) {
	    calc.add(100, 200);
	}
	@Test
	public void testCalculorAndStderrLogger() {
		BeanFileLogger logger = new BeanFileLogger();
		logger.setFileName("/tmp/myapp.log");
		logger.start();
	    // create, inject and start the calculator service
	    SimpleCalculator calculator = new SimpleCalculator();
	    calculator.setLogger(logger);
	    calculator.start();
	    // use the calculator service
	    use(calculator);
	    // stop the calculator service
	    calculator.stop();
	    // stop the logger service
	    logger.stop();
	}

}
