package ExoEntreprise;

import java.util.ArrayList;

public class Employe {

	private static ArrayList<Employe> tousLesEmployes= new ArrayList<Employe>();
	private static double salaire = 1000;
	public static void setSalaire(double salaire) {
		Employe.salaire = salaire;
	}
	private static int matricule;
	private String nom;
	private int monmat;
	private double indiceSalaire;
	public Employe (String nom, double indice) {    
		matricule++;
		this.nom=nom;
		this.indiceSalaire=indice;
		monmat= matricule;
	}
	public static double calSalaire(int mat) {  
		boolean pastrouve=false;
		if(tousLesEmployes.size()==0) {
			System.out.println("Ton entreprise est vide, il faut recruter si tu veux afficher le salaire d'un employ� pauvre con");
		}
		System.out.println();
		for (Employe employe : tousLesEmployes) {
			pastrouve=false;
			if(mat==employe.getMonmat()) {
				if(employe instanceof Commercial==true) {
					System.out.println(employe.toString()+" le salaire du commercial primes incluses "+ ((Commercial) employe).calculeSalaireCommercial()+" �");
				}else {
					double salaire=0;
					salaire = getSalaire()*employe.indiceSalaire;
					System.out.println("    "+employe.toString()+" "+salaire);
					return salaire;

				}
			}else { pastrouve = true;
			}
		}
		if(pastrouve==true) {
			System.out.println("Aucun salarie ne correspond au matricule rentr�, Veuillez verifier dans la liste le bon matricule");
			System.out.println();
			for (Employe employe : tousLesEmployes) {
				System.out.println(employe.toString()); 
			}
		}
		return 0;
	}
	public static void creerEmploye(String nomEmploye, double indiceSalEmploye ) {
		Employe e = new Employe(nomEmploye, indiceSalEmploye);
		tousLesEmployes.add(e);
		System.out.println("bien ajout�");
	}
	public static void creerCommercial(String nomEmploye, double indiceSalEmploye , double vente) {
		Commercial e = new Commercial(nomEmploye, indiceSalEmploye, vente);
		tousLesEmployes.add(e);
	}

	public static void creerResponsable(String nomEmploye, double indiceSalEmploye ) {
		Responsable e = new Responsable(nomEmploye, indiceSalEmploye);
		tousLesEmployes.add(e);
		
	}
	public static void afficherSalaire(int mat) {
		boolean pastrouve=false;
		if(tousLesEmployes.size()==0) {
			System.out.println("Ton entreprise est vide, il faut recruter si tu veux afficher le salaire d'un employ� pauvre con");
		}
		System.out.println();
		for (Employe employe : tousLesEmployes) {
			pastrouve=false;
			if(mat==employe.getMonmat()) {
				if(employe instanceof Commercial==true) {
					System.out.println(employe.toString()+" le salaire du commercial primes incluses "+ ((Commercial) employe).calculeSalaireCommercial()+" �");
				}else {
					double salaire = employe.getIndiceSalaire()*getSalaire();
					System.out.println("    "+employe.toString()+salaire);
					break;}
			}else { pastrouve = true;
			}
		}
		if(pastrouve==true) {
			System.out.println("Aucun salarie ne correspond au matricule rentr�, Veuillez verifier dans la liste le bon matricule");
			System.out.println();
			for (Employe employe : tousLesEmployes) {
				System.out.println(employe.toString()); 
			}
		}
	}
	public static void masseSalariale() {

		double total = 0;
		if(tousLesEmployes.size()==0) {
			System.out.println("Ton entreprise est vide, il faut recruter si tu veux afficher le salaire d'un employ� pauvre con");
		}
		for (Employe employe : tousLesEmployes) {
			total+=employe.getIndiceSalaire()*getSalaire();

		}
		System.out.println("    "+"la masse Salariale est de "+total);  
	}
	public static void modifIndiceSalarial(int m, double d) {
		boolean pastrouve = false;
		for (Employe employe : tousLesEmployes) {
			pastrouve=false;
			if(m==employe.monmat) {
				employe.setIndiceSalaire(d);
				break;
			}else { pastrouve = true;
			}
		}
		if(pastrouve==true) {
			System.out.println("Aucun salarie ne correspond au matricule rentr�, Veuillez verifier dans la liste le bon matricule");
			System.out.println();
			for (Employe employe : tousLesEmployes) {
				System.out.println(tousLesEmployes.toString()); 
			}
		}
	}
	public static void modifSalairebase(double nouveau) {
		setSalaire(nouveau);
	}

	public static void afficher() {
        for (Employe employe : tousLesEmployes) {
            System.out.println(employe); 
        }
    }


	//----------------------------------------                                                                         methode responsable
	//                                      ajouter un employer a la liste des soufifre
	public static void ajoutSousEmpl(int matriculeResponsable, int matriculeE) {
		//cherche le responsable
		for(Employe e : getTousLesEmployes()) {
			//si  il est un responsable
			if(e.getMonmat() == matriculeResponsable && e instanceof Responsable && matriculeResponsable != matriculeE) {
				Responsable res = (Responsable) e;
				//cherche l' employer dans la liste des employer
				for(Employe r : getTousLesEmployes()) {
					// si on trouve l'employer et qu il n est pas dans la liste du responsable on l'ajoute
					if(r.getMonmat() == matriculeE && !res.getListeInferieurs().contains(r)) {
						res.getListeInferieurs().add(r);
						System.out.println("bien ajouter");
					}
				}
			}
		}
	}
	
	
	
	
	
	//                                              supprimer un employ� de la liste des souffrifre
	public static void retireUnEmp(int matriculRes, int matriculEmp) {
		for (Employe e : getTousLesEmployes()) {
			if(e.getMonmat() == matriculRes && e instanceof Responsable) {
				Responsable res = (Responsable) e;
				for(Employe a : res.getListeInferieurs()) {
					if (a.getMonmat() == matriculEmp) {
						res.getListeInferieurs().remove(a);
						System.out.println("l'employer a �t� suprimer");
					}
				}
			}else {
				System.out.println("cette employer n'existe pas");
			}
		}
	}
	//                                                    afficher la liste des sous employer d un responsable
	public static void afficheListSoufiffre(int matriculRes) {
		for(Employe z : getTousLesEmployes()) {
			if(z.getMonmat() == matriculRes && z instanceof Responsable) {
				Responsable res = (Responsable) z;
				for (Employe h : res.getListeInferieurs()) {
					System.out.println(" Empluy� n� : " +z.getMonmat()+" Nom : "+h.getNom()+ " Salaire : "+ h.getSalaire());
				}
			}
		}
	}
	//--------------     supprimer un reponsable et refiler ses larbin a so supperieur
	public static void supRes(int matrespon) {
		for(Employe resDuRes : getTousLesEmployes()) {
			if (resDuRes instanceof Responsable) {
				for(Employe arp : ((Responsable) resDuRes).getListeInferieurs()) {
					if(matrespon == arp.getMonmat()) {
						Responsable a = (Responsable)arp;
						if(!a.getListeInferieurs().isEmpty()) {
							for (Employe fr : a.getListeInferieurs()) {
								((Responsable) resDuRes).getListeInferieurs().add(fr);
							}
							getTousLesEmployes().remove(arp);
						}
					}
				}

			}
		}
	}
	
	
	
	
	
	
	
	
	//----- modifier somme des vznte d un Commercial

	public static void modifSommeVente(int matricommercial, double newSommeVente) {

		for(Employe el : getTousLesEmployes()) {
			if(el.getMonmat() == matricommercial && el instanceof Commercial) {
				Commercial com = (Commercial) el;
				com.setSommeDesventes(newSommeVente);
			}
		}

	}
	
	
	public static void modifInteret(double interet) {
        Commercial.setInteressement(interet);
        System.out.println("Le nouveau taux d'interessement est de "+ interet);
    }
	
	
	
	
	
	
	@Override
	public String toString() {
		return "Employe [nom=" + nom + ", monmat=" + monmat + ", indiceSalaire=" + indiceSalaire + "]";
	}
	public static ArrayList<Employe> getTousLesEmployes() {
		return tousLesEmployes;
	}
	public static void setTousLesEmployes(ArrayList<Employe> tousLesEmployes) {
		Employe.tousLesEmployes = tousLesEmployes;
	}
	public static int getMatricule() {
		return matricule;
	}
	public static void setMatricule(int matricule) {
		Employe.matricule = matricule;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getMonmat() {
		return monmat;
	}
	public void setMonmat(int monmat) {
		this.monmat = monmat;
	}
	public double getIndiceSalaire() {
		return indiceSalaire;
	}
	public void setIndiceSalaire(double indiceSalaire) {
		this.indiceSalaire = indiceSalaire;
	}
	public static double getSalaire() {
		return salaire;
	}
}

