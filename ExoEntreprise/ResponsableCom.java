package ExoEntreprise;

import java.util.HashSet;

public class ResponsableCom extends Commercial {
	
	// attrinbut
		private HashSet<Employe> listeInferieursHierarchiquesResCom;


		// constructeur
		public ResponsableCom (String nom, int indiceSalarial, double sommeDesventes) {
			super(nom, indiceSalarial, sommeDesventes);
			this.listeInferieursHierarchiquesResCom = new HashSet<Employe>();
		}

		
		// get set

		public HashSet<Employe> getListeInferieursHierarchiquesResCom() {
			return listeInferieursHierarchiquesResCom;
		}
	

}
