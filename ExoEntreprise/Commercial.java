package ExoEntreprise;

public class Commercial extends Employe {
    // attributes
    private double sommeDesventes;
    private static double interessement = 10;
    // methods
    @Override
    public String toString() {
        String str = "[Commercial] " + super.toString() + " sommeDesventes = " + sommeDesventes;
        return str;
    }
    private double calculePrime() {
        return this.sommeDesventes*(interessement/100);
    }
    public double calculeSalaireCommercial() {
        return Employe.getSalaire() * this.getIndiceSalaire() + this.calculePrime();
    }
    public static void remiseZeroVentes() {
        for (Employe employed : Employe.getTousLesEmployes()) {
            if (employed instanceof Commercial) {
                ((Commercial) employed).setSommeDesventes(0);
            }
        }
    }
    
    
    // constructors
    
    public Commercial(String nom, double indiceSalarial, double sommeDesventes) {
        super(nom, indiceSalarial);
        this.sommeDesventes = sommeDesventes;
    }
    // getters & setters
    
    public double getSommeDesventes() {
        return sommeDesventes;
    }
    public static double getInteressement() {
        return interessement;
    }
    public static void setInteressement(double interessement) {
        Commercial.interessement = interessement;
    }
    public void setSommeDesventes(double sommeDesventes) {
        this.sommeDesventes = sommeDesventes;
    }
}