package ExoEntreprise;

import java.util.HashSet;

public class Responsable extends Employe {

	// attrinbut
	private HashSet<Employe> listeInferieursHierarchiques;


	// constructeur

	public Responsable(String Nom, double IndiceSalarial){
		super(Nom,IndiceSalarial);
		this.listeInferieursHierarchiques = new HashSet<Employe>();
	}

	//--------- methodess


	//                   set et get

	public HashSet<Employe> getListeInferieurs() {
		return listeInferieursHierarchiques;
	}

	

}
