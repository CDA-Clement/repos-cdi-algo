package point;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import java.util.TreeMap;

public class Point {
	private static ArrayList<Point>touslesPoints= new ArrayList<Point>();
	


	
	private double abscisse;
	private double ordonnee;
	private String nomPoint;
	private int num;

	public Point(String nom, int numero, double abscisseP, double ordonneP) {
		nomPoint=nom;
		num=numero;
		abscisse=abscisseP;
		ordonnee=ordonneP;
		touslesPoints.add(this);
		

	}


	public static ArrayList<Point> getTouslesPoints() {
		return touslesPoints;
	}


	public static PointExistant creerPoint(String nom2, int num)throws PointExistant { {
		boolean trouve = false;
		for (Point point : touslesPoints) {
			trouve = false;

			if(nom2.equals(point.nomPoint)&& num==point.num) {
				trouve=true;
				break;
			}
		}
		if(trouve==true ) {
			return new PointExistant(); 
		}

	}
	
	return null;

	}


	public double getAbscisse() {
		return abscisse;
	}

	public double getOrdonnee() {
		return ordonnee;
	}

	public void afficher() {
		System.out.println("distance "+this.distanceOrigin()+" nom "+this.nomPoint+this.num+" pour abscisse "+this.abscisse+" pour ordonnee "+this.ordonnee);
	}



	/**
	 * calcule la distance entre le point en cours et le point pass� en param�tre
	 * @param param le point avec lequel on calcule la distance
	 * @return la distance
	 */
	

	public double distanceOrigin() {
		double distance = 0;
		distance = Math.sqrt(Math.pow((this.getAbscisse()),2) + Math.pow((this.getOrdonnee()),2));
		return( Math.round(distance * 100.0) / 100.0);

	}



	public static void listerPoint() {
		Point.order(touslesPoints);
	}

		
	
	private static void order(List<Point> listpoints) {

	    Collections.sort(listpoints, new Comparator<Point>() {

	        public int compare(Point p1, Point p2) {

	            Double dist1 = p1.distanceOrigin();
	            Double dist2 = p2.distanceOrigin();
	            int sComp = dist1.compareTo(dist2);

	            if (sComp != 0) {
	               return sComp;
	            } 

	            String nom1 = p1.nomPoint;
	            String nom2 = p2.nomPoint;
	            int sComp1 = nom1.compareTo(nom2);
	            
	            if (sComp1 != 0) {
		               return sComp1;
		            }
	            
	            Integer num1 = p1.num;
	            Integer num2 = p2.num;
	            return num1.compareTo(num2);
	    }});
	
	for (Point point : listpoints) {
		point.afficher();
		
	}
	}


	
	
	

	



}


