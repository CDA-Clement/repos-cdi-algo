 <%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c" %>
<header>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a id ="accueil" class="navbar-brand" href="#">Accueil</a>
    <c:if test="${empty user }">
     <a id="login" class="navbar-brand" href="connexion.do">login</a>
     </c:if>
     <c:if test="${not empty user }">
  
    	<c:out value="${user.nom}"/> 
    		    	
  
      <a id="logout" class="navbar-brand" href="deconnexion.do">logout</a>
     </c:if>
    
  </div>
</nav>
</header>