<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@include file="Header.jsp" %>

<c:if test="${not empty user }">

<c:if test="${total/1000==0.0}">
<div class="alert alert-primary alert-dismissible fade show" role="alert">
					<strong>Bienvenu</strong>
					</div>
</c:if>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<strong>cela fait ${total/1000} secondes que l'on ne vous a pas vu</strong>
					</div>
					</c:if>
<div class="row justify-content-center">
<div class="col-6" >
<table class="table">
  <thead class="thead-dark">
    <tr>
     
      <th scope="col">Nom</th>
      <th scope="col">Prix</th>
     
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${liste}" var="p">
    <tr>
      
      <td>${p.label}</td>
      <td>${p.prix}</td>
    </tr>
     </c:forEach>
  </tbody>
</table>
</div>
</div>


<c:if test="${not empty user }">

<div class="row justify-content-center">
<form action="ajouter.do">
<input type="submit" class="fadeIn fourth" value="ajouter un produit">
</form>
</div>
</c:if>




<script src="jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>
</html>