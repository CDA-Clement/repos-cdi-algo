<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@include file="Header.jsp" %>

<form action="ajouter.do" method="post">
  <div class="form-group">
  
    <label for="exampleInputText">Nom</label>
    <input type="text" class="form-control" id="nomProduit" name="nomProduit" placeholder="Enter nom Produit">
    <small id="textHelp" class="form-text text-muted">veuillez choisir un nom pour le produit.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPrix">Prix</label>
    <input type="number" class="form-control" id="prixProduit" name="prixProduit" placeholder="prix en euros">
  </div>
  <button type="submit" class="btn btn-primary">ajouter le produit</button>
</form>





<script src="jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>
</html>