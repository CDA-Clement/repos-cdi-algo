package com.afpa.cookie.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.afpa.cookie.Dto.ProduitDto;
import com.afpa.cookie.Dto.UserDto;

/**
 * Servlet implementation class Accueil
 */
@WebServlet({ "/ajouter.do"})
public class Ajouter extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static List<ProduitDto> produits= new ArrayList<ProduitDto>();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserDto user=(UserDto)session.getAttribute("user");

		System.out.println("user: "+user+";");



		if(user==null) {
			System.out.println("pas de user");
			response.sendRedirect("index.html");
		}else {

			request.setAttribute("liste", produits);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Ajouter.jsp").forward(request, response);
		}

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		UserDto user=(UserDto)session.getAttribute("user");
		
		if(user==null) {
			System.out.println("pas de user");
			response.sendRedirect("index.html");
		}else {
			String nomProduit=request.getParameter("nomProduit");
			String prixProduit=request.getParameter("prixProduit");
			System.out.println("nomProduit: "+nomProduit+":");
			System.out.println("prixProduit: "+prixProduit+":");
			
			if(nomProduit!=null&&prixProduit!=null) {
				ProduitDto produit=new ProduitDto();
				produit.setLabel(nomProduit);
				produit.setPrix(Integer.parseInt(prixProduit));
				AccueilServlet.produits.add(produit);
				System.out.println("je viens d'ajouter un produit");
				
			}
			
			getServletContext().getRequestDispatcher("/accueil.do").forward(request, response);
		}
	}
}