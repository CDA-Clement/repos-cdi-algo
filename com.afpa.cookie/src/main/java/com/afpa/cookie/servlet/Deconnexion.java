package com.afpa.cookie.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.afpa.cookie.Dto.ProduitDto;
import com.afpa.cookie.Dto.UserDto;

/**
 * Servlet implementation class Accueil
 */
@WebServlet({ "/deconnexion.do" })
public class Deconnexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	static List<ProduitDto> produits= new ArrayList<ProduitDto>();
	
    
	  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session= request.getSession();
		UserDto user =(UserDto)session.getAttribute("user");
		Cookie cookie=new Cookie("fincookie",user.getNom()+"/"+System.currentTimeMillis());
		response.addCookie(cookie);
		session.invalidate();
		this.getServletContext().getRequestDispatcher("/index.html").forward(request, response);
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		doGet(request, response);
		
		
		
	}
}
