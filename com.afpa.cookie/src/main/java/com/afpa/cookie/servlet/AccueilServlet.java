package com.afpa.cookie.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.core.env.SystemEnvironmentPropertySource;

import com.afpa.cookie.Dto.ProduitDto;
import com.afpa.cookie.Dto.UserDto;

/**
 * Servlet implementation class Accueil
 */
@WebServlet({ "/accueil.do", "/index.html" })
public class AccueilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	static List<ProduitDto> produits= new ArrayList<ProduitDto>();
	
    
	  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String login=request.getParameter("login");
		String password=request.getParameter("password");
		System.out.println("login: "+login+";");
		System.out.println("password :"+password+";");
		
		if(login!=null && password!=null) {
			UserDto user= new UserDto();
			user.setNom(login);
			user.setPassword(password);
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			Cookie cookie= new Cookie("debutcookie", login+"/"+System.currentTimeMillis());
			response.addCookie(cookie);
			Cookie[] cookies=request.getCookies();
			
			String fin="";
//			List<Cookie> lCookie = new ArrayList<Cookie>();
//			for(Cookie c : cookies) {
//				lCookie.add(c);
//			}
			
				
				for (int i = 0; i < cookies.length; i++) {
					
					if(cookies[i].getName().equals("fincookie")) {
						
						fin =cookies[i].getValue();
						String[]tabFin=fin.split("/");
						fin=tabFin[1];
						System.out.println("fin"+fin);
						Long total=System.currentTimeMillis()-Long.parseLong(fin);
						request.setAttribute("total", total);
					}
					
				
			}
			
			System.out.println("je viens de creer un user");
		}
		
		request.setAttribute("liste", produits);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		
	}
}
