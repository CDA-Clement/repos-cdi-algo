package com.afpa.cookie.Dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProduitDto {

	String label;
	Integer prix;
}


