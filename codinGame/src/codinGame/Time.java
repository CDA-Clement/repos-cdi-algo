package codinGame;

import java.util.*;
import java.io.*;
import java.math.*;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class Time {
	public static void main(String args[]) throws FileNotFoundException {
		//Scanner in = new Scanner(System.in);

			Scanner in = new Scanner(new File("C:\\git-repos\\repos-cdi-algo\\codinGame\\river/time.txt"));
		// new Scanner(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/codinGame/river/time.txt"));
		String Begin = in.next();
		String End = in.next();
		String[]BeginTab = new String [3]; 
		String[]EndTab = new String [3];
		BeginTab=Begin.split("\\.");
		EndTab=End.split("\\.");

		//24-May-2017, change this to your desired Start Date
		LocalDate Begin1 = LocalDate.of(Integer.parseInt(BeginTab[2]), Month.of(Integer.parseInt(BeginTab[1])), Integer.parseInt(BeginTab[0]));
		//29-July-2017, change this to your desired End Date
		LocalDate End1 = LocalDate.of(Integer.parseInt(EndTab[2]), Month.of(Integer.parseInt(EndTab[1])), Integer.parseInt(EndTab[0]));
		long totalDays = ChronoUnit.DAYS.between(Begin1, End1);
		long totalMonths= ChronoUnit.MONTHS.between(Begin1, End1);
		long totalYears = totalMonths/12;
		totalMonths = totalMonths%12;


		System.err.println(totalDays);
		System.err.println(totalMonths);
		System.err.println(totalYears);

		if (totalYears>1 && totalMonths%12==0) {
			System.out.println(totalYears+" years, "+"total "+ totalDays+" days");
		} 
		if (totalYears==1 && totalMonths%12==0) {
			System.out.println(totalYears+" year, "+"total "+ totalDays+" days");
		} 
		if(totalYears==0 && totalMonths==1) {
			System.out.println(totalMonths+" month, "+"total "+ totalDays+" days");
		}
		if(totalYears==0 && totalMonths>1) {
			System.out.println(totalMonths+" months, "+"total "+ totalDays+" days");
		}

		if(totalYears>1 && totalMonths==1) {
			System.out.println(totalYears+" years, "+totalMonths+" month "+"total " + totalDays+" days");
		}

		if(totalYears==1 && totalMonths>1) {
			System.out.println(totalYears+" year, "+totalMonths+" months " +"total "+ totalDays+" days");
		}

		if(totalYears>1 && totalMonths>1) {	
			System.out.println(totalYears+" years, "+totalMonths+" months " +"total "+ totalDays+" days");
		}

		if(totalYears==0 && totalMonths==0) {	
			System.out.println("total "+ totalDays+" days");
		}
	}
}



