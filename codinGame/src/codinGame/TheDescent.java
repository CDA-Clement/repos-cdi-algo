package codinGame;
import java.util.Scanner;

public class TheDescent {

	public static void main(String[] args) {

		/**
		 * The while loop represents the game.
		 * Each iteration represents a turn of the game
		 * where you are given inputs (the heights of the mountains)
		 * and where you have to print an output (the index of the mountain to fire on)
		 * The inputs you are given are automatically updated according to your last actions.
		 **/

		Scanner in = new Scanner(System.in);
		int max =0;
		int index =0;
		// game loop
		while (true) {
			for (int i = 0; i < 8; i++) {
				System.out.println("Please enter the height of the mountain "+i);
				int mountainH = in.nextInt(); // represents the height of one mountain.
				if(mountainH>max) {
					mountainH =max;
					index =i;
				}
				
					System.out.println("the mountain to fire on is "+index);
				}
				max = 0;
				System.out.println("\n");	

				in.close();		}
		
		
			// The index of the mountain to fire on.

			}
	
	}



