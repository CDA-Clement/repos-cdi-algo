package codinGame;
import java.util.*;
import java.io.*;
import java.math.*;

public class Defibrilator {


	public static void main(String args[]) throws FileNotFoundException {
		//Scanner in = new Scanner(System.in);

		Scanner in = new Scanner(new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/codinGame/river/defibril.txt"));
		TreeMap<Double, String> result = new TreeMap<Double, String>();
		double min = Double.MAX_VALUE;
		String lon = in.next();
		String lat = in.next();
		int n = in.nextInt();
		if (in.hasNextLine()) {
			in.nextLine();
		}
		for (int i = 0; i < n; i++) {
			String defib = in.nextLine();
			String[] res = defib.split("\\;");
			String longidefib = res[res.length-2].replace(',', '.');
			String latidefib = res[res.length-1].replace(',', '.');
			String latusr= lat.replace(',', '.');
			String longusr =lon.replace(',', '.');
			
			double x = (((Double.valueOf(longidefib))-(Double.valueOf(longusr)))*Math.cos((Double.valueOf(latusr)+Double.valueOf(latidefib))/2));
			double y = (Double.valueOf(latidefib))-Double.valueOf(latusr);
			double distance = (Math.sqrt(Math.pow(x, 2)+ Math.pow(y, 2)))*6371;
			int j=0;
			j++;
			result.put(distance, res[j]);
			double firstK=result.firstKey();
			System.out.println("dis "+distance );
			System.err.println("min "+min);
			System.err.println("key "+result.firstKey());
			if (min>result.firstKey()) {
				min = result.firstKey();
				
			}

		}

		System.out.println(result.get(min));	
	}
}




