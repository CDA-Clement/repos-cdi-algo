package exerciceNombreComplexe;

public class Programme {

	public static void main(String[] args) {
		Complexes A = new Complexes();
		System.out.println("A partie reel vaut "+A.getNombreReel());
		System.out.println("A partie imaginaire vaut "+ A.getNombreImaginaire());
		System.out.println("A i vaut "+A.getI());
		System.out.println("La valeur du nombre reel A PartieRéelle + PartieImaginaire * i ");
		A.afficher();
		System.out.println("*****************************************************");

		Complexes B = new Complexes(5,7,2);
		System.out.println("B partie reel vaut "+B.getNombreReel());
		System.out.println("B partie imaginaire vaut "+ B.getNombreImaginaire());
		System.out.println("B i vaut "+B.getI());
		System.out.println("La valeur du nombre reel B PartieRéelle + PartieImaginaire * i ");
		B.afficher();
		System.out.println("*****************************************************");

		Complexes C = A.plus(B);
		System.out.println("C partie reel vaut "+C.getNombreReel());
		System.out.println("C partie imaginaire vaut "+ C.getNombreImaginaire());
		System.out.println("C i vaut "+C.getI());
		System.out.println("La valeur du nombre reel C PartieRéelle + PartieImaginaire * i ");
		C.afficher();
		System.out.println("*****************************************************");




	}

}
