package exerciceNombreComplexe;

public class Complexes {
	private double nombreReel;
	private double nombreImaginaire;
	private double i;
	
	public Complexes() {
	}
	
	public double getNombreReel() {
		return nombreReel;
	}

	public double getNombreImaginaire() {
		return nombreImaginaire;
	}

	public double getI() {
		return i;
	}

	public Complexes(double j, double k, double l) {
		nombreReel = j; 
		nombreImaginaire = k; 
		i = l;
	}

	
	public Complexes plus(Complexes usinePlus) {
		this.nombreReel += usinePlus.nombreReel;
		this.nombreImaginaire += usinePlus.nombreImaginaire;
		this.i += usinePlus.i;
		return usinePlus;
		
	}
	public Complexes moins(Complexes usineMoins) {
		this.nombreReel += usineMoins.nombreReel;
		this.nombreImaginaire += usineMoins.nombreImaginaire;
		this.i += usineMoins.i;
		return usineMoins;
		
	}
	public void afficher(){
		System.out.println(this.nombreReel+this.nombreImaginaire*this.i);
	}
	

}
