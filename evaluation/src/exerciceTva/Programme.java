package exerciceTva;
import java.util.Scanner;
public class Programme {
	Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Article A = new Article();
		A.calculTva();
		A.afficher();
		
		System.out.println("**********************************************************************");
		Article B = new Article(3, "AAZ", 20,19.6);
		B.calculTva();
		B.afficher();
		
		System.out.println("**********************************************************************");
		Article C = new Article	(0, "c");
		C.calculTva();
		C.afficher();
		
		System.out.println("**********************************************************************");
		Article D =new Article(B);
		D.calculTva();
		D.afficher();

	System.out.println("La valeur de l'attribut Tva de la classe Article est "+Article.Tva+"%");
	}
}


