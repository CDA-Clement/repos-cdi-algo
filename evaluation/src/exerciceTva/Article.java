package exerciceTva;
import java.util.Scanner;
import java.lang.Math;
import java.text.DecimalFormat;

public class Article {
	Scanner sc = new Scanner(System.in);
	static double Tva;
	static{
		Tva = 19.6;
	}
	private int reference;
	private String designation;
	private double prixHT;
	double tauxTva = Tva;


	public int getReference() {
		return reference;
	}
	public void setReference(int reference) {
		reference = reference;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		designation = designation;
	}
	public double getPrixHT() {
		return prixHT;
	}
	public void setPrixHT(double prixHT) {
		prixHT = prixHT;
	}
	public double getTva() {
		return Tva;
	}
	public void setTauxTVA(double tauxTVA) {
		tauxTVA = tauxTVA;
	}

	public Article() {			
	}

	public Article(int ref, String des, double HT, double TVA) {
		reference = ref;
		designation = des;
		prixHT = HT;
		Tva =TVA;
	}


	public Article (double ref, String des) {
		System.out.println("veuillez saisir une reference");
		reference = sc.nextInt();
		System.out.println("veuillez saisir une designation");
		designation = sc.next();
	}


	public Article(Article c) {
		this.reference = c.reference;
		this.designation = c.designation;
		this.prixHT = c.prixHT;
		this.Tva = c.Tva;

	}
	public String calculTva() {
		double prixTTC =0;
		prixTTC =prixHT*((Tva/100)+1);
		DecimalFormat f = new DecimalFormat("##.00");

		return f.format(prixTTC);
	}

	public void afficher() {
		System.out.println("La reference est "+reference);
		System.out.println("La designation du produit est "+designation);
		System.out.println("Le prix HT est de "+prixHT+"€");
		System.out.println("Le taux de TVA est de "+Tva+"%");
		System.out.println("Le prix TTC est de "+calculTva()+"€");

	}
	
}

