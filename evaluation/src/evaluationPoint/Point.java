package evaluationPoint;

public class Point {
	double abscisse;
	double ordonnee;

	public double getAbscisse() {
		return abscisse;
	}

	public double getOrdonnee() {
		return ordonnee;
	}

	public Point() {
		abscisse =5;
		ordonnee =3;
		
	}
	public Point(double i, double j) {
		abscisse = i;
		ordonnee = j;
	}

	public void afficher() {
		System.out.println("Abscisse "+this.abscisse + " Ordonnee "+this.ordonnee);
	}

	public void Norme(Point origin) {

		double distance = Math.sqrt((this.ordonnee - origin.ordonnee ) * (this.ordonnee - origin.ordonnee) + (this.abscisse - origin.abscisse) * (this.abscisse - origin.abscisse));  
		System.out.println("Le point se trouve à " +distance+" de l'origine du repère");
	}

}
