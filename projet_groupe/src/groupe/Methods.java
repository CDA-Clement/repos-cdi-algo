package groupe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Methods {

	
	public static ArrayList<String> transformeA2AL(String[] tab) {

		ArrayList<String> transformation = new ArrayList<>();
		for (String string : tab) {
			transformation.add(string);
		}
		return transformation;

	}

	
	public static String[] transformeAL2A(ArrayList<String> al) {

		String[] transformation = new String[al.size()];

		for (int i = 0; i < al.size(); i++) {
			transformation[i] = al.get(i);
		}
		return transformation;
	}

	
	public static String[] transformeHM2A(HashMap<Voiture, String> hm) {

		
		Map<Voiture, String> transformation = new HashMap<Voiture, String>();
		transformation = hm;

		Voiture voitureBulle = new Voiture();
		Voiture voitureBulle2 = new Voiture();
		String bubulle = new String();
		String[] renvoi = new String[transformation.keySet().size()];
		int i = 0;
		while (!transformation.isEmpty()) {
			for (Entry<Voiture, String> mapentry : transformation.entrySet()) {

				voitureBulle2 = mapentry.getKey();

				if (voitureBulle.getAnnee() == 0) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();

				}
				if (voitureBulle2.getAnnee() < voitureBulle.getAnnee()) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();

				} else if (voitureBulle2.getMarque().compareTo(voitureBulle.getMarque()) > 0
						&& voitureBulle2.getAnnee() == voitureBulle.getAnnee()) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();

				} else if (voitureBulle2.getModel().compareTo(voitureBulle.getModel()) > 0
						&& voitureBulle2.getMarque().compareTo(voitureBulle.getMarque()) == 0) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();
				}

			}
			transformation.remove(voitureBulle);
			renvoi[i] = bubulle;
			i++;
			voitureBulle.setAnnee(0);
			;

		}

		return renvoi;

	}


	public static HashMap<Integer, String> transformeA2HM(String[] tab) {

		Methods.triString(tab);
		HashMap<Integer, String> transformation = new HashMap<>();

		for (int i = 0; i < tab.length; i++) {
			transformation.put(i, tab[i]);
		}
		return transformation;
	}

	
	public static ArrayList<String> transformeHM2AL(HashMap<Voiture, String> hm) {

		
		ArrayList<String> renvoi = new ArrayList<String>();
		Map<Voiture, String> transformation = new HashMap<Voiture, String>();
		transformation = hm;

		Voiture voitureBulle = new Voiture();
		Voiture voitureBulle2 = new Voiture();
		String bubulle = new String();

		while (!transformation.isEmpty()) {
			for (Entry<Voiture, String> mapentry : transformation.entrySet()) {

				voitureBulle2 = mapentry.getKey();

				if (voitureBulle.getAnnee() == 0) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();

				}
				if (voitureBulle2.getAnnee() < voitureBulle.getAnnee()) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();

				} else if (voitureBulle2.getMarque().compareTo(voitureBulle.getMarque()) > 0
						&& voitureBulle2.getAnnee() == voitureBulle.getAnnee()) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();

				} else if (voitureBulle2.getModel().compareTo(voitureBulle.getModel()) > 0
						&& voitureBulle2.getMarque().compareTo(voitureBulle.getMarque()) == 0) {
					voitureBulle = voitureBulle2;
					bubulle = mapentry.getValue();
				}

			}
			transformation.remove(voitureBulle);
			renvoi.add(bubulle);
			voitureBulle.setAnnee(0);
			;

		}

		return renvoi;

	}

	
	private static void triString(String[] tab) {

		boolean tri;
		String bulle = new String();

		do {

			tri = false;
			for (int i = 0; i < tab.length - 1; i++) {
				if (tab[i].compareTo(tab[i + 1]) > 0) {
					bulle = tab[i];
					tab[i] = tab[i + 1];
					tab[i + 1] = bulle;
					tri = true;
				}
			}

		} while (tri);

	}

}