package groupe;

public class Voiture {

	// attributes
	private String marque;
	private String model;
	private int annee;

	// methods

	@Override
	public String toString() {
		return "Voiture [marque=" + marque + ", model=" + model + ", annee=" + annee + "]";
	}

	// constructors

	public Voiture() {

	}

	public Voiture(String marque, String model, int annee) {
		this.marque = marque;
		this.model = model;
		this.annee = annee;
	}

	// getters & setters

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

}
