--afficher toutes les colonnes
select service,noserv,ville from serv;
select serv.* from serv;

--afficher deux colonnes
select service,noserv from serv;

-- afficher les differents emploi
select distinct emploi from emp;

-- S�lectionner les employ�s du service NN�3.
select * from emp where noserv='3';


--S�lectionner les noms, pr�noms, num�ro d�employ�, num�ro de service de tous les techniciens.
select nom,prenom,noemp,noserv from emp where emploi='TECHNICIEN';


--S�lectionner les noms, num�ros de service de tous les services dont le num�ro est inf�rieur ou �gal � 2.
select service, noserv from serv where noserv<='2';

--S�lectionner les noms, num�ros de service de tous les services dont le num�ro est sup�rieur � 2.
select service, noserv from serv where noserv>='2';

--S�lectionner les employ�s dont la commission est inf�rieure au salaire.
select * from emp where comm<sal;

--S�lectionner les employ�s qui ne touchent jamais de commission.
select * from emp where comm isnull;

-- S�lectionner les employ�s qui touchent �ventuellement une commission et dans l�ordre croissant des commissions.
select * from emp where comm>='0' ORDER BY comm ASC;


--S�lectionner les employ�s qui n�ont pas de chef.
select * from emp where sup isnull;

--S�lectionner les employ�s qui ont un chef.
select * from emp where sup is not null;

-- S�lectionner les noms, emploi, salaire, num�ro de service de tous les employ�s du service 5 qui gagnent plus de 20000
select nom, emploi, sal, noserv from emp where (sal+coalesce(comm,0)>20000) and noserv='5';


--S�lectionner les vendeurs du service 6 qui ont un revenu mensuel sup�rieur ou �gal � 9500
select * from emp where emploi='VENDEUR' and noserv ='6' and (sal+coalesce(comm,0)>=9500);


-- S�lectionner dans les employ�s tous les pr�sidents et directeurs. Attention, le fran�ais et la logique sont parfois contradictoires.
select * from emp where emploi='DIRECTEUR' or emploi='PRESIDENT';
select * from emp where emploi in ('PRESIDENT', 'DIRECTEUR');

-- S�lectionner les directeurs qui ne sont pas dans le service 3.
select * from emp where not noserv='3' and emploi='DIRECTEUR';

--S�lectionner les directeurs et � les techniciens du service 1 �.
select * from emp where emploi='DIRECTEUR' or (noserv='1'and emploi='TECHNICIEN');

--S�lectionner les � directeurs et les techniciens � du service 1.
select * from emp where noserv=1 and emploi in('DIRECTEUR', 'TECHNICIEN');

--S�lectionner les employ�s du service 1 qui sont directeurs ou Techniciens.
select * from emp where (noserv=1 and emploi = 'DIRECTEUR' or noserv=1 and emploi='TECHNICIEN');

--S�lectionner les employ�s qui ne sont ni directeur, ni technicien et travaillant dans le service 1.
select * from emp where emploi not in('DIRECTEUR','TECHNICIEN') and noserv=1;

select * from emp where nom like '%D';

--24 : S�lectionner les employ�s qui sont techniciens, comptables ou vendeurs.
select * from emp where emploi in('TECHNICIEN', 'COMPTABLE', 'VENDEUR');

--25 : S�lectionner les employ�s qui ne sont ni technicien, ni comptable, ni vendeur.
select * from emp where emploi not in('TECHNICIEN', 'COMPTABLE', 'VENDEUR');

--26 : S�lectionner les directeurs des services 2, 4 et 5.
select* from emp where emploi='DIRECTEUR' and noserv in('2','4','5');

--27 : S�lectionner les subalternes qui ne sont pas dans les services 1, 3, 5.
select* from emp where sup is not null and noserv not in('1','3','5');

--28 : S�lectionner les employ�s qui gagnent entre 20000 et 40000 euros, bornes comprises.
select * from emp where sal+coalesce(comm,0) between 20000 and 40000;

--29: S�lectionner les employ�s qui gagnent moins de 20000 et plus de 40000 euros.
--select* from emp where (sal+coalesce(comm,0) <20000) or (sal+coalesce(comm,0) > 40000);
select * from emp where sal+coalesce(comm,0) not between 20000 and 40000;

--30 : S�lectionner les employ�s qui ne sont pas directeur et qui ont �t� embauch�s en 88.
select * from emp  where emploi !='DIRECTEUR' and embauche between '01/01/88' and '31/12/88';

--31 : S�lectionner les directeurs des services 2 ,3 , 4, 5 sans le IN
select * from emp where emploi='DIRECTEUR' and noserv between 2 and 5;

--32 :S�lectionner les employ�s dont le nom commence par la lettre M.
select* from emp where nom like 'M%';

--33 : S�lectionner les employ�s dont le nom se termine par T.
select* from emp where nom like '%T';

--34 : S�lectionner les employ�s ayant au moins deux E dans leur nom.
select * from emp where nom like '%E%E%';

--35 : S�lectionner les employ�s ayant exactement un E dans leur nom.
select * from emp where nom like '%E%' and nom not like '%E%E%';

--36 : S�lectionner les employ�s ayant au moins un N et un O dans leur nom.
--select * from emp where nom like '%N%O%' or nom like '%O%N%';
 select * from emp where nom like '%N%' and nom like '%O%';

--37 : S�lectionner les employ�s dont le nom s'�crit avec 6 caract�res et qui se termine par N.
select * from emp where nom like '_____N' ;

--38 : S�lectionner les employ�s dont la troisi�me lettre du nom est un R.
select * from emp where nom like '__R%' ;

--39 : S�lectionner les employ�s dont le nom ne s'�crit pas avec 5 caract�res.
select * from emp where nom not like'_____' ;

--40 : Trier les employ�s (nom, pr�nom, nn�de service, salaire) du service 3 par ordre de salaire croissant.
select nom, prenom, noserv, sal from emp where noserv=3 order by  sal asc; 

--41 : Trier les employ�s (nom, pr�nom, nn�de service , salaire) du service 3 par ordre de salaire d�croissant.
select nom, prenom, noserv, sal from emp where noserv=3 order by  sal desc; 

--42 : Idem en indiquant le num�ro de colonne � la place du nom colonne.
select nom, prenom, noserv, sal from emp where noserv=3 order by  4 desc; 

--43 : Trier les employ�s (nom, pr�nom, nn�de service, salaire, emploi) par emploi, et pour chaque emploi par ordre d�croissant de salaire.
select nom, prenom, noserv, sal, emploi from emp order by  emploi, sal desc; 


--44 : Idem en indiquant les num�ros de colonnes.
SELECT nom, prenom, noserv, sal, emploi from emp order by  5, 4 desc; 

--45 : Trier les employ�s (nom, pr�nom, nn�de service, commission) du service 3 par ordre croissant de commission.
select nom, prenom, noserv, comm from emp where noserv = 3 order by coalesce(comm,-1);

--46 : Trier les employ�s (nom, pr�nom, nn�de service, commission) du service 3 par ordre d�croissant de commission, en consid�rant que celui dont la commission est nulle ne touche pas de commission.
select nom, prenom, noserv, comm from emp where noserv = 3 and comm is not null order by comm desc;
select nom, prenom, noserv, comm from emp where noserv='3' and comm!='0' order by comm desc;
--47bis : S�lectionner le nom, le pr�nom, l'emploi, le nom du service de l'employ� pour un employ�.
select nom, prenom, emploi, service from emp inner join serv on emp.noserv=serv.noserv where noemp=1100;

--47bis : S�lectionner le nom, le pr�nom, l'emploi, le nom du service de l'employ� pour tous les employ�s.
select nom, prenom, emploi,serv.noserv, service from emp left join serv on emp.noserv=serv.noserv;

--49 : Idem en utilisant des alias pour les noms de tables.
select nom, prenom, emploi,s.noserv, service from emp as e left join serv as s on e.noserv=s.noserv;

--50 : S�lectionner le nom, l'emploi, suivis de toutes les colonnes de la table SERV pour tous les employ�s.
select emp.nom, emp.emploi, serv.* from emp left join serv on emp.noserv = serv.noserv;

--51 : S�lectionner les nom et date d'embauche des employ�s suivi des nom et date d'embauche de leur sup�rieur pour les employ�s plus ancien que leur sup�rieur, dans l'ordre
--nom employ�s, noms sup�rieurs.
select superieur.nom,superieur.embauche,employe.nom, employe.embauche 
from emp as employe 
inner join emp as superieur on employe.sup = superieur.noemp 
where employe.embauche<superieur.embauche 
order by employe.nom, superieur.nom; 

--52 : S�lectionner sans doublon les pr�noms des directeurs et � les pr�noms des techniciens du service 1 � avec un UNION
select prenom, emploi 
from emp 
where emploi = 'DIRECTEUR'
union
select prenom, emploi 
from emp
where emploi = 'TECHNICIEN' and noserv=1;

select e.prenom
from emp as e
where e.emploi='DIRECTEUR'
union distinct
select e.prenom
from emp as e
where noserv='1' and e.emploi='TECHNICIEN'

--53 : S�lectionner les num�ros de services n�ayant pas d�employ�s sans une jointure externe
select serv.noserv
from serv
where serv.noserv not in (select distinct emp.noserv from emp where emp.noserv is not null);

--54 : S�lectionner les services ayant au moins un employ�
select serv.noserv 
from serv
inner join emp on emp.noserv=serv.noserv;

select distinct s.service
from emp as e
inner join serv as s on e.noserv=s.noserv
where e.nom is not null and e.noserv is not null

--55 : S�lectionner les employ�s qui travaillent � LILLE.
select nom
from serv
inner join emp on serv.noserv=emp.noserv
where serv.ville='LILLE';

--56 : S�lectionner les employ�s qui ont le m�me chef que DUBOIS, DUBOIS exclu.
select distinct e.*
from emp as e
where e.nom!='DUBOIS' and e.sup= (select e1.sup from emp as e1 where e1.nom='DUBOIS');

select 
    collegue.nom, collegue.prenom
from
    emp as employer
inner join emp as collegue on (collegue.sup = employer.sup and employer.nom='DUBOIS' and collegue.nom!='DUBOIS')
order by 
    collegue.nom;

--57 : S�lectionner les employ�s embauch�s le m�me jour que DUMONT.
select distinct e.*
from emp as e
where e.embauche= (select embauche from emp as e1 where e1.nom='DUMONT');


--58 : S�lectionner les nom et date d'embauche des employ�s plus anciens que MINET, dans l�ordre des embauches.
select employe.nom,employe.embauche
from emp as employe 
where employe.embauche<(select embauche from emp as e1 where e1.nom='MINET');

--59 : S�lectionner le nom, le pr�nom, la date d�embauche des employ�s plus anciens que tous les employ�s du service NN�6. (Attention MIN)
select employe.nom, employe.prenom, employe.embauche
from emp as employe
where employe.embauche<(select min(e1.embauche) from emp as e1 where e1.noserv=6 );

--60 : S�lectionner le nom, le pr�nom, le revenu mensuel des employ�s qui gagnent plus 
--qu'au moins un employ� du service NN�3, trier le r�sultat dans l'ordre croissant des revenus mensuels.

select nom, prenom, sal, comm
from emp as e
where e.sal+coalesce(e.comm,0)>(select min(e1.sal+coalesce(e1.comm,0)) from emp as e1 where e1.noserv=3);

--61 : S�lectionner les noms, le num�ro de service, l�emplois et le salaires des personnes travaillant dans la m�me ville que HAVET.
with tablevilleHavet as (select s.ville from emp as e inner join serv as s on e.noserv=s.noserv where e.nom='HAVET')
select e.nom, e.prenom, s.ville, emploi
from emp as e
inner join serv as s on e.noserv=s.noserv
where s.ville = (select * from tablevilleHavet )

--62 : S�lectionner les employ�s du service 1, ayant le m�me emploi qu'un employ� du service NN�3.
select distinct e.emploi from emp as e inner join serv as s on e.noserv=s.noserv where e.noserv=3;

with emploiservice3 as (select distinct e.emploi from emp as e inner join serv as s on e.noserv=s.noserv where e.noserv=3)
select *
from emp as e
where e.emploi in (select * from emploiservice3)
and e.noserv=1
;

--63 : S�lectionner les employ�s du service 1 dont l'emploi n'existe pas dans le service 3.
select distinct e.emploi from emp as e inner join serv as s on e.noserv=s.noserv where e.noserv=3;

with emploiservice3 as (select distinct e.emploi from emp as e inner join serv as s on e.noserv=s.noserv where e.noserv=3)
select *
from emp as e
where e.emploi not in (select * from emploiservice3)
and e.noserv=1;

--71 :S�lectionner le nom, l�emploi, le revenu mensuel (nomm� Revenu) avec deux d�cimales pour tous les employ�s, dans l�ordre des revenus d�croissants.
select e.nom, e.emploi, round (cast((sal +coalesce(comm,0)) as numeric),2) as revenu
from emp as e
order by revenu desc;

--72 : S�lectionner le nom, le salaire, commission des employ�s dont la commission repr�sente plus que le double du salaire.
select nom, sal, comm
from emp
where comm>= sal*2;

--73 : S�lectionner nom, pr�nom, emploi, le pourcentage de commission (deux d�cimales) par
--rapport au revenu mensuel ( renomm� "% Commissions") , pour tous les vendeurs dans l'ordre d�croissant de ce pourcentage.
select nom, prenom, emploi, round(cast(((comm)/(sal+comm)*100)as numeric),2) as pourcentage
from emp
where comm is not null and comm!=0
order by pourcentage desc; 

--74 : S�lectionner le nom, l�emploi, le service et le revenu annuel ( � l�euro pr�s) de tous les vendeurs.
select e.nom, e.emploi, s.service, round(cast(((e.sal+coalesce(e.comm,0))*12)as numeric))
from emp as e
inner join serv as s on e.noserv=s.noserv

--75 : S�lectionner nom, pr�nom, emploi, salaire , commissions , revenu mensuel pour les employ�s des services 3,5,6
select noserv,nom, prenom, emploi, sal, coalesce(comm,0), (sal +coalesce(comm,0)) as revenu
from emp
where emp.noserv in(3,5,6);

--76 : Idem pour les employ�s des services 3,5,6 en rempla�ant les noms des colonnes : SAL par
--SALAIRE, COMM par COMMISSIONS, SAL+IFNULL(COMM,0 ) par GAIN_MENSUEL.
select nom, prenom, emploi, sal as "SALAIRE", comm as "COMMISSION", (sal +coalesce(comm,0)) as "REVENU_MENSUEL"
from emp
where noserv in (3,5,6);

--77 Idem pour les employ�s des services 3,5,6 en rempla�ant GAIN_ MENSUEL par GAIN MENSUEL
select nom, prenom, emploi, sal as "SALAIRE", comm as "COMMISSION", (sal +coalesce(comm,0)) as "REVENU MENSUEL"
from emp
where noserv in (3,5,6);

--78 : Afficher le nom, l'emploi, les salaires journalier et horaire pour les employ�s des services
--3,5,6 (22 jours/mois et 8 heures/jour), sans arrondi, arrondi au centime pr�s.


