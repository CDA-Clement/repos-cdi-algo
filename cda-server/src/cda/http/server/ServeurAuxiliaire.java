package cda.http.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ServeurAuxiliaire implements Runnable {
	private final Socket client;

	public ServeurAuxiliaire(Socket c) {
		this.client = c;
	}

	@Override
	public void run() {
		try {
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

			String uneLigne = br.readLine();
			System.out.println(uneLigne);
			String[] requeteTab = uneLigne.split(" ");
			if ("get".equalsIgnoreCase(requeteTab[0])) {
				File file = new File("C:\\outils\\cda-www\\" + requeteTab[1].substring(1));
			//		File file = new File("/Users/clement/Desktop/gitkraken/" + requeteTab[1].substring(1));
				if (file.getCanonicalPath().contains("outils")==false) {
					clientOut.write(("HTTP/1.1 " + CdaStatus.NOTfOUND.getCode() + " " + CdaStatus.NOTfOUND.getDescription() + "\n").getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					clientOut.write(("YOU CAN'T MOTHERFUCKER".getBytes()));

				}else
					if(file.exists()) {
						if(requeteTab[1].substring(1).endsWith("html")) {

							clientOut.write(
									("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
							System.err.println("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n");

							clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
							clientOut.write("\n".getBytes());
							FileInputStream fis = new FileInputStream(file);
							int contenu = 0;
							while ( (contenu = fis.read()) != -1) {
								fis.close();
								clientOut.write((byte)contenu);
							}
						}else if(requeteTab[1].substring(1).endsWith("jpeg")) {

							clientOut.write(
									("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
							//System.err.println("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n");

							clientOut.write("content-type:image/jpeg; charset=UTF-8\n".getBytes());
							clientOut.write("\n".getBytes());
							FileInputStream fis = new FileInputStream(file);
							int npreBytesLus = 0;
							byte[] tampon = new byte[1000];
							while ( (npreBytesLus = fis.read(tampon)) != -1) {
							clientOut.write(tampon,0,npreBytesLus);
							}
							fis.close();
						}


					}else {
						clientOut.write(("HTTP/1.1 " + CdaStatus.NOTfOUND.getCode() + " " + CdaStatus.NOTfOUND.getDescription() + "\n").getBytes());
						clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
						clientOut.write("\n".getBytes());
						clientOut.write((CdaStatus.NOTfOUND.getCode() + " " + CdaStatus.NOTfOUND.getDescription() + "\n").getBytes());
						System.err.println("HTTP/1.1 " + CdaStatus.NOTfOUND.getCode() + " " + CdaStatus.NOTfOUND.getDescription() + "\n");

					}
				System.err.println(requeteTab[1].substring(1));

			}else {
				clientOut.write(
						("HTTP/1.1 " + CdaStatus.ERROR.getCode() + " " + CdaStatus.ERROR.getDescription() + "\n").getBytes());
				clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
				clientOut.write("\n".getBytes());
				System.err.println("HTTP/1.1 " + CdaStatus.ERROR.getCode() + " " + CdaStatus.ERROR.getDescription() + "\n");

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
