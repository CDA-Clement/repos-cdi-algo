
//Seulement un objet de cette classe peut etre cree
public class Singleton1 {
	
	//Creation d'une instance statique du Singleton
		private static Singleton1 uniqueInstance= new Singleton1();
		
		//Un constructeur prive qui empeche toute autre classe de l'instancier
		private Singleton1() {	
		}
		
		
	
		//comme nous avons deja une instance, il nous suffit de la retourner
		public static Singleton1 getInstance() {
			return uniqueInstance;
		}
}
