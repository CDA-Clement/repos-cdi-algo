package iterator;

import java.util.Iterator;

public class Aggregate1 {
	String[] tab = {"Anthony", "Linda", "Clement", "Redouane", "Amaia", "Yassine", "Ketsia", "Maxime", "Laurent", "Soulaiman", "Badrane", "Mustapha", "Jean-Philippe", "Mohamed"};
	
	//Contient et utilise un it�rator
	public Iterator iterator() {
		return new Iterator1(this);
	}
	
	public int length() {
		return tab.length;
	}
	
	public String get(int indice) {
		return tab[indice];
	}	

}
