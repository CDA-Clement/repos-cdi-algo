package iterator;

import java.util.Iterator;

public class Main {
	public static void main(String[] args) {
		
		Aggregate1 list = new Aggregate1(); // creation d'un objet liste
		Iterator iter = list.iterator(); // creation d'un iterator associ�
		
		while(iter.hasNext()) { // parcours de la liste en cours
			System.out.println(iter.next());
		}
	}
	
}



