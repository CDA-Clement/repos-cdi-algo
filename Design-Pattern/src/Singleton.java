// Seulement un seul objet de cette classe peut etre cree
public class Singleton {
	
	//instance unique non préinitialisée
	private static Singleton instance=null;
	
	//Un constructeur prive qui empeche toute autre classe de l'instancier
	private Singleton() {	
	}
	
	//Methode qui fournit un moyen d'instancier la classe et d'en retourner une instance
	public static Singleton getInstance() {
		if(instance==null)
			instance = new Singleton();
	return instance;
	}
	

}
