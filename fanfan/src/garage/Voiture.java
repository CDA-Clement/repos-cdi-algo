package garage;

public class Voiture {
	private String marque;
	private String model;
	private int annee;

	public Voiture(String marque, String model, int annee) {
		this.marque= marque;
		this.model=model;
		this.annee=annee;
	}
	
	public String toString() {
		return "Voiture [marque=" + marque + ", model=" + model + ", annee=" + annee + "]";
	}	
	
	
	//GETTERS SETTERS
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	public int comparer(Voiture v) {
		if(this.annee < v.annee) {
			return -10;
		} else if(this.annee > v.annee) {
			return 50;
		} else {
			int res = this.marque.compareTo(v.marque);
			if(res != 0) {
				return res;
			} else {
				return this.model.compareTo(v.model);
			}
		}
	}
	
	
}