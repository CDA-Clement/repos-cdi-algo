package garage;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Scenario {
	public static void main(String[] args) {
		
		
		Voiture voiture1 = new Voiture("Mercedes", "Z3", 2011);
		Voiture voiture2 = new Voiture("Peugeot", "206", 2001);
		Voiture voiture3 = new Voiture("Citroen", "C3", 2005);

		ArrayList<String> voituresList = new ArrayList<>();

		voituresList.add(voiture1.getMarque());
		voituresList.add(voiture2.getMarque());
		voituresList.add(voiture3.getMarque());

		String[] voituresTab = new String[voituresList.size()];

		voituresTab = Methodes.ArrayToString(voituresList);

		System.out.println();
		System.out.println("***************************************************");
		System.out.println("AFFICHAGE DE LA METHODE transformeAL2A");
		System.out.println("***************************************************");
		System.out.println();

		for (String string : voituresTab) {
			System.out.println(string);
		}

		ArrayList<String> listeVide = new ArrayList<String>();

		listeVide = Methodes.StrToArray(voituresTab);

		System.out.println();
		System.out.println("***************************************************");
		System.out.println("AFFICHAGE DE LA METHODE transformeA2AL");
		System.out.println("***************************************************");
		System.out.println();
		for (String string : listeVide) {
			System.out.println(string);
		}

		System.out.println();
		System.out.println("***************************************************");
		System.out.println("AFFICHAGE DE LA METHODE transformeA2HM");
		System.out.println("***************************************************");
		System.out.println();

		Map<Integer, String> recupTab = new HashMap<Integer, String>();
		recupTab = Methodes.StrToHash(voituresTab);

		for (Entry<Integer, String> mapentry : recupTab.entrySet()) {
			System.out.println(mapentry.getKey()  + "  " + mapentry.getValue() );
		}
		
		System.out.println();
		System.out.println("***************************************************");
		System.out.println("AFFICHAGE DE LA METHODE transformeHM2AL");
		System.out.println("***************************************************");
		System.out.println();
		
		HashMap<Voiture, String> testage = new HashMap<Voiture, String>();
		ArrayList<String> recup = new ArrayList<String>();
		testage.put(voiture1, voiture1.getModel());
		testage.put(voiture2, voiture2.getModel());
		testage.put(voiture3, voiture3.getModel());
		
		recup = Methodes.HashMaptoArrayString(testage);
		
		for (String string : recup) {
			System.out.println(string);
		}
		
		System.out.println();
		System.out.println("***************************************************");
		System.out.println("AFFICHAGE DE LA METHODE transformeHM2A");
		System.out.println("***************************************************");
		System.out.println();
		
		HashMap<Voiture, String> testage2 = new HashMap<Voiture, String>();
		testage2.put(voiture1, voiture1.getModel());
		testage2.put(voiture2, voiture2.getModel());
		testage2.put(voiture3, voiture3.getModel());
		
		String[] tableauFinal = new String [recup.size()];
		tableauFinal = Methodes.HashMaptoStringTab(testage2);
		

		for (String string : tableauFinal) {
			System.out.println(string);
		}
		
		
	}
}