package garage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Methodes {

	// methode String[] vers ArrayList<String>
	public static ArrayList<String> StrToArray(String[] param){
		ArrayList<String> strToArray = new ArrayList();
		for (int i = 0; i < param.length; i++) {
			strToArray.add(param[i]);
		}

		return  strToArray;

	}
	//methode ArrayList<String> vers String[]

	public static String[] ArrayToString(ArrayList<String> param) {
		String[] stringTab = new String[param.size()];
		for (int i = 0; i < stringTab.length; i++) {
			stringTab[i] = param.get(i);
		}
		return stringTab ;
	}




	//methode string[] vers HashMap<Integer, String>

	public static HashMap<Integer, String> StrToHash (String [] param){
		HashMap<Integer, String> hashStr = new  HashMap<>();

		//Sorting methode
		for (int i = 0; i < param.length-1; i++) {
			if(param[i].compareTo(param[i+1])>0 ){
				String temp ="";
				temp = param[i];
				param[i] = param[i+1];
				param [i+1] = temp;
			}
		}

		// filling the hashmap
		for (int i = 0; i < param.length; i++) {
			hashStr.put(i, param[i]);
		}
		return hashStr;
	}



	//methode HashMap<Voiture, String> vers String[]

	public static String[] HashMaptoStringTab (HashMap<Voiture, String> param) {
		Set<Voiture> ensembleDesvoitures = param.keySet();

		int i = 0;
		Voiture[] tabVoiture = new Voiture[ensembleDesvoitures.size()];
		for(Voiture v : ensembleDesvoitures) {
			tabVoiture[i] = v;
			i++;
		}

		tabVoiture = triAscendant(tabVoiture);

		String[] stringTab = new String[param.size()];
		i=0;
		for(Voiture v : tabVoiture) {
			String laValeur = param.get(v);
			stringTab[i] = laValeur;
			i++;
		}

		return stringTab;
	}



	public static ArrayList <String> HashMaptoArrayString (HashMap<Voiture, String> param) {
		Set<Voiture> ensembleDesvoitures = param.keySet();

		int i = 0;
		Voiture[] tabVoiture = new Voiture[ensembleDesvoitures.size()];
		for(Voiture v : ensembleDesvoitures) {
			tabVoiture[i] = v;
			i++;
		}
		tabVoiture = triAscendant(tabVoiture);

		ArrayList<String> result = new ArrayList<>(); 

		
		for (Voiture v : tabVoiture) {
			String laValeur = param.get(v);
			result.add(param.get(v));
		}
		return result;
	}


	public static Voiture[] triAscendant(Voiture[] tab) {


		for (int i = 0; i < tab.length; i++) {

			for (int j = i+1; j < tab.length; j++) {
				if (tab[i].comparer(tab[j]) < 0) {
					Voiture temp = tab[i];
					tab[i] = tab[j];
					tab[j] = temp;
				}


			}
		}
		return tab;
	}		


}





