package com.afpa.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.afpa.main.Product;
import com.afpa.main.ProductNotFoundException;
import com.afpa.main.ShoppingCart;

class ShoppingTest {

	ShoppingCart shopTest;
	static Product itemTest;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		itemTest = new Product("livre", 25);

	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		shopTest = new ShoppingCart();

	}

	@AfterEach
	void tearDown() throws Exception {

	}

	@Test
	void testGetBalance() {
		assertTrue(shopTest.getBalance() == 0.0);
		shopTest.addItem(itemTest);
		assertTrue(shopTest.getBalance() == itemTest.getPrice());
	}

	@Test
	void testAddItem() {
		assertTrue(shopTest.getItemCount() == 0);
		shopTest.addItem(itemTest);
		shopTest.addItem(itemTest);
		assertTrue(shopTest.getItemCount() == 2, "impossible 2");
	}

	@Test
	void testRemoveItem() {
		assertTrue(shopTest.getItemCount() == 0);
		shopTest.addItem(itemTest);
		assertTrue(shopTest.getItemCount() == 1);
		try {
			shopTest.removeItem(itemTest);
			assertTrue(shopTest.getItemCount() == 0);
		} catch (Exception e) {
			fail("no exception in this case !!!");
		}

	}

	@Test
	void testRemoveItemUnknown() {
		assertTrue(shopTest.getItemCount() == 0);
		try {
			shopTest.removeItem(itemTest);
			fail("item not found ");
		} catch (Exception e) {
			assertTrue(e instanceof ProductNotFoundException);
		}
	}

	@Test
	void testGetItemCount() {
		assertTrue(shopTest.getItemCount() == 0);
		shopTest.addItem(itemTest);
		assertTrue(shopTest.getItemCount() == 1);
	}

	@Test
	void testEmpty() {
		assertTrue(shopTest.getItemCount() == 0);
		shopTest.addItem(itemTest);
		shopTest.addItem(itemTest);
		assertTrue(shopTest.getItemCount() == 2);
		shopTest.empty();
		assertTrue(shopTest.getItemCount() == 0);

	}

}
