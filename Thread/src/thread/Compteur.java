package thread;

import java.util.Date;

public class Compteur implements Runnable {
	String jeton = "joueur";
	private static Integer i = 0;
	private String nom;
	private Integer num;

	public Compteur(String name, Integer numero) {
		this.nom = name;
		this.num = numero;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < num; i++) {
				System.out.println(nom + " :" + i + " " + new Date().getTime());
				Thread.sleep(500);
			}
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

		synchronized (jeton) {
			i++;
			System.out.println("\n" + nom + " a fini de compter jusqu'a " + num + " en position " + i);
		}

	}
}
