package thread;

public class Compte {
	private int solde = 0;

	public void ajouter(int somme) {
		solde += somme;
		System.out.print(" ajoute " + somme);
	}

	public void retirer(int somme) {
		solde -= somme;
		System.out.print(" retire " + somme);
	}

	public synchronized void operationNulle(int somme) {
		solde += somme;
		System.out.print(" ajoute " + somme);
		solde -= somme;
		System.out.print(" retire " + somme);
	}

	public synchronized int getSolde() {
		return solde;
	}
}
