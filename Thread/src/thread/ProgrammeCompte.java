package thread;

public class ProgrammeCompte {
	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Thread(new Compteur("clement", 10));
		Thread t2 = new Thread(new Compteur("Anthony", 10));
		Thread t3 = new Thread(new Compteur("Amaia", 10));
		Thread t4 = new Thread(new Compteur("Redouane", 10));
		Thread t5 = new Thread(new Compteur("Richard", 10));
		Thread t6 = new Thread(new Compteur("Soulaiman", 10));
		Thread t7 = new Thread(new Compteur("Badrane", 10));
		Thread t8 = new Thread(new Compteur("Linda", 10));
		Thread t9 = new Thread(new Compteur("Maxime", 10));
		Thread t10 = new Thread(new Compteur("Yassine", 10));
		Thread t11 = new Thread(new Compteur("Laurent", 10));
		Thread t12 = new Thread(new Compteur("JP", 10));
		Thread t13 = new Thread(new Compteur("Mostapha", 10));
		Thread t14 = new Thread(new Compteur("Mohammed", 10));
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
		t7.start();
		t8.start();
		t9.start();
		t10.start();
		t11.start();
		t12.start();
		t13.start();
		t14.start();

	}

}
