package thread;

public class Program {

	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Perroquet("jacko");
		Thread t2 = new Perroquet("jacki");
		t2.start();
		t2.join();
		t1.start();

	}

}
