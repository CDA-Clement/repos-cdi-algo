package com.afpa.binstring;

public class Representation implements BinString {

	@Override
	public String convert(String s) throws ChaineVideException {
		if (s=="" || s==null) {
			throw new ChaineVideException();
		}
		else if (s=="0"){
			String s1 = "0";
			return s1;
		}
		else {
			return  Integer.toBinaryString(sum(s));
		}
	}


	@Override
	public int sum(String s) throws ChaineVideException {
		if (s=="" || s==null) {
			throw new ChaineVideException();
		}else {
			int prod = 0; 

			// Traverse string to find the product 
			for (int i = 0; i < s.length(); i++)  
			{ 
				prod += s.charAt(i); 
			} 

			// Return the product 
			System.out.println(prod);
			return prod; 
		}
	}

	@Override
	public String binarise(int x) throws ParametreNegatifException {
		if (x<0) {
			throw new ParametreNegatifException();
		}else {
			return Integer.toBinaryString(x);
		}
	}

}
