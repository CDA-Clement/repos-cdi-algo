package maven_project.exo1;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class CalculTest {
	
	Calcul test;
	
	@BeforeEach
	public void setUp() throws Exception {
		System.out.println("\n-----------------------------------   COCOU");
		test = new Calcul();

	}

    @Test
    public void testAddition(){
        assertTrue( test.addition(1, 2)==3);
    }
    
    @Test
    public void testdivision(){
        assertTrue( test.division(2, 2)==1);
    }
}
