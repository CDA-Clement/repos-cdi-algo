package barnYard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.Scanner;

public class Programme {
	public static void main(String args[]) throws Exception {

		//Scanner in = new Scanner(new File("C:\\git-repos\\repos-cdi-algo\\codinGame\\river/textformat.txt"));
		//Scanner sc =new Scanner (new File("/Users/clement/Desktop/gitkraken/repos-cdi-algo/codinGame/river/banyard.txt"));
		Scanner sc = new Scanner(System.in);
		Volaille.initpricemarketpoulet();
		Volaille.initpricemarketcanard();
		Volaille.initslaugtherhistorycanard();
		Volaille.initslaugtherhistorypoulet();
		String action="";
		boolean run=false;
		while(run==false ) {
			sc.reset();

			System.out.println("####################################################################################################################");
			System.out.println("####################################################################################################################");
			ASCIIArtGenerator.ART_SIZE_MEDIUM("     HOME");
			System.out.println("####################################################################################################################");
			System.out.println("####################################################################################################################");
			System.out.println();
			System.out.println();
			System.out.println("      1.	ADD A POULTRY");
			System.out.println("			Add Chicken");
			System.out.println("			Add Duck");
			System.out.println("			Add Peacock");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();
			System.out.println("      2.	DISPLAY STOCK");
			System.out.println("			Display all poultries");
			System.out.println("			Display Chicken");
			System.out.println("			Display Duck");
			System.out.println("			Display Peacock");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();									 
			System.out.println("      3.	DISPLAY THE LIST OF POULTRY READY TO BE SOLD");
			System.out.println("			Display list of poultry ready to be sold");
			System.out.println("			Display Sum of poultry ready to be sold");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();
			System.out.println("      4.	DISPLAY AND SET NEW PRICE");
			System.out.println("			Display price of the day");
			System.out.println(" 			Set new price");
			System.out.println("			Display history of prices");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();
			System.out.println("      5.	DISPLAY AND SET NEW SLAUGTHER WEIGHT");
			System.out.println("			Display slaugther weight of the day");
			System.out.println(" 			Set new slaughter weight");
			System.out.println("			Display history of slaugther weigth");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();
			System.out.println("      6.	UPDATE WEIGHT OF POULTRIES");
			System.out.println("			History of weight");
			System.out.println("			Update weight");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();
			System.out.println("      7.	SELL POULTRY");
			System.out.println("			Sell a Poultry");
			System.out.println("			Display revenue");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();
			System.out.println("      8.	RETURN PEACOCK");
			System.out.println("			HOME");
			System.out.println();
			System.out.println();
			System.out.println("      9.	END");
			System.out.println();
			System.out.println();
			System.out.println("####################################################################################################################");
			System.out.println("####################################################################################################################");
			action= sc.nextLine();
			if(action.equalsIgnoreCase("")) {
				action=sc.next();
			}

			if(action.equalsIgnoreCase("1")) {
				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("								  PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("								1. Add Chicken");
					System.out.println("								2. Add Duck");
					System.out.println("								3. Add Peacock");
					System.out.println("								4. HOME");
					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter the weight of the chicken");
						String weight= sc.next();
						Poulet.creerPoulet(Double.parseDouble(weight));	
					}

					if(choice.equalsIgnoreCase("2")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter the weight of the duck");
						String weight= sc.next();
						Canard.creerCanard(Double.parseDouble(weight));
					}

					if(choice.equalsIgnoreCase("3")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter the weight of the peacock");
						String weight= sc.next();
						Paon.creerPaon(Double.parseDouble(weight));
					}
					if(choice.equalsIgnoreCase("4")) {
						run1=true;
					}



				}
			}

			if(action.equalsIgnoreCase("2")) {
				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("								  PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("								1. Display all poultries");
					System.out.println("								2. Display Chickens");
					System.out.println("								3. Display Duck");
					System.out.println("								4. Display Peacock");
					System.out.println("								5. HOME");

					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						Volaille.afficherToutesLesVolailles();	
					}

					if(choice.equalsIgnoreCase("2")) {
						String param="Poulet";
						Volaille.afficherVolailleParType(param);
					}
					if(choice.equalsIgnoreCase("3")) {
						String param="Canard";
						Volaille.afficherVolailleParType(param);
					}
					if(choice.equalsIgnoreCase("4")) {
						String param="Paon";
						Volaille.afficherVolailleParType(param);
					}
					if(choice.equalsIgnoreCase("5")) {
						run1=true;
					}

				}
			}



			if(action.equalsIgnoreCase("3")) {

				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("									  PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("								1. Display list of poultry ready to be sold");
					System.out.println("								2. Display Sum of poultry ready to be sold");
					System.out.println("								3. HOME");

					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						System.out.println();
						System.out.println();
						System.out.println("*****************************************");
						System.out.println("Please enter 1 for CHICKEN,  2 for DUCK or 3 for ALL ready to be sold");
						String type= sc.next();
						if(type.equalsIgnoreCase("1")) {
							System.out.println("*****************************************");
							Poulet.listpoidAbattage();
						}
						else if(type.equalsIgnoreCase("2")) {
							System.out.println("*****************************************");
							Canard.listpoidAbattage();
						}
						else if(type.equalsIgnoreCase("3")) {
							System.out.println("*****************************************");
							Volaille.ready();
						}
						else {
							System.out.println("the type of poultry doesn't exist or not to sell");


						}
					}

					if(choice.equalsIgnoreCase("2")) {
						System.out.println();
						System.out.println();
						System.out.println("*****************************************");
						Volaille.sommeTypeAbattable();
					}

					if(choice.equalsIgnoreCase("3")) {
						run1=true;
					}

				}	

			}


			if(action.equalsIgnoreCase("4")) {

				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("									   PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("									1. Display price of the day");
					System.out.println("									2. Set new price");
					System.out.println("									3. Display history of prices");
					System.out.println("									4. HOME");

					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter 1 for CHICKEN, 2 for DUCK");
						choice=sc.next();
						if(choice.equalsIgnoreCase("1")) {
							System.out.println(Poulet.getPrixPoulet());
						}
						if(choice.equalsIgnoreCase("2")) {
							System.out.println(Canard.getPrixCanard());
						}

					}

					if(choice.equalsIgnoreCase("2")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter 1 for CHICKEN, 2 for DUCK");
						String type= sc.next();
						if(type.equalsIgnoreCase("1")) {
							System.out.println("*****************************************");
							System.out.println("Please enter a new price for the chicken");
							String newprice=sc.next();
							Poulet.ChangerPrixDuJour(Double.parseDouble(newprice));
						}

						else if(type.equalsIgnoreCase("2")) {
							System.out.println("*****************************************");
							System.out.println("Please enter a new price for the duck");
							String newprice=sc.next();
							Canard.ChangerPrixDuJour(Double.parseDouble(newprice));
						}
						else {
							System.out.println("the type of poultry doesn't exist or not to sell");
						}
					}
					if(choice.equalsIgnoreCase("3")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter 1 for CHICKEN, 2 for DUCK");
						String type= sc.next();
						if(type.equalsIgnoreCase("1")) {
							Volaille.displayhistoryofmarketprice(type);
						}

						if(type.equalsIgnoreCase("2")) {
							Volaille.displayhistoryofmarketprice(type);
						}

					}

					if(choice.equalsIgnoreCase("4")) {
						run1=true;
					}
				}
			}

			if(action.equalsIgnoreCase("5")) {
				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("									  PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("									1. Display slaugther Weight");
					System.out.println("									2. Set new slaugther Weight ");
					System.out.println("									3. Display history of Weight");
					System.out.println("									4. HOME");

					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter 1 for CHICKEN, 2 for DUCK");
						choice=sc.next();
						if(choice.equalsIgnoreCase("1")) {
							System.out.println(Poulet.getPoidsabattage());
						}
						if(choice.equalsIgnoreCase("2")) {
							System.out.println(Canard.getPoidsabattage());
						}
					}

					if(choice.equalsIgnoreCase("2")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter 1 for CHICKEN, 2 for DUCK");
						String type= sc.next();
						if(type.equalsIgnoreCase("1")) {
							System.out.println("*****************************************");
							System.out.println("Please enter a new weight for chickens");
							String newWeight=sc.next();
							Poulet.ChangerPoidsAbattage(Double.parseDouble(newWeight));
						}

						else if(type.equalsIgnoreCase("2")) {
							System.out.println("*****************************************");
							System.out.println("Please enter a new weight for ducks");
							String newWeight=sc.next();
							Canard.ChangerPoidsAbattage(Double.parseDouble(newWeight));
						}
						else {
							System.out.println("the type of poultry doesn't exist or not to sell");
						}
					}
					if(choice.equalsIgnoreCase("3")) {
						System.out.println();
						System.out.println();
						System.out.println("please enter 1 for CHICKEN, 2 for DUCK");
						String type= sc.next();
						if(type.equalsIgnoreCase("1")) {
							Volaille.displayhistoryofslaugtherWeigth(type);
						}

						if(type.equalsIgnoreCase("2")) {
							Volaille.displayhistoryofslaugtherWeigth(type);
						}



					}

					if(choice.equalsIgnoreCase("4")) {
						run1=true;
					}
				}


			}

			if(action.equalsIgnoreCase("6")) {
				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("									PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("								1. Update weigth of the animals");
					System.out.println("								2. History of weights");
					System.out.println("								3. HOME");

					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						System.out.println("Please enter the ID num");
						int id=sc.nextInt();
						System.out.println("Please enter the new weight");
						String weight= sc.next();
						Volaille.modifierPoidsVolaille(id, Double.parseDouble(weight));
					}

					if(choice.equalsIgnoreCase("2")) {
						System.out.println("Please enter the ID num");
						int id=sc.nextInt();
						Volaille.displayhistoryWeight(id);
					}


					if(choice.equalsIgnoreCase("3")) {
						run1=true;
					}

				}

			}


			if(action.equalsIgnoreCase("7")) {
				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("									  PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("								1. Sell a Poultry");
					System.out.println("								2. Display revenue");
					System.out.println("								3. HOME");

					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						System.out.println();
						System.out.println();
						System.out.println("*****************************************");
						System.out.println("Please enter the ID num of the poultry whom you want to sell");
						String id= sc.next();
						Volaille.sellPoulrty(Integer.parseInt(id));

					}

					if(choice.equalsIgnoreCase("2")) {
						System.out.println();
						System.out.println();
						System.out.println("*****************************************");
						Volaille.revenue();
					}

					if(choice.equalsIgnoreCase("3")) {
						run1=true;
					}

				}

			}
			if(action.equalsIgnoreCase("8")) {
				boolean run1=false;
				while(run1==false) {
					System.out.println();
					System.out.println();
					System.out.println("####################################################################################################################");
					System.out.println("####################################################################################################################");
					System.out.println("									  PLEASE CHOOSE ACTION");
					System.out.println();
					System.out.println("								1. Return Peacock");
					System.out.println("								2. HOME");

					String choice=sc.next();

					if(choice.equalsIgnoreCase("1")) {
						System.out.println();
						System.out.println();
						System.out.println("*****************************************");
						System.out.println("Please enter the ID num of the peacock whom you want to return");
						String id= sc.next();
						Volaille.returnPeacock(Integer.parseInt(id));

					}

					if(choice.equalsIgnoreCase("2")) {
						run1=true;
					}
				}
			}
			if(action.equalsIgnoreCase("9")) {
				run=true;

			}
		}

		System.out.println();
		System.out.println();
		System.out.println("####################################################################################################################");
		System.out.println("####################################################################################################################");
		System.out.println();
		System.out.println();
		ASCIIArtGenerator.ART_SIZE_SMALL("  SEE YOU SOON");
	}
}






