package barnYard;

import java.util.Date;
import java.util.HashMap;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

import javax.management.loading.PrivateClassLoader;

public class Volaille {

	private static int  num;
	private double poids;
	private int numvol;
	private  TreeMap<String, Double>weightOfPoultry= new TreeMap<String, Double>();
	
	//***********************************************************************************************************
	// CONSTRUCTORS
	
	public Volaille(double aPoids) {
		num++;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String formatDateTime = now.format(formatter);
		
		this.poids=aPoids;
		this.numvol=num;
		toutesLesVollailes.add(this);
		weightOfPoultry.put(formatDateTime, this.poids);
		System.err.println(weightOfPoultry.toString());



	}

	public Volaille() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String formatDateTime = now.format(formatter);
	}

	//***********************************************************************************************************
	
	// ATTRIBUTS OF CLASS 

	private static ArrayList<ArrayList<Volaille>> hystoryweight= new ArrayList<ArrayList<Volaille>>();
	private static TreeMap<String ,Double> historySlaugtherWeightPoulet= new TreeMap<String,Double>();
	private static TreeMap<String ,Double> historySlaugtherWeightCanard= new TreeMap<String,Double>();
	private static ArrayList<Volaille>toutesLesVollailes= new ArrayList<Volaille>();
	private static TreeMap<String, Double>historymarketpricePoulet= new TreeMap<String, Double>();
	private static TreeMap<String, Double>historymarketpriceCanard= new TreeMap<String, Double>();

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	private static LocalDateTime now = LocalDateTime.now();
	private static String formatDateTime = now.format(formatter);
	private static double revenue; 
	
	//***********************************************************************************************************

	// INIT ARRAYLISTS OR MAPS
	public static void initpricemarketpoulet() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		historymarketpricePoulet.put(formatDateTime, Poulet.getPrixPoulet());
	}
	public static void initpricemarketcanard() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		historymarketpriceCanard.put(formatDateTime, Canard.getPrixCanard());
	}

	public static void initslaugtherhistorypoulet() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		historySlaugtherWeightPoulet.put(formatDateTime, Poulet.getPoidsabattage());
	}
	public static void initslaugtherhistorycanard() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		historySlaugtherWeightCanard.put(formatDateTime, Canard.getPoidsabattage());
	}

	
	//***********************************************************************************************************
	
	// METHODS
	
	// AFFICHAGE

	public static void afficherToutesLesVolailles() {
		if(toutesLesVollailes.size()>0) {
			for (Volaille volaille : toutesLesVollailes) {

				if(volaille instanceof Slaugtherhouse) {
					Slaugtherhouse p =(Slaugtherhouse)volaille;
					System.out.print(volaille);
					p.vente();
				}else {
					System.out.println(volaille);
				}	

			}
		}
		else {
			System.out.println("your stock is empty");
		}
	}
	
	
	public static void afficherVolailleParType(String type) {
		boolean trouve=false;
		if(toutesLesVollailes.size()>0) {
			for (Volaille volaille : toutesLesVollailes) {

				if(type.equalsIgnoreCase("Canard")) {
					if(volaille instanceof Canard) {
						trouve=true;
						System.out.println(volaille);
					}
				}
				if(type.equalsIgnoreCase("Poulet")) {
					if(volaille instanceof Poulet) {
						trouve=true;
						System.out.println(volaille);
					}
				}
				if(type.equalsIgnoreCase("Paon")) {
					if(volaille instanceof Paon) {
						trouve=true;
						System.out.println(volaille);
					}
				}
			}
			if(trouve==false) {
				System.out.println("type of poultry does not exist");	
			}


		}else {
			System.out.println("your Stock is empty");

		}

	}
	
	//***********************************************************************************************************
	
		// WEIGHT

	public static void modifierPoidsVolaille(int a, double nvPoids) {
		boolean trouve=false;
		if(toutesLesVollailes.size()>0) {
			for (Volaille volaille : toutesLesVollailes) {

				if(volaille.numvol==a) {
					volaille.setPoids(nvPoids);
					formatDateTime = new SimpleDateFormat("yyyy.MM.dd").format(new Date());
					volaille.weightOfPoultry.put(formatDateTime, volaille.poids);
					System.out.println(("weight changed sucessfully"));
					trouve =true;
					break;
				}

				if(trouve==true) {
					break;
				}	
			}
			if(trouve==false) {
				System.out.println("ID doesn't match, no poultry found");
			}

		}
		else {
			System.out.println("your Stock is empty");
		}

	}


	public static void displayhistoryWeight(int a) {
		boolean trouve=false;
		if(toutesLesVollailes.size()>0) {
			for (Volaille volaille : toutesLesVollailes) {

				if(volaille.numvol==a) {
					System.out.println((volaille.weightOfPoultry.toString()));
					trouve =true;
					break;
				}

				if(trouve==true) {
					break;
				}	
			}if(trouve==false) {
				System.out.println("ID doesn't match, no poultry found");
			}

		}
		else {
			System.out.println("your Stock is empty");
		}
	}
	
	public static void displayhistoryofslaugtherWeigth(String type) {

		if(type.equalsIgnoreCase("1")) {	
			System.out.println(historySlaugtherWeightPoulet.toString());

		}
		else if(type.equalsIgnoreCase("2")) {
			System.out.println(historySlaugtherWeightCanard.toString());
		}


	}





	
	//***********************************************************************************************************
	
			// SALES

	public static void sommeTypeAbattable() {
		double total=0;
		double total1=0;
		ArrayList<Volaille>readypoulet = new ArrayList<Volaille>();
		ArrayList<Volaille>readycanard = new ArrayList<Volaille>();
		boolean trouve=false;
		if(toutesLesVollailes.size()>0) {
			for (Volaille volaille : toutesLesVollailes) {
				if(volaille instanceof Poulet) {
					if(volaille.poids>=Poulet.getPoidsabattage()) {
						trouve=true;
						readypoulet.add(volaille);
						total+= volaille.poids;

					}
				}
				if(volaille instanceof Canard) {
					if(volaille.poids>=Canard.getPoidsabattage()) {
						trouve=true;
						readycanard.add(volaille);
						total1+= volaille.poids;

					}
				}
			}
			if(trouve==false) {
				System.out.println("your stock doesn't contain any poultry ready yet");
			}

			for (Volaille volaille : readypoulet) {
				System.out.println(volaille);
			}
			total=total*Poulet.getPrixPoulet();
			System.out.println("Le prix de vente total pour les poulets est de "+total+"euros");

			System.out.println();
			for (Volaille volaille : readycanard) {
				System.out.println(volaille);
			}
			total1=total1*Canard.getPrixCanard();
			System.out.println("Le prix de vente total pour les canards est de "+total1+"€");
			total1+=total;

			System.out.println("LE PRIX DE VENTE TOTAL EST DE "+total1+"€");
		}

		else {
			System.out.println("your stock is empty, therefore the value of your stock = 0€");
		}

	}

	public static void ready() {
		if(getToutesLesVollailes().size()>0) {
			boolean trouve= false;
			for (Volaille volaille : toutesLesVollailes) {


				if(volaille instanceof Poulet) {
					Poulet p =(Poulet) volaille;
					if(volaille.getPoids()>=p.getPoidsabattage()) {
						trouve=true;
						System.out.println(volaille);
					}
				}

				if(volaille instanceof Canard) {
					Canard c = (Canard) volaille;
					if(volaille.getPoids()>=c.getPoidsabattage()) {
						trouve=true;
						System.out.println(volaille);
					}
				}
			}
			if(trouve==false) {
				System.out.println("your stock doesn't contain any poultry ready yet");
			}


		}
		else {
			System.out.println("your stock is empty");
		}

	}




	public static void sellPoulrty(int a) {
		if(toutesLesVollailes.size()>0) {
			boolean found= false;
			for (Volaille volaille : toutesLesVollailes) {

				found=false;
				if(volaille.numvol==a) {
					if(volaille instanceof Paon) {
						System.out.println("You can't sell a Peacock");
						found=true;
						break;
					}else {
						if(volaille instanceof Poulet) {
							Poulet p = (Poulet) volaille;
							if(volaille.poids>=p.getPoidsabattage()) {
								revenue+= volaille.poids*p.getPrixPoulet();
								toutesLesVollailes.remove(volaille);
									System.out.println("Chicken sold successfully");
									
									found=true;
									break;
							}
							
						}
						
						if(volaille instanceof Canard) {
							Canard c = (Canard) volaille;
							if(volaille.poids>=c.getPoidsabattage()) {
								revenue+= volaille.poids*c.getPrixCanard();
								toutesLesVollailes.remove(volaille);
									System.out.println("Duck sold successfully");
									found=true;
									break;
							}
							
						}
						
						}
						
					}
				}
			if(found==false) {
				System.out.println("Please check again the ID num of the poultry");
				Scanner sc= new Scanner(System.in);
				System.out.println("Please enter the type of poultry you want to sell");
				String type = sc.next();
				Volaille.afficherVolailleParType(type);
				sc.close();	
			}
			}
			
		else {
			System.out.println("ERREUR your stock is empty");
		}

	}



	public static void returnPeacock(int a) {
		boolean found= false;
		if(toutesLesVollailes.size()>0) {
			for (Volaille volaille : toutesLesVollailes) {
				found=false;
				if(volaille.numvol==a) {
					if(volaille instanceof Paon) {
						toutesLesVollailes.remove(volaille);
						found=true;
						System.out.println("Peacock successfully returned");
						break;
					}else {
						System.out.println("You can't return a chicken or a duck");
						break;
					}
				}
			}
			if(found==false) {
				System.out.println("Please check again the ID num of the Peacock");
			}
		}
		else {
			System.out.println("ERREUR your stock does not contain any peacock");
		}

	}



	


	/// GETTER SETTER
	
	// LIST OF
	public static  TreeMap<String, Double> getHistorymarketpriceCanard() {
		return historymarketpriceCanard;
	}
	public static void setHistorymarketpriceCanard(TreeMap<String, Double> historymarketpriceCanard) {
		Volaille.historymarketpriceCanard = historymarketpriceCanard;
	}

	public static ArrayList<Volaille> getToutesLesVollailes() {
		return toutesLesVollailes;
	}
	public static void setToutesLesVollailes(ArrayList<Volaille> toutesLesVollailes) {
		Volaille.toutesLesVollailes = toutesLesVollailes;
	}
	public static TreeMap<String, Double> getHistorySlaugtherWeightPoulet() {
		return historySlaugtherWeightPoulet;
	}

	public static TreeMap<String, Double> getHistorySlaugtherWeightCanard() {
		return historySlaugtherWeightCanard;
	}


	public static void setHistorySlaugtherWeightPoulet(TreeMap<String, Double> historySlaugtherWeightPoulet) {
		Volaille.historySlaugtherWeightPoulet = historySlaugtherWeightPoulet;
	}



	public static void setHistorySlaugtherWeightCanard(TreeMap<String, Double> historySlaugtherWeightCanard) {
		Volaille.historySlaugtherWeightCanard = historySlaugtherWeightCanard;
	}
	
	//***********************************************************************************************************
	
	// ATTRIBUT OF OBJECT

	public String getformatDateTime() {
		return formatDateTime;
	}

	public void setformatDateTime(String formatDateTime) {
		this.formatDateTime = formatDateTime;
	}


	public double getPoids() {
		return poids;
	}
	public void setPoids(double poids) {
		this.poids = poids;
	}


	public int getNumvol() {
		return numvol;
	}
	public void setNumvol(int numvol) {
		this.numvol = numvol;
	}


	/// TO STRING
	@Override
	public String toString() {
		return "Arrival Date= "+this.formatDateTime+" numero= "+numvol+" poids= " + poids;
	}

	public static void displayhistoryofmarketprice(String type) {
		if(type.equalsIgnoreCase("1")) {
			System.out.println(historymarketpricePoulet.toString());

		}


		else if(type.equalsIgnoreCase("2")) {
			System.out.println(historymarketpriceCanard.toString());
		}		
	}

	public static TreeMap<String, Double> getHistorymarketpricePoulet() {
		return historymarketpricePoulet;
	}

	public static void setHistorymarketpricePoulet(TreeMap<String, Double> historymarketpricePoulet) {
		Volaille.historymarketpricePoulet = historymarketpricePoulet;
	}

	public static void revenue() {
		
			System.out.println(revenue+"euros");
		}
	}



