package exo5;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class FenetreImageBougeResponsive extends JFrame {
	private BufferedImage monImage1;
	private BufferedImage monImage;
	static boolean bool = false;
	int a;
	int b;

	public FenetreImageBougeResponsive(int posX, int posY) throws Exception {

		int a = 1280;
		int b = 800;
		this.setSize(a, b);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		monImage = ImageIO.read(new File("/Users/clement/Desktop/biere.jpg"));
		monImage1 = ImageIO.read(new File("/Users/clement/Desktop/Biere2.jpg"));

		this.setVisible(true);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Random random = new Random();
		int wid = this.getWidth() * 4 / 10;
		int hei = this.getHeight() * 4 / 10;

		if (bool == false) {
			g.drawImage(monImage, random.nextInt(this.getWidth() - wid), random.nextInt(this.getHeight() - hei), wid,
					hei, null);
			bool = true;

		} else {
			g.drawImage(monImage1, random.nextInt(this.getWidth() - wid), random.nextInt(this.getHeight() - hei), wid,
					hei, null);
			bool = false;

		}

	}

}
