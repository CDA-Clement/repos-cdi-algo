package exo2;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class FenetreAppuiRelache2 extends JFrame implements MouseListener {
	private static Integer nom = 0;

	public FenetreAppuiRelache2(int posX, int posY) {
		nom++;
		setTitle(nom.toString());
		setSize(300, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addMouseListener(this);// la fenetre est son propre ecouteur
		this.setVisible(true);

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println(this.getTitle() + " appui " + e.getLocationOnScreen());

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println(this.getTitle() + " relachement " + e.getLocationOnScreen());

	}

}
