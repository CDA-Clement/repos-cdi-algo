package exo2;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Ecouteur2 implements MouseListener {
	private FenetreAppuiRelache2 laFenetre;
	private FenetreAppuiRelache2 lafenetre2;

	public Ecouteur2(FenetreAppuiRelache2 maFenetre, FenetreAppuiRelache2 maFenetre1) {
		this.laFenetre = maFenetre;
		this.lafenetre2 = maFenetre1;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getSource().equals(laFenetre)) {
			System.out.println(laFenetre.getTitle() + " appui2 " + e.getLocationOnScreen());
		} else {
			System.out.println(lafenetre2.getTitle() + " appui2 " + e.getLocationOnScreen());
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getSource().equals(laFenetre)) {
			System.out.println(laFenetre.getTitle() + "release2 " + e.getLocationOnScreen());
		} else {
			System.out.println(lafenetre2.getTitle() + " release2 " + e.getLocationOnScreen());
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
