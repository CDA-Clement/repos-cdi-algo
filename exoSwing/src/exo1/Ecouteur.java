package exo1;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Ecouteur implements MouseListener {
	private FenetreAppuiRelache laFenetre;

	public Ecouteur(FenetreAppuiRelache maFenetre) {
		this.laFenetre = maFenetre;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

		System.out.println("%%%%%appui " + e.getLocationOnScreen());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("relachement " + e.getLocationOnScreen());
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
