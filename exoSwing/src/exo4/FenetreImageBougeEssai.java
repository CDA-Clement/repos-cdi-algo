package exo4;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class FenetreImageBougeEssai extends JFrame implements MouseListener {
	private static Integer nom = 0;
	Random random = new Random();

	public FenetreImageBougeEssai(int posX, int posY) throws InterruptedException {
		int a = 1920;
		int b = 1080;
		boolean run = false;
		this.setSize(a, b);

		JLabel label = new JLabel();
		label.setIcon(new ImageIcon("/Users/clement/Desktop/biere.jpg"));
		label.repaint(0, 0, 10, 10);
		this.add(label);

		nom++;
		setTitle(nom.toString());

		int x = random.nextInt(600);
		int y = random.nextInt(600);
		label.setLocation(x, y);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addMouseListener(this);
		this.setVisible(true);
		Thread t1 = new Thread();
		while (run == false) {
			int x1 = random.nextInt(a - 500);
			int y1 = random.nextInt(b - 500);
			t1.sleep(1000);
			label.setLocation(x1, y1);

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
