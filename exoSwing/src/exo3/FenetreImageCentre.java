package exo3;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class FenetreImageCentre extends JFrame {

	private BufferedImage monImage1;
	private BufferedImage monImage;
	static boolean bool = false;
	int a;
	int b;

	public FenetreImageCentre(int posX, int posY) throws Exception {

		int a = 1280;
		int b = 800;
		this.setSize(a, b);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		monImage = ImageIO.read(new File("/Users/clement/Desktop/biere.jpg"));
		monImage1 = ImageIO.read(new File("/Users/clement/Desktop/Biere2.jpg"));
//		monImage = ImageIO.read(new File("C:\\Users\\59013-57-03\\Downloads\\Capture.JPG"));
//		monImage1 = ImageIO.read(new File("C:\\Users\\59013-57-03\\Downloads\\Capture1.JPG"));

		this.setVisible(true);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		int wid = this.getWidth() * 4 / 10;
		int hei = this.getHeight() * 4 / 10;

		if (bool == false) {
			g.drawImage(monImage, this.getWidth() / 2 - wid / 2, this.getHeight() / 2 - hei / 2, wid, hei, null);
			bool = true;

		} else {
			g.drawImage(monImage1, this.getWidth() / 2 - wid / 2, this.getHeight() / 2 - hei / 2, wid, hei, null);
			bool = false;

		}

	}

}
