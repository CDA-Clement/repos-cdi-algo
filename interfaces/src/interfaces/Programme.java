package interfaces;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class Programme {
	public static void main(String[] args) {
		ArrayList<Animals> foodChain = new ArrayList<Animals>();
		Chats cha= new Chats();
		foodChain.add(cha);

		Chats cha2= new Chats();
		foodChain.add(cha2);

		Chats cha3= new Chats();
		foodChain.add(cha3);

		Chiens chi= new Chiens();
		foodChain.add(chi);

		Chiens chi2= new Chiens();
		foodChain.add(chi2);

		Sardines sa= new Sardines();
		foodChain.add(sa);

		Sardines sa2= new Sardines();
		foodChain.add(sa);

		for (Animals animals : foodChain) {
			animals.nom();
			animals.respirer();
			animals.comportement();

			if(animals instanceof Marcheur) {
				Marcheur p =(Marcheur)animals;
				p.marcher();
				
			}

			if(animals instanceof Criant) {
				Criant p =(Criant)animals;
				p.crier();
				
			}

			if(animals instanceof Nageur) {
				Nageur p =(Nageur)animals;
				p.nager();
				
			}

			System.out.println("*********************************************");

		}
	}

}
