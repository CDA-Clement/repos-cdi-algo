package interfaces;

public class Chiens extends Animals implements Criant, Marcheur  {
	
	

	@Override
	public void crier() {
		System.out.println("Crier");
	}


	@Override
	public void marcher() {
		System.out.println("Marcher");
		
	}
	
	
	@Override
	public void nom() {
		System.out.println("Chien");
	}


	@Override
	public void comportement() {
		System.out.println("barkling");
		
	}



}
