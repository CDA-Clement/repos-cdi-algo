package barnYard;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

public class Canard extends Volaille implements Slaugtherhouse {	
	private static double prixCanard=17.06;
	private static double poidsabattage = 3;
	public Canard(double a) {
		super(a);
	}

	public static void creerCanard(double a) {
		if(getToutesLesVollailes().size()<7) {
			boolean trouve = false;
			int cptPo=0;
			if(getToutesLesVollailes().size()>0) {
				for (Volaille volaille : getToutesLesVollailes()) {

					trouve = false;

					if(volaille instanceof Canard) {
						cptPo++;
					}
				}
				if(cptPo==4) {
					System.out.println("Impossible you have reached the limit of duck which is set to 4");
					trouve=true;
				}
				if(trouve==false) {
					new Canard(a);
					System.out.println("Duck added successfully");
				}

			}
			else {
				new Canard(a);
				System.out.println("Duck added successfully");
			}
			
		}
		else {
			System.out.println("You have reached the limit of the barn");
		}



	}



	public void vente() {
		double a=getPoids();
		double b=prixCanard;
		System.out.println("le prix de vente est de "+a*b+"€");
	}

	@Override
	public String toString() {
		return "Canard ["+super.toString()+" prix au kilo= "+prixCanard+"]";
	}


	public static void listpoidAbattage() {
		if(getToutesLesVollailes().size()>0) {
			boolean trouve=false;
			for (Volaille volaille : getToutesLesVollailes()) {
				if(volaille instanceof Canard &&volaille.getPoids()>=poidsabattage) {
					trouve=true;
					System.out.println(volaille.toString());
				}

			}
			if(trouve==false) {
				System.out.println("your stock doesn't contain any duck ready yet");
			}

		}
		else {
			System.out.println("Your stock is empty");
		}


	}


	public static void ChangerPrixDuJour(double a) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		setPrixCanard(a);
		getHistorymarketpriceCanard().put(formatDateTime, Canard.getPrixCanard());

	}

	public static void ChangerPoidsAbattage(double d) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = now.format(formatter);
		setPoidsabattage(d);
		getHistorySlaugtherWeightCanard().put(formatDateTime,poidsabattage);

	}

	public static double getPrixCanard() {
		return prixCanard;
	}

	public static void setPrixCanard(double prixCanard) {
		Canard.prixCanard = prixCanard;
	}

	public static double getPoidsabattage() {
		return poidsabattage;
	}

	public static void setPoidsabattage(double poidsabattage) {
		Canard.poidsabattage = poidsabattage;
	}


}

