package chaines;
import java.util.Scanner;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;

public class Objet {
	private int numero;
	String element;
	private ArrayList<String> liste;

	public Objet(){
		this.liste = new ArrayList<String>();
	}

	public void init() {
		Scanner sc = new Scanner (System.in);
		System.out.println("Please enter a number");
		numero = sc.nextInt();
		for (int i = 0; i < numero; i++) {
			System.out.println("please enter your String");
			element = sc.next();
			liste.add(element);
		}
	}

	public Objet ordreCroissant() {
		Objet ordre = new Objet();
		ordre.liste.addAll(this.liste);
		Collections.sort(ordre.liste);
		return ordre;
	}

	public Objet ordreCroissant2() {
		String temp="";
		Objet ordre = new Objet();
		ordre.liste.addAll(this.liste);
		for (int j = 0; j < this.liste.size(); j++) {


			for (int i = 0; i < this.liste.size()-i; i++) {
				if(ordre.liste.get(i).compareTo(ordre.liste.get(i+1))>0){
					temp = ordre.liste.get(i);
					ordre.liste.set(i,ordre.liste.get(i+1) );
					ordre.liste.set(i+1, temp);
				}
			}
		}
		return ordre;
	}

	public String toString() {
		String s="";
		if(this.liste.isEmpty()==true) {
			System.out.println("Ensemble vide");
		}else {
			s+=this.liste;
		}	
		return s;
	}

}
