package circle;

public class ColoredCircle extends Circle {
	private String colour;

	public String getColour() {
		return colour;
	}

	public void setColor(String colour) {
		this.colour = colour;
	}
	
	public ColoredCircle(String colour) {
		this.colour=colour;
	}

	public ColoredCircle(double x, double y, double z, String colour) {
		super(x,y,z);
		this.colour=colour;
		
		
	}

	@Override
	public String toString() {
		return super.toString()+ " ColoredCircle [the colour of the circle is " + colour + "]";
	}

}
