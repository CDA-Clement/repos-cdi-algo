package circle;

public class Circle extends Shape {
	final static double PI = 3.141592564 ;
	private double radius ;
	private double surface;


	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
		this.surface = PI * (radius*radius);
	}



	public double getSurface() {
		return surface;
	}

	public void setSurface(double surface) {
		this.surface = surface;
	}
	
	public void init (Circle c) {
		
	}



	public Circle() {
		super(2,2);
		radius = 0 ;
		if(radius==0) {
			surface=0;
		}else
		{
			surface = PI * (radius*radius);
		}
	}

	public Circle(double x, double y, double r) {
		super(x,y) ;
		radius = r ;
		
		if(radius==0) {
			surface=0;
		}else
		{
			surface = PI * (radius*radius);
		}
	}
	public String toString() {
		return super.toString() + " Rayon : " + radius+ " Surface : " + surface ;
	}


	public boolean isBigger(Circle c) {
		return c.radius<this.radius;
	}
	
	public static boolean isBigger2(Circle c, Circle c2) {
		return c.isBigger(c2);
	}

}
