package circle;

public class Shape {
	  final double x, y ;
	 
	  public Shape(double x, double y) {
	      this.x = x ;
	       this.y = y ;
	  }
	  public String toString() {
	      return "Position : (" + x + "," + y + ")" ;
	  }
	}