package circle;

public class Cylinder extends Circle {
	double hauteur;
	
	public Cylinder(double x, double y, double z, double hauteur) {
		super(x,y,z);
		this.hauteur=hauteur ;
		
	}
	

	public double volume() {
		return this.getSurface()*this.hauteur;
	}

	public double getHauteur() {
		return hauteur;
	}

	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}

	@Override
	public String toString() {
		return super.toString() +" Cylinder [hauteur=" + hauteur + " Volume= "+volume()+"]";
	}
	
	
}
