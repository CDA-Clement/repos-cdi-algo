package circle;

public class TestIncrement {
    public static void main(String[] args) {
        {
            int a = 4;
            int b = 3;
            int c = somme1(a,b);
            System.out.println("a1["+a+"] + b1["+b+"] = c1["+c+"]");
        }
        {
            Integer a = new Integer(4);
            Integer b = new Integer(3);
            Integer c = somme2(a,b);
            System.out.println("a2["+a+"] + b2["+b+"] = c2["+c+"]");
        }
        {
            IntegerCda a = new IntegerCda(4);
            IntegerCda b = new IntegerCda(3);
            IntegerCda c = somme3(a,b);
            System.out.println("a3["+a+"] + b3["+b+"] = c3["+c+"]");
        }        
    }
    private static int somme1(int a, int b) {
        a = a + b;
        return a;
    }
    private static Integer somme2(Integer a, Integer b) {
        a = a + b;
        return a;
    }
    private static IntegerCda somme3(IntegerCda a, IntegerCda b) {
        a.val = a.val + b.val;
        return a;
    }
}

