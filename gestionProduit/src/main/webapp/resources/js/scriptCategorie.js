$(document).ready(
		function() {
			$(".validation-suppr").on(
					'click',
					function(event) {
						event.stopPropagation();
						var idToDelete = $(this).parent().find(
								"input[id='identifiant']")[0].value;
						var idModal = idToDelete;
						console.log(idToDelete);
						$.post("supprimerCategorie", {
							id : idToDelete,
						}).done(function(data, status) {
							console.log("ça marche");
							$("tr[id='" + idToDelete + "']").hide();
							$('#exampleModal' + idModal).modal('hide');
							$('#reussi').modal('show');
						}).fail(function(data, status) {
							console.log("ça marche pas");
							$('#exampleModal' + "" + idToDelete).modal('hide');
							$('#echouer').modal('show');
						});
						return false;
					});
		});