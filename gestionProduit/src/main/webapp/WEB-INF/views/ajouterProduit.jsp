<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="erreur.jsp"%>
	 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js" ></script>
<script src="resources/jquery/clickable.js" ></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<title>Ajouter un produit</title>
</head>
<body>

<jsp:include page="header.jsp" >
  <jsp:param name="origin" value="ajouter" />
</jsp:include>
	<div class="container my-4">
		<h1>Ajouter un nouveau Bernie</h1>

		<form action="ajouterProduit?page=${pageStr}&trier=${trier}" method="post">
			<input type="hidden" id="page" name="page" value="${page}">
			<input type="hidden" id="trier" name="trier" value="${trier}">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputNom">Label</label> 
					<input type="text"
						class="form-control" name="label" placeholder="label" required>
				</div>
				<div class="form-group col-md-6">
					<label for="inputPrenom">Quantite</label> <input type="number"
						class="form-control" name="quantite" placeholder="00" required>
				</div>
				<div class="form-group col-md-6">
					<label for="inputDOB">Prix</label> <input type="number"
						class="form-control" name="prix" placeholder="00" required>
				</div>
				<div class="form-group">
					<label for="inputCategorie">Categorie</label> 
					<select
						class="form-control" name="categorie">
						<c:forEach items="${liste}" var="p">
						<option>${p.label}</option>
						</c:forEach>
									
					</select>
				</div>
				
			</div>
			<button type="submit" class="btn btn-primary">Ajouter</button>
		</form>
	</div>

<%@include file="footer.jsp" %>
</body>
</html>