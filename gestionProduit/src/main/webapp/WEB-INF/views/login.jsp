<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="resources/jquery/clickable.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
</head>
<body>

<jsp:include page="header.jsp" >
  <jsp:param name="origin" value="login" />
</jsp:include>

<c:if test="${not empty message }">
		<div class="alert alert-warning" role="alert">
			<p align="center" class="parap">${message}</p>
		</div>

	</c:if>


  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign In</h5>
            <form class="form-signin" method="post" action="login">
            
              <div class="form-label-group">
                <input type="text" id="login" class="form-control" name="login" placeholder="Login" required autofocus>
                <label for="login">Email address</label>
              </div>

              <div class="form-label-group">
                <input type="password" id="password" class="form-control" name="password" placeholder="Password" required>
                <label for="password">Password</label>
                
              </div>

              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Remember password</label>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
              <hr class="my-4">
              <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign in with Google</button>
              <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign in with Facebook</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<%@include file="footer.jsp" %>
</body>
</html>