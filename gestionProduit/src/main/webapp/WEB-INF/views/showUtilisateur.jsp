<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="resources/jquery/clickable.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<title></title>
</head>
<body>
	<%@include file="header.jsp"%>
	<c:if test="${not empty utilisateur }">
		<div class="container my-4">
			<div class="row justify-content-md-center">
				<div class="col card col-lg-8\">
					<form>
						<div class="form-group row">
							<label for="idPers" class="col-sm-2 col-form-label">Id</label>
							<div class="col-sm-10">
								<input type="text" readonly class="form-control-plaintext"
									id="idPers" value="${utilisateur.id}">
							</div>
						</div>
						<div class="form-group row">
							<label for="idPers" class="col-sm-2 col-form-label">Login</label>
							<div class="col-sm-10">
								<input type="text" readonly class="form-control-plaintext"
									id="idPers" value="${utilisateur.login}">
							</div>
						</div>
						<div class="form-group row">
							<label for="idPers" class="col-sm-2 col-form-label">Edit</label>
							<div class="col-sm-10">
								<a href="editUtilisateur?id=${utilisateur.id}" type="button" class="btn btn-primary"><i
									class="fas fa-edit"></i></a>
							</div>
						</div>
					</form>
					<a class="btn btn-secondary" href="listUtilisateur" role="button">retour
						vers la liste</a>
				</div>
			</div>
		</div>
	</c:if>

	<%@include file="footer.jsp"%>
</body>
</html>