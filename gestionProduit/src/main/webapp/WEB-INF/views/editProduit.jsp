<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="resources/jquery/clickable.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<title>Editer un produit</title>
</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container my-4">
		<h1>Modification d'un produit</h1>

		<form action="editProduit" method="post">
			<input type="hidden" name="produit" value="${produit}">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="label">Label</label> <input type="text"
						class="form-control" name="label" placeholder="label" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="quantite">Quantite</label> <input type="number"
						class="form-control" name="quantite" placeholder="quantite"
						required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="prix">Prix</label> <input type="number"
						class="form-control" name="prix" placeholder="prix" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputCategorie">Categorie</label> <select
					class="form-control" name="categorie">
					<c:forEach items="${listCategorie}" var="p">
						<option value="${p.id}">${p.label}</option>
					</c:forEach>

				</select>
			</div>
			<button type="submit" class="btn btn-primary">Ajouter</button>
		</form>
	</div>

	<%@include file="footer.jsp"%>
</body>
</html>