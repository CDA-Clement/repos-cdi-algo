<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="resources/jquery/clickable.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">
<title>Editer un utilisateur</title>
</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container my-4">
		<h1>Modification d'un utilisateur</h1>

		<form action="editUtilisateur" method="post">
			<input type="hidden" name="utilisateuraedit" value="${utilisateuraedit}">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="login">Login</label> <input type="text"
						class="form-control" name="login" placeholder="login">
					<label for="password">Password</label> <input type="password"
						class="form-control" name="password" placeholder="password">
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Ajouter</button>
		</form>
	</div>

	<%@include file="footer.jsp"%>
</body>
</html>