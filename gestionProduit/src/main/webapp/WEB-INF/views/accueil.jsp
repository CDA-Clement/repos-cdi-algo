<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Accueil</title>
<link rel="stylesheet" type="text/css" href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">


</head>
<body>
<jsp:include page="header.jsp" >
  <jsp:param name="origin" value="accueil" />
</jsp:include>

<c:if test="${not empty message }">
		<div class="alert alert-warning" role="alert">
			<p align="center" class="parap">${message}</p>
		</div>
		</c:if>


<div class="container">
<div class="row center-block" style="text-align: center; display: block; margin : auto;">
<h1>Bienvenue à l'Accueil </h1>

<br/>
<br/>
</div>
  <div class="row">
    <div class="col">
      <img src="resources/image/personne.jpg" style="width : 400px; height: 500px">
      <a class="btn btn-success" href="listProduit?page=0&trier=null" role="button">Produit</a>
    </div>
    <div class="col">
     <img src="resources/image/metier2.jpg" style="width : 400px; height: 500px">
     <a class="btn btn-warning" href="listCategorie?page=0&trier=null" role="button">Categorie</a>
    </div>
  </div>
  </div>

<script src="resources/jquery/jquery-3.3.1.slim.min.js" ></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js" ></script>
<%@include file="footer.jsp" %>
</body>
</html>