<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="resources/jquery/clickable.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/css.css">
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap/css/bootstrap.min.css">
<link href="resources/fontawesome/css/all.min.css" rel="stylesheet">

</head>
<body>
	<%@include file="header.jsp"%>
	<div class="col align-self-center">
		<div class="modal fade" id="reussi" tabindex="-1" role="dialog"
			aria-hidden="true">
			<div class="modal-dialog" role="document"></div>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">La suppression est un succes</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="col align-self-center">
		<div class="modal fade" id="echouer" tabindex="-1" role="dialog"
			aria-hidden="true">
			<div class="modal-dialog" role="document"></div>
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">La suppression a echouer</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	</div>

	<table class="table">
		<thead>
			<tr>
				<th scope="col">Id</th>
				<th scope="col">login</th>
				<th scope="col">Supprimer</th>
			</tr>
		</thead>
		<tbody class="tbody">
			<c:forEach items="${liste}" var="p" varStatus="c">
				<c:set var="idStr" value="${Integer.toString(p.getId())}" />
				<tr id="${idStr}">
					<th class='clickable-row' data-href='showUtilisateur?id=${p.id}'
						scope="row">${c.count}</th>
					<td class='clickable-row' data-href='showUtilisateur?id=${p.id}'>${p.login}</td>
					<td><i class="fas fa-trash-alt" type="button"
						data-toggle="modal" data-target="#exampleModal${idStr}"></i></td>
					<div class="modal fade" id="exampleModal${idStr}" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Achtung!!!!</h5>
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">Veux-tu tuer ce pauvre ${p.login}?
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary"
										data-dismiss="modal">Close</button>
									<input type="hidden" value="${idStr}" id="identifiant">
									<a href="#" type="button"
										class="validation-suppr btn btn-primary">Valider la mort</a>
								</div>
							</div>
						</div>
					</div>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="row center-block"
		style="text-align: center; display: block; margin: auto;">
		<c:if test="${not empty utilisateur1 }">
			<a class="btn btn-success" href="#" role="button">Ajouter
				un utilisateur</a>
		</c:if>
		<a class="btn btn-warning" href="/kcdq" role="button">Revenir a
			l'accueil</a>
	</div>

	<script src="resources/js/scriptUtilisateur.js"></script>
	<%@include file="footer.jsp"%>
</body>
</html>