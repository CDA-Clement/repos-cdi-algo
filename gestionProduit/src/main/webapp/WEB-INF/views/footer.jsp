<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Footer -->
<footer class="page-footer font-small special-color-dark pt-4 fixed-bottom">

  <!-- Footer Elements -->
  <div class="container">

    <!-- Social buttons -->
    <ul class="list-unstyled list-inline text-center">
      <li class="list-inline-item">
        <a class="btn-floating btn-fb mx-1">
          <i class="fab fa-facebook-f"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-tw mx-1">
          <i class="fab fa-twitter"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-gplus mx-1">
          <i class="fab fa-google-plus-g"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-li mx-1">
          <i class="fab fa-linkedin-in"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-dribbble mx-1">
          <i class="fab fa-dribbble"> </i>
        </a>
      </li>
    </ul>
    <!-- Social buttons -->

  </div>
  <!-- Footer Elements -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">� 2019 Copyright:
    <a href="https://www.afpa.fr/centre/centre-de-roubaix"> Team from ilot 2</a>
  </div>
  
   <div class="footer-copyright text-center py-3">
    <c:if test="${empty utilisateur }">
    visiteur
    </c:if>
    <c:if test="${not empty utilisateur&&utilisateur.admin==null }">
    vous �tes connect� en tant qu'utilisateur
    </c:if>
    <c:if test="${not empty utilisateur&&utilisateur.admin==true}">
    vous �tes connect� en tant qu'admin
    </c:if>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->