
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Gestion des personnes</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<c:if test="${empty accueil }">
					<a class="nav-item nav-link active" href="accueil"><i
						class="fas fa-home"></i> Accueil <span class="sr-only">(current)</span></a>
				</c:if>
				<ul class="navbar-nav mr-auto">
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Produits </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="listProduit?page=0&trier=null"><i
								class="fas fa-list"></i> Liste des produits</a>
							<c:if test="${not empty utilisateur }">
								<a class="dropdown-item" href="ajouterProduit"><i
									class="fas fa-plus"></i> Ajouter des produits</a>
							</c:if>
							<div class="dropdown-divider"></div>
						</div></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Catégories </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="listCategorie?page=0&trier=null"><i
								class="fas fa-list"></i> Liste des catégories</a>
							<c:if test="${not empty utilisateur }">
								<a class="dropdown-item" href="ajouterCategorie"><i
									class="fas fa-plus"></i> Ajouter des catégories</a>
							</c:if>
							<div class="dropdown-divider"></div>
						</div></li>
						<c:if test="${not empty utilisateur&&utilisateur.admin==true }">
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Utilisateurs </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="listUtilisateur"><i
								class="fas fa-list"></i> Liste des utilisateurs</a> <a
								class="dropdown-item" href="ajouterUtilisateur"><i
								class="fas fa-plus"></i> Ajouter des utilisateurs</a>
							<div class="dropdown-divider"></div>
						</div></li>
						</c:if>
				</ul>
				<c:if test="${empty utilisateur }">
					<a class="nav-item nav-link" href="login?origin=${param.origin}"><i
						class="fas fa-user-alt"></i> Login</a>
				</c:if>
				<c:if test="${not empty utilisateur }">
					<a class="nav-item nav-link" href="logout?origin=${param.origin}"><i
						class="fas fa-user-alt-slash"></i> Logout</a>
				</c:if>
			</div>
			<span class="navbar-text"> ${user.nom} </span>
		</div>
	</nav>
</header>