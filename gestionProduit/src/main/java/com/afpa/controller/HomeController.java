package com.afpa.controller;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
@Scope("session")
public class HomeController {


	@GetMapping({"/","accueil","/accueil"})
	public ModelAndView accueil(ModelAndView mv) {
		mv.setViewName("accueil");
		return mv;
	}
}