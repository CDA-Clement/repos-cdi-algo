package com.afpa.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.ProduitDto;
import com.afpa.dto.ReponseStatut;
import com.afpa.dto.UtilisateurDto;
import com.afpa.service.ICategorieService;
import com.afpa.service.IProduitService;
import com.afpa.service.ProduitServiceImpl;



@Controller
public class ProduitController {

	@Autowired
	private IProduitService produitService;
	@Autowired
	private ICategorieService categorieService;

	@RequestMapping(value = "/listProduit", method = RequestMethod.GET)
	public ModelAndView listProduit(
			@RequestParam(value = "page") String pageStr,
			@RequestParam(value = "trier", defaultValue = "null") String trier,
			ModelAndView mv, HttpServletRequest request) {
		System.out.println("je passe par listproduit");
		int page = 0;
		mv.setViewName("listProduits");

		System.out.println("pageStr: "+pageStr);
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr = "0";
		}else {
			page = Integer.parseInt(pageStr);
			mv.addObject("page", page);
			mv.addObject("page1", pageStr);

		}
		mv.addObject("trier", trier);
		
		if(trier.equals("nomUp")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParNom(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("nomDown")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParNomDesc(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("prixUp")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParPrix(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("prixDown")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParPrixDesc(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("quantiteUp")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParQuantite(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("quantiteDown")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParQuantiteDesc(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("idDown")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParIdDesc(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("idUp")) {
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduitsParId(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		
		if(trier.equals("null")) {
			
			List<ProduitDto> liste = this.produitService.chercherToutesLesProduits(page);
			mv.addObject("fin", ProduitServiceImpl.fin );
			System.out.println("fin=: "+ProduitServiceImpl.fin);
			for (ProduitDto produitDto : liste) {
				System.err.println(produitDto.toString());
			}
			
			mv.addObject("liste", liste);
		}
		return mv;
	}


		@RequestMapping(value = "/showProduct", method = RequestMethod.GET, params = { "id" })
		public ModelAndView showProduit(
				@RequestParam(value = "id") int id, 
				@RequestParam(value = "page") String pageStr,
				@RequestParam(value = "trier") String trier,
				ModelAndView mv, HttpServletRequest request) {
			System.out.println("produit " + id);
			int page = 0;
			System.out.println("page "+pageStr);
			if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
				pageStr = "0";
			}else {
				page = Integer.parseInt(pageStr);
			}

			mv.setViewName("showproduit");
			Optional<ProduitDto> produitOptional = this.produitService.findById(id);
			if (produitOptional.isPresent()) {
				ProduitDto produit = new ProduitDto();
				produit = produitOptional.get();
				System.err.println("produit" + produit.toString());
				mv.addObject("produit", produit);
				mv.addObject("page", pageStr);
				mv.addObject("trier", trier);
			} else {

				mv.addObject("message", "le produit n'existe pas");
				listProduit(pageStr,trier,mv, request);
			}
			return mv;
		}

		@RequestMapping(value = "/ajouterProduit", method = RequestMethod.GET)
		public ModelAndView formulaireProduit(
				@RequestParam(value = "page",defaultValue = "0") String pageStr,
				@RequestParam(value = "trier",defaultValue = "null") String trier,
				ModelAndView mv, 
				HttpServletRequest request) {

			HttpSession session = request.getSession();
			UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
			if (utilisateur == null) {
				mv.setViewName("login");
			} else {
				int page = 0;
				if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
					pageStr = "0";
				}else {
					page = Integer.parseInt(pageStr);
				}
				System.out.println("par la car formulaire rempli");
				List<CategorieDto> allcategorie = this.categorieService.chercherToutesLesCategories(page);
				mv.addObject("liste", allcategorie);
				mv.addObject("page", page);
				mv.addObject("trier", trier);
				mv.setViewName("ajouterProduit");
			}
			return mv;
		}

		@RequestMapping(value = "/ajouterProduit", method = RequestMethod.POST, params = { "trier", "page","label", "quantite", "prix",
		"categorie" })
		public ModelAndView ajouterProduit(
				@RequestParam(value = "label", defaultValue = "aucun") String label,
				@RequestParam(value = "quantite", defaultValue = "0") Integer quantite,
				@RequestParam(value = "prix", defaultValue = "0") long prix,
				@RequestParam(value = "page",defaultValue = "0") String pageStr,
				@RequestParam(value = "trier",defaultValue = "null") String trier,
				@RequestParam(value = "categorie", defaultValue = "aucune") String categorie, ModelAndView mv,
				HttpServletRequest request) {
			HttpSession session = request.getSession();
			UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
			System.out.println("je passe par ajoutproduit");
			if (utilisateur == null) {
				mv.setViewName("login");
			} else {
				int page = 0;
				if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
					pageStr = "0";
				}else {
					page = Integer.parseInt(pageStr);
				}


				System.out.println("par la car formulaire rempli");

				List<CategorieDto> allcategorie = this.categorieService.chercherToutesLesCategories(0);

				Integer idCat = null;
				for (CategorieDto categorieDto : allcategorie) {
					if (categorieDto.getLabel().equals(categorie)) {
						idCat = categorieDto.getId();
					}
				}
				Integer id = this.produitService.ajouter(ProduitDto.builder().label(label).prix(prix).quantite(quantite)
						.categorie(CategorieDto.builder().id(idCat).build()).build());


				mv.addObject("message", "le produit a bien été ajouté");
				listProduit(pageStr,trier,mv, request);
			}
			return mv;
		}

		@RequestMapping(value = "/supprimerProduit", method = RequestMethod.POST, params = { "id" })
		public ModelAndView supprimer(@RequestParam(value = "id") Integer id, HttpServletRequest request, ModelAndView mv) {
			HttpSession session = request.getSession();
			UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
			if (utilisateur == null) {
				mv.setViewName("login");
			} else {

				String idParam = id.toString();

				String msg = "";
				ReponseStatut statut = ReponseStatut.OK;

				if (idParam == null || idParam.length() == 0) {
					statut = ReponseStatut.KO;
					msg = "le parametre id est obligatoire";
				} else {
					if (!idParam.matches("^\\p{Digit}+$")) {
						statut = ReponseStatut.KO;
						msg = "le parametre id doit etre un numero";
					} else {
						id = Integer.parseInt(idParam);

						if (this.produitService.deleteById(id)) {
							System.err.println("supprimé");
							msg = "suppression reussie avec succes";
						} else {
							System.err.println("PAS supprimé");
							statut = ReponseStatut.KO;
							msg = "aucune produit n'a cet id " + id;
						}
					}
				}
			}

			return mv;
		}

		@RequestMapping(value = "/editProduit", method = RequestMethod.GET, params = { "id" })

		public ModelAndView formulaireEditProduit(@RequestParam(value = "id") int id, ModelAndView mv,
				HttpServletRequest request) {
			HttpSession session = request.getSession();
			UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
			if (utilisateur == null) {
				mv.setViewName("login");
			}else {

				System.out.println("produitlala : " + id);

				List<CategorieDto> list = this.categorieService.chercherToutesLesCategories(0);
				mv.addObject("listCategorie", list);
				mv.setViewName("editProduit");
				Optional<ProduitDto> produitOptional = this.produitService.findById(id);
				if (produitOptional.isPresent()) {
					mv.addObject("produit", produitOptional.get().getId());
				} else {
					mv.addObject("message", "le produit n'existe pas");
				}
			}
			return mv;
		}

		@RequestMapping(value = "/editProduit", method = RequestMethod.POST, params = { "produit", "label", "quantite",
				"prix", "categorie" })

		public ModelAndView editCategorie(@RequestParam(value = "produit") int id,
				@RequestParam(value = "label") String label, @RequestParam(value = "quantite") int quantite,
				@RequestParam(value = "prix") long prix, @RequestParam(value = "categorie") int idCate, ModelAndView mv,
				HttpServletRequest request) {

			HttpSession session = request.getSession();
			UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
			if (utilisateur == null) {
				mv.setViewName("login");
				System.out.println("produitlalapost : " + id);
			}else {

				Optional<ProduitDto> produitOptionalDto = this.produitService.findById(id);
				Optional<CategorieDto> categorieOptionalDto = this.categorieService.findById(idCate);
				mv.setViewName("editProduit");
				if (produitOptionalDto.isPresent()) {
					produitOptionalDto.get().setLabel(label);
					produitOptionalDto.get().setQuantite(quantite);
					produitOptionalDto.get().setPrix(prix);
					if (categorieOptionalDto.isPresent()) {
						produitOptionalDto.get().setCategorie(categorieOptionalDto.get());
					}
					this.produitService.mettreAJourProduit(produitOptionalDto.get());
					System.err.println("produit" + produitOptionalDto.get().toString());
					mv.addObject("produit", produitOptionalDto.get());
				} else {

					mv.addObject("message", "le produit n'existe pas");
				}
			}

			return mv;
		}
	}
