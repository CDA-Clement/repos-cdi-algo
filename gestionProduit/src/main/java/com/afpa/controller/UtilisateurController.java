package com.afpa.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.ProduitDto;
import com.afpa.dto.ReponseStatut;
import com.afpa.dto.UtilisateurDto;
import com.afpa.service.ICategorieService;
import com.afpa.service.IProduitService;
import com.afpa.service.IUtilisateurService;



@Controller
public class UtilisateurController {

	@Autowired
	private IProduitService produitService;

	@Autowired
	private ICategorieService categorieService;

	@Autowired
	private IUtilisateurService userService;

	static String origin = "";

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "origin", defaultValue = "accueil") String param1, ModelAndView mv,
			HttpServletRequest request) {
		System.err.println("par ici login en get +param1 " + param1);

		mv.setViewName("login");
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		origin = param1;
		if (utilisateur != null) {
			mv.setViewName("accueil");
		} else {
			mv.setViewName("login");
		}
		return mv;
	}

	@RequestMapping(value="/login", method = RequestMethod.POST,params = {"login","password"})
	public  ModelAndView loginUser(
			@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password,

			ModelAndView mv, HttpServletRequest request) {

		System.out.println("origin"+origin);




		System.err.println("par la login en post");
		if(origin.contentEquals("listCategorie")) {
			List<CategorieDto> liste2=this.categorieService.chercherToutesLesCategories(0);
			mv.addObject("liste", liste2);
		}

		if(origin.contentEquals("listProduits")) {
			List<ProduitDto> liste2=this.produitService.chercherToutesLesProduits(0);
			mv.addObject("liste", liste2);
		}

		if(origin.contains("showproduit")) {
			String[]tabOrigin=origin.split("\\d+");
			String origin1 = tabOrigin[0];
			tabOrigin=origin.split("[a-zA-Z]+");
			origin=origin1;
			Optional<ProduitDto> pd = produitService.findById(Integer.parseInt(tabOrigin[1]));
			if(pd.isPresent()) {
				mv.addObject("produit", pd.get());				
			}
		}

		if(origin.contains("showCategorie")) {
			String[]tabOrigin=origin.split("\\d+");
			String origin1 = tabOrigin[0];
			tabOrigin=origin.split("[a-zA-Z]+");
			origin=origin1;
			Optional<CategorieDto> cd = categorieService.findById(Integer.parseInt(tabOrigin[1]));
			if(cd.isPresent()) {
				mv.addObject("categorie", cd.get());				

			}
		}

		List<UtilisateurDto> liste = userService.chercherToutLeMonde();

		for (UtilisateurDto utilisateurDto : liste) {
			if (utilisateurDto.getLogin().equals(login) && utilisateurDto.getPassword().equals(password)) {
				System.out.println("trouvé:" + utilisateurDto.toString());
				Optional<UtilisateurDto> utilisateurOptional = this.userService.findById(utilisateurDto.getId());
				if(utilisateurOptional.isPresent()) {
					HttpSession session = request.getSession();
					session.setAttribute("utilisateur", UtilisateurDto.builder()
							.login(login)
							.admin(utilisateurOptional.get().getAdmin())
							.build());


					mv.addObject("message", "authentification reussi");
					if(origin.equals("login")) {
						origin="accueil";
					}

					mv.setViewName(origin);
					return mv;
				}
			} else {
				mv.addObject("message", "utilisateur n'existe pas");
			}
		}
		mv.setViewName(origin);
		return mv;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(@RequestParam(value = "origin") String param1, ModelAndView mv,
			HttpServletRequest request) {
		System.err.println("par ici logout en get");
		HttpSession session = request.getSession();
		session.invalidate();
		origin = param1;

		mv.setViewName("accueil");
		return mv;
	}

	@GetMapping(value = "/listUtilisateur")
	public ModelAndView listeUsers(ModelAndView mv, HttpServletRequest request) {

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {

			List<UtilisateurDto> liste = this.userService.chercherToutLeMonde();
			for (UtilisateurDto utilisateur : liste) {
				System.err.println(utilisateur.toString());
			}
			mv.addObject("liste", liste);
			mv.setViewName("listUtilisateur");
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}
		return mv;
	}

	@GetMapping(value = "/showUtilisateur")
	public ModelAndView showUtilisateur(@RequestParam(value = "id") int id, ModelAndView mv,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {

			mv.setViewName("showUtilisateur");
			Optional<UtilisateurDto> userOptional = this.userService.findById(id);
			if (userOptional.isPresent()) {
				UtilisateurDto utilisateur = new UtilisateurDto();
				utilisateur = userOptional.get();
				System.err.println("user" + utilisateur.toString());
				mv.addObject("utilisateur", utilisateur);
			} else {
				mv.addObject("message", "l'utilisateur n'existe pas");
			}
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}

		return mv;
	}

	@PostMapping(value = "/supprimerUtilisateur", params = { "id" })
	public ModelAndView supprimerUtilisateur(@RequestParam(value = "id") Integer id, ModelAndView mv,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {

			mv.setViewName("listeUtilisateur");
			String idParam = id.toString();
			String msg = "";
			ReponseStatut statut = ReponseStatut.OK;

			if (idParam == null || idParam.length() == 0) {
				statut = ReponseStatut.KO;
				msg = "le parametre id est obligatoire";
				mv.addObject("message", msg);
			} else {
				if (!idParam.matches("^\\p{Digit}+$")) {
					statut = ReponseStatut.KO;
					msg = "le parametre id doit etre un numero";
					mv.addObject("message", msg);
				} else {
					id = Integer.parseInt(idParam);
					String rep = this.userService.deleteById(id);
					switch (rep) {
					case "OK":
						statut = ReponseStatut.OK;
						System.err.println("supprimé");
						msg = "suppression reussie avec succes";
						mv.addObject("message", msg);
						break;
					case "KO_SQL_EXCEPTION":
						System.err.println("PAS supprimé sqlexception");
						statut = ReponseStatut.KO;
						msg = "aie";
						System.out.println(msg);
						mv.addObject("message", msg);
						return mv;
					case "KO":
						System.err.println("PAS supprimé id");
						statut = ReponseStatut.KO;
						msg = "aucun utilisateur n'a cet id " + id;
						mv.addObject("message", msg);
						break;
					default:
						System.err.println("PAS supprimé je sais pas");
						statut = ReponseStatut.KO;
						msg = "erreur inconnu, contacter le support";
						mv.addObject("message", msg);
						break;
					}
				}
			}
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}
		return mv;
	}

	@GetMapping(value = "/ajouterUtilisateur")
	public ModelAndView formulaireUtilisateur(ModelAndView mv, HttpServletRequest request) {
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {
			mv.setViewName("ajouterUtilisateur");
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}

		return mv;

	}

	@PostMapping(value = "/ajouterUtilisateur", params = { "login", "password" })
	public ModelAndView ajouterUtilisateur(@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password, ModelAndView mv, HttpServletRequest request) {
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur1 = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur1.getAdmin() != null) {

			boolean test = this.userService.ajouterUtilisateur(login, password);
			if (test) {
				mv.addObject("message", "l'utilisateur a bien été ajouté");
			} else {
				mv.addObject("message", "l'utilisateur n'as pas été rajouté");
			}
			mv.setViewName("ajouterUtilisateur");
		} else {
			mv.addObject("message", "vous n'avez pas les droits super Admin");
			mv.setViewName("accueil");
		}

		return mv;

	}
	
	@RequestMapping(value = "/editUtilisateur", method = RequestMethod.GET, params = { "id" })

	public ModelAndView formulaireEditUtilisateur(@RequestParam(value = "id") int id,ModelAndView mv, HttpServletRequest request) {
		System.out.println("Utilisateurlala : " + id);

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		}else {

			mv.setViewName("editUtilisateur");
			Optional<UtilisateurDto> utilisateurOptional = this.userService.findById(id);
			if (utilisateurOptional.isPresent()) {
				mv.addObject("utilisateuraedit", utilisateurOptional.get().getId());
			} else {
				mv.addObject("message", "l'utilisateur n'existe pas");
			}

		}
		return mv;
	}

	@RequestMapping(value = "/editUtilisateur", method = RequestMethod.POST, params = { "utilisateuraedit", "login", "password" })
	public ModelAndView editCategorie(@RequestParam(value = "login") String login,@RequestParam(value = "password") String password,@RequestParam(value = "utilisateuraedit") int id ,ModelAndView mv, HttpServletRequest request) {
		System.out.println("Utilisateurlalapost : " + login);

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		}else {
			Optional<UtilisateurDto> userOpDto = this.userService.findById(id);
			mv.setViewName("editUtilisateur");
			if (userOpDto.isPresent()) {
				userOpDto.get().setLogin(login);
				userOpDto.get().setPassword(password);
				this.userService.mettreAJourUtilisateur(userOpDto.get());
				System.err.println("utilisateur" + userOpDto.get().toString());
				mv.addObject("utilisateur", userOpDto.get());
			} else {

				mv.addObject("message", "l'utilisateur n'existe pas");
			}
		}
		return mv;
	}
}
