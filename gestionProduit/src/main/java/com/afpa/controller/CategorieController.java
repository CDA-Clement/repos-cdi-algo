package com.afpa.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.ProduitDto;
import com.afpa.dto.ReponseStatut;
import com.afpa.dto.UtilisateurDto;
import com.afpa.service.CategorieServiceImpl;
import com.afpa.service.ICategorieService;
import com.afpa.service.CategorieServiceImpl;



@Controller
public class CategorieController {

	@Autowired
	private ICategorieService categorieService;

	@RequestMapping(value = "/listCategorie", method = RequestMethod.GET)
	public ModelAndView listCategorie(
			@RequestParam(value = "page") String pageStr,
			@RequestParam(value = "trier", defaultValue = "null") String trier,
			ModelAndView mv, HttpServletRequest request) {
		int page = 0;
		mv.setViewName("listCategorie");
		System.out.println("pageStr: "+pageStr);
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr = "0";
		}else {
			page = Integer.parseInt(pageStr);
			mv.addObject("page", page);
			mv.addObject("page1", pageStr);

		}
		mv.addObject("trier", trier);

		if(trier.equals("nomUp")) {
			List<CategorieDto> liste = this.categorieService.chercherToutesLesCategoriesParNom(page);
			mv.addObject("fin", CategorieServiceImpl.fin );
			mv.addObject("liste", liste);
		}

		if(trier.equals("nomDown")) {
			List<CategorieDto> liste = this.categorieService.chercherToutesLesCategoriesParNomDesc(page);
			mv.addObject("fin", CategorieServiceImpl.fin );
			mv.addObject("liste", liste);
		}


		if(trier.equals("idDown")) {
			List<CategorieDto> liste = this.categorieService.chercherToutesLesCategoriesParIdDesc(page);
			mv.addObject("fin", CategorieServiceImpl.fin );
			mv.addObject("liste", liste);
		}

		if(trier.equals("idUp")) {
			List<CategorieDto> liste = this.categorieService.chercherToutesLesCategoriesParId(page);
			mv.addObject("fin", CategorieServiceImpl.fin );
			mv.addObject("liste", liste);
		}
		if(trier.equals("idUp")) {

		}
		if(trier.equals("null")) {

			List<CategorieDto> liste = this.categorieService.chercherToutesLesCategories(page);
			for (CategorieDto categorieDto : liste) {
				System.err.println(categorieDto.toString());
				mv.addObject("fin", CategorieServiceImpl.fin );
				mv.addObject("liste", liste);
			}
		}


		return mv;

	}

	@RequestMapping(value = "/supprimerCategorie", method = RequestMethod.POST, params = { "id" })
	public ModelAndView supprimerCategorie(@RequestParam(value = "id") Integer id, ModelAndView mv,HttpServletRequest request) {

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		}else {

			mv.setViewName("listCategorie");
			String idParam = id.toString();
			String msg = "";
			ReponseStatut statut = ReponseStatut.OK;

			if (idParam == null || idParam.length() == 0) {
				statut = ReponseStatut.KO;
				msg = "le parametre id est obligatoire";
				mv.addObject("message", msg);
			} else {
				if (!idParam.matches("^\\p{Digit}+$")) {
					statut = ReponseStatut.KO;
					msg = "le parametre id doit etre un numero";
					mv.addObject("message", msg);
				} else {
					id = Integer.parseInt(idParam);
					String rep = this.categorieService.deleteById(id);
					switch (rep) {
					case "OK":
						statut = ReponseStatut.OK;
						System.err.println("supprimé");
						msg = "suppression reussie avec succes";
						mv.addObject("message", msg);
						break;
					case "KO_SQL_EXCEPTION":
						System.err.println("PAS supprimé sqlexception");
						statut = ReponseStatut.KO;
						msg = "la catégorie possèdent des produits";
						System.out.println(msg);
						mv.addObject("message", msg);
						return mv;
					case "KO":
						System.err.println("PAS supprimé id");
						statut = ReponseStatut.KO;
						msg = "aucune produit n'a cet id " + id;
						mv.addObject("message", msg);
						break;
					default:
						System.err.println("PAS supprimé je sais pas");
						statut = ReponseStatut.KO;
						msg = "erreur inconnu, contacter le support";
						mv.addObject("message", msg);
						break;
					}
				}
			}
		}
		return mv;
	}

	@RequestMapping(value = "/ajouterCategorie", method = RequestMethod.GET)
	public ModelAndView formulaireCategorie(
			@RequestParam(value = "page",defaultValue = "0") String pageStr,
			@RequestParam(value = "trier",defaultValue = "null") String trier,
			ModelAndView mv, 
			HttpServletRequest request) {

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		} else {
			int page = 0;
			if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
				pageStr = "0";
			}else {
				page = Integer.parseInt(pageStr);
			}
			System.out.println("par la car formulaire rempli");
			mv.addObject("page", page);
			mv.addObject("trier", trier);
			mv.setViewName("ajouterCategorie");
		}
		return mv;

	}

	@RequestMapping(value = "/ajouterCategorie", method = RequestMethod.POST, params = {"trier", "page","label", "quantite", "prix",
	"categorie" })
	public ModelAndView ajouterProduit(
			@RequestParam(value = "label", defaultValue = "aucun") String label,
			@RequestParam(value = "quantite", defaultValue = "0") Integer quantite,
			@RequestParam(value = "prix", defaultValue = "0") long prix,
			@RequestParam(value = "page",defaultValue = "0") String pageStr,
			@RequestParam(value = "trier",defaultValue = "null") String trier,
			@RequestParam(value = "categorie", defaultValue = "aucune") String categorie, ModelAndView mv,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		System.out.println("je passe par ajoutproduit");
		if (utilisateur == null) {
			mv.setViewName("login");
		} else {
			int page = 0;
			if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
				pageStr = "0";
			}else {
				page = Integer.parseInt(pageStr);
			}


			System.out.println("par la car formulaire rempli");

			System.out.println("par la car formulaire rempli");

			boolean test = this.categorieService.ajouterCategorie(label);
			if (test) {
				mv.addObject("message", "le produit a bien été ajouté");
			} else {
				mv.addObject("message", "le produit n'as pas été rajouté");
			}
			listCategorie(pageStr,trier,mv, request);
		}

		return mv;

	}

	@RequestMapping(value = "/showCategorie", method = RequestMethod.GET, params = { "id" })

	public ModelAndView showCategorie(
			@RequestParam(value = "id") int id, 
			@RequestParam(value = "page") String pageStr,
			@RequestParam(value = "trier") String trier,
			ModelAndView mv, HttpServletRequest request) {
		System.out.println("produit " + id);
		int page = 0;
		System.out.println("page "+pageStr);
		if(pageStr == null || pageStr.length() == 0||!pageStr.matches("^\\p{Digit}+$")) {
			pageStr = "0";
		}else {
			page = Integer.parseInt(pageStr);
		}

		mv.setViewName("showCategorie");
		Optional<CategorieDto> categorieOptional = this.categorieService.findById(id);
		if (categorieOptional.isPresent()) {
			CategorieDto categorie = new CategorieDto();
			categorie = categorieOptional.get();
			System.err.println("categorie" + categorie.toString());
			mv.addObject("categorie", categorie);
			mv.addObject("page", pageStr);
			mv.addObject("trier", trier);
		} else {

			mv.addObject("message", "la categorie n'existe pas");
		}
		return mv;
	}

	@RequestMapping(value = "/editCategorie", method = RequestMethod.GET, params = { "id" })

	public ModelAndView formulaireEditCategorie(@RequestParam(value = "id") int id,ModelAndView mv, HttpServletRequest request) {
		System.out.println("Catégorielala : " + id);

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		}else {

			mv.setViewName("editCategorie");
			Optional<CategorieDto> categorieOptional = this.categorieService.findById(id);
			if (categorieOptional.isPresent()) {
				mv.addObject("categorie", categorieOptional.get().getId());
			} else {
				mv.addObject("message", "la catégorie n'existe pas");
			}

		}
		return mv;
	}

	@RequestMapping(value = "/editCategorie", method = RequestMethod.POST, params = { "categorie", "label" })
	public ModelAndView editCategorie(@RequestParam(value = "label") String label,@RequestParam(value = "categorie") int id ,ModelAndView mv, HttpServletRequest request) {
		System.out.println("Catégorielalapost : " + label);

		HttpSession session = request.getSession();
		UtilisateurDto utilisateur = (UtilisateurDto) session.getAttribute("utilisateur");
		if (utilisateur == null) {
			mv.setViewName("login");
		}else {

			Optional<CategorieDto> cateOpDto = this.categorieService.findById(id);
			mv.setViewName("editCategorie");
			if (cateOpDto.isPresent()) {
				cateOpDto.get().setLabel(label);
				this.categorieService.mettreAJourCategorie(cateOpDto.get());
				System.err.println("categorie" + cateOpDto.get().toString());
				mv.addObject("categorie", cateOpDto.get());
			} else {

				mv.addObject("message", "la catégorie n'existe pas");
			}
		}
		return mv;
	}
}
