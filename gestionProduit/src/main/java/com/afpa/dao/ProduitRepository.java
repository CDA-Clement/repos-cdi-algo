package com.afpa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Produit;

@Repository
public interface ProduitRepository extends PagingAndSortingRepository<Produit, Integer> {
	
	
	public Page<Produit> findAll(Pageable pageable);
	public Page<Produit> findAllByOrderByPrixAsc(Pageable pageable);
	public Page<Produit> findAllByOrderByQuantiteAsc(Pageable pageable);
	public Page<Produit> findAllByOrderByLabelAsc(Pageable pageable);
	public Page<Produit> findAllByOrderByIdAsc(Pageable pageable);
	
	public Page<Produit> findAllByOrderByPrixDesc(Pageable pageable);
	public Page<Produit> findAllByOrderByLabelDesc(Pageable pageable);
	public Page<Produit> findAllByOrderByQuantiteDesc(Pageable pageable);
	
	public Page<Produit> findAllByOrderByIdDesc(Pageable pageable);

}

