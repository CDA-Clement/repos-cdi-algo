package com.afpa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Categorie;
import com.afpa.entity.Categorie;



@Repository
public interface CategorieRepository extends PagingAndSortingRepository<Categorie, Integer> {

	
	public Page<Categorie> findAll(Pageable pageable);
	
	
	public Page<Categorie> findAllByOrderByLabelAsc(Pageable pageable);
	public Page<Categorie> findAllByOrderByIdAsc(Pageable pageable);
	
	public Page<Categorie> findAllByOrderByLabelDesc(Pageable pageable);
	
	
	public Page<Categorie> findAllByOrderByIdDesc(Pageable pageable);

	
}