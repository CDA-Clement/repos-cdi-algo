package com.afpa.service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.dao.CategorieRepository;
import com.afpa.dto.CategorieDto;
import com.afpa.dto.UtilisateurDto;
import com.afpa.dto.CategorieDto;
import com.afpa.entity.Categorie;

@Service
public class CategorieServiceImpl implements ICategorieService {
	static int INC = 5;
	public static int fin;
	@Autowired
	private CategorieRepository categorieRepository;
	@Override
	public List<CategorieDto> chercherToutesLesCategories(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<CategorieDto> maliste = this.categorieRepository.findAll(firstPageWithTwoElements).stream()
				.map(e -> CategorieDto.builder().id(e.getId()).label(e.getLabel()).build()).collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	@Override
	public String deleteById(int id) {
		if (this.categorieRepository.existsById(id)) {
			try {
				this.categorieRepository.deleteById(id);	
				return "OK";
			} catch (Exception e) {
				return "KO_SQL_EXCEPTION";				
			}
		}
		return "KO";
	}
	@Override
	public Optional<CategorieDto> findById(int id) {
		Optional<Categorie> pers = this.categorieRepository.findById(id);
		Optional<CategorieDto> res = Optional.empty();
		if (pers.isPresent()) {
			Categorie p = pers.get();
			res = Optional.of(CategorieDto.builder().id(p.getId()).label(p.getLabel()).build());
		}
		return res;
	}
	@Override
	public Boolean ajouterCategorie(String label) {
		Boolean creer = true;
		Iterable<Categorie> categories = this.categorieRepository.findAll();
		
		for (Categorie categorie : categories) {
			if (categorie.getLabel().equalsIgnoreCase(label)) {
				creer = false;
				break;
			}
		}
		if (creer == true) {
			Categorie categorie1 = Categorie.builder().label(label).build();
			try {
				categorieRepository.save(categorie1);
				return true;
			} catch (Exception e) {
				return false;				
			}
		}
		return creer;
	}
	@Override
	public Boolean mettreAJourCategorie(CategorieDto categorieDto) {
		Categorie categorie = Categorie.builder().id(categorieDto.getId()).label(categorieDto.getLabel()).build();
		try {
			this.categorieRepository.save(categorie);			
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	@Override
	public List<CategorieDto> chercherToutesLesCategoriesParNom(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<CategorieDto> maliste = this.categorieRepository.findAllByOrderByLabelAsc(firstPageWithTwoElements).stream()
				.map(e -> CategorieDto.builder().id(e.getId()).label(e.getLabel()).build()).collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	@Override
	public List<CategorieDto> chercherToutesLesCategoriesParNomDesc(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<CategorieDto> maliste = this.categorieRepository.findAllByOrderByLabelDesc(firstPageWithTwoElements).stream()
				.map(e -> CategorieDto.builder().id(e.getId()).label(e.getLabel()).build()).collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	
	@Override
	public List<CategorieDto> chercherToutesLesCategoriesParIdDesc(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<CategorieDto> maliste = this.categorieRepository.findAllByOrderByIdDesc(firstPageWithTwoElements).stream()
				.map(e -> CategorieDto.builder().id(e.getId()).label(e.getLabel()).build()).collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	@Override
	public List<CategorieDto> chercherToutesLesCategoriesParId(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<CategorieDto> maliste = this.categorieRepository.findAllByOrderByIdAsc(firstPageWithTwoElements).stream()
				.map(e -> CategorieDto.builder().id(e.getId()).label(e.getLabel()).build()).collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
}