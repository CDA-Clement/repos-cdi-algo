package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.dao.ProduitRepository;
import com.afpa.dto.CategorieDto;
import com.afpa.dto.ProduitDto;
import com.afpa.entity.Categorie;
import com.afpa.entity.Produit;


@Service
public class ProduitServiceImpl implements IProduitService {
	static int INC = 5;
	public static int fin;

	@Autowired
	private ProduitRepository produitRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<ProduitDto> chercherToutesLesProduits(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAll(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	@Override
	public boolean deleteById(int id) {
		if(this.produitRepository.existsById(id)) {
			this.produitRepository.deleteById(id);
			return true;
		}
		return false;
	}
	
	@Override
	public List<ProduitDto> chercherToutesLesProduitsParNom(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByLabelAsc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public List<ProduitDto> chercherToutesLesProduitsParPrix(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByPrixAsc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	
	@Override
	public List<ProduitDto> chercherToutesLesProduitsParQuantite(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByQuantiteAsc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	

	@Override
	public Optional<ProduitDto> findById(int id) {
		Optional<Produit> prod = this.produitRepository.findById(id);
		Optional<ProduitDto> res = Optional.empty();
		if(prod.isPresent()) {
			Produit p = prod.get();
			ProduitDto prodDto = this.modelMapper.map(p, ProduitDto.class);
			if(p.getCategorie() != null) {
				CategorieDto metDto = this.modelMapper.map(p.getCategorie(),CategorieDto.class);
				prodDto.setCategorie(metDto);
			}
			res = Optional.of(prodDto);
		}
		return res;
	}
	
	@Override
	public Integer ajouter(ProduitDto prod) {
		Produit p = this.modelMapper.map(prod,Produit.class);
		p = this.produitRepository.save(p);
		return p.getId();
	}
	@Override
	public Boolean mettreAJourProduit(ProduitDto produitDto) {
		Produit produit = Produit.builder()
				.id(produitDto.getId())
				.label(produitDto.getLabel())
				.prix(produitDto.getPrix())
				.quantite(produitDto.getQuantite())
				.categorie(Categorie.builder()
						.id(produitDto.getCategorie().getId())
						.label(produitDto.getCategorie().getLabel())
						.build())
				.build();
		try {
			this.produitRepository.save(produit);			
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	@Override
	public List<ProduitDto> chercherToutesLesProduitsParNomDesc(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByLabelDesc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	
	}
	@Override
	public List<ProduitDto> chercherToutesLesProduitsParPrixDesc(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByPrixDesc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	@Override
	public List<ProduitDto> chercherToutesLesProduitsParQuantiteDesc(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByQuantiteDesc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	
	@Override
	public List<ProduitDto> chercherToutesLesProduitsParIdDesc(int page) {
		
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByIdDesc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
}
	@Override
	public List<ProduitDto> chercherToutesLesProduitsParId(int page) {
		Pageable firstPageWithTwoElements = PageRequest.of(page, INC);
		List<ProduitDto> maliste = this.produitRepository.findAllByOrderByIdAsc(firstPageWithTwoElements)
				.stream()
				.map(e->ProduitDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
					//	.image(e.image())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
}
