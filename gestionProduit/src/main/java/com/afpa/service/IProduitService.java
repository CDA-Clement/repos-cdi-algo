package com.afpa.service;

import java.util.List;
import java.util.Optional;

import com.afpa.dto.ProduitDto;

public interface IProduitService {

	public List<ProduitDto> chercherToutesLesProduits(int page);

	public boolean deleteById(int id);

	public Optional<ProduitDto> findById(int id);

	Integer ajouter(ProduitDto prod);

	List<ProduitDto> chercherToutesLesProduitsParNom(int page);

	List<ProduitDto> chercherToutesLesProduitsParPrix(int page);

	List<ProduitDto> chercherToutesLesProduitsParQuantite(int page);

	public Boolean mettreAJourProduit(ProduitDto produitDto);

	public List<ProduitDto> chercherToutesLesProduitsParNomDesc(int page);

	public List<ProduitDto> chercherToutesLesProduitsParPrixDesc(int page);

	public List<ProduitDto> chercherToutesLesProduitsParQuantiteDesc(int page);

	

	public List<ProduitDto> chercherToutesLesProduitsParIdDesc(int page);

	public List<ProduitDto> chercherToutesLesProduitsParId(int page);

}
