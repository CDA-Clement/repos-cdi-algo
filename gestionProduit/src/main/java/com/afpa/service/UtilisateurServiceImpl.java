package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.UtilisateurRepository;
import com.afpa.dto.UtilisateurDto;
import com.afpa.entity.Categorie;
import com.afpa.entity.Utilisateur;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService{
	
	@Autowired
	private UtilisateurRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<UtilisateurDto> chercherToutLeMonde() {
	
		List<UtilisateurDto> maliste =this.userRepository
				.findAll()
				.stream()
				.map(u->UtilisateurDto.builder()
						.id(u.getId())
						.login(u.getLogin())
						.password(u.getPassword())
						.admin(u.getAdmin())
						.build())
				.collect(Collectors.toList());
		return maliste;
	}

	@Override
	public String deleteById(int id) {
		if (this.userRepository.existsById(id)) {
			try {
				this.userRepository.deleteById(id);	
				return "OK";
			} catch (Exception e) {
				return "KO_SQL_EXCEPTION";				
			}
		}
		return "KO";
	}

	@Override
	public Optional<UtilisateurDto> findById(int id) {
		Optional<Utilisateur> user = this.userRepository.findById(id);
		Optional<UtilisateurDto> res = Optional.empty();
		if(user.isPresent()) {
			Utilisateur u = user.get();
			UtilisateurDto userDto = this.modelMapper.map(u, UtilisateurDto.class);
			res = Optional.of(userDto);
		}
		return res;
	}

	@Override
	public Boolean ajouterUtilisateur(String login, String password) {
		Boolean creer = true;
		Iterable<Utilisateur> utilisateurs = this.userRepository.findAll();
		for (Utilisateur utilisateur : utilisateurs) {
			if (utilisateur.getLogin().equals(login)) {
				creer = false;
				break;
			}
		}
		if (creer) {
			Utilisateur utilisateur1 = Utilisateur.builder().login(login).password(password).build();
			try {
				userRepository.save(utilisateur1);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return creer;
	}

	@Override
	public Boolean mettreAJourUtilisateur(UtilisateurDto utilisateurDto) {
		Utilisateur utilisateur = Utilisateur.builder().id(utilisateurDto.getId()).login(utilisateurDto.getLogin()).password(utilisateurDto.getPassword()).build();
		try {
			this.userRepository.save(utilisateur);			
		} catch (Exception e) {
			return false;
		}
		return true;
	}


}
