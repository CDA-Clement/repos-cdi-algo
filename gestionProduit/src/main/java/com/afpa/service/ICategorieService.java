package com.afpa.service;

import java.util.List;
import java.util.Optional;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.UtilisateurDto;



public interface ICategorieService {
	public List<CategorieDto> chercherToutesLesCategories(int page);

	public String deleteById(int id);

	public Optional<CategorieDto> findById(int id);

	public Boolean ajouterCategorie(String label);
	
	public Boolean mettreAJourCategorie(CategorieDto categorieDto);

	public List<CategorieDto> chercherToutesLesCategoriesParNom(int page);

	public List<CategorieDto> chercherToutesLesCategoriesParNomDesc(int page);


	public List<CategorieDto> chercherToutesLesCategoriesParIdDesc(int page);

	public List<CategorieDto> chercherToutesLesCategoriesParId(int page);
}
