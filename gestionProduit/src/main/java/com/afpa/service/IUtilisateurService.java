package com.afpa.service;

import java.util.List;
import java.util.Optional;

import com.afpa.dto.UtilisateurDto;

public interface IUtilisateurService {

	public List<UtilisateurDto> chercherToutLeMonde();

	public String deleteById(int id);

	public Optional<UtilisateurDto> findById(int id);

	public Boolean ajouterUtilisateur(String login, String password);
	
	public Boolean mettreAJourUtilisateur(UtilisateurDto utilisateurDto);

}