package arraylist;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import javax.print.attribute.HashAttributeSet;

public class Ensemble{
	private ArrayList<String> elements;

	public Ensemble () {
		this.elements = new ArrayList<String>();
	}

	public void ajoute(String a) {
		if (this.elements.contains(a)) {	
		}else {
			this.elements.add(a);
		}
	}

	public int taille() {
		int taille =0;
		if(this.elements.isEmpty()==true) {
			System.out.println("Ensemble vide");
		}else {	
			taille = this.elements.size();
		}

		return taille;}


	public String toString() {
		String s="";
		if(this.elements.isEmpty()==true) {
			System.out.println("Ensemble vide");
		}else {
			s+=this.elements;
		}	
		return s;
	}

	public Ensemble union(Ensemble x) {
		Ensemble union = new Ensemble();
		union.elements.addAll(x.elements);
		union.elements.removeAll(this.elements);
		union.elements.addAll(this.elements);
		Collections.sort(union.elements);
		return union;
	}


	Ensemble intersection(Ensemble y) {
		Ensemble inters = new Ensemble();
		//System.out.println(this.elements.size());
		for (int i = 0; i < this.elements.size(); i++) {
			if(y.elements.contains(this.elements.get(i))) {
				inters.ajoute(this.elements.get(i));

			}

		}

		return inters;
	}
}
