package arraylist;

public class Scenario {
	public static void main(String [] args){
		Ensemble a = new Ensemble();
		Ensemble b = new Ensemble();
		Ensemble c = new Ensemble();

		a.ajoute("1");
		a.ajoute("2");
		a.ajoute("3");
		a.ajoute("3");

		b.ajoute("3");
		b.ajoute("5");
		b.ajoute("7");

		System.out.println(a);
		System.out.println(b);
		System.out.println(c);

		c = a.union(b);
		System.out.println("union de a et b = "+c);
		System.out.println(b);
		Ensemble d = new Ensemble();
		d = a.intersection(b);
		System.out.println("intersection de a et b = "+d);
	}
}

