package com.afpa.app;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.apache.commons.lang3.StringUtils;

import com.afpa.tools.CdaTools;
import com.afpa.tools.ParametreNonValideException;




public class AppTest {
	CdaTools cda;
	CdaApp cdaApp;
	@BeforeEach
	void setUp() throws Exception { 
		cda = new CdaTools();
		cdaApp =  new CdaApp();
	}

	@Test
	public void factorielTestExecption(){
		Throwable e = null;
		try {
			cda.doubleFactoriel(20);
			fail("normalement exception !!!! ");
		} catch (Exception e2) {
			e = e2;
		}
		assertTrue(e instanceof ParametreNonValideException );
	}
	@Test
	public void factorielTest(){
		try {
			assertTrue(cda.doubleFactoriel(3)==12);
		} catch (Exception e) {
			fail("normalement pas d'exception !!!! ");
		}
	}
	@Test
	public void factorielTestExcepnegatif(){
		Throwable e = null;
		try {
			cda.doubleFactoriel(-2);
			fail("normalement exception !!!! ");
		} catch (Exception e2) {
			e = e2;
		}
		assertTrue(e instanceof ParametreNonValideException );
	}
	
	@Test
	public void padTest() {
		try {
			assertTrue(cdaApp.doubleFactAndLeftPad(3).equals("0000000000000000012"));
															  
		} catch (ParametreNonValideException e) {
			
			fail("pas execption ici");
		}
	}
	
	@Test
	public void padTestExecption(){
		Throwable e = null;
		try {
			cdaApp.doubleFactAndLeftPad(20);
			fail("normalement exception !!!! ");
		} catch (Exception e2) {
			e = e2;
		}
		assertTrue(e instanceof ParametreNonValideException );
	}
		
	}



	
	

