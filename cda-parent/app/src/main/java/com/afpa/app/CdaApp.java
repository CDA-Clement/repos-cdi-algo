package com.afpa.app;
import com.afpa.tools.CdaTools;
import com.afpa.tools.ParametreNonValideException;
import org.apache.commons.lang3.StringUtils;

/**
 * Hello world!
 *
 */
public class CdaApp  {
	public String doubleFactAndLeftPad(int n) throws ParametreNonValideException{

		CdaTools cda = new CdaTools();
		Long s = cda.doubleFactoriel(n);
		String fin = s.toString();
		
		System.err.println(StringUtils.leftPad(fin, 19,"0"));		
		return StringUtils.leftPad(fin, 19,"0");

	}

}
