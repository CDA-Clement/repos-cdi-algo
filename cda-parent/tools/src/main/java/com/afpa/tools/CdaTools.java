package com.afpa.tools;

import org.apache.commons.math3.util.CombinatoricsUtils;

public class CdaTools  {
	 public long doubleFactoriel(int a) throws ParametreNonValideException{
		if (a>19 || a<0){
			throw new ParametreNonValideException();
		}else {
			
			return  (CombinatoricsUtils.factorial(a))*2;
		} 
	 }
   
}
