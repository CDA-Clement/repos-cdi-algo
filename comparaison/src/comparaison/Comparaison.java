package comparaison;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;   
public class Comparaison {


	public static void main(String[] args)  {
		
		LinkedHashSet<String> linkedset = new LinkedHashSet<String>();  
		String[] StringTab = new String[linkedset.size()];
		ArrayList<String> arrayList = new ArrayList();
		LinkedList<String> linkedList = new LinkedList();
		
		//The size of a String[] can not be changed once initialized
		System.out.println("Size of the StringTab initialized at the very beginning "+StringTab.length);

		for (int i = 0; i < StringTab.length; i++) {
			Iterator<String> iter = linkedset.iterator();
			if (iter.hasNext()) {
				StringTab[i] = iter.next();
			}
			System.out.println("the element i from the loop FOR i: as the size was 0 the reuslt is therefore " +StringTab[i]);
		}
		System.out.println("Size of the StringTab after the loop FOR i: as the size was 0 the size remains zero "+StringTab.length);
		
		
		// Adding element to LinkedHashSet & ArrayList  
		
		linkedset.add("B");   
		linkedset.add("C"); 
		linkedset.add("D");   
		linkedset.add("A"); 
		System.out.println("size of the arraylist before adding elements "+arrayList.size());
		System.out.println("contents of the arraylist before adding elements "+arrayList);
		arrayList.add("B");
		arrayList.add("C");
		arrayList.add("D");   
		arrayList.add("A"); 
		System.out.println("size of the arraylist after adding elements "+arrayList.size());
		System.out.println("contents of the arraylist after adding elements "+arrayList);
		arrayList.add("B");
		arrayList.add("C");
		arrayList.add("D");   
		arrayList.add("A"); 
		System.out.println("size of the arraylist after adding duplicate elements "+arrayList.size());
		System.out.println("contents of the arraylist after adding duplicate elements "+arrayList);
		System.out.println("************************************************");
		
		
		
		System.out.println("************************************************");
		System.out.println("size of the linkedlist before adding elements "+linkedList.size());
		System.out.println("contents of the linkedlist initially "+linkedList);
		linkedList.add("B");
		linkedList.add("C");
		linkedList.add("D");   
		linkedList.add("A"); 
		System.out.println("size of the linkedlist after adding elements "+linkedList.size());
		System.out.println("contents of the linkedlist after adding  elements "+linkedList);
		System.out.println("************************************************");
		linkedList.add("B");
		linkedList.add("C");
		linkedList.add("D");   
		linkedList.add("A"); 
		System.out.println("size of the linkedlist after adding duplicate elements "+linkedList.size());
		System.out.println("contents of the linkedlist after adding duplicate elements "+linkedList);
		linkedList.addFirst("F");
		System.out.println("after adding an element with the method addfirst "+linkedList);
		String[] StringTab1 = new String[linkedset.size()];
		
		System.out.println(StringTab1.length);
		Iterator<String> iter = linkedset.iterator();
		for (int i = 0; i < StringTab1.length; i++) {
			System.out.println("the elements of an ArrayList can be retrieved with a loop for i "+arrayList.get(i));
			if (iter.hasNext()) {
				StringTab1[i] = iter.next();
			}
			System.out.println(StringTab1[i]);
		}
		
		System.out.println(StringTab1.length);

		// This will not add new element as A already exists , 
		// Therefore the LinkedHashSet does not contain duplicate.
		linkedset.add("A");  
		linkedset.add("E");   
		
		System.out.println("size of the linkedset before adding elements "+linkedset.size());
		System.out.println("contents of the linkedset initially"+linkedset);
		System.out.println("Removing D from LinkedHashSet: " + linkedset.remove("D"));   
		System.out.println("Trying to Remove Z which is not "+ "present: " + linkedset.remove("Z"));   
		System.out.println("Checking if A is present=" +  linkedset.contains("A")); 
		System.out.println("Updated LinkedHashSet: " + linkedset);   

		System.out.println("*********************************************************");
		System.out.println("*********************************************************");
		
	}   

}
