package MockitoSimple.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import MockitoSimple.main.BloodType;
import MockitoSimple.main.Etudiant;
import static org.mockito.Mockito.*;
class TestEtudiant {
	Etudiant e;
	@BeforeEach
	public void setup() {
		
		e= new Etudiant();
		e.setName("Clement");
	
		e.aE=mock(BloodType.class);
	}
	
	
	@Test
	void testInfo(){
		
		
		when(e.aE.getGroupeSanguain(e)).thenReturn("AB+");
		
		assertTrue(e.infoOnStudent(e).equals("Clement AB+"));
		
		
		
	}
	

}
