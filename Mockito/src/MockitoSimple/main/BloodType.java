package MockitoSimple.main;

public interface BloodType {

	public String getGroupeSanguain(Etudiant e);
	
	//very complex method to build in order to know the blood type of a person.
	//it takes months to develop.
}
