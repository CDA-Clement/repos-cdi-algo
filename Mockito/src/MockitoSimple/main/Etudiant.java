package MockitoSimple.main;

public class Etudiant implements BloodType {
	private String name;
	public BloodType aE;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String infoOnStudent(Etudiant e) {
		String retour = e.getName() + " " + e.aE.getGroupeSanguain(e);
		System.err.println(retour);
		return retour;
	}

	@Override
	public String getGroupeSanguain(Etudiant e) {
		// TODO Auto-generated method stub
		return null;
	}
}
