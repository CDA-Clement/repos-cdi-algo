<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="userAdd" />
	<jsp:param name="titre" value="user add" />
</jsp:include>

<sec:authorize access="isAuthenticated()">
	<form method="post" action="/user/add.html">
	<input type="text" name="username">
	<input type="password" name="password">
	<input type="number" name="id">
	activer user <input type="checkbox" name="statut" value="activer"> 
	<br/>
	
		<input type="submit" value="ajouter">
	</form>
</sec:authorize>

</body>
</html>