<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="userModif" />
	<jsp:param name="titre" value="user modif" />
</jsp:include>

<sec:authorize access="isAuthenticated()">
	<form method="post" action="/user/modif.html">
	<input hidden="true" type="text" name="idUser" value="${user.id}">
	<input type="text" name="username">
	<input type="password" name="password">
	<input type="number" name="idRole">
	<c:if test="${user.statut}">
	desactiver<input type="checkbox" name="statut" value="desactiver">
	</c:if>
	<c:if test="${user.statut==false}">
	activer<input type="checkbox" name="statut" value="activer">
	</c:if>
		<input type="submit" value="modifier">
	</form>
</sec:authorize>

</body>
</html>