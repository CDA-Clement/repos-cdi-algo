<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="userList" />
	<jsp:param name="titre" value="users liste" />
</jsp:include>

<table class="table w-75">
	<thead class="thead-dark">
		<tr class="head">
			<th>user</th>
			<th>date creation</th>
			<th>date derni�re connexion</th>
			<sec:authorize access="isAuthenticated()">
				<th>edit</th>
				<th>Activer</th>
				<th>Desactiver</th>

			</sec:authorize>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${users}" var="user" varStatus="index">
			<tr class="${index.count%2==0?'pair':'impair'}">
				<td>${user.name}</td>
				<td>${user.date}</td>
				<td>${user.connexion}</td>
				<sec:authorize access="isAuthenticated()">
				
				<td>
				<a href="/user/modif?id=${user.id}">modifier</a>
				</td>	
				<c:if test="${user.statut==false}">
					<td>
					<a href="/user/activer?id=${user.id}">activer</a>
					</td>
					</c:if>
					<c:if test="${user.statut}">
					<td>
					<a href="/user/desactiver?id=${user.id}">desactiver</a>
					</td>	
					</c:if>
					
					
					</sec:authorize>
			</tr>
		</c:forEach>
</table>

</body>
</html>