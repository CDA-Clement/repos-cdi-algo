package com.insy2s.xss.victime.service;

import java.util.List;
import java.util.Optional;

import com.insy2s.xss.victime.Dto.RoleDto;
import com.insy2s.xss.victime.entity.Role;



public interface IRoleService {
	public List<RoleDto> chercherTousLesRoles();
	Optional<RoleDto> chercherParId(Integer Id);
	public Integer enregistrer(Role role);

}
