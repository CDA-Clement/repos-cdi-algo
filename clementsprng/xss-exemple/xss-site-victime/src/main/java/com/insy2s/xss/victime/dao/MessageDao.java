package com.insy2s.xss.victime.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.insy2s.xss.victime.entity.Message;

@Repository
public interface MessageDao extends CrudRepository<Message, Integer> {
	public List<Message> findAll();
}
