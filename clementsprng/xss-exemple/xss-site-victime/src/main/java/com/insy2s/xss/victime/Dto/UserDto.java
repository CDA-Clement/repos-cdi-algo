package com.insy2s.xss.victime.Dto;



import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
	private Integer id;
    private String name;
    private String password;
    private Boolean statut;
    private String date;
    private RoleDto role;
    private String connexion;

}
