package com.insy2s.xss.victime.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.insy2s.xss.victime.dao.MessageDao;
import com.insy2s.xss.victime.dao.UserDao;
import com.insy2s.xss.victime.entity.Message;

@Controller
@RequestMapping("msg")
public class MessageAdminController {

	@Autowired
	private MessageDao messageDao;
	
	@Autowired
	private UserDao userDao;
	
	@GetMapping(path = { "list.html","/"})
	public ModelAndView list(ModelAndView mv) {
		mv.addObject("messages", this.messageDao.findAll());
		mv.setViewName("msg/list");
		return mv;
	}
	
	@GetMapping("add.html")
	public ModelAndView init(ModelAndView mv) {
		mv.setViewName("msg/add");
		return mv;
	}
	
	@PostMapping("add.html")
	public ModelAndView add(@RequestParam String msg,Principal principal, ModelAndView mv) {
		String userName = principal.getName();
		
		messageDao.save(
				Message.builder()
				.value(msg)
				.user(userDao.findUserByName(userName).get())
				.build());
		
		mv.setViewName("redirect:/");
		return mv;
	}
	
}
