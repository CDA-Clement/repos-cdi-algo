package com.insy2s.xss.victime.security;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.insy2s.xss.victime.dao.UserDao;
import com.insy2s.xss.victime.entity.User;

@Service
public class CdaUserDetailsService implements UserDetailsService {
 
    @Autowired
    private final UserDao userDao;
    
    public CdaUserDetailsService(UserDao userDao) {
        this.userDao = userDao;
    }
 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	
        Objects.requireNonNull(username);
        User user = userDao.findUserByName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String dateStr=dateFormat.format(date).toString();
        System.err.println(dateStr);
        user.setConnexion(dateStr);
        userDao.save(user);
        return new CdaUserPrincipal(user);
    }
}
