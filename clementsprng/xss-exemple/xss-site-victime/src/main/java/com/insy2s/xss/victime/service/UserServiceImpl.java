package com.insy2s.xss.victime.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.insy2s.xss.victime.Dto.RoleDto;
import com.insy2s.xss.victime.Dto.UserDto;
import com.insy2s.xss.victime.dao.UserDao;
import com.insy2s.xss.victime.entity.User;
@Service
public class UserServiceImpl implements IUserService {
	@Autowired
	private UserDao userRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<UserDto> chercherTousLesUsers() {
		List<UserDto> maliste = this.userRepository.findAll()
				.stream()
				.map(e->UserDto.builder()
						.id(e.getId())
						.name(e.getName())
						.password(e.getPassword())
						.date(e.getDate())
						.role(new RoleDto(e.getRole().getId(),e.getRole().getName()))
						.statut(e.getStatut())
						.connexion(e.getConnexion())
						.build())
				.collect(Collectors.toList());
		return maliste;
		
	}

	@Override
	public Integer enregistrer(UserDto user) {
		User u = this.modelMapper.map(user,User.class);
		u = this.userRepository.save(u);
		return u.getId();
		
	}



}
