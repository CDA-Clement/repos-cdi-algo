package com.insy2s.xss.victime.service;

import java.util.List;

import com.insy2s.xss.victime.Dto.UserDto;



public interface IUserService {
	public List<UserDto> chercherTousLesUsers();

	public Integer enregistrer(UserDto userDto);

}
