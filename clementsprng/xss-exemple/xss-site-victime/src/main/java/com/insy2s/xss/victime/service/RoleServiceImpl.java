package com.insy2s.xss.victime.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.insy2s.xss.victime.Dto.RoleDto;
import com.insy2s.xss.victime.dao.RoleDao;
import com.insy2s.xss.victime.entity.Role;
@Service
public class RoleServiceImpl implements IRoleService {
	@Autowired
	private RoleDao roleRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<RoleDto> chercherTousLesRoles() {
		List<RoleDto> maliste = this.roleRepository.findAll()
				.stream()
				.map(e->RoleDto.builder()
						.id(e.getId())
						.label(e.getName())
						.build())
				.collect(Collectors.toList());
		return maliste;
		
	}

	@Override
	public Integer enregistrer(Role role) {
		Role r = this.modelMapper.map(role,Role.class);
		r = this.roleRepository.save(r);
		return r.getId();
		
	}

	@Override
	public Optional<RoleDto> chercherParId(Integer Id) {
		Optional<Role> role = this.roleRepository.findById(Id);
		Optional<RoleDto> res = Optional.empty();
		if(role.isPresent()) {
			Role r = role.get();
			RoleDto prodDto = this.modelMapper.map(r, RoleDto.class);
			
			res = Optional.of(prodDto);
		}
		return res;
	}

}
