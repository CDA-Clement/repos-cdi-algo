package com.insy2s.xss.victime.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.insy2s.xss.victime.Dto.RoleDto;
import com.insy2s.xss.victime.Dto.UserDto;
import com.insy2s.xss.victime.service.IRoleService;
import com.insy2s.xss.victime.service.IUserService;

@Controller
@RequestMapping("user")
public class UserAdminController {
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IRoleService roleService;
	
	
	
	
	@GetMapping(path = { "list.html","/"})
	public ModelAndView list(ModelAndView mv) {
		List<UserDto>liste=this.userService.chercherTousLesUsers();
		mv.addObject("users", this.userService.chercherTousLesUsers());
		mv.setViewName("user/list");
		return mv;
	}
	
	@GetMapping("add.html")
	public ModelAndView init(ModelAndView mv) {
		mv.setViewName("user/add");
		return mv;
	}
	
	@PostMapping("add.html")
	public ModelAndView add(
			@RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "id") Integer id,
			@RequestParam (value = "statut", defaultValue = "null") String choix,
			ModelAndView mv) {
		System.err.println("choix : "+choix);
		Boolean statut=true;
		if(choix.equals("null")) {
			statut=false;
		}
		
		String encoded = new BCryptPasswordEncoder().encode(password);
		
		Optional<RoleDto>roleOp=roleService.chercherParId(id);
		if(roleOp.isPresent()) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String dateStr=dateFormat.format(date).toString();
			RoleDto role=roleOp.get();
			userService.enregistrer(
					UserDto.builder()
					.name(username)
					.password(encoded)
					.statut(statut)
					.role(role)
					.date(dateStr)
					.connexion(dateStr)
					.build());
		}
		
		
		mv.setViewName("redirect:/");
		return mv;
	}
	
	@RequestMapping(value ="/modif", method = RequestMethod.GET, params = { "id" })
	public ModelAndView modif(
			@RequestParam Integer id,
			ModelAndView mv) {
		
		List<UserDto> liste=userService.chercherTousLesUsers();
		for (UserDto userDto : liste) {
			System.err.println("taille "+liste.size());
			if(userDto.getId().equals(id)) {
				System.err.println(userDto.getStatut());
				mv.addObject("user", userDto);
				break;
			}
		}
		
		
		System.err.println("passe tu ici ");
		mv.addObject("idUser", id);
		
		mv.setViewName("user/modif");
		return mv;
	}
	
	@PostMapping("modif.html")
	public ModelAndView modif(
			@RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password,
			@RequestParam(value ="idRole") Integer idRole,
			@RequestParam(value ="idUser") Integer idUser,
			@RequestParam(value="statut", defaultValue="null") String stat,
			ModelAndView mv) {
		Boolean statut=false;
		if(stat.equals("activer")) {
			statut=true;
		}
		
		if(stat.equals("null")) {
			List<UserDto>liste=userService.chercherTousLesUsers();
			for (UserDto userDto : liste) {
				if(userDto.getId().equals(idUser)) {
					statut=userDto.getStatut();
				}
			}
		}
		
		System.err.println("idUSer "+idUser);
		
		Optional<RoleDto>roleOp=roleService.chercherParId(idRole);
		System.err.println(roleOp.get().getId());
		
		
		if(roleOp.isPresent()) {
			RoleDto role=roleOp.get();
			userService.enregistrer(
					UserDto.builder()
					.id(idUser)
					.role(role)
					.statut(statut)
					.name(username)
					.password(new BCryptPasswordEncoder().encode(password))
					.build());
		}
		
		
		mv.setViewName("redirect:/");
		return mv;
	}
	
	@RequestMapping(value ="/activer", method = RequestMethod.GET, params = { "id" })
	public ModelAndView activer(
			@RequestParam Integer id,
			ModelAndView mv) {
		Boolean activer=true;
		System.err.println("ifffd "+id);
		
		List<UserDto> liste=userService.chercherTousLesUsers();
		for (UserDto userDto : liste) {
			if(userDto.getId().equals(id)) {
				System.err.println("numero role "+userDto.getRole().getId());
				
				Optional<RoleDto>roleOp=roleService.chercherParId(userDto.getRole().getId());
				
				if(roleOp.isPresent()) {
					RoleDto role=roleOp.get();
				
				userService.enregistrer(
						UserDto.builder()
						.id(userDto.getId())
						.name(userDto.getName())
						.password(userDto.getPassword())
						.statut(activer)
						.role(role)
						.build());
			}
			
		}
		}
		
	
		list(mv);
		
		
		
		
				return mv;
		
	}
	
	@RequestMapping(value ="/desactiver", method = RequestMethod.GET, params = { "id" })
	public ModelAndView desactiver(
			@RequestParam Integer id,
			ModelAndView mv) {
		System.err.println("ifffd "+id);
		Boolean activer=false;
		
		List<UserDto> liste=userService.chercherTousLesUsers();
		for (UserDto userDto : liste) {
			if(userDto.getId().equals(id)) {
				
Optional<RoleDto>roleOp=roleService.chercherParId(userDto.getRole().getId());
				
				if(roleOp.isPresent()) {
					RoleDto role=roleOp.get();
				
				userService.enregistrer(
						UserDto.builder()
						.id(userDto.getId())
						.name(userDto.getName())
						.password(userDto.getPassword())
						
						.statut(activer)
						.role(role)
						.build());
			}
		}
		}
		
	
		list(mv);
		
		
		
				return mv;
		
	}
	
	
	
	
	
	
}
