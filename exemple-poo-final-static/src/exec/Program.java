package exec;

import modele.Stagiaire;

public class Program {
	public static void main(String[] args) {

		Stagiaire stg = new Stagiaire("Maxime");
		stg.nouveauSalaire(1500);
		System.out.println(stg);

		stg.nouveauSalaire(2000);
		System.out.println(stg);

		System.out.println(stg);

		Stagiaire stg2 = new Stagiaire("Laurent");
		System.out.println(stg2);

		System.out.println("duree de stage : " + Stagiaire.DUREE_STAGE);
	}
}
