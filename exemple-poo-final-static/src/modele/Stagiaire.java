package modele;

public class Stagiaire {
	
	public static final int DUREE_STAGE = 6;
	
	private final String nom;
	private int salaire;
	
	public Stagiaire (String n) {
		nom = n;
	}
	
	public String getNom() {
		return nom;
	}
	public int getSalaire() {
		return salaire;
	}
	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	
	
	public void nouveauSalaire(int salaire) {
		this.salaire=salaire;
	}

	public String toString() {
		return "Stagiaire [nom=" + this.nom + ", salaire=" + this.salaire + "]";
	}

	
	

}
