package com.afpa.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQueries({
		@NamedQuery(name = "getMarqueByLabel", query = "select m from Marque m where UPPER(m.label)=UPPER( :labelParam)"), 
		@NamedQuery(name = "Marque.findAll", query = "select m from Marque m") 
})
@Table(name = "t_marque", uniqueConstraints = { @UniqueConstraint(columnNames = { "label" }) })
public class Marque {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;

	@Column(nullable = false)
	private String label;

	@Builder.Default
	@OneToMany(mappedBy = "marque", fetch = FetchType.LAZY)
	private Set<Modele> modeles = new HashSet<>();
}
