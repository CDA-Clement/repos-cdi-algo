package com.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="t_energie",
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = { "label" }) 
	})
@NamedQueries({
	@NamedQuery(name = "getEnergieByLabel",query = "select e from Energie e where e.label= :labelParam"),
	@NamedQuery(name = "Energie.findAll",query = "select e from Energie e")
})
public class Energie {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;
	
	@Column(nullable = false)
	private String label;
}
