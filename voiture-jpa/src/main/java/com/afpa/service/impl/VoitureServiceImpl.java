package com.afpa.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.afpa.dao.ICouleurDao;
import com.afpa.dao.IModeleDao;
import com.afpa.dao.IVoitureDao;
import com.afpa.dao.impl.CouleurDaoImpl;
import com.afpa.dao.impl.ModeleDaoImpl;
import com.afpa.dao.impl.VoitureDaoImlp;
import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementsListeDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.dto.reponse.VoitureDto;
import com.afpa.dto.requete.VoitureCreationDto;
import com.afpa.entity.Couleur;
import com.afpa.entity.Modele;
import com.afpa.entity.Voiture;
import com.afpa.service.IVoitureService;

public class VoitureServiceImpl implements IVoitureService {

	private IModeleDao modeleDao;
	private ICouleurDao couleurDao;
	private IVoitureDao voitureDao;
	
	public VoitureServiceImpl() {
		this.couleurDao = new CouleurDaoImpl();
		this.modeleDao = new ModeleDaoImpl();
		this.voitureDao = new VoitureDaoImlp();
	}
	
	@Override
	public ReponseDto creerVoiture(VoitureCreationDto voitureCreationDto) {
		Voiture voiture = this.voitureDao.getVoitureByMatricule(voitureCreationDto.getMatricule());
		if(voiture != null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette matricule existe d�j�").build();
		}
		Modele modele = this.modeleDao.getModeleByLabel(voitureCreationDto.getModele());
		if(modele == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : ce modele n'existe pas").build();
		}
		Couleur couleur = this.couleurDao.getCouleurByLabel(voitureCreationDto.getCouleur());
		if(couleur == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette couleur n'existe pas").build();
		}
		voiture = Voiture.builder()
			.couleur(couleur)
			.modele(modele)
			.matricule(voitureCreationDto.getMatricule())
			.build();
		
		voiture = this.voitureDao.add(voiture);
		
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("creation de la voiture code " + voiture.getId() + " ok!")
				.contenu(CreationReponseDto.builder().code(voiture.getId()).build())
				.build();
	}

	@Override
	public ReponseDto recupererToutesLesVoitures() {
		List<Voiture> voituresEntities = this.voitureDao.findAllByNamedQuery("Voiture.findAll");
		
		List<VoitureDto> res = voituresEntities.stream()
			.map(x->VoitureDto.builder()
					.id(x.getId())
					.matricule(x.getMatricule())
					.marque(x.getModele().getMarque().getLabel())
					.modele(x.getModele().getLabel())
					.energie(x.getModele().getEnergie().getLabel())
					.puissance(x.getModele().getPuissance())
					.couleur(x.getCouleur().getLabel())
					.build())
			.collect(Collectors.toList());
		
		return ReponseDto.builder()
				.code(Status.OK)
				.contenu(ElementsListeDto.builder().elements(res).build())
				.build();
	}

}
