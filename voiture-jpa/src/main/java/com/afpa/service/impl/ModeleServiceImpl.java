package com.afpa.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.afpa.dao.IEnergieDao;
import com.afpa.dao.IMarqueDao;
import com.afpa.dao.IModeleDao;
import com.afpa.dao.impl.EnergieDaoImpl;
import com.afpa.dao.impl.MarqueDaoImlp;
import com.afpa.dao.impl.ModeleDaoImpl;
import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementsListeDto;
import com.afpa.dto.reponse.ModeleDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.dto.requete.ModeleCreationDto;
import com.afpa.entity.Energie;
import com.afpa.entity.Marque;
import com.afpa.entity.Modele;
import com.afpa.service.IModeleService;

public class ModeleServiceImpl implements IModeleService {

	private IEnergieDao energieDao;
	private IMarqueDao marqueDao;
	private IModeleDao modeleDao;

	public ModeleServiceImpl() {
		this.energieDao = new EnergieDaoImpl();
		this.modeleDao = new ModeleDaoImpl();
		this.marqueDao = new MarqueDaoImlp();
	}

	@Override
	public ReponseDto creerModele(ModeleCreationDto creationDto) {
		Modele modele = modeleDao.getModeleByLabel(creationDto.getModele());
		if (modele != null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : ce modele existe d�j�").build();
		}
		if (creationDto.getPuissance() <= 0) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : la puissance doit �tre strictement positive")
					.build();
		}
		Energie energie = this.energieDao.getEnergieByLabel(creationDto.getEnergie());
		if (energie == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette energie n'existe pas").build();
		}
		Marque marque = this.marqueDao.getMarqueByLabel(creationDto.getMarque());
		if (marque == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette marque n'existe pas").build();
		}

		modele = Modele.builder().puissance(creationDto.getPuissance()).energie(energie).marque(marque)
				.label(creationDto.getModele()).build();

		modele = this.modeleDao.add(modele);

		return ReponseDto.builder().code(Status.OK).msg("creation de la couleur code " + modele.getCode() + " ok!")
				.contenu(CreationReponseDto.builder().code(modele.getCode()).build()).build();
	}

	@Override
	public ReponseDto chercherModeleParLabel(String modele) {
		Modele m = this.modeleDao.getModeleByLabel(modele);
		if (m == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette marque n'existe pas").build();
		}
		return ReponseDto.builder()
				.code(Status.OK).msg("modele trouv�")
				.contenu(CreationReponseDto.builder().code(m.getCode()).build())
				.build();
	}

	@Override
	public ReponseDto recupererTousLesModeles() {
		List<Modele> marquesEntities = this.modeleDao.findAllByNamedQuery("Modele.findAll");
		
		List<ModeleDto> res = marquesEntities.stream()
			.map(x->ModeleDto.builder()
					.code(x.getCode())
					.label(x.getLabel())
					.marque(x.getMarque().getLabel())
					.build())
			.collect(Collectors.toList());
		
		return ReponseDto.builder()
				.code(Status.OK)
				.contenu(ElementsListeDto.builder().elements(res).build())
				.build();
	}

}
