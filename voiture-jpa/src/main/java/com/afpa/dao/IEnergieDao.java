package com.afpa.dao;

import com.afpa.entity.Energie;

public interface IEnergieDao extends IDao<Energie> {

	Energie getEnergieByLabel(String energie);

}
