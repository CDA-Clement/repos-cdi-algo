package com.afpa.dao;

import com.afpa.entity.Couleur;

public interface ICouleurDao extends IDao<Couleur> {

	Couleur getCouleurByLabel(String couleur);

}
