package com.afpa.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.afpa.dao.IMarqueDao;
import com.afpa.entity.Marque;

public class MarqueDaoImlp extends AbstractDao<Marque> implements IMarqueDao {

	@Override
	public Marque getMarqueByLabel(String marque) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Marque> q = em.createNamedQuery("getMarqueByLabel", Marque.class);
			q.setParameter("labelParam", marque);
			Marque m = q.getSingleResult();
			m.getModeles().size();
			return m;
		}catch(NoResultException e) {
			return null;
		} finally {
			closeEntityManager(em);
		}
	}

}
