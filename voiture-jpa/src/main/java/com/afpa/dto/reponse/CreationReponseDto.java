package com.afpa.dto.reponse;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class CreationReponseDto implements IContenuDto {
	int code;
}
