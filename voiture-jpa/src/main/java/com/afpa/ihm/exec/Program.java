package com.afpa.ihm.exec;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.afpa.dto.reponse.ElementsListeDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.dto.requete.ModeleCreationDto;
import com.afpa.dto.requete.VoitureCreationDto;
import com.afpa.ihm.MenuAction;
import com.afpa.ihm.MenuArretSignal;
import com.afpa.ihm.MenuInOut;
import com.afpa.service.ICouleurService;
import com.afpa.service.IEnergieService;
import com.afpa.service.IMarqueService;
import com.afpa.service.IModeleService;
import com.afpa.service.IVoitureService;
import com.afpa.service.impl.CouleurServiceImpl;
import com.afpa.service.impl.EnergieServiceImpl;
import com.afpa.service.impl.MarqueServiceImpl;
import com.afpa.service.impl.ModeleServiceImpl;
import com.afpa.service.impl.VoitureServiceImpl;

public class Program {
	public static void main(String[] args) throws Exception {
		exec(System.in, System.out, System.out);
	}

	public static void exec(InputStream in, OutputStream outMenu, OutputStream outStatus) throws Exception {
		final MenuInOut menu = new MenuInOut(in, outMenu, outStatus);

		menu.ecrire("Bienvenu dans l'appli gestion de voitures ! ");

		final MenuArretSignal programEtat = new MenuArretSignal();

		final IMarqueService marqueService = new MarqueServiceImpl();
		final ICouleurService couleurService = new CouleurServiceImpl();
		final IModeleService modeleService = new ModeleServiceImpl();
		final IEnergieService energieService = new EnergieServiceImpl();
		final IVoitureService voitureService = new VoitureServiceImpl();

		final TreeSet<MenuAction> actionsSet = new TreeSet<>();
		actionsSet.addAll(Arrays.asList(new MenuAction(0, "arr�ter", () -> arreterProgramme(menu, programEtat)),
				new MenuAction(1, "ajouter une couleur", () -> ajoutCouleur(menu, couleurService)),
				new MenuAction(2, "ajouter une energie", () -> ajoutEnergie(menu, energieService)),
				new MenuAction(3, "ajouter une marque", () -> ajoutMarque(menu, marqueService)),
				new MenuAction(4, "ajouter un modele", () -> ajoutModele(menu, modeleService)),
				new MenuAction(5, "ajouter une voiture", () -> ajoutVoiture(menu, voitureService)),
				
				new MenuAction(6, "lister les couleurs", () -> listerLesCouleurs(menu, couleurService)),
				new MenuAction(7, "lister les marques", () -> listerLesMarques(menu, marqueService)),
				new MenuAction(8, "lister les energies", () -> listerLesEnergies(menu, energieService)),
				new MenuAction(9, "lister les modeles", () -> listerLesModeles(menu, modeleService)),
				new MenuAction(10, "lister les voiture", () -> listerLesVoitures(menu, voitureService)),
				
				new MenuAction(11, "mettre � jour une couleur", () -> mettreAJourCouleur(menu, couleurService)),
				new MenuAction(12, "supprimer une couleur", () -> supprimerCouleur(menu, couleurService)),
				
				new MenuAction(13, "mettre � jour une energie", () -> mettreAJourEnergie(menu, energieService)),
				new MenuAction(14, "supprimer une energie", () -> supprimerEnergie(menu, energieService))
		));

		Map<Integer, MenuAction> actionsMap = actionsSet.stream()
				.collect(Collectors.toMap(MenuAction::getId, Function.identity()));

		final MenuAction choixIncorrectAction = new MenuAction(-1, "", () -> menu.ecrire("choix incorrect"));

		int choix = 0;

		while (programEtat.isContinuer()) {
			menu.ecrire("choisir une action");
			actionsSet.forEach(a -> menu.ecrire(a.getId() + "-" + a.getDesc()));

			menu.ecrire(" > ", false);
			choix = menu.lireEntier(true, "saisir une valeur num�rique entre 1 et 7\n > ", false);

			actionsMap.getOrDefault(choix, choixIncorrectAction).run();

			menu.ecrire("");
		}
	}

	private static void supprimerCouleur(MenuInOut menu, ICouleurService couleurService) {
		menu.ecrire("saisir le nom de la couleur :");

		menu.ecrire(" > ", false);
		String couleur = menu.lireString();

		ReponseDto res = couleurService.supprimerCouleur(couleur);
		menu.ecrire(res.getMsg());
	}

	private static void mettreAJourCouleur(MenuInOut menu, ICouleurService couleurService) {
		menu.ecrire("saisir la couleur � modifier :");
		menu.ecrire(" > ", false);
		String couleur = menu.lireString();
		
		ReponseDto res = couleurService.chercherCouleurParLabel(couleur);
		if(res.getCode() == Status.KO) {
			menu.ecrire(res.getMsg());
		} else {
			menu.ecrire("saisir le nouveau label de la couleur :");
			menu.ecrire(" > ", false);
			String nouveauLabel = menu.lireString();
			res = couleurService.mettreAjour(couleur,nouveauLabel);
			menu.ecrire(res.getMsg());
		}

	}
	
	
	private static void mettreAJourEnergie(MenuInOut menu, IEnergieService energieService) {
		menu.ecrire("saisir l'energie � modifier :");
		menu.ecrire(" > ", false);
		String energie = menu.lireString();
		
		ReponseDto res = energieService.chercherEnergieParLabel(energie);
		if(res.getCode() == Status.KO) {
			menu.ecrire(res.getMsg());
		} else {
			menu.ecrire("saisir le nouveau label de la couleur :");
			menu.ecrire(" > ", false);
			String nouveauLabel = menu.lireString();
			res = energieService.MiseAjourEnergie(energie,nouveauLabel);
			menu.ecrire(res.getMsg());
		}

	}
	
	private static void supprimerEnergie(MenuInOut menu, IEnergieService couleurService) {
		menu.ecrire("saisir le nom de l'energie :");

		menu.ecrire(" > ", false);
		String energie = menu.lireString();

		ReponseDto res = couleurService.supprimerEnergie(energie);
		menu.ecrire(res.getMsg());
	}

	private static void listerLesVoitures(MenuInOut menu, IVoitureService voitureService) {
		ReponseDto res = voitureService.recupererToutesLesVoitures();

		menu.ecrire("La liste des voitures : ");
		ElementsListeDto esl = (ElementsListeDto) res.getContenu();

		esl.getElements().stream().map(x -> " -" + x).forEach(menu::ecrire);
	}

	private static void listerLesModeles(MenuInOut menu, IModeleService modeleService) {
		ReponseDto res = modeleService.recupererTousLesModeles();

		menu.ecrire("La liste des mod�les : ");
		ElementsListeDto esl = (ElementsListeDto) res.getContenu();

		esl.getElements().stream().map(x -> " -" + x).forEach(menu::ecrire);
	}

	private static void listerLesEnergies(MenuInOut menu, IEnergieService energieService) {
		ReponseDto res = energieService.recupererToutesLesEnergies();

		menu.ecrire("La liste des energies : ");
		ElementsListeDto esl = (ElementsListeDto) res.getContenu();

		esl.getElements().stream().map(x -> " -" + x).forEach(menu::ecrire);
	}

	private static void listerLesMarques(MenuInOut menu, IMarqueService marqueService) {
		ReponseDto res = marqueService.recupererToutesLesMarques();

		menu.ecrire("La liste des marques : ");
		ElementsListeDto esl = (ElementsListeDto) res.getContenu();

		esl.getElements().stream().map(x -> " -" + x).forEach(menu::ecrire);
	}

	private static void listerLesCouleurs(MenuInOut menu, ICouleurService couleurService) {
		ReponseDto res = couleurService.recupererToutesLesCouleurs();

		menu.ecrire("La liste des couleurs : ");
		ElementsListeDto esl = (ElementsListeDto) res.getContenu();

		esl.getElements().stream().map(x -> " -" + x).forEach(menu::ecrire);
	}

	private static void ajoutVoiture(MenuInOut menu, IVoitureService voitureService) {
		menu.ecrire("saisir le nom du modele :");
		menu.ecrire(" > ", false);
		String modele = menu.lireString();

		menu.ecrire("saisir le nom d'une couleur :");
		menu.ecrire(" > ", false);
		String couleur = menu.lireString();

		menu.ecrire("saisir le metricule :");
		menu.ecrire(" > ", false);
		String matricule = menu.lireString();

		ReponseDto res = voitureService.creerVoiture(
				VoitureCreationDto.builder().modele(modele).couleur(couleur).matricule(matricule).build());
		menu.ecrire(res.getMsg());
	}

	private static void ajoutModele(MenuInOut menu, IModeleService modeleService) {
		menu.ecrire("saisir le nom du modele :");
		menu.ecrire(" > ", false);
		String modele = menu.lireString();

		menu.ecrire("saisir le nom de la marque :");
		menu.ecrire(" > ", false);
		String marque = menu.lireString();

		menu.ecrire("saisir le nom l'energie du mod�le :");
		menu.ecrire(" > ", false);
		String energie = menu.lireString();

		menu.ecrire("saisir la puissance du modele :");
		menu.ecrire(" > ", false);
		int puissance = menu.lireEntier();

		ReponseDto res = modeleService.creerModele(ModeleCreationDto.builder().modele(modele).marque(marque)
				.puissance(puissance).energie(energie).build());
		menu.ecrire(res.getMsg());
	}

	private static void ajoutEnergie(MenuInOut menu, IEnergieService energieService) {
		menu.ecrire("saisir le nom de l'energie :");

		menu.ecrire(" > ", false);
		String energie = menu.lireString();

		ReponseDto res = energieService.creerEnergie(energie);
		menu.ecrire(res.getMsg());
	}

	private static void ajoutCouleur(MenuInOut menu, ICouleurService couleurService) {
		menu.ecrire("saisir le nom de la couleur :");

		menu.ecrire(" > ", false);
		String marque = menu.lireString();

		ReponseDto res = couleurService.creerCouleur(marque);
		menu.ecrire(res.getMsg());
	}

	private static void arreterProgramme(MenuInOut menu, MenuArretSignal programEtat) {
		menu.ecrire("saisir le nom de la marque :");
		programEtat.stop();
	}

	private static void ajoutMarque(MenuInOut menu, IMarqueService marqueService) {
		menu.ecrire("saisir le nom de la marque :");

		menu.ecrire(" > ", false);
		String marque = menu.lireString();

		ReponseDto res = marqueService.creerMarque(marque);
		menu.ecrire(res.getMsg());
	}
}
