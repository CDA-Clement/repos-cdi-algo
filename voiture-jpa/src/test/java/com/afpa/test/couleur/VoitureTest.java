package com.afpa.test.couleur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementSimpleDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.dto.requete.ModeleCreationDto;
import com.afpa.dto.requete.VoitureCreationDto;
import com.afpa.service.ICouleurService;
import com.afpa.service.IEnergieService;
import com.afpa.service.IMarqueService;
import com.afpa.service.IModeleService;
import com.afpa.service.IVoitureService;
import com.afpa.service.impl.CouleurServiceImpl;
import com.afpa.service.impl.EnergieServiceImpl;
import com.afpa.service.impl.MarqueServiceImpl;
import com.afpa.service.impl.ModeleServiceImpl;
import com.afpa.service.impl.VoitureServiceImpl;

@TestMethodOrder(OrderAnnotation.class)
public class VoitureTest {

	static IModeleService modeleService;
	static IMarqueService marqueService;
	static ICouleurService couleurService;
	static IVoitureService voitureService;
	static IEnergieService energieService;

	static final String MARQUE_BMW_LABEL = "bmw";
	static int MARQUE_BMW_CODE;
	
	static final String MODELE_M6_LABEL = "M6";
	static int MODELE_M6_CODE;

	static final String COULEUR_NOIR_LABEL = "noir";
	static int COULEUR_NOIR_CODE;
	
	static final String ENERGIE_DIESEL_LABEL = "diesel";
	static int ENERGIE_DIESEL_CODE;
	
	static final String MATRICULE = "AN123YY";
	static final int PUISSANCE = 270;

	@BeforeAll
	public static void init() {
		couleurService = new CouleurServiceImpl();
		modeleService = new ModeleServiceImpl();
		marqueService = new MarqueServiceImpl();
		energieService = new EnergieServiceImpl();
		voitureService = new VoitureServiceImpl();
		{
			ReponseDto couleurReponse = couleurService.chercherCouleurParLabel(COULEUR_NOIR_LABEL);
			if (couleurReponse.getCode() != Status.OK) {
				couleurReponse = couleurService.creerCouleur(COULEUR_NOIR_LABEL);
			}
			assertEquals(couleurReponse.getCode(), Status.OK);
			assertNotNull(couleurReponse.getContenu());
			if (couleurReponse.getContenu() instanceof CreationReponseDto) {
				COULEUR_NOIR_CODE = ((CreationReponseDto) couleurReponse.getContenu()).getCode();
			} else {
				COULEUR_NOIR_CODE = ((ElementSimpleDto) couleurReponse.getContenu()).getCode();
			}
		}
		{
			ReponseDto reponseEnergie = energieService.chercherEnergieParLabel(ENERGIE_DIESEL_LABEL);
			if (reponseEnergie.getCode() != Status.OK) {
				reponseEnergie = energieService.creerEnergie(ENERGIE_DIESEL_LABEL);
			}
			assertEquals(reponseEnergie.getCode(), Status.OK);
			assertNotNull(reponseEnergie.getContenu());
			if (reponseEnergie.getContenu() instanceof CreationReponseDto) {
				ENERGIE_DIESEL_CODE = ((CreationReponseDto) reponseEnergie.getContenu()).getCode();
			} else {
				ENERGIE_DIESEL_CODE = ((ElementSimpleDto) reponseEnergie.getContenu()).getCode();
			}
		}
		{
			ReponseDto reponseMarque = marqueService.chercherMarqueParLabel(MARQUE_BMW_LABEL);
			if (reponseMarque.getCode() != Status.OK) {
				reponseMarque = marqueService.creerMarque(MARQUE_BMW_LABEL);
			}
			assertEquals(reponseMarque.getCode(), Status.OK);
			assertNotNull(reponseMarque.getContenu());
			if (reponseMarque.getContenu() instanceof CreationReponseDto) {
				MARQUE_BMW_CODE = ((CreationReponseDto) reponseMarque.getContenu()).getCode();
			} else {
				MARQUE_BMW_CODE = ((ElementSimpleDto) reponseMarque.getContenu()).getCode();
			}
		}
		{
			ReponseDto modeleReponse = modeleService.chercherModeleParLabel(MODELE_M6_LABEL);
			if (modeleReponse.getCode() != Status.OK) {
				modeleReponse = modeleService.creerModele(
						ModeleCreationDto.builder()
						.energie(ENERGIE_DIESEL_LABEL)
						.marque(MARQUE_BMW_LABEL)
						.modele(MODELE_M6_LABEL)
						.puissance(PUISSANCE)
						.build());
			}
			assertEquals(modeleReponse.getCode(), Status.OK);
			assertNotNull(modeleReponse.getContenu());
			if (modeleReponse.getContenu() instanceof CreationReponseDto) {
				MODELE_M6_CODE = ((CreationReponseDto) modeleReponse.getContenu()).getCode();
			} else {
				MODELE_M6_CODE = ((ElementSimpleDto) modeleReponse.getContenu()).getCode();
			}
		}

	}

	static int idVoiture;
	
	@Test
	@Order(1)
	void creation_modele_existe_couleur_existe() {
		ReponseDto reponse = voitureService.creerVoiture(
				VoitureCreationDto.builder()
				.modele(MODELE_M6_LABEL)
				.couleur(COULEUR_NOIR_LABEL)
				.matricule("MAT"+System.currentTimeMillis())
				.build());
		assertNotNull(reponse);
		assertEquals(reponse.getCode(), Status.OK);
		assertNotNull(reponse.getContenu());
		idVoiture = ((CreationReponseDto) reponse.getContenu()).getCode();
	}

}
