package examen;

import java.util.ArrayList;
import java.util.List;

//enum MentionExam {
//	N("non admis"), P("passable"), AB("assez bien"), B("bien"), TB("tres bien");
//	
//	private String valeur;
//	
//	private MentionExam(String value) {
//		this.valeur=value;
//	}
//}

public class Eleve {
	private String nom;
	private double moyenne;
	private Mention mentionEleve;
	
	public Eleve(String nomEleve,double a, double b, double c) {
		this.nom=nomEleve;
		this.moyenne=(a+b+c)/3;
	}
	
	public static void mention(Eleve e) {
		if(e.moyenne<10) {
			e.mentionEleve=Mention.N;
		}
		if(e.moyenne>=10 && e.moyenne<12) {
			e.mentionEleve=Mention.P;
		}
		if(e.moyenne>=12 && e.moyenne<14) {
			e.mentionEleve=Mention.AB;
		}
		if(e.moyenne>=14 && e.moyenne<16) {
			e.mentionEleve=Mention.B;
		}
		if(e.moyenne>=16) {
			e.mentionEleve=Mention.TB;
		}
		
	}
	

	@Override
	public String toString() {
		return "Eleve [nom=" + nom + ", moyenne=" + moyenne + ", mentionEleve=" + mentionEleve.getValeur() + "]";
	}
}




