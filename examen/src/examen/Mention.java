package examen;

public enum Mention {
	N("non admis"), P("passable"), AB("assez bien"), B("bien"), TB("tres bien");
	
	private String valeur;
	
	private Mention(String value) {
		this.valeur=value;
	}

	public String getValeur() {
		return valeur;
	}

	public void setValeur(String valeur) {
		this.valeur = valeur;
	}
	
	
}
