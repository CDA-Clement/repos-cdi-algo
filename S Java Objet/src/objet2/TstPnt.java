package objet2;

public class TstPnt	{
	public static void main (String args[])	{
		Point a ;
		a = new Point(3, 5) ;
		
		System.out.println("abscisse " + a.getAbscisse()+ " Ordonne "+a.getOrdonne());
		
		a.deplace(2, 0) ;
		System.out.println("abscisse " + a.getAbscisse()+ " Ordonne "+a.getOrdonne());
		
		Point b = new Point(6, 8) ;
		System.out.println("abscisse " + b.getAbscisse()+ " Ordonne "+b.getOrdonne());
	}
}

