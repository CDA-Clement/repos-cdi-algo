package objet2;

public class Point {
	public Point (int abs, int ord) {
		x = abs ;
		y = ord ;
	}
	
	public void deplace (int dx, int dy) {
		x += dx ; y += dy ;
	}
	
	public double getAbscisse() {
		return x;
	}


	public double getOrdonne() {
		return y;
	}

	private double x ; // abscisse
	private double y ; // ordonnee
}



