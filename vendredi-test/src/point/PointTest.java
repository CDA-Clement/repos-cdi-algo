package point;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

class PointTest {
	private int x, y;

	public PointTest(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void affiche() {
		System.out.print(" [ " + x + ", " + y + "] ");
	}

	public static void traiteListe(List<PointTest> lp, Predicate<PointTest> pred, Comparator<PointTest> comp,
			Consumer<PointTest> cons) {
//        List<Point> lptmp = new ArrayList<Point>();
//        for(Point p : lp) {
//            if(pred.test(p)) {
//                lptmp.add(p);
//            }
//        }
//        lptmp.sort(comp);
//        lptmp.forEach(cons);

		lp.stream().filter(pred).sorted(comp).forEach(cons);

	}

	public static void main(String[] args) {
		List<PointTest> points = new ArrayList<PointTest>();
		points.add(new PointTest(2, 5));
		points.add(new PointTest(7, 3));
		points.add(new PointTest(6, -3));

		traiteListe(points, x -> x.getX() > 0, (a, b) -> Integer.valueOf(a.x).compareTo(b.x), PointTest::affiche);
	}

}