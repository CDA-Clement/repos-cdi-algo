package com.zoo.dto;



import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class AnimalDto {

	private Integer id;
	private String appelation;
	private int enclos;
}
