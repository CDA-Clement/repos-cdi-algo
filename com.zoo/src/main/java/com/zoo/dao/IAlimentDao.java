package com.zoo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.zoo.entity.Aliment;
import com.zoo.entity.Responsable;

@Repository
public interface IAlimentDao extends CrudRepository<Aliment,Integer> {
	
	List<Aliment> findByNomOrderByNom(String nom);
	List<Aliment> findAllByOrderByNomDesc();

}
