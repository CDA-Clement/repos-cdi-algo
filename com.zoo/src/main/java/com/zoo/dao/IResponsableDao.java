package com.zoo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.zoo.entity.Responsable;


@Repository
public interface IResponsableDao extends CrudRepository<Responsable,Integer> {

	List<Responsable> findByNomOrderByNom(String nom);

	List<Responsable> findAllByOrderByNomDesc();

	

}
	
	
