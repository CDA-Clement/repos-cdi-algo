package com.zoo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.zoo.entity.Animal;


@Repository
public interface IAnimalDao extends CrudRepository<Animal,Integer> {
	List<Animal> findByRaceOrderByRace(String nom);

	List<Animal> findAllByOrderByRaceDesc();

}
