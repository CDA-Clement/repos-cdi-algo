package com.zoo.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(of= {"matricule","nom"})
@Table(name = "t_responsable")
@NamedQueries({ @NamedQuery(name = "listeResponsable", query = "SELECT r FROM Responsable r") })


public class Responsable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int matricule;


	private String nom;

	
	
 
//	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "responsable")
//	private Enclos enclos;
	
	

}
