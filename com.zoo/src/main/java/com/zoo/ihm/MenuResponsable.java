package com.zoo.ihm;

import java.util.Collection;

import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.zoo.dto.ResponsableDto;
import com.zoo.service.IResponsableService;



public class MenuResponsable {
	
	
	public static void menu() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();

		IResponsableService responsableService = ctx.getBean(IResponsableService.class);

		boolean vrai = true;

		while(vrai) {
			System.out.println("");
			System.out.println("------------ Menu Responsable------------");
			System.out.println("");
			System.out.println("1: ajouter une Responsable");
			System.out.println("2: afficher tout les Responsable");
			System.out.println("3: afficher une responsable par son nom");
			System.out.println("4: metre a jour une responsable");
			System.out.println("5: supprimer une responsable");
			System.out.println("6: afficher aliments gérés responsable");
			System.out.println("7: fermer menu dus responsable");
			
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();

			switch (choix) {
			case "1":
				System.out.println("ajouter une responsable");
				System.out.println("entré un label pour  responsable");
				String label = sc.next();
				
				responsableService.creer(label);
				
				
				break;

			case "2":
				System.out.println("afficher toute les responsable");
				
				Collection<ResponsableDto> listresponsabledto =responsableService.lister();
				for (ResponsableDto responsableDto : listresponsabledto) {
					System.err.println(responsableDto);
				}
				break;

			case "3":
				System.out.println("afficher une responsable par son nom");
				System.out.println("entrer le nom du  responsable a afficher");
				String n=sc.next();
				System.out.println(responsableService.responsableParNom(n));
				break;

			case "4":
				System.out.println("metre a jour un responsable");
				System.out.println("entrer nom du  responsable a mettre a jour");
				String oldName = sc.next();
				System.out.println("entrer  nouvel responsable string labele");
				String newName = sc.next();
				responsableService.miseAjourNom(oldName, newName);
				break;
				
			case "5":
				System.out.println("Supprimer une responsable");
				System.out.println("entrer nom du  responsable a supprimer");
				String nom = sc.next();
				System.out.println("entrer id du  responsable a supprimer");
				Integer id = sc.nextInt();
				responsableService.supprimer(nom, id);
				break;
//			
//			case "6":
//				System.out.println("afficher aliments gérés");
//				System.out.println("entrer id du  responsable a afficher");
//				id2 = sc.nextInt();
//				ms.AfficherAliment(id2);
//				System.err.println(ms.AfficherAliment(id2));
//				
//				
//				break;
				
			case "7":
				System.out.println("fin du programme");
				vrai = false;
				break;

			default:
				System.out.println("le choix entrer n'existe pas");
				break;
			}
		}
	}

}
