package com.zoo.ihm;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.zoo.dto.AnimalDto;


import com.zoo.service.IAnimalService;

public class MenuAnimal {

	
	
	
	public static void menu() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();
		
		IAnimalService animalService = ctx.getBean(IAnimalService.class);


		boolean vrai = true;

		while(vrai) {
			System.out.println("");
			System.out.println("------------ Menu Animal------------");
			System.out.println("");
			System.out.println("1: ajouter une Animal");
			System.out.println("2: afficher tout les Animal");
			System.out.println("3: afficher une animal par son ID");
			System.out.println("4: metre a jour une animal");
			System.out.println("5: supprimer une animal");
			System.out.println("6: fermer menu dus animal");
			
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();

			switch (choix) {
			case "1":
				System.out.println("aouter une animal");
				System.out.println("entré un label pour  animal");
				String label = sc.next();
				 animalService.creer(label);
				
				break;

			case "2":
				System.out.println("afficher tous les animaux");
				
				Collection<AnimalDto> listanimaldto =animalService.lister();
				for (AnimalDto animalDto : listanimaldto) {
					System.err.println(animalDto);
				}
				break;

			case "3":
				System.out.println("afficher une animal par son ID");
				System.out.println("entré id du  animal a afficher");
				String nom = sc.next();
				System.out.println(animalService.animalParNom(nom));
				break;

			case "4":
				System.out.println("metre a jour une animal");
				System.out.println("entrer id du  animal a mettre a jour");
				String oldName = sc.next();
				System.out.println("entrer  nouvel animal string labele");
				String newName = sc.next();
				animalService.miseAjourNom(oldName, newName);
				break;
				
			case "5":
				System.out.println("Supprimer une animal");
				System.out.println("entrer nom animal a supprimer");
				 nom = sc.next();
				System.out.println("entrer ID animal a supprimer");
				
				int id2 = sc.nextInt();
				animalService.supprimer(nom,id2);
				break;
				
			case "6":
				System.out.println("fin du programme");
				vrai = false;
				break;

			default:
				System.out.println("le choix entrer n'existe pas");
				break;
			}
		}
	}

}
