package com.zoo.ihm;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;

import com.zoo.dto.AlimentDto;
import com.zoo.service.IAlimentService;


public class MenuAliment {
	
	
	public static void menu() {
		
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();
		
		IAlimentService alimentService = ctx.getBean(IAlimentService.class);

		boolean vrai = true;

		while(vrai) {
			System.out.println("");
			System.out.println("------------ Menu Aliment------------");
			System.out.println("");
			System.out.println("1: ajouter un Aliment");
			System.out.println("2: afficher tous les Aliments");
			System.out.println("3: afficher une aliment par son ID");
			System.out.println("4: metre a jour une aliment");
			System.out.println("5: supprimer une aliment");
			System.out.println("6: fermer menu dus aliment");
			
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();

			switch (choix) {
			case "1":
				System.out.println("ajouter une aliment");
				System.out.println("entrer un nom pour  aliment");
				String label = sc.next();
				 alimentService.creer(label);
				
				break;

			case "2":
				System.out.println("afficher tous les aliments");
				
				Collection<AlimentDto> listalimentdto =alimentService.lister();
				for (AlimentDto alimentDto : listalimentdto) {
					System.err.println(alimentDto);
				}
				break;

			case "3":
				System.out.println("afficher un aliment par son nom");
				System.out.println("entrer nom de l'aliment a afficher");
				String nom = sc.next();
				System.out.println(alimentService.alimentParNom(nom));
				break;

			case "4":
				System.out.println("mettre a jour un aliment");
				System.out.println("entrer nom de l'aliment a mettre a jour");
				String oldName = sc.next();
				System.out.println("entrer le nouveau nom de l'aliment");
				String newName = sc.next();
				alimentService.miseAjourNom(oldName, newName);
				break;
				
			case "5":
				System.out.println("Supprimer un aliment");
				System.out.println("entrer nom de l'aliment a supprimer");
				nom = sc.next();
				alimentService.supprimer(nom);
				break;
				
			case "6":
				System.out.println("fin du programme");
				vrai = false;
				break;

			default:
				System.out.println("le choix entrer n'existe pas");
				break;
			}
		}
	}

}
