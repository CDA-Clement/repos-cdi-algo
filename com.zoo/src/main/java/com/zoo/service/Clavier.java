package com.zoo.service;

import java.util.Scanner;

public class Clavier {
	public static int lireEntier(Scanner clavier) {
		try {
			int res = clavier.nextInt();
			clavier.nextLine();
			return res;
		} catch (Exception e) {
			clavier.nextLine();
			return -1;
		}
	}

	public static double lireDouble(Scanner clavier) {
		try {
			double res = clavier.nextDouble();
			clavier.nextLine();
			return res;
		} catch (Exception e) {
			clavier.nextLine();
			return -1;
		}
	}

	public static String lireTxt(Scanner clavier) {
		String res = clavier.next();
		clavier.nextLine();
		return res;
	}
}
