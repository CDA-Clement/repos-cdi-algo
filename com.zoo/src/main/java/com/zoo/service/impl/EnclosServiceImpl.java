package com.zoo.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoo.dao.IAnimalDao;
import com.zoo.dao.IEnclosDao;
import com.zoo.dao.IResponsableDao;
import com.zoo.dto.AnimalDto;
import com.zoo.dto.AnimalDto.AnimalDtoBuilder;
import com.zoo.dto.EnclosDto;
import com.zoo.entity.Animal;
import com.zoo.entity.Enclos;
import com.zoo.entity.Responsable;
import com.zoo.error.Erreur;
import com.zoo.service.IEnclosService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EnclosServiceImpl implements IEnclosService {

	@Autowired
	IEnclosDao enclosDao;
	@Autowired
	IResponsableDao responsableDao;
	@Autowired
	IAnimalDao animalDao;

	@Override
	public EnclosDto creer(String appelation) {
		List<Enclos> encloss = this.enclosDao.findByNomOrderByNom(appelation);
		if (encloss != null && encloss.size() != 0) {
			log.error("des encloss ont deja ce appelation : " + encloss);
			return null;
		}
		Enclos enclos = Enclos.builder().nom(appelation).build();

		enclos = this.enclosDao.save(enclos);

		return EnclosDto.builder().id(enclos.getId()).build();
	}

	@Override
	public List<EnclosDto> lister() {
		Iterator<Enclos> iterator = this.enclosDao.findAllByOrderByNomDesc().iterator();
		List<EnclosDto> lst = new ArrayList<>();
		while (iterator.hasNext()) {
			Enclos x = iterator.next();
			if (x.getResponsable() == null) {
				lst.add(EnclosDto.builder().id(x.getId()).appelation(x.getNom()).build());
			} else {
				lst.add(EnclosDto.builder().id(x.getId()).appelation(x.getNom())
						.Responsable(x.getResponsable().getNom()).build());
			}
		}
		return lst;
	}

	@Override
	public EnclosDto enclosParNom(String n) {
		Iterator<Enclos> iterator = this.enclosDao.findByNomOrderByNom(n).iterator();
		EnclosDto res = null;
		if (iterator.hasNext()) {
			Enclos x = iterator.next();
			res = EnclosDto.builder().id(x.getId()).appelation(x.getNom()).build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Enclos> iterator = this.enclosDao.findByNomOrderByNom(oldName).iterator();
		EnclosDto res = null;
		if (iterator.hasNext()) {
			Enclos x = iterator.next();
			x.setNom(newName);
			this.enclosDao.save(x);
		}
	}

	@Override
	public Erreur supprimer(String nom, Integer id) {
		
		List<Enclos> encloss = this.enclosDao.findByNomOrderByNom(nom);
		Erreur e=new Erreur();
		if (encloss == null) {
			e.setNom("aucun enclos existe avec ce nom: " + nom);
			log.error("aucun enclos existe avec ce nom: " + nom);
			return e;

		}
		for (Enclos enclos : encloss) {
			if (enclos.getId() == id) {
				this.enclosDao.delete(enclos);
				e.setNom("le enclos : " + nom + " id : " + id + " a bien ete supprime");
				return e;
				
			}

		}
		return e;

	}


	@Override
	public Erreur ajoutResponsableEnclos(int idEnclos, int idResp) {
		Erreur e=null;
		Enclos enclos1 = null;
		Responsable responsable1 = null;

		Optional<Responsable> responsable = this.responsableDao.findById(idResp);
		if (responsable.isPresent()) {
			responsable1 = responsable.get();
		} else {
			e=new Erreur();
			e.setNom("Pas de responsable qui a cet id");
			log.error("Pas de responsable qui a cet id");
			
			return e;
			
		}
		Optional<Enclos> enclos = this.enclosDao.findById(idEnclos);
		if (enclos.isPresent()) {
			enclos1 = enclos.get();
		} else {
			e=new Erreur();
			e.setNom("Pas d'enclos qui a cet id");
			log.error("Pas d'enclos qui a cet id");
			return e;
		}

		if (responsable1 != null && enclos1 != null) {
			enclos1.setResponsable(responsable1);
			this.enclosDao.save(enclos1);

		}
		return e;

	}

	@Override
	public Erreur ajoutAnimalEnclos(int idEnclos, int idAnimal) {
		Enclos enclos1 = null;
		Animal animal1 = null;
		Erreur e=null;

		Optional<Enclos> enclos = this.enclosDao.findById(idEnclos);
		if (enclos.isPresent()) {
			enclos1 = enclos.get();
		} else {
			e=new Erreur();
			e.setNom("Pas d'enclos qui a cet id");
			return e;
			
		}

		Optional<Animal> animal = this.animalDao.findById(idAnimal);
		if (animal.isPresent()) {
			animal1 = animal.get();
		} else {
			e=new Erreur();
			e.setNom("Pas d'animal qui a cet id");
			return e;
		}

		if (animal1 != null && enclos1 != null) {
			List<Animal> animals = Arrays.asList(animal1);
			enclos1.setAnimals(new HashSet<Animal>(animals));
//			Set<Animal> animals=new HashSet<Animal>();
			// animals.add(animal1);
			animal1.setEnclos(enclos1);
			// enclos1.setAnimals(animals);
			this.enclosDao.save(enclos1);
			this.animalDao.save(animal1);

		}
		return e;

	}

	@Override
	public List<AnimalDto> listerAnimalEnclos(int idEnclos) {
		Iterator<Animal> iterator = this.animalDao.findAll().iterator();
		List<AnimalDto> lst = new ArrayList<>();
		while (iterator.hasNext()) {
			Animal x = iterator.next();
			AnimalDtoBuilder animalDtoBuilder = AnimalDto.builder().id(x.getId()).appelation(x.getRace());
			if (x.getEnclos() != null) {
				animalDtoBuilder.enclos(x.getEnclos().getId());
			}
			lst.add(animalDtoBuilder.build());
		}
		return lst;

	}

}
