package com.zoo.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoo.dao.IAnimalDao;
import com.zoo.dto.AnimalDto;
import com.zoo.entity.Animal;
import com.zoo.service.IAnimalService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AnimalServiceImpl implements IAnimalService {
	
	@Autowired
	IAnimalDao animalDao;
	
	@Override
	public AnimalDto creer(String appelation) {
		List<Animal> animals = this.animalDao.findByRaceOrderByRace(appelation);
		if(animals != null && animals.size() != 0) {
			log.error("des animals ont deja ce appelation : " + animals);
			return null;
		}
		Animal animal = Animal.builder().race(appelation).build();
		
		animal = this.animalDao.save(animal);
		
		return AnimalDto.builder().id(animal.getId()).build();
	}

	@Override
	public List<AnimalDto> lister() {
		Iterator<Animal> iterator = this.animalDao.findAllByOrderByRaceDesc().iterator();
		List<AnimalDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Animal x = iterator.next();
			if(x.getEnclos()==null) {
				
				lst.add(AnimalDto.builder()
						.id(x.getId())
						.appelation(x.getRace())
						.build());
			}else {
				lst.add(AnimalDto.builder()
						.id(x.getId())
						.appelation(x.getRace())
						.enclos(x.getEnclos().getId())
						.build());
			}
		}
		return lst;
	}

	@Override
	public AnimalDto animalParNom(String n) {
		Iterator<Animal> iterator = this.animalDao.findByRaceOrderByRace(n).iterator();
		AnimalDto res = null;
		if(iterator.hasNext()) {
			Animal x = iterator.next();
			res = AnimalDto.builder()
					.id(x.getId())
					.appelation(x.getRace())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Animal> iterator = this.animalDao.findByRaceOrderByRace(oldName).iterator();
		AnimalDto res = null;
		if(iterator.hasNext()) {
			Animal x = iterator.next();
			x.setRace(newName);
			this.animalDao.save(x);
		}
	}

	@Override
	public void supprimer(String nom, Integer id) {
		List<Animal> animals = this.animalDao.findByRaceOrderByRace(nom);
		if(animals == null) {
			log.error("aucun animal existe avec ce nom: " + nom);
			
		}
		for (Animal animal : animals) {
			if(animal.getId()==id) {
				this.animalDao.delete(animal);
				System.out.println("le animal : "+nom+" id : " +id+" a bien ete supprim�");
			}
			
		}
		
		
		
		
	}

}
