package com.zoo.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoo.dao.IResponsableDao;
import com.zoo.dto.ResponsableDto;
import com.zoo.entity.Responsable;
import com.zoo.service.IResponsableService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ResponsableServiceImpl implements IResponsableService {
	
	@Autowired
	IResponsableDao responsableDao;
	
	@Override
	public ResponsableDto creer(String appelation) {
		List<Responsable> responsables = this.responsableDao.findByNomOrderByNom(appelation);
		if(responsables != null && responsables.size() != 0) {
			log.error("des responsables ont deja ce appelation : " + responsables);
			return null;
		}
		Responsable responsable = Responsable.builder().nom(appelation).build();
		
		responsable = this.responsableDao.save(responsable);
		
		return ResponsableDto.builder().id(responsable.getMatricule()).build();
	}

	@Override
	public List<ResponsableDto> lister() {
		Iterator<Responsable> iterator = this.responsableDao.findAllByOrderByNomDesc().iterator();
		List<ResponsableDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Responsable x = iterator.next();
			lst.add(ResponsableDto.builder()
					.id(x.getMatricule())
					.appelation(x.getNom())
					.build());
		}
		return lst;
	}

	@Override
	public ResponsableDto responsableParNom(String n) {
		Iterator<Responsable> iterator = this.responsableDao.findByNomOrderByNom(n).iterator();
		ResponsableDto res = null;
		if(iterator.hasNext()) {
			Responsable x = iterator.next();
			res = ResponsableDto.builder()
					.id(x.getMatricule())
					.appelation(x.getNom())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Responsable> iterator = this.responsableDao.findByNomOrderByNom(oldName).iterator();
		ResponsableDto res = null;
		if(iterator.hasNext()) {
			Responsable x = iterator.next();
			x.setNom(newName);
			this.responsableDao.save(x);
		}
	}

	@Override
	public void supprimer(String nom, Integer id) {
		List<Responsable> responsables = this.responsableDao.findByNomOrderByNom(nom);
		if(responsables == null) {
			log.error("aucun responsable existe avec ce nom: " + nom);
			
		}
		for (Responsable responsable : responsables) {
			if(responsable.getMatricule()==id) {
				this.responsableDao.delete(responsable);
				System.out.println("le responsable : "+nom+" id : " +id+" a bien ete supprim�");
			}
			
		}
		
		
		
		
	}

}
