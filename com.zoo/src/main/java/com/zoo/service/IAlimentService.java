package com.zoo.service;


import java.util.List;

import com.zoo.dto.AlimentDto;


public interface IAlimentService {
	
	AlimentDto creer(String nom);

	List<AlimentDto> lister();

	AlimentDto alimentParNom(String n);

	void miseAjourNom(String oldName, String newName);
	void supprimer(String nom);


}
