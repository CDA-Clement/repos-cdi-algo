package com.zoo.service;

import java.util.List;

import com.zoo.dto.AnimalDto;
import com.zoo.dto.EnclosDto;
import com.zoo.error.Erreur;



public interface IEnclosService {
	
	EnclosDto creer(String nom);

	List<EnclosDto> lister();

	EnclosDto enclosParNom(String n);

	void miseAjourNom(String oldName, String newName);
	
	Erreur supprimer(String nom, Integer id);

	Erreur ajoutResponsableEnclos(int idEnclos, int idResp);

	Erreur ajoutAnimalEnclos(int idEnclos, int idAnimal);

	List<AnimalDto> listerAnimalEnclos(int idEnclos);
	

	

////
////	public void ajoutResponsableEnclos(int idEnclos, int idResponsable) {
////		daoR = new ResponsableDao();
////		Responsable b = daoR.find(idResponsable);
////		Enclos a= md.find(idEnclos);
////		a.setResponsable(b);
////		md.update(a);
////
////	}
//	
//	
}
