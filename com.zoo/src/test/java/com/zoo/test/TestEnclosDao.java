package com.zoo.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import com.zoo.dao.AlimentDao;
import com.zoo.dao.EnclosDao;
import com.zoo.dao.ResponsableDao;
import com.zoo.entity.Aliment;
import com.zoo.entity.Animal;
import com.zoo.entity.Enclos;
import com.zoo.entity.Responsable;

@TestMethodOrder(value = OrderAnnotation.class)
class TestEnclosDao {

	static EnclosDao dao;
	static ResponsableDao daoR;
	static AlimentDao daoA;

	@BeforeAll
	public static void beforeAll() {
		dao = new EnclosDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Enclos enclos;

	@Test
	@Order(1)
	public void ajoutEnclos() {
		Enclos a = Enclos.builder().appelation("Felin").build();
		a = dao.add(a);
		assertNotNull(a);
		assertNotEquals(0, a.getId());
		enclos = a;
	}

	@Test
	@Order(2)
	public void miseAjourEnclos() {
		enclos.setAppelation("Marin");
		dao.update(enclos);
		Enclos a = dao.find(enclos.getId());
		assertNotNull(a);
		assertEquals("Marin", enclos.getAppelation());
	}

	@Test
	@Order(3)
	public void supprimerEnclos() {
		dao.remove(enclos.getId());
		Enclos a = dao.find(enclos.getId());
		assertNull(a);
	}

	@Test
	@Order(4)
	public void ajoutAnimalEnclos() {
		Enclos a = Enclos.builder().appelation("reptile").build();

		Animal animal = Animal.builder().race("Serpent").build();
		animal.setEnclos(a);

		Animal animal2 = Animal.builder().race("lezard").build();
		animal2.setEnclos(a);

		List<Animal> animals = Arrays.asList(animal, animal2);
		a.setAnimals(new HashSet<Animal>(animals));

		enclos = dao.add(a);
		assertNotNull(enclos);
		assertNotEquals(0, enclos.getId());
	}


	@Test
	@Order(6)
	public void listerEnclos() {
		Collection<Enclos> enclos = dao.findAllNamedQuery("listeEnclos");
		assertNotEquals(0, enclos.size());
		enclos.stream().forEach(System.err::println);

	}

	@Test
	@Order(7)
	public void trouverEnclos() {
		Enclos a = dao.find(enclos.getId());
		assertNotNull(a);
		assertEquals(enclos.getId(), a.getId());


	}
	@Test
	@Order(8)
	public void ajoutResponsableEnclos() {
		daoR = new ResponsableDao();
		Responsable b = daoR.find(18);
		Enclos a = Enclos.builder().appelation("volailles").build();
		enclos=a;
		a.setResponsable(b);
		dao.add(a);
		assertNotNull(a);
		assertEquals(enclos.getId(), a.getId());
		daoR.close();

	}
	public void ajoutAlimentEnclos() {
		daoA = new AlimentDao();
		Enclos a = Enclos.builder().appelation("volailles").build();
		Aliment al = daoA.find(11);
		enclos=a;
		List<Aliment> aliments = Arrays.asList(al);
		a.setAliments(new HashSet<Aliment>(aliments));
		dao.add(a);
		assertNotNull(a);
		assertEquals(enclos.getId(), a.getId());
		daoR.close();
	
	}
}
