package com.zoo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.zoo.dao.AnimalDao;
import com.zoo.entity.Animal;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;



@TestMethodOrder(value = OrderAnnotation.class)
public class TestAnimalDao {
	static AnimalDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new AnimalDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Animal animal;

	@Test
	@Order(1)
	public void ajoutAnimal() {
		Animal m = Animal.builder().race("206").build();
		m = dao.add(m);
		assertNotNull(m);
		assertNotEquals(0, m.getId());
		animal = m;
	}

	@Test
	@Order(2)
	public void miseAjourAnimal() {
		animal.setRace("306");
		dao.update(animal);
		Animal m = dao.find(animal.getId());
		assertNotNull(m);
		assertEquals("306", animal.getRace());
	}

	@Test
	@Order(3)
	public void trouverAnimal() {
		Animal m = dao.find(animal.getId());
		assertNotNull(m);
		assertEquals(animal.getId(), m.getId());
	}
	@Disabled
	@Test
	@Order(4)
	public void ajoutAlimentAvecAnimal() {
//		Animal m = Animal.builder().race("c3").puissance(12).build();
//
//		Aliment v = new Aliment();
//		v.setAnimal(m);
//
//		Aliment v2 = new Aliment();
//		v.setAnimal(m);
//
//		List<Aliment> aliments = Arrays.asList(v, v2);
//		m.setAliments(new HashSet<Aliment>(aliments));
//
//		animal = dao.add(m);
//		assertNotNull(animal);
//		assertNotEquals(0, animal.getId());
	}

	@Test
	@Order(5)
	public void listerAnimal() {
		Collection<Animal> animal = dao.findAllNamedQuery("listeAnimal");
		assertNotEquals(0, animal.size());
		animal.stream().forEach(System.err::println);
	}

	@Test
	@Order(6)
	public void supprimerAnimal() {
		dao.remove(animal.getId());
		Animal m = dao.find(animal.getId());
		assertNull(m);
	}
}
